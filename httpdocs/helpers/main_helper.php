<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');

/*********************************************
 *  Project     : Projects Management
 *  File        : list.php
 *  Created at  : 5/12/15 - 2:31 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/

/*
* A mathematical decimal difference between two informed dates
*
* Author: Sergio Abreu
* Website: http://sites.sitesbr.net
*
* Features:
* Automatic conversion on dates informed as string.
* Possibility of absolute values (always +) or relative (-/+)
*/

function s_datediff($str_interval, $dt_menor, $dt_maior, $relative = false)
{

    if (is_string($dt_menor)) $dt_menor = date_create($dt_menor);
    if (is_string($dt_maior)) $dt_maior = date_create($dt_maior);

    $diff = date_diff($dt_menor, $dt_maior, !$relative);


    switch ($str_interval) {
        case "y":
            $total = $diff->y + $diff->m / 12 + $diff->d / 365.25;
            break;
        case "m":
            $total = $diff->y * 12 + $diff->m + $diff->d / 30 + $diff->h / 24;
            break;
        case "d":
            $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h / 24 + $diff->i / 60;
            break;
        case "h":
            $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i / 60;
            break;
        case "i":
            $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s / 60;
            break;
        case "s":
            $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i) * 60 + $diff->s;
            break;
    }
    if ($diff->invert) return -1 * $total;
    else    return $total;
}


function dateDifference($date_1, $date_2, $differenceFormat = '%a')
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    return $interval->format($differenceFormat);

}

/**
 * This Function Used with Multi languages
 *  translate passed array or object
 *
 * will return the same string that you are passed, if you translate in language not existes in supported languages
 * @param mixed(array or object) $item
 * @param string $lang
 * @return string.
 */


function _t($item, $lang = "en")
{

    $str = null;
    $serialize = @unserialize($item);
    $json = @json_decode($item);

    // the string not valid serialize and not valid json
    if ($serialize === false && $json === false) {
        return $item;
    }

    if ($item != NULL && @unserialize($item) !== FALSE) {
        $v = @unserialize($item);
        if (key_exists($lang, $v)) $str = $v[$lang];
        else $str = reset($v);

    } else {
        $v = @json_decode($item);
        if ($item != NULL && is_object($v)) {

            if (key_exists($lang, $v)) $str = $v->$lang;
            else {
                $v = (array)$v;
                $str = reset($v);
            }
        }

    }

    return $str;
}

function current_module()
{
    $CI =& get_instance();
    // Modular Separation / Modular Extensions has been detected
    if (method_exists($CI->router, 'fetch_module')) {
        $module = $CI->router->fetch_module();
        return (!empty($module)) ? $module : '';
    } else {
        return '';
    }
}

if (!function_exists(("module_info"))) {
    /**
     * @return mixed
     */
    function module_info($key = "")
    {
        $module_name = current_module();
        $user_lang = get_user_lang();
        $ins =& get_instance();
        $ins->load->model("components/Components_model");
        $data = $ins->Components_model->get_by_name($module_name);
        if ($key) {
            if (isset($data->$key)) return _t($data->$key, $user_lang);
            else return false;

        }

        return $data;
    }
}


function list_modules()
{
    $ins =& get_instance();
    $ins->load->model("components/Components_model", "m");
    return $ins->m->list_modules();
}

function admin_themepath()
{
    $admin_dir = config_item("theme.backend_dir");
    $theme_name = config_item("theme.backend_theme");
    return $admin_dir . "/" . $theme_name;
}

function get_user_lang()
{

    return get_instance()->session->userdata("user_lang");

}

function get_logged_in_user()
{
    return get_instance()->session->userdata("logged_in");
}

function convert_currency($from, $to, $amount = 1)
{

    $api_url = "http://rate-exchange.appspot.com/currency?from=$from&to=$to&q=$amount";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    return json_decode($result);


}

/* translate helper */
function t($line)
{
    global $LANG;
    return ($t = $LANG->line($line)) ? $t : $line;
}

/**
 * Rendering View file
 * @param (str) file name
 * @param (array) optional data array to rendering in view file .
 * @param (boolian) if true will return view file content and don't display the file .
 * @return (void).
 */

function view($html, $vars = '', $return = FALSE)
{

    $ins = &get_instance();
    return $ins->load->view($html, $vars, $return);

}

function time_ago($date)
{

    if (empty($date)) {
        return "No date provided";
    }

    $periods = array(langline("second"), langline("minute"), langline("hour"), langline("day"), langline("week"), langline("month"), langline("year"), langline("decade"));
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");
    $times = array(0 => "second", 1 => "minute", 2 => "hour", 3 => "day", 4 => "week", 5 => "month", 6 => "year", 7 => "decade");

    $now = time();
    $unix_date = strtotime($date);

    // check validity of date
    if (empty($unix_date)) {
        return "Bad date";
    }

    // is it future date or past date
    if ($now > $unix_date) {
        $difference = $now - $unix_date;
        $tense = langline("ago");

    } else {
        $difference = $unix_date - $now;
        $tense = langline("from_now");
    }

    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];

    }

    $difference = round($difference);


    if ($difference != 1 && get_user_lang() != "ar") {
        $periods[$j] .= "s";
    }

    $time_ago = "$difference $periods[$j] {$tense}";

    if ($difference <= 15 && $times[$j] == "second") {
        // just now
        $time_ago = langline("just_now");
    } elseif ($times[$j] == "day" && $difference == 1 && $now > strtotime(date("Y-m-d 11:59:59"))) {
        // yesterday
        $time_ago = sprintf(langline("yesterday_at"), date("h:i A", $unix_date));

    } elseif ($times[$j] == "day" && $difference >= 2) {
        // in this week
        $time_ago = date("D H:i A", $unix_date);

    } elseif ($times[$j] == "week") {

        $time_ago = date("d M h:i A", $unix_date);

    }


    return $time_ago;

}

function default_settings()
{
    $ins =& get_instance();
    $ins->load->model('settings/Settings_model');
    $settings = new Settings_model();
    return $settings->get_default_settings();
}

/**
 * Send_mail
 * @param :$to (str) email address to receive the email
 * @param :$subject (str) email subject
 * @param :$message (str) message body
 * @param :$from(array){optional} if not passed .automatically will use site email and site name
 *            index[0] email
 *            index[1] site name
 *
 * @param :attache (mix[array/str]) {optional} pass an array if you have more than file
 * @param :$config (array) if you need to pass more email configurations or ove
 * @return (boolean) return true if send success.
 * @author Ahmed Fathi
 * @link  php.ahmedfathi@gmail.com
 *
 */
function send_email($to, $subject, $message, $from = null, $attache = null, $config = array())
{
    $ins =& get_instance();
    $emailErr = "";
    //set email settings
    $ins->load->library('email');
    // get email settings
    $es = default_settings();
//    die(print_r($es));
    $config['protocol'] = $es->mail_function;
    $config['smtp_host'] = $es->smtp_server;
    $config['smtp_port'] = $es->smtp_port;
    $config['smtp_user'] = $es->smtp_user;
    $config['smtp_pass'] = $es->smtp_password;

    //$config['mailpath'] = '/usr/sbin/sendmail';
    $config['charset'] = 'utf-8';
    $config['mailtype'] = "html";
    $config['wordwrap'] = TRUE;
    $ins->email->initialize($config);

    if ($from && is_array($from)) {
        $ins->email->from($from[0], $from[1]);
    } else {
        $ins->email->from($es->site_email, $es->site_name);
    }

    if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
        $emailErr = "Invalid email format";
    }
    $ins->email->to($to);
    $ins->email->subject($subject);
    $ins->email->message($message);
    //if array passed
    if ($attache) {
        if (is_array($attache)) {
            for ($i = 0; $i < count($attache); $i++) {
                $this->email->attach($attache[$i]);
            }
        } else {
            $ins->email->attach($attache);
        }
    }
    if ($emailErr) return $emailErr;
    return $ins->email->send();
}


/**
 * print the  time ago.
 * @param (int) datetime as initger .. use strtotime.
 * @return (string) print the time.
 */

function datetime_ago($ts, $format = "l d/m/Y")
{
    $ins =& get_instance();

    if (!ctype_digit($ts)) $ts = strtotime($ts);

    $diff = time() - $ts;
    if ($diff == 0) return langline("date.just_now");
    elseif ($diff > 0) {
        $day_diff = floor($diff / 86400);
        if ($day_diff == 0) {
            if ($diff < 60) return langline("date.just_now");
            if ($diff < 120) return langline("date.one_minute");
            if ($diff < 3600) return floor($diff / 60) . langline("date.minutes") . " " . langline('date.many_ago');
            if ($diff < 7200) return langline("date.one_hour") . " " . langline('date.many_ago');
            if ($diff < 86400) return floor($diff / 3600) . langline("date.hours") . " " . langline('date.many_ago');
        }
        if ($day_diff == 1) return langline("date.yesterday");
        if ($day_diff < 7) return $day_diff . langline("date.days") . " " . langline('date.many_ago');
        if ($day_diff < 31) return ceil($day_diff / 7) . langline("date.weeks") . langline('date.many_ago');
//			if($day_diff < 60) return 'last month';
        return date($format, $ts);
    } else {
        $diff = abs($diff);
        $day_diff = floor($diff / 86400);
        if ($day_diff == 0) {
            if ($diff < 120) return langline("date.one_minute") . langline('date.many_ago');
            if ($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
            if ($diff < 7200) return 'in an hour';
            if ($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
        }
        if ($day_diff == 1) return 'Tomorrow';
        if ($day_diff < 4) return date('l', $ts);
        if ($day_diff < 7 + (7 - date('w'))) return 'next week';
        if (ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
        if (date('n', $ts) == date('n') + 1) return 'next month';
        return date($format, $ts);
    }

}

/**
 * site url and index page if set in config file will
 * = base_url().index_page();
 * @return (str) site url .
 */
function base()
{
    $slash = index_page() != "" ? "/" : "";
    if (segment(2) == "en" || segment(2) == "ar") $slash .= segment(2) . "/";

    return base_url() . index_page() . $slash;
}

/**
 * Short method to load helper
 * @param (string) $helper : helper Name
 * Example : helper("products/productsHelper").
 *            use module name if you are using this function in auther Modules.
 * @return (void)
 */
function helper($helper)
{
    $ins = &get_instance();
    return $ins->load->helper($helper);
}

/**
 * posted data array = $this->input->post();
 * @param (str) optional - the index that need to return his value.
 * @return (mixed) - array if no paramiter passed- str if param passed.
 */
function post($key = '', $feltring = FALSE)
{
    $ins = &get_instance();
    return $key ? $ins->input->post($key, $feltring) : $ins->input->post();
}

/**
 * query string data $this->input->get();
 * @param (str) optional - the index that need to return his value.
 * @return (mixed) - array if no paramiter passed- str if param passed.
 */
function get($key = '')
{
    $ins = &get_instance();
    return $key ? $ins->input->get($key) : $ins->input->get();
}

function get_post($key)
{
    $ins = &get_instance();
    return $ins->input->get_post($key, TRUE);
}


/**
 * Get Segment value
 * this like using $this->uri->segment();
 * @param (int) segment number
 * @return (mix) return passed segment value
 * @access (public)
 */
function segment($segment = 0)
{
    $ins = &get_instance();
    return $ins->uri->segment((int)$segment);
}

function LoopOptions($data, $val = '', $txt = '', $field = '')
{
    if ($data) {

        foreach ($data as $item => $value) {
            $select = '';
            $select2 = '';
            if ($field && post($field)) {
                $select = post($field) == $item ? "selected='selected'" : "";
                $select2 = post($field) == $value->$val ? "selected='selected'" : "";
            }
            if (!$val or !$txt) {
                echo "<option value='$item'" . $select . ">$value</option>";
            } else {
                echo "<option value='" . $value->$val . "'" . $select2 . ">" . $value->$txt . "</option>";
            }
        }
    }
}


/**
 * Get language line From Defined language file
 * useing default Language or Active Language .
 * @access public .
 * @param (string) language key.
 * @return (string)
 */

function langline($key)
{

    $ins = &get_instance();
    $keyresult = $ins->lang->line($key);
    if (!$keyresult) {
        $keyresult = ucwords(str_replace("_", " ", $key));

    }
    return $keyresult;

}

/**
 * load the default language saved into database.
 * @param (str) index that wanted to return (example: folder,id,name)
 * @return (str) language name if not  param passed.
 */

function defaultlang($param = '')
{
    $ins = &get_instance();
    $lang = $ins->session->userdata('lang');

    if (!$lang && $param) {
        /*
         $ins->load->model("MainLoad/Main_model");
         $query=$ins->Main_model->get_default_lang();*/
        $query = $ins->db->get_where("languages", array("default" => 1))->row();
        $lang = $query;
        return $param != '' ? $lang->$param : $lang;
    }

    return $lang;
}

function admin_url($url = null)
{
    $ins =& get_instance();
    $link = rtrim($ins->config->item("url.admin"), "/") . "/" . $url;
    return site_url(rtrim($link));
}

function onlineusers()
{
    $ins = &get_instance();
    $like = 's:8:"loggedin";b:1;';
    $Q = $ins->db->query("SELECT *  FROM `sessions`
WHERE `last_activity` >= DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 2 MINUTE)
AND `user_data` LIKE '%$like%'");
    //return $Q->num_rows();
    //echo "$Q";
    print_r($Q->result());
}

function site_closed($settings)
{
    $ins = &get_instance();
    if ($settings->site_closed == 1) {
        $html = "";
        $loggedin = $ins->session->userdata("logged_in");
        $group = $ins->session->userdata("group");
        if ($loggedin && $group == 1) {
            $html = "<center><p style='color:#444;border:1px solid red;padding:5px;
					 margin:5px;background:#fca1a1;width:100%;z-index:2000;
					 text-shadow:1px 1px 2px #ccc;'>Site Closed For All users</p></center>";
            echo $html;

        } else {

            $html = "<html><head>";
            $html .= "<title>" . settings("site_name") . " |closed form maintenance</title>";
            $html .= "</head>";
            $html .= "<body>";
            $html .= $settings->closed_msg;
            $html .= "</body></html>";
            echo $html;
            die;

        }


    }

}

function set_message($message, $type = 'error')
{
    $ins = &get_instance();
    $ins->session->set_flashdata($type, $message);
}

if (!function_exists("input_value")) {
    function input_value($filed, $default = "")
    {
        $value = $default;
        $ins =& get_instance();
        // get all post data from session.
        $data = $ins->session->flashdata("form_data");
        $name = $filed;


        if (strpos($filed, "[", 0)) {
            $name = strstr($filed, "[", true);

        }
        if (!$data and $default) {
            $value = $default;
        } elseif (!$default and !$data and strpos($filed, "[]", 0)) {
            $value = array();
        }
        if ($data && array_key_exists($name, $data)) {
            $value = $data[$name];
            if (is_array($value)) ;
            {
                $key = get_string_between($filed, "[", "]");
                if ($key) {
                    $value = $value[$key];
                }

            }
        }
        return $value;
    }
}

if (!function_exists("flash_messages")) {
    function flash_messages()
    {
        $ins =& get_instance();
        $messages = $ins->session->flashdata("message");
        return $messages;
    }
}

function get_string_between($string, $start, $end)
{
    $string = " " . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function css($href)
{
    return "<link type='text/css' href='{$href}' rel='stylesheet' media='all' />";
}

function js($src)
{
    return "<script type='text/javascript' src='{$src}'></script>";
}

function message_flashdata()
{
    $ins = &get_instance();

    $flash = $ins->session->all_flashdata();
    $message = "";
    $types = array('error', 'success', 'warning', 'info');

    foreach ($flash as $key => $value) {

        if (in_array($key, $types)) {
            $message .= "<div class='$key message'>$value</div>";
        }
    }

    return $message;
}

/**
 * Generate image thumbinal
 * @param string $src : Image path
 * @param string $dest : thumbinal directory path.
 * @param int $desired_width : thumbinal width.
 */
function make_thumb($src, $dest, $desired_width = 250)
{

    /* read the source image */
    $source_image = @imagecreatefromjpeg($src);
    $width = @imagesx($source_image);
    $height = @imagesy($source_image);

    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));

    /* create a new, "virtual" image */
    $virtual_image = @imagecreatetruecolor($desired_width, $desired_height);

    /* copy source image at a resized size */
    @imagecopyresized($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

    /* create the physical thumbnail image to its destination */
    $dest = (substr($dest, 1, -1) == '/') ? $dest : $dest . "/";
    $thumbname = "" . substr($src, strripos($src, "/") + 1);
    @imagejpeg($virtual_image, $dest . $thumbname);
}


/**
 * check if the current page is Home page or not
 * @return bool
 */
function is_home()
{
    $ins = &get_instance();
    return ($ins->router->fetch_class() === $ins->router->default_controller) && ($ins->router->fetch_method() == "index") ? true : false;
}

function home_page()
{
    $ins = &get_instance();
    return $ins->router->default_controller;
}

function act_do_resize($source_image = "", $file_module = "")
{
    $ins = &get_instance();
    $ins->load->library('image_lib');
    $config_thumb['image_library'] = 'gd2';
    $config_thumb['source_image'] = $source_image;
    //$config_thumb['create_thumb'] = TRUE;
    $config_thumb['maintain_ratio'] = TRUE;
    $config_thumb['width'] = 250;
    $config_thumb['height'] = 250;
    $config_thumb['new_image'] = $file_module;
    $ins->image_lib->initialize($config_thumb);
    $ins->image_lib->resize();
}