<?php
/*********************************************
 *  Project     : Real_state
 *  File        : AjaxUploader.php
 *  Created at  : 9/30/15 - 2:33 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
$ins=&get_instance();
$ins->load->library("upload");
 class ajaxUpload extends CI_Upload{

     public $upload_dir="uploads";
     public $extensions=array("jpg","gif","png");// Allowed Extensions.
     public $clean_upload_dir=true;
     public $max_file_age = 18000; // Temp file age in seconds

     private $json=array();

     function __construct($config=array()){
         

         $this->max_file_age=5 * 3600;
         $this->json['jsonrpc'] = "2.0";
         $this->json['error']   = null;
         $this->json['result'] = null;
         $this->json['id'] = "id";
     }

     function go($input_name="user_file"){
         $config['upload_path'] = $this->upload_dir;
         $config['allowed_types'] = implode("|",$this->extensions);
         $file_name=uniqid("file_").  strtotime("now");
         $file_title=$file_name;

         if (isset($_REQUEST["name"])) {
             $file_title = $_REQUEST["name"];
         } elseif (!empty($_FILES)) {
             $file_title = $_FILES["file"]["name"];
         }
         
         $config['file_name']=$file_name;
         $config['orig_name']=$file_title;
         parent::__construct($config);
         // Make sure file is not cached (as it happens for example on iOS devices)
         header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
         header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
         header("Cache-Control: no-store, no-cache, must-revalidate");
         header("Cache-Control: post-check=0, pre-check=0", false);
         header("Pragma: no-cache");

         $filePath = $this->upload_dir . DIRECTORY_SEPARATOR . $file_name;

         // Create target dir
         if($this->upload_dir and !file_exists($this->upload_dir)){
                 @mkdir($this->upload_dir);
         }

// Remove old temp files
         if ($this->clean_upload_dir) {
             if (!is_dir($this->upload_dir) || !$dir = opendir($this->upload_dir)) {
                 die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
             }

             while (($file = readdir($dir)) !== false) {
                 $tmpfilePath = $this->upload_dir . DIRECTORY_SEPARATOR . $file;

                 // If temp file is current file proceed to the next
                 if ($tmpfilePath == "{$this->upload_dir}.part") {
                     continue;
                 }

                 // Remove temp file if it is older than the max age and is not the current file
                 if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $this->max_file_age)) {
                     @unlink($tmpfilePath);
                 }
             }
             closedir($dir);
         }

        // Chunking might be enabled
         $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
         $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


// Open temp file
         if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {

             $this->json['error']["code"]="102";
             $this->json['error']["message"]="Failed to open output stream.";
             echo json_encode($this->json);die;
         }


         if($input_name){

             if(!$this->do_upload($input_name)){
                 $this->json['error']["code"]="102";
                 $this->json['error']["message"]=$this->display_errors();
                echo json_encode($this->json);die;
             }

             $upload_data=$this->data();

             $this->json['result']=null;
             $upload_data['file_title']=$file_title;
             $this->json['data']=$upload_data;
             echo json_encode($this->json);die;

         }
     }
 }