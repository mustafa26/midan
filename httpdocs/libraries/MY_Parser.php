<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : MY_Parser.php
 *  Created at  : 5/14/15 - 9:23 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 class MY_Parser extends CI_Parser{
     function __construct(){
         parent :: __construct();
     }

     /**
      *  Parse a template
      *
      * Parses pseudo-variables contained in the specified template,
      * replacing them with the data in the second param
      *
      * @access	public
      * @param	string
      * @param	array
      * @param	bool
      * @return	string
      */
     function _parse($template, $data, $return = FALSE)
     {
         if ($template == '')
         {
             return FALSE;
         }


         foreach ($data as $key => $val)
         {
             if (is_array($val))
             {
                 $template = $this->_parse_pair($key, $val, $template);
             }
             else
             {
                 $template = $this->_parse_single($key, (string)$val, $template);
             }

         }
         $template=$this->_lang_parse($template);

         if ($return == FALSE)
         {
             $CI =& get_instance();
             $CI->output->append_output($template);
         }

         return $template;
     }

     // --------------------------------------------------------------------

     /**
      *  Set the left/right variable delimiters
      *
      * @access	public
      * @param	string
      * @param	string
      * @return	void
      */
     function set_delimiters($l = '{', $r = '}')
     {
         $this->l_delim = $l;
         $this->r_delim = $r;
     }

     // --------------------------------------------------------------------

     /**
      *  Parse a single key/value
      *
      * @access	private
      * @param	string
      * @param	string
      * @param	string
      * @return	string
      */
     function _parse_single($key, $val, $string)
     {

         return str_replace($this->l_delim.$key.$this->r_delim, $val, $string);
     }

     function _lang_parse($template)
     {

         if ($template)     //build the view normally
         {
             while(preg_match('/\{lang:(\w*)\}/siU', $template, $match)) //parse the language variables
             {
                 //if no translation is found use the variable as a literal
                 if (($line = langline("$match[1]")) === FALSE) $line = $match[1];

                 $template = str_replace($match[0], $line, $template);
             }

             return $template;
         }
     }

 }