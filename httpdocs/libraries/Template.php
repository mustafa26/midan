<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');

/******************************************
 * @author    : Ahmed Fathi Saad (MR DEVELOPER).
 * @email    : php.ahmedfathi@gmail.com
 ******************************************/

/**
 * Template Library
 */

$themepath;
class template
{
    protected $CI;
    public $theme_path;
    public $theme_name;
    public $full_path;
    public $vars = array();
    public $scripts = array();


    function __construct()
    {
        $this->CI =& get_instance();
//        $this->CI->load->library("parser");
        $this->CI->load->config("settings");
    }

    function content($file)
    {

        $this->global_vars();
        $template="test";

        return $this->CI->parser->parse($this->full_path . "/" . $file, $this->vars, true);
    }

    function get_themepath(){
        return base_url($this->CI->config->item("theme.templates_dir") . '/' . $this->full_path);
    }

    function renderview($file, $data = array(), $return = false)
    {

        $this->global_vars();

        $data = is_array($data) ? array_merge($this->vars, $data) : $this->vars;

        return $this->CI->load->view($this->full_path . "/" . $file, $data, $return);

    }

    function render($file, $data = array(), $return = false)
    {

        $this->global_vars();

        $data = is_array($data) ? array_merge($this->vars, $data) : $this->vars;



        //load template functions file if exist just like wordpress.
        IF (file_exists(VIEWPATH . "/" . $this->full_path . "/functions.php")) {

            include_once(VIEWPATH . "/" . $this->full_path . "/functions.php");
        }

        return $this->CI->load->view($this->full_path . "/" . $file, $data, $return);

    }

    /**
     * CSS
     *
     * An asset function that returns a CSS stylesheet
     *
     * @access public
     * @param $file
     * @return string
     */
    public function css($file, $attributes = array())
    {

        $defaults = array('media' => 'screen', 'rel' => 'stylesheet', 'type' => 'text/css');

        $attributes = array_merge($defaults, $attributes);

        $return = '<link rel="' . $attributes['rel'] . '" type="' . $attributes['type'] . '" href="' . base_url($this->full_path . $file) . '" media="' . $attributes['media'] . '">';

        return $return;
    }

    /**
     * JS
     *
     * An asset function that returns a script embed tag
     *
     * @access public
     * @param $file
     * @return string
     */
    public function js($file, $attributes = array())
    {

        $defaults = array('type' => 'text/javascript');

        $attributes = array_merge($defaults, $attributes);

        $return = '<script type="' . base_url($attributes['type'] . '" src="' . $this->full_path . $file) . '"></script>';

        return $return;
    }

    public function script($code=null){
        $script="<script type='text/javascript'> $code </script>";
        return $script;
    }

    /**
     * IMG
     *
     * An asset function that returns an image tag
     *
     * @access public
     * @param $file
     * @return string
     */
    public function img($file, $attributes = array())
    {
        $defaults = array('alt' => '', 'title' => '');

        $attributes = array_merge($defaults, $attributes);

        $return = '<img src ="' . base_url($this->full_path . $file) . '" alt="' . $attributes['alt'] . '" title="' . $attributes['title'] . '" />';

        return $return;
    }


    /**
     * Current Module
     *
     * Just a fancier way of getting the current module
     * if we have support for modules
     *
     * @access public
     * @return string
     */
    public function current_module()
    {
        // Modular Separation / Modular Extensions has been detected
        if (method_exists($this->CI->router, 'fetch_module')) {
            $module = $this->CI->router->fetch_module();
            return (!empty($module)) ? $module : '';
        }
        else {
            return '';
        }
    }

    function global_vars()
    {

        $this->vars['base']         = rtrim(base_url() . index_page(), "/") . "/";
        $this->vars['base_url']     = base_url();
        $this->vars['theme_name']   = $this->theme_name;
        $this->vars['theme_dir']    = $this->theme_path;
        $this->full_path            = $this->theme_path . "/" . $this->theme_name;
        $this->vars['themepath']    = base_url($this->CI->config->item("theme.templates_dir") . '/' . $this->full_path);
        $this->vars['template']     = $this->full_path;
        $this->vars['current_module'] = $this->current_module();
        

        $jsvars=null;
        foreach($this->vars as $k=>$v){
            if(is_object($v)||is_array($v)){

               if($k!=="user"){
                   $json=json_encode($v);
                   $jsvars.=" var $k=JSON.parse('$json'); \r\n \t\t\t";

               }
            }else{
                $jsvars.="var $k='$v'; \r\n \t\t\t";
            }

        }
        $this->scripts[]=$this->script($jsvars);

        $js=null;
        if(is_array($this->scripts)){
            for($i=0;$i<count($this->scripts);$i++){
                $js.=$this->scripts[$i]."\r\n";
            }
        }else{
            $js=$this->scripts;
        }
        $this->vars['scripts']      =$js;


        return $this->vars;

    }
}

