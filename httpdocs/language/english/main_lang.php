<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : main.php
 *  Created at  : 5/14/15 - 11:02 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
$lang['language_tab_arabic']        = "Arabic";
$lang['language_tab_english']       = "English";
$lang['language_arabic']            = "Arabic";
$lang['label_attachments']          = "Attachments";
$lang['language_english']           = "English";
$lang['link_dashboard']             = "Dashboard";
$lang['link_settings']              = "Settings";
$lang['link_projects']              = "Projects";
$lang['link_tasks']                 = "Tasks";
$lang['link_users']                 = "Users";
$lang['link_groups']                = "Groups";
$lang['link_logout']                = "Logout";
$lang['link_login']                 = "Login";
$lang['link_show']                  = "Show";
$lang['input_search']               = "Search...";
$lang['link_edit']                  = "Edit";
$lang['link_delete']                = "Delete";
$lang['link_add_new']               = "Add New";
$lang['link_tools']                 = "Tools";
$lang['btn_save']                   = "Save";
$lang['btn_close']                  = "Close";
$lang['select_option']              = "Select Option";
$lang['status_active']              = "Active";
$lang['status_inactive']            = "Inactive";
$lang['text_you']                   = "You";
$lang['text_yeas']                  = "Yeas";
$lang['text_no']                    = "No";
$lang['label_remember_me']          = "Remember me";
$lang['text_days']                  = "Days";
$lang['text_to']                    = "To";
$lang['no_results']                 = "No Results Found";
$lang['no_attaches']                = "No Attachments";
$lang['list_all']                   = "List";
$lang['filter_label']               ="Filter";
$lang['filter_label']               ="Filter";
$lang['select_date_range']          = "Select date range";
$lang['recent_activities']          = "آخر الأنشطة داخل النظام";
$lang['see_all']          = "مشاهدة الكل";

$lang['second']                     ="second";
$lang['minute']                     ="minute";
$lang['hour']                       ="hour";
$lang['day']                        ="day";
$lang['week']                       ="week";
$lang['month']                      ="month";
$lang['year']                       ="year";
$lang['decade']                     ="decade";
$lang['ago']                        ="ago";
$lang['from_now']                   ="from now";
$lang['time_from']                   ="from";
$lang['just_now']                   ="Just Now";
$lang['yesterday_at']               ="Yesterday at %s";


// javascript
$lang['js']['btn_save']             ="Save";
$lang['js']['btn_close']            ="Close";
$lang['js']['btn_delete']           ="Delete";
$lang['js']['btn_edit']             ="Edit";
$lang['js']['btn_settings']         ="Settings";
$lang['js']['text_saving']          ="Saving...";
$lang['js']['text_loading']         ="Loading...";
$lang['js']['text_upload']          ="Upload";
$lang['js']['time_from']          = "from";
$lang['js']['delete_confirm_message'] ="Are you sure. confirm delete?";
$lang['js']['logout_confirm_message'] = "Are you sure. You want to logout from your account?";

