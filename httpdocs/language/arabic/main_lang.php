<?php
/*********************************************
 *  Project     : CMS
 *  File        : language
 *  Created at  : 5/14/15 - 11:02 AM
 *********************************************/
$lang['language_tab_arabic'] = "عربي";
$lang['language_tab_english'] = "الإنجليزية";
$lang['label_attachments'] = "المرفقات";
$lang['language_arabic'] = "عربي";
$lang['language_english'] = "الإنجليزية";
$lang['link_dashboard'] = "لوحة التحكم";
$lang['link_settings'] = "الإعدادات";
$lang['link_tasks'] = "المهام";
$lang['link_users'] = "المستخدمين";
$lang['link_groups'] = "المجموعات";
$lang['link_logout'] = "خروج";
$lang['link_login'] = "دخول";
$lang['link_show'] = "عرض";
$lang['link_add_new'] = "إضافة جديد";
$lang['link_edit'] = "تعديل";
$lang['link_delete'] = "حذف";
$lang['btn_save'] = "حفظ";
$lang['Send'] = "ارسال";
$lang['Select'] = "-- أختر --";
$lang['Continue'] = "متابعة";
$lang['btn_close'] = "إغلاق";
$lang['link_tools'] = "أدوات";
$lang['input_search'] = "بحث...";
$lang['label_remember_me'] = "تذكر دخولي";
$lang['text_days'] = "يوم";
$lang['select_option'] = "حدد اختيار";
$lang['status_active'] = "مفعل";
$lang['status_inactive'] = "معطل";
$lang['text_you'] = "أنت";
$lang['text_yeas'] = "نعم";
$lang['text_no'] = "لا";
$lang['text_to'] = "إلى";
$lang['list_all'] = "الكل";
$lang['filter_label'] = "تصفية";
$lang['filter_btn'] = "تصفية";
$lang['no_results'] = "لا يوجد نتائج";
$lang['no_attaches'] = "لا يوجد مرفقات";
$lang['feature_not_available'] = "هذه الخاصية غير متاحة في الوقت الحالي";
$lang['select_date_range'] = "حدد فترة زمنية";
$lang['recent_activities'] = "آخر الأنشطة داخل النظام";
$lang['see_all'] = "مشاهدة الكل";
$lang['More'] = "التفاصيل";
$lang['Note'] = "ملاحظة";
$lang['Requests'] = "مشاركاتي ";
$lang['Profile_Info'] = "المعلومات الشخصية";

// Main Menu 
$lang['Camel_Sport'] = "رياضة الهجن";
$lang['Bashayer_Department'] = "دائرة ميدان البشائر";
$lang['Informations'] = "معلومات";
$lang['News'] = "الأخبار";
$lang['Race_schedule'] = "جدول السباقات";
$lang['Violations'] = "المخالفات";
$lang['Camel_registration'] = "تسجيل الهجن";
$lang['MediaCenter'] = "المركز الإعلامي"; 
$lang['ProgramStage'] = "برنامج المنصة";
$lang['Racing'] = "السباقات";
$lang['SpecialInterviews'] = "مقابلات خاصة";
$lang['Pictures_library'] = "مكتبة الصور"; 
$lang['Heritage_Village'] = "القرية التراثية";
$lang['Versions'] = "أصدرات"; 

//SMS
$lang['Send_SMS'] = "رسالة نصية SMS";
$lang['Send_SMS_Limit'] = "فضلا : عدد حروف الرسالة لا يتعدى 160 حرف";


// Form Transaction  Notification
$lang['Success'] = '<hr>
        <div class="alert alert-success">
            <strong>تهانينا!</strong> تم حفظ البيانات بنجاح.
        </div>';
$lang['Success_Sms'] = '<hr>
        <div class="alert alert-success">
            <strong>تهانينا!</strong> تم الأرسال بنجاح.
        </div>';
$lang['Error'] = '<hr>
        <div class="alert alert-danger">
            <strong>خطأ!</strong> فضلا راجع البيانات وحاول مرة أخرى.
        </div>';
$lang['Warning'] = '<hr>
        <div class="alert alert-warning">
            <strong>تنبية!</strong> يرجع الأنتباه %s.
        </div>';
$lang['Warning_idcardDuplicat'] = '
        <div class="alert alert-warning">
            <strong>تنبية!</strong> رقم الشريحة مسجل من قبل وبالتالي لايمكن قبول الطلب.
        </div>';
$lang['Info'] = '<hr>
        <div class="alert alert-info">
            <strong>لطفا!</strong> %s;
        </div>';
$lang['delete_confirm_message'] = "هل تريد تأكيد الحذف؟";
$lang['btn_select_File'] = "أختر الملف";

//time
$lang['second'] = "ثانية";
$lang['minute'] = "دقيقة";
$lang['hour'] = "ساعة";
$lang['day'] = "يوم";
$lang['week'] = "أسبوع";
$lang['month'] = "شهر";
$lang['year'] = "سنة";
$lang['decade'] = "عقد";
$lang['ago'] = "مضت";
$lang['from_now'] = "من الآن";
$lang['time_from'] = "منذ";
$lang['just_now'] = "الآن";
$lang['yesterday_at'] = "أمس الساعة %s";
$lang['Date'] = "التاريخ ";

// javascript
$lang['js']['btn_save'] = "حفظ";
$lang['js']['btn_close'] = "إغلاق";
$lang['js']['btn_delete'] = "حذف";
$lang['js']['btn_edit'] = "تعديل";
$lang['js']['btn_settings'] = "إعدادات";
$lang['js']['text_saving'] = "جاري الحفظ ...";
$lang['js']['text_loading'] = "جاري التحميل...";
$lang['js']['text_upload'] = "ارفع";
$lang['js']['time_from'] = "منذ";
$lang['js']['delete_confirm_message'] = "هل تريد تأكيد الحذف؟";
$lang['js']['logout_confirm_message'] = "هل تريد تأكيد الخروج من الحساب";




