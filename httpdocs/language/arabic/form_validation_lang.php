<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'حقل {field} مطلوب.';
$lang['form_validation_isset']			= 'حقل {field} لابد أن يحتوي على قيمة.';
$lang['form_validation_valid_email']		= 'حقل {field} لابد أن يحتوي بريد إلكتروني صحيح.';
$lang['form_validation_valid_emails']		= 'حقل {field} لابد أن يكون جميع عناصره بريد إلكتروني صحيح.';
$lang['form_validation_valid_url']		= 'حقل {field} لابد أن يحتوي رابط صحيح.';
$lang['form_validation_valid_ip']		= 'حقل {field} لابد أن يحتوي عنوان IP صحيح.';
$lang['form_validation_min_length']		= 'حقل {field} لابد أن يحتوي على {param} حرف على الأقل.';
$lang['form_validation_max_length']		= 'حقل {field} لا يمكن أن يتجاوز {param} حرفاً';
$lang['form_validation_exact_length']		= 'حقل {field} لابد أن يحتوي على {param} حرفاً لا أكثر ولا أقل.';
$lang['form_validation_alpha']			= 'حقل {field} لابد أن يحتوي حروف أبجدية فقط.';
$lang['form_validation_alpha_numeric']		= 'حقل {field} يمكن أين يحتوي حروف أبجدية رقمية فقط.';
$lang['form_validation_alpha_numeric_spaces']	= 'حقل {field} يمكن أن يحتوي حروف أبجدية رقمية ومسافات فقط.';
$lang['form_validation_alpha_dash']		= 'حقل {field} يمكن أن يحتوي على حروف أبجدية رقمية والعلامات (_,-).';
$lang['form_validation_numeric']		= 'حقل  {field} لابد أن يحوي أرقاماُ فقط.';
$lang['form_validation_is_numeric']		= 'حقل {field} لابد أن يحوي أرحف رقمية فقط.';
$lang['form_validation_integer']		= 'حقل {field} لابد أن يحوي أرقاماً صحيحة فقط.';
$lang['form_validation_regex_match']		= 'حقل {field} لا يحتوي تنسيق صحيح.';
$lang['form_validation_matches']		= 'حقل {field} لا يطابق حقل {param}.';
$lang['form_validation_differs']		= 'حقل {field} لابد أن يختلف عن حقل {param}.';
$lang['form_validation_is_unique'] 		= 'حقل {field} لابد أن يحتوي على قيمة فريدة .';
$lang['form_validation_is_natural']		= 'حقل {field} لابد أن يحوي أرقاما فقط.';
$lang['form_validation_is_natural_no_zero']	= 'حقل {field} لابد أن يحوي أرقاما وتكون أكبر من صفر.';
$lang['form_validation_decimal']		= 'حقل {field} لابد أن يحوي أرقاماً عشرية فقط.';
$lang['form_validation_less_than']		= 'حقل {field} لابد أن يحوي رقم أقل من {param}.';
$lang['form_validation_less_than_equal_to']	= 'حقل {field} لابد أن يحوي رقم أقل من أو يساوي {param}.';
$lang['form_validation_greater_than']		= 'حقل {field} لابد أن يحوي رقماً أكبر من {param}.';
$lang['form_validation_greater_than_equal_to']	= 'حقل {field} لابد أن يحوي رقماً أكبر من أو يساوي {param}.';
$lang['form_validation_error_message_not_set']	= 'غير قادر على الوصول إلى رسالة الخطأ الخاصة بالحقل {field} .';
$lang['form_validation_in_list']		= 'الحقل {field} لابد أن يكون واحداً من : {param}.';
