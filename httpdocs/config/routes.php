<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */

require_once("settings.php");

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route[$config['url.admin'] . "/permissions/(:num)"] = "permissions/admin/index/$1";
$route[$config['url.admin'] . "/(:any)"] = "$1/admin";
$route[$config['url.admin'] . "/(:any)/(:any)"] = "$1/admin/$2";
$route[$config['url.admin'] . "/(:any)/(:any)/(:any)"] = "$1/admin/$2/$3";
$route[$config['url.admin'] . "/(:any)/(:any)/(:any)/(:any)"] = "$1/admin/$2/$3/$4";
$route["users/accntlgn"] = "users/login";
$route["cpsite/lg"] = "users/admin/login";
$route[$config['url.admin'] . "/logout"] = "users/admin/logout"; 

$route["pages/(:any)/(:any)"] = "pages/index/$1/$2";
$route['translate_uri_dashes'] = FALSE;
