<?php
/*********************************************
 *  Project     : projects_management
 *  File        : pagination.php
 *  Created at  : 5/24/15 - 1:57 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
$config['use_global_url_suffix'] = true;
$config['num_links']        = 5;

// full
$config['full_tag_open']    = '<ul class="pagination">';
$config['full_tag_close']   = '</ul>';

// first
$config['first_link']       = '«';
$config['first_tag_open']   = '<li>';
$config['first_tag_close']  = '</li>';

// last
$config['last_link']       = '»';
$config['last_tag_open']    = '<li>';
$config['last_tag_close']   = '</li>';

// number
$config['num_tag_open']     = '<li class="inactive">';
$config['num_tag_close']     = '</li>';

// current
$config['cur_tag_open']     = '<li class="active"><a href="#">';
$config['cur_tag_close']    = '</a></li>';

// next
$config['next_link']        = '&rsaquo;';
$config['next_tag_open']    = '<li>';
$config['next_tag_close']   = '</li>';

// previous
$config['prev_link']        = '&lsaquo;';
$config['prev_tag_open']    = '<li>';
$config['prev_tag_close']   = '</li>';