<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : settings.php
 *  Created at  : 5/11/15 - 9:20 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *********************************************/

$config['url.admin']                ="admin";
$config['app.name']                ="admin";
$config['theme.templates_dir']      = "templates";
$config['theme.backend_dir']        = "admin";
$config['theme.backend_theme']      = "default";
$config['theme.frontend_dir']       = "site";
$config['theme.frontend_theme']     = "default";
$config['lang.languages']           = array("ar"=>"arabic","en"=>"english");
$config['lang.default']             = "ar";
$config['remember_me_token']        = "user_rememberToken";
// login types username or email
$config['admin.login_type']         = "username";
$config['site.login_type']          = "username";
$config['upload_dir']               = "uploads/";
