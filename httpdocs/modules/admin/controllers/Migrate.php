<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : Migrate.php
 *  Created at  : 5/11/15 - 4:32 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : php.ahmedfathi@gmail.com
 *  site        : http://durar-it.com
 *********************************************/
  class Migrate extends Admin_Controller{
      public function index()
      {

          $this->load->library('migration');


          if ($this->migration->current() === FALSE)
          {
              show_error($this->migration->error_string());
          }else{
              echo "good";
          }
      }
  }