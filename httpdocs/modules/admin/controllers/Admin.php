<?php 

class Admin extends Admin_Controller
{

    function __construct()
    {

        parent::__construct();

    }

    function index()
    {

        $this->data['title'] = $this->site->site_name;

        $this->data['content'] = "admin/dashboard";
        $this->data['users_log'] = $this->recent_activities();
//        $this->data['projects_status'] = $this->projects_status();

        $this->template->render("template", $this->data);
    }

    function notifications($s="")
    {
        redirect(admin_url());
    }
    private function recent_activities() {

        //if (check_user_permission("log", "show")) {


            $this->load->model("log/Log_model");

            $results = $this->Log_model->get_all(0, 0, 50);
            $data = $results['data'];
            if ($results) {
                $this->lang->load("log/log", $this->languages[$this->user_lang]);
                foreach ($results['data'] as $lang) {
                    $this->lang->load($lang->ul_module . "/{$lang->ul_module}", $this->languages[$this->user_lang]);
                }
            }


            $events_color = array("show" => "info", "add" => "success", "edit" => "warning", "delete" => "danger");
            $this->data['events_color'] = $events_color;

            return $data;
        //}
    } 


}