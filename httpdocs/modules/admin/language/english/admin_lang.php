<?php
/*********************************************
 *  Project     : projects_management
 *  File        : admin_lang.php
 *  Created at  : 6/2/15 - 5:47 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 $lang['my_account_link']       = "My Account";
 $lang['my_calender_link']      = "My Calendar";
 $lang['my_inbox_link']         = "My Inbox";
 $lang['my_tasks_link']         = "My Tasks";
 $lang['full_screen_link']      = "Full Screen";
 $lang['logout_link']           = "Logout";
$lang['log_logged_in']          = "Log in Control Panel";