<?php
/*********************************************
 *  Project     : projects_management
 *  File        : admin_lang.php
 *  Created at  : 6/2/15 - 5:47 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 $lang['my_account_link']       = "حسابي";
 $lang['my_calender_link']      = "التقويم";
 $lang['my_inbox_link']         = "الرسائل";
 $lang['my_tasks_link']         = "مهماتي";
 $lang['full_screen_link']      = "ملئ الشاشة";
 $lang['logout_link']           = "خروج";
