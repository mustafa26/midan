<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : 001_Modules.php
 *  Created at  : 5/11/15 - 1:58 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : php.ahmedfathi@gmail.com
 *  site        : http://www.durar-it.com
 *********************************************/
die("test code");
class Migration_modules extends MY_Migration{

    function up(){

        $this->dbforge->add_field(array(
            'module_id'=>array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'module_title'=>array(
                'type'=>'text',
                'null'=>true
            ),
            'module_name'=>array(
                'type'=>"VARCHAR",
                'null'=>false,
                'constraint'=>100
            ),
            'module_icon'=>array(
                'type'=>"VARCHAR",
                'constraint'=>150,
                'null'=>true
            ),
            'module_parent'=>array(
                'type'=>"INT",
                'null'=>false,
                'default'=>0
            ),
            'module_created'=>array(
                'type'=>"DATETIME",
                'null'=>true,
            ),
            'module_updated'=>array(
                'type'=>'TIMESTAMP',
                'null'=>true
            )
        ));

        $this->dbforge->add_key('module_id', TRUE);
        $this->dbforge->create_table('modules');

    }

    function down(){
        $this->dbforge->drop_table('modules');
    }
}