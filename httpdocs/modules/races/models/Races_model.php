<?php

/* * *******************************************
 *  Project     : Pages Management
 *  File        : sliders_model.php
 *  Created at  : 11/10/15 - 12:12 PM 
 *  site        : http://durar-it.com
 * ******************************************* */

class Races_Model extends CI_Model
{

    private $table = "races";

    function __construct()
    {
        parent:: __construct();
    }

    /**
     * @param int $group
     * @param int $offset
     * @param int $limit
     * @return mixed
     */
    function get_all($fid = 0, $offset = 0, $limit = 0)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->select("c.*,COUNT(r.rqst_ID) as total");
        $this->db->join("requests r", "r.rqst_NumRace=c.rac_ID AND r.rqst_Status=2", "left"); // Active Request - > rqst_Status=2
        $this->db->order_by('c.rac_Sort');
        $this->db->group_by('c.rac_ID');
        $Q = $this->db->get_where("{$this->table} c", array("c.rac_FID" => $fid, "c.rac_Deleted" => 0));

        $this->db->select("c.*,COUNT(r.rqst_ID) as total");
        $this->db->join("requests r", "r.rqst_NumRace=c.rac_ID AND r.rqst_Status=2", "left");
        $this->db->group_by('c.rac_ID');
        $total = $this->db->get_where("{$this->table} c", array("c.rac_FID" => $fid, "c.rac_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_subsrbrs($fid = 0, $offset = 0, $limit = 0)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->select("c.*,COUNT(r.id) as total");
        $this->db->join("las_list r", "r.RaceNum=c.rac_ID", "left"); // Active Request - > rqst_Status=2
        $this->db->order_by('c.rac_Sort');
        $this->db->group_by('c.rac_ID');
        $Q = $this->db->get_where("{$this->table} c", array("c.rac_FID" => $fid, "c.rac_Deleted" => 0));

        $this->db->select("c.*,COUNT(r.id) as total");
        $this->db->join("las_list r", "r.RaceNum=c.rac_ID", "left");
        $this->db->group_by('c.rac_ID');
        $total = $this->db->get_where("{$this->table} c", array("c.rac_FID" => $fid, "c.rac_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }
    function get_all_byevent($fid = 0, $offset = 0, $limit = 50)
    {
        $this->db->limit($limit, $offset);
        $Q = $this->db->get_where($this->table, array("rac_FID" => $fid,"rac_Deleted" => 0, "rac_Status" => 1, "rac_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("rac_FID" => $fid,"rac_Deleted" => 0, "rac_Status" => 1, "rac_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_active($offset = 0, $limit = 14)
    {
        $this->db->limit($limit, $offset);
        $Q = $this->db->get_where($this->table, array("rac_Deleted" => 0, "rac_Status" => 1));
        $total = $this->db->get_where($this->table, array("rac_Deleted" => 0, "rac_Status" => 1))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get($id = 0)
    {
        $Q = $this->db->get_where($this->table, array("rac_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_ID();
    }

    function update($id, $data)
    {
        $this->db->update($this->table, $data, array("rac_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id = 0, $data)
    {
        $this->db->update($this->table, $data, array("rac_ID" => $id));
        return $this->db->affected_rows();
    }

}
