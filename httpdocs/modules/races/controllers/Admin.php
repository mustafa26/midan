<?php

class Admin extends Admin_Controller
{

    private $model;

    function __construct()
    {
        parent:: __construct();
        $this->load->model("Races_Model");
        $this->lang->load("races", $this->languages[$this->user_lang]);
        $this->model = new Races_model();
        if (!check_user_permission(current_module(), "show")) {
            //redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("races_status");
    }

    function index($fid = 0, $offset = 0)
    {
        if (!check_user_permission(current_module(), "show")) {
            //redirect(admin_url("permissions/denied"));
        }

        $this->data['title'] = langline("races_rac_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($fid,$offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_races");
        $this->data['content'] = "races/index";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("races/index/$fid"), $result['total'], $limit, 5);
        $this->template->render("template", $this->data);
    }

    private function _validate()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("rac_Name", langline("rac_Name"), "required|trim|max_length[255]");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add($fid = 0)
    {
        if (!check_user_permission(current_module(), "add")) {
            //redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add($fid);
            exit;
        }

        $this->data['title'] = langline("races_rac_title") . " - " . $this->site->site_name;
        $this->data['content'] = "races/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add($fid = 0)
    {
        $this->_validate();
        if (!$this->errors) {
            $data['rac_FID'] = $fid;
            $data['rac_Sort'] = post("rac_Sort");
            $data['rac_Name'] = post("rac_Name");
            $data['rac_Age'] = post("rac_Age");
            $data['rac_Gender'] = post("rac_Gender");
            $data['rac_ord1'] = post("rac_ord1");
            $data['rac_ord2'] = post("rac_ord2");
            $data['rac_ord3'] = post("rac_ord3");
            $data['rac_ord4'] = post("rac_ord4");
            $data['rac_ord5'] = post("rac_ord5");
            $data['rac_ord6'] = post("rac_ord6");
            $data['rac_ord7'] = post("rac_ord7");
            $data['rac_ord8'] = post("rac_ord8");
            $data['rac_ord9'] = post("rac_ord9");
            $data['rac_ord10'] = post("rac_ord10");
            $data['rac_CreatedBy'] = $this->user->user_id;
            $data['rac_Created'] = date("Y-m-d H:i:s");
            $data['rac_Status'] = post("rac_status");

            if ($project_id = $this->model->insert($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("races/index/$fid"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("races/index/$fid"));
    }

    function edit($id = 0)
    {
        if (!check_user_permission(current_module(), "edit")) {
            //redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }
        $cat = $this->model->get($id);
        $this->data['title'] = langline("races_rac_title") . " - " . $this->site->site_name;
        log_add_user_action($this->user->user_id, $id, "show", "log_show_races");
        $this->data['content'] = "races/edit";
        $this->data['data'] = $cat;
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0)
    {
        $this->_validate();
        if (!$this->errors) {
            $data['rac_Sort'] = post("rac_Sort");
            $data['rac_Name'] = post("rac_Name");
            $data['rac_Age'] = post("rac_Age");
            $data['rac_Gender'] = post("rac_Gender");
            $data['rac_ord1'] = post("rac_ord1");
            $data['rac_ord2'] = post("rac_ord2");
            $data['rac_ord3'] = post("rac_ord3");
            $data['rac_ord4'] = post("rac_ord4");
            $data['rac_ord5'] = post("rac_ord5");
            $data['rac_ord6'] = post("rac_ord6");
            $data['rac_ord7'] = post("rac_ord7");
            $data['rac_ord8'] = post("rac_ord8");
            $data['rac_ord9'] = post("rac_ord9");
            $data['rac_ord10'] = post("rac_ord10");
            $data['rac_Status'] = post("rac_status");
            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_races");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("races/index/".post("rac_FID")));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("races/edit/$id"));
    }

    function delete($fid = 0,$id=0)
    {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            //redirect(admin_url("permissions/denied"));
        }

        $data['rac_deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("races/index/$fid"));
    }

}
