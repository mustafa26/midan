<?php

$lang['races_slider_title'] = "الصفحات";
$lang['races_slider_header'] = "الصفحات";
$lang['event_link'] = "السباقات";
$lang['races_link'] = "الأشواط";
$lang['Name'] = "الاسم";
$lang['slider_Url'] = "الرابط";
$lang['Sort'] = "رقم الشوط";
$lang['races_Name'] = "اسم الشوط";
$lang['Age'] = "الفئة العمرية";
$lang['Gender'] = "الجنس";
$lang['Orders'] = "المراكز";
$lang['rac_ord1'] = "الأول";
$lang['rac_ord2'] = "الثاني";
$lang['rac_ord3'] = "الثالث";
$lang['rac_ord4'] = "الرابع";
$lang['rac_ord5'] = "الخامس";
$lang['rac_ord6'] = "السادس";
$lang['rac_ord7'] = "السابع";
$lang['rac_ord8'] = "الثامن";
$lang['rac_ord9'] = "التاسع";
$lang['rac_ord10'] = "العاشر";
$lang['project_status'] = "الحالة";
$lang['project_description'] = "وصف";
$lang['project_sub_tasks_num'] = "%s مهمة";
$lang['link_multi_media'] = "وسائط";

//Messages
$lang['success_categories_created'] = "تم إنشاء التصنيف بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات التصنيف بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات التصنيف ";
$lang['success_project_deleted'] = "تم حذف التصنيف بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف التصنيف , حاول مرة أخرى بعد تحديث الصفحة";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_sites'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_site'] = "تحديث بيانات الصفحة";
$lang['log_delete_project'] = "حذف مشروع";
