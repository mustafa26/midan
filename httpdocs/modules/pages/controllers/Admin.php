<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("Pages_Model");
        $this->lang->load("pages", $this->languages[$this->user_lang]);
        $this->model = new Pages_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("pages_status");
    }

    function index($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("pages_page_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_cats($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_pages");
        $this->data['content'] = "pages/cats";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("pages"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    private function _validate_cats() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("cat_name[ar]", langline("cat_name") . "(عربي)", "required|trim|max_length[255]");
        $this->form_validation->set_rules("cat_name[en]", langline("cat_name") . "(English)", "required|trim|max_length[255]");


        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add() {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add();
            exit;
        }

        $this->data['title'] = langline("pages_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "pages/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add() {
        $this->_validate_cats();
        if (!$this->errors) {
            $data['cat_name'] = serialize(post("cat_name"));
            $data['cat_created_by'] = $this->user->user_id;
            $data['cat_created'] = date("Y-m-d H:i:s");
            $data['cat_status'] = post("cat_status");

            if ($project_id = $this->model->insert_cats($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("pages"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("pages?open=form"));
    }

    function edit($id = 0) {
        if (!check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }
        $cat = $this->model->get_cats($id);
        $this->data['title'] = langline("pages_page_title") . " - " . $this->site->site_name;
        log_add_user_action($this->user->user_id, $id, "show", "log_show_pages");
        $this->data['content'] = "pages/edit_cat";
        $this->data['data'] = $cat;
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0) {
        $this->_validate_cats();
        if (!$this->errors) {
            $data['cat_name'] = serialize(post("cat_name"));
            $data['cat_status'] = post("cat_status");
            if ($this->model->update_cats($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_pages");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("pages"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("pages/edit/$id"));
    }

    function delete($id) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['cat_deleted'] = 1;
        if ($this->model->delete_cats($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("pages"));
    }

    function cats($id = 0, $offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("pages_page_title") . " - " . $this->site->site_name;
        $limit = 20;
        $cat = $this->model->get_cats($id);
        $result = $this->model->get_all($id, $offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_pages");
        $this->data['content'] = "pages/index";
        $this->data['data'] = $result['data'];
        $this->data['cats'] = $cat;
        $this->data['pagination'] = $this->pagination->pager(admin_url("pages"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        foreach ($this->languages as $key => $value) {
            $this->form_validation->set_rules("page_Title[$key]", langline("page_Title") . $value, "required|trim|max_length[255]");
            $this->form_validation->set_rules("page_SubTitle[$key]", langline("page_SubTitle") . $value, "required|trim|max_length[255]");
            $this->form_validation->set_rules("page_Content[$key]", langline("page_Content") . $value, "required");
        }

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function addpage($fid = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_addpage($fid);
            exit;
        }

        $this->data['title'] = langline("pages_page_title") . " - " . $this->site->site_name;
        $cat = $this->model->get_cats($fid);
        $this->data['cats'] = $cat;
        $this->data['content'] = "pages/add";
        $this->template->render("template", $this->data);
    }

    private function _do_addpage($fid = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("page_HeaderPhoto");
        if (!$this->errors) {
            $data['page_ParentID'] = $fid;
            $data['page_Title'] = serialize(post("page_Title"));
            $data['page_SubTitle'] = serialize(post("page_SubTitle"));
            $data['page_Content'] = serialize(post("page_Content"));
            $data['page_MetaDescr'] = serialize(post("page_MetaDescr"));
            $data['page_MetaKeywords'] = serialize(post("page_MetaKeywords"));
            $data['page_Created_By'] = $this->user->user_id;
            $data['page_Created'] = date("Y-m-d H:i:s");
            $data['page_Active'] = post("page_Active")?1:0;

            if ($upload_data) {
                $data['page_HeaderPhoto'] = $upload_data['file_name'];
            }
            if ($project_id = $this->model->insert($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("pages/cats/$fid"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("pages/addpage/$fid"));
    }

    function editpage($fid = 0, $id = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_updatepage($fid, $id);
            exit;
        }

        $this->data['title'] = langline("pages_page_title") . " - " . $this->site->site_name;
        $cat = $this->model->get_cats($fid);
        $this->data['cats'] = $cat;
        $this->data['content'] = "pages/edit";
        $this->data['page'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    private function _do_updatepage($fid = 0, $id = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("page_HeaderPhoto");
        if (!$this->errors) {
            $data['page_Title'] = serialize(post("page_Title"));
            $data['page_SubTitle'] = serialize(post("page_SubTitle"));
            $data['page_Content'] = serialize(post("page_Content"));
            $data['page_MetaDescr'] = serialize(post("page_MetaDescr"));
            $data['page_MetaKeywords'] = serialize(post("page_MetaKeywords"));
            $data['page_Active'] = post("page_Active")?1:0;

            if ($upload_data) {
                $data['page_HeaderPhoto'] = $upload_data['file_name'];
            }

            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_pages");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("pages/cats/$fid"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("pages/editpage/$fid/$id"));
    }

    function deletepage($fid = 0, $id = 0) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['page_Active'] = 0;
        $data['page_Deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("pages/cats/$fid"));
    }

    private function do_upload($file_name = "userfile") {
        $this->load->library("upload");
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/pages";
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                return $this->upload->data();
            }
        } else {
            return false;
        }
    }

}
