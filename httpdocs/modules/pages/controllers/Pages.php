<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pages extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Pages_Model",'model');
    }

    public function index($id = 0) {
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "pages/index";
        $this->data['page'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }
    
    public function location() {
        $this->data['title'] = $this->site->site_name . ' | ' . ' موقع الميدان';
        $this->data['content'] = "pages/location"; 
        $this->template->render("template", $this->data);
    }
	
    public function contact() {
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "pages/contact"; 
        $this->template->render("template", $this->data);
    }
    

}
