<?php
/*********************************************
 *  Project     : projects_management
 *  File        : users_lang.php
 *  Created at  : 5/24/15 - 2:19 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 $lang['users_page_header']             = "Users";
 $lang['users_page_title']             = "Users";
$lang['users_update_profile']           = "Update your Profile";
$lang['change_password_title']          = "Changing Password";
 $lang['users_user_avatar_col']         = "Avatar";
 $lang['users_user_name_col']           = "Name";
 $lang['users_user_login_col']          = "Login Name";
 $lang['users_user_email_col']          = "Email";
 $lang['users_user_groups_col']          = "Group";
 $lang['users_user_created_col']        = "Created at";
 $lang['users_link']                    = "Users";
$lang['users_user_history']                      = "History";
 $lang['btn_change_password']           = "Change Password";
$lang['btn_select_image']               = "Select image";
// labels
$lang['users_user_name']                = "Member Name";
$lang['users_user_avatar']              = "Avatar";
$lang['users_login_name']               = "Login name ";
$lang['users_old_password']            = "Old Password";
$lang['users_new_password']            = "New Password";
$lang['users_user_password']            = "Password";
$lang['users_user_password_confirm']    = "Confirm Password";
$lang['users_user_email']               = "Email";
$lang['users_user_phone']               = "Phone";
$lang['users_user_groups']              = "Groups";
$lang['users_user_about']               = "About Member";
$lang['users_user_status']              = "Active";
$lang['btn_login']                      ="Login";
$lang['notifications_label']                     = "Notifications";
$lang['by_email_label']                     = "By Email";
$lang['by_sms_label']                     = "By Sms";
$lang['notify_by_email']                        = "Send Notifications to Email";
$lang['notify_by_sms']                          = "Send Notifications to mobile";
$lang['login_page_header']              ="Login to your account";
$lang['login_page_title']               ="Login";

//Messages
$lang['error_email_already_exist']              = "Error: Email Already Exists";
$lang['error_username_already_exist']              = "Error: Login Name Already Exists";
$lang['success_user_created']              = "User Created Successfully";
$lang['success_password_changed']                   = "Password changed successfully";
$lang['error_invalid_old_password']                   = "<b>Error :</b> Old Password is invalid";
$lang['success_your_account_updated']                   = "Your profile Updated Successfully";
$lang['error_not_valid_username_password']  ="Login name or password Not valid";
$lang['error_not_valid_email_password']  ="Email or password Not valid";

$lang['log_logged_in']                          = "Login to Control Panel";
$lang['log_logged_out']                          = "Logout from his account";
$lang['log_show_users']                         = "Display users List";
$lang['log_add_users']                          = "Add a new user";
$lang['log_edit_users']                         = "Update user information";
$lang['log_delete_users']                       = "Delete user account";