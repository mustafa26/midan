<?php

$lang['users_page_header'] = "المستخدمين";
$lang['users_page_title'] = "المستخدمين";
$lang['users_update_profile'] = "تحديث الصفحة الشخصية";
$lang['change_password_title'] = "تغيير كلمة المرور";
$lang['users_user_avatar_col'] = "صورة رمزية";
$lang['users_user_name_col'] = "الاسم";
$lang['users_user_login_col'] = "اسم الدخول";
$lang['users_user_email_col'] = "البريد الإلكتروني";
$lang['users_user_groups_col'] = "المجموعة";
$lang['users_user_created_col'] = "انشئ في";
$lang['users_link'] = "المستخدمين";
$lang['btn_change_password'] = "تغيير كلمة المرور";
$lang['btn_select_image'] = "ارفع صورة";

// labels
$lang['users_user_name'] = "الاسم";
$lang['users_nikName'] = "اللقب";
$lang['users_nikName_1'] = "صاحب السمو السيد";
$lang['users_nikName_2'] = "السيد";
$lang['users_nikName_3'] = "الدكتور";
$lang['users_nikName_4'] = "الشيخ";
$lang['users_nikName_5'] = "الفاضل";  
	
$lang['users_FName'] = "الاسم الأول";
$lang['users_SName'] = "الاسم الثاني";
$lang['users_TName'] = "الاسم الثالث";
$lang['users_QName'] = "القبيلة";

$lang['users_user_avatar'] = "صورة";
$lang['users_login_name'] = "اسم الدخول";
$lang['forget_password'] = " نسيت كلمة المرور";
$lang['users_old_password'] = "كلمة المرور القديمة";
$lang['users_new_password'] = "كلمة المرور الجديدة";
$lang['users_user_password'] = "كلمة المرور";
$lang['users_user_password_confirm'] = "تأكيد كلمة المرور";
$lang['users_user_email'] = "البريد الإلكتروني";
$lang['users_user_phone'] = "الهاتف/النقال";
$lang['users_user_groups'] = "المجموعات";
$lang['users_user_about'] = "نبذة عن المستخدم";
$lang['users_user_status'] = "مفعل";
$lang['users_user_history'] = "السجل";
$lang['notifications_label'] = "الإشعارات";
$lang['by_email_label'] = "بالبريد الإلكتروني";
$lang['by_sms_label'] = "بالرسائل القصيرة(SMS)";
$lang['notify_by_email_label'] = "إرسال الإشعارات إلى الإيميل";
$lang['notify_by_sms_label'] = "إرسال الإشعارات إلى الهاتف";
$lang['btn_login'] = "دخول";
$lang['login_page_header'] = "تسجيل الدخول إلى حسابك";
$lang['login_page_title'] = "تسجيل الدخول";
$lang['login'] = "تسجيل الدخول";
$lang['login_new'] = "تسجيل جديد";
$lang['Active_Code'] = "رمز التفعيل";

$lang['UR_Nationlaty'] = "الجنسية";

// Country
$lang['oman'] = "سلطنة عُمان";
$lang['uea'] = "الإمارات العربية المتحدة";
$lang['ksa'] = "المملكة العربية السعودية";
$lang['ksa'] = "ممكلة البحرين";
$lang['qatar'] = "قطر";
$lang['kiwat'] = "الكويت";

$lang['UR_NationalID'] = "رقم البطاقة الشخصية (الرقم المدني)";
$lang['UR_NationalIDImage'] = "صورة البطاقة";
$lang['UR_MemberID'] = "رقم المشارك";
$lang['UR_MemberIDImag'] = "صورة بطاقة المشارك";
$lang['ImageCode'] = "رمز الصورة";
$lang['Retype_IMGCode'] = "اطبع الرمز الموضح بالصورة";

$lang['rqst_Name'] = "اسم المطية";
$lang['rqst_NumCard'] = "رقم الشريحة";
$lang['rqst_NumCard_'] = "برجاء كتابة الرقم الخاص بالشريحة للمطية";
$lang['rqst_NumRace'] = "رقم الشوط";
$lang['rqst_Image'] = "صورة للمطية";
$lang['rqst_PermitImage'] = "صورة التصريح (برنت) من الاتحاد المعني";
$lang['rqst_AgeGroup'] = "الفئة العمرية للمطية ";
$lang['rqst_AgeGroup_2'] = "الحجائج (سنتان)";
$lang['rqst_AgeGroup_3'] = "اللقايا (ثلاث سنوات)";
$lang['rqst_AgeGroup_4'] = "اليداع أو الجذع (أربع سنوات)";
$lang['rqst_AgeGroup_5'] = "الثنايا (خمس سنوات)";
$lang['rqst_AgeGroup_6'] = "الرباع والحايل والحول والزمول  (ست سنوات فأكثر)";
$lang['rqst_Gender'] = "جنس المطية";
$lang['rqst_Gender_1'] = "أبكار";
$lang['rqst_Gender_2'] = "جعدان";
$lang['rqst_Gender_3'] = "حول";
$lang['rqst_Gender_4'] = "زمول";
$lang['rqst_Gender_01'] = "ذكر";
$lang['rqst_Gender_02'] = "أنثى"; 
$lang['statusNew'] = "جديد";
$lang['statusAccept'] = "مقبول";
$lang['statusNonAccept'] = "مرفوض";

$lang['captchaCode'] = "صورة الرمز";
$lang['UR_Phone'] = "رقم الهاتف ";
$lang['UR_BD'] = "تاريخ الميلاد";
$lang['Day'] = "يوم";
$lang['Month'] = "شهر";
$lang['Year'] = "سنة"; 
//Messages
$lang['error_Maxfile_NotAllow'] = "<b>خطأ :</b>  حجم الصورة تجاوز الحد الأقصى وهو 4MB";
$lang['error_Maxfile_Allow'] = "<b>ملاحظة :</b>  يجب أن لا يتجاوز حجم الصورة عن  4MB.";
$lang['error_Maxfile_Upload1'] = "<b>ملاحظة :</b>  حجم الصورة المضافة هو :  ";
$lang['error_Maxfile_Upload2'] = " يرجى تصغير حجم الصورة وإعادة رفعها. ";

$lang['error_email_already_exist'] = "<b>خطأ :</b> البريد الإلكتروني مسجل من قبل";
$lang['error_NationalID_existes'] = "<b>خطأ :</b>  الرقم المدني مسجل من قبل";
$lang['error_mobile_existes'] = "<b>خطأ :</b>   رقم الهاتف مسجل من قبل";
$lang['error_MemberID_existes'] = "<b>خطأ :</b>   رقم المشارك مسجل من قبل";
$lang['error_mobile_notnumeric'] = "<b>خطأ :</b> رقم الهاتف يجب أن يكون أرقام فقط";
$lang['error_captchaCode_notmath'] = "<b>خطأ :</b> الرمز لا يتطابق مع الصورة"; 
$lang['error_username_already_exist'] = "<b>خطأ :</b> اسم الدحول موجود من قبل";
$lang['success_user_created'] = "تم تسجيل بيانات المستخدم بنجاح";
$lang['success_password_changed'] = "تم تغيير كلمة المرور بنجاح";
$lang['success_your_account_updated'] = "تم تحديث صفحتك الشخصية بنجاح <br><br>";
$lang['error_invalid_old_password'] = "<b>خطأ :</b> كلمة المرور القديمة غير صحيحة ";
$lang['error_not_valid_username_password'] = "خطأ في اسم الدخول أو كلمة المرور";
$lang['error_not_valid_email_password'] = "<b>خطأ :</b> في البريد الإلكتروني أو كلمة المرور";
$lang['error_not_valid_phone_password'] = "<b>خطأ :</b> في الهاتف أو كلمة المرور";
$lang['error_not_valid_phone_NationalID'] = "<b>خطأ :</b> في الهاتف أو الرقم المدني";
$lang['error_invalid_password'] = "كلمة المرور غير صحيحة";
$lang['error_Type_Active_Code'] = "<strong>لطفا! ....</strong> ادخل كود التفعيل الذي ارسل لك عن طريق رسالة نصية بالهاتف والمكون من 6 أرقام.";
$lang['error_Active_Code'] = "<strong>حطأ! ....</strong> رمز التفعيل غير صحيح";
$lang["Send_Active_Code"] = "(سيصلك رمز التفعيل علي هاتفك)";
$lang['error_Deactive_Account'] = "<strong>لطفا! ....</strong> تواصل مع الإدارة فقد تم إيقاف حسابك بالموقع."; 

$lang['log_logged_in'] = "تسجيل الدخول إلى لوحة التحكم";
$lang['log_logged_out'] = "تسجيل الخروج من الحساب";
$lang['log_show_users'] = "عرض قائمة المستخدمين";
$lang['log_add_users'] = "إضافة مستخدم جديد";
$lang['log_edit_users'] = "تحديث بيانات مستخدم";
$lang['Edit'] = "تحديث البيانات الشخصية";
$lang['log_delete_users'] = "حذف حساب مستخدم من النظام"; 
$lang['cannt_updated'] = "<strong>خطأ :</strong> لايمكن تحديث البيانات";
