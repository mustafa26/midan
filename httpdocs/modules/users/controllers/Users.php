<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Users extends Public_Controller
{

    public function __construct()
    {
        parent:: __construct();
        $this->load->helper('captcha');
        $this->lang->load("users", $this->languages[$this->user_lang]);
        $this->load->model("Users_Model", 'model');
        //$this->session->set_userdata('captchaCode', "0000");
    }

    public function index()
    {
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "users/index";
        $this->template->render("template", $this->data);
    }

    public function regist()
    {
        if (post()) {
            $this->_do_add($_SESSION['captchaCode']);
            exit;
        }
        $this->data['title'] = $this->site->site_name;
        $this->data["captchaImg"] = $this->creat_cap(random_string('numeric', 4));
        $this->data['content'] = "users/regist";
        $this->template->render("template", $this->data);
    }

    private function _validate()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('FName', langline("users_FName"), 'required|trim|max_length[16]');
        $this->form_validation->set_rules('SName', langline("users_SName"), 'required|trim|max_length[16]');
        $this->form_validation->set_rules('TName', langline("users_TName"), 'required|trim|max_length[16]');
        $this->form_validation->set_rules('QName', langline("users_QName"), 'required|trim|max_length[16]');
        $this->form_validation->set_rules('UR_Password', langline("users_user_password"), 'required|trim|max_length[32]|min_length[6]');
        $this->form_validation->set_rules('ReUR_Password', langline("users_user_password_confirm"), 'required|matches[UR_Password]');
        //$this->form_validation->set_rules('UR_Email', 'lang:users_user_email', 'required|trim|valid_email');
        $this->form_validation->set_rules('UR_Nationlaty', 'lang:UR_Nationlaty', 'required|trim');
        $this->form_validation->set_rules('UR_NationalID', 'lang:UR_NationalID', 'required|trim|numeric|max_length[30]');
        $this->form_validation->set_rules('UR_MemberID', 'lang:UR_MemberID', 'required|trim|numeric|max_length[15]');
        $this->form_validation->set_rules('UR_Phone', 'lang:UR_Phone', 'required|trim'); //numeric
        //$this->form_validation->set_rules('UR_Job', 'lang:UR_Job', 'required|trim');
        //$this->form_validation->set_rules('UR_JobPlace', 'lang:UR_JobPlace', 'required|trim');
        //$this->form_validation->set_rules('UR_Address', 'lang:UR_Address', 'required|trim');
        //$this->form_validation->set_rules('UR_Image', 'lang:UR_Image', 'required');
        if (empty($_FILES['UR_NationalIDImage']['name'])) {
            $this->form_validation->set_rules('UR_NationalIDImage', 'lang:UR_MemberIDImag', 'required');
        } elseif ($_FILES['UR_NationalIDImage']['size'] > 4248000) {
            $this->errors .= "<p>" . langline("error_Maxfile_NotAllow") . "</p>"
                . "<p>" . langline("error_Maxfile_Upload1") . round(($_FILES['UR_NationalIDImage']['size'] / 1000000), 1) . "MB، " . langline("error_Maxfile_Upload2") . "</p>";
        }

        /* if (post("username")) {
          if ($this->model->check_username(post("username"))) {
          $this->errors .= "<p>" . langline("error_username_existes") . "</p>";
          }
          } */
        /*
          if (post("UR_Email")) { // && post("UR_Email") != post("hidden_email")
          if ($this->model->check_email_rgst(post("UR_Email"))) {
          $this->errors .= "<p>" . langline("error_email_existes") . "</p>";
          }
          }
         */

        if (post("UR_Phone")) {
            $n = str_replace("+", "", post("UR_Phone", true));
            $nn = str_replace(" ", "", $n);
            $numbers = $nn;
            if ($this->model->check_mobile_rgst($numbers)) {
                $this->errors .= "<p>" . langline("error_mobile_existes") . "</p>";
            }

            if (!is_numeric($numbers))
                $this->errors .= "<p>" . langline("error_mobile_notnumeric") . "</p>";
        }

        if (post("UR_NationalID")) {
            if ($this->model->check_nationid(post("UR_NationalID"))) {
                $this->errors .= "<p>" . langline("error_NationalID_existes") . "</p>";
            }
        }

        if (post("UR_MemberID")) {
            if ($this->model->check_memberid(post("UR_MemberID"))) {
                $this->errors .= "<p>" . langline("error_MemberID_existes") . "</p>";
            }
        }

        if ($this->form_validation->run() == FALSE) {
            $this->errors .= validation_errors();
            return FALSE;
        }
        return TRUE;
    }

    private function _do_add($sessCaptcha = 0)
    {
        $this->_validate();
        $inputCaptcha = post("captcha", true);
        $sessCaptcha = post("captchaCode");
        //$sessCaptcha = $this->session->userdata('captchaCode');
        if ($inputCaptcha <> $sessCaptcha)
            $this->errors .= "<p>" . langline("error_captchaCode_notmath") . "</p>";


        if (!$this->errors) {
            //$data['rqst_FID'] = $id;
            //$data['rqst_Type'] = 'Request';abs()
            $n = str_replace("+", "", post("UR_Phone", true));
            $nn = str_replace(" ", "", $n);
            $numbers = $nn;
            $data['UR_nikName'] = post("nikName", true);
            $data['UR_FName'] = post("FName", true);
            $data['UR_SName'] = post("SName", true);
            $data['UR_TName'] = post("TName", true);
            $data['UR_QName'] = post("QName", true);
            $data['UR_Name'] = post("nikName", true) . ' ' . post("FName", true) . ' ' . post("SName", true) . ' ' . post("QName", true);
            $data['UR_UserName'] = 'NULL';
            $password = $this->model->encrypt_password(post("UR_Password", true));
            $data['UR_Password'] = $password['hash'];
            $data['UR_Salt'] = $password['salt'];
            //$data['UR_Email'] = post("UR_Email");
            $data['UR_Phone'] = ltrim($numbers, '00');
            //$data['UR_Job'] = post("UR_Job");
            //$data['UR_JobPlace'] = post("UR_JobPlace");
            $data['UR_MemberID'] = post("UR_MemberID", true);
            $data['UR_NationalID'] = post("UR_NationalID", true);
            $data['UR_Nationlaty'] = post("UR_Nationlaty", true);
            //$data['UR_Address'] = post("UR_Address");
            $data['UR_Status'] = 1; //0:underprocessing,1:New,2:Active,3:Deactive
            $data['UR_Deleted'] = 0;
            $data['UR_Created'] = date("Y-m-d H:i:s");

            //$_DOB = "";
            //$_DOB = post("UR_BD_y") . '-' . post("UR_BD_m") . '-' . post("UR_BD_d"); //0000-00-00
            //$data['UR_BD'] = $_DOB;
            //$_IDExp = "";
            //$_IDExp = post("UR_NationalIDExp_y") . '-' . post("UR_NationalIDExp_m") . '-' . post("UR_NationalIDExp_d"); //0000-00-00
            //$data['UR_NationalIDExp'] = $_IDExp;
            //$upload_Image = $this->do_upload("UR_Image");
            //$data['UR_Image'] = $upload_Image ? $upload_Image['file_name'] : '';
            $upload_NationalIDImage = $this->do_upload("UR_NationalIDImage");
            $data['UR_NationalIDImage'] = $upload_NationalIDImage ? $upload_NationalIDImage['file_name'] : '';
            // Generate ActiveCode
            $_ActiveCode = random_string('numeric', 6);
            $data['UR_ActiveCode'] = $_ActiveCode;
            if ($rqst_id = $this->model->insert_rgst($data)) {
                // Send SMS Config
                $user = "USER";
                $password = "*********";
                $sender = "USER";
                // %20
                $mess = "مرحبا" . "%20" . "بك" . "%20" . "في" . "%20" . "ميدان" . "%20" . "البشائر" . "%20";
                $mess .= "،رمز" . "%20" . "التفعيل" . "%20";
                $mess .= $_ActiveCode;

                //$send = "http://sms4world.net/smspro/sendsms.php?user=" . $user . "&password=" . $password . "&numbers=" . $numbers . "&sender=" . $sender . "&message=" . $mess . "&lang=ar";
                //$return_status = file_get_contents($send);
                //set action in user log   && $return_status
                $this->set_message(langline("Success"), "success");
                redirect(site_url("users/login?rt=1"));
            }
        }

        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(site_url("users/regist"));
    }

    public function result()
    {
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "users/result";
        $this->template->render("template", $this->data);
    }


// #################################### Login Area ##########################
    public function login()
    {
        if (post()) {
            $this->_do_login($_SESSION['captchaCode']); //$this->session->userdata('captchaCode')
            exit;
        }
        $random_num = random_string('numeric', 4);//$_SESSION['captchaCode']
        $this->data['title'] = $this->site->site_name;
        $this->data["captchaImg"] = $this->creat_cap($random_num);
        $this->data['content'] = "users/login";
        $this->template->render("template", $this->data);
    }

    private function _do_login($sessCaptcha = 0)
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("Phone", "lang:UR_Phone", "required|trim"); //|max_length[8] //is_natural
        $this->form_validation->set_rules("Password", "lang:users_user_password", "required|trim");
        $this->form_validation->set_rules("captcha", "lang:captchaCode", "required|numeric|trim");
        ######### Check Captcha
        $inputCaptcha = post("captcha");
        $sessCaptcha = post("captchaCode");

        //$sessCaptcha = $this->session->userdata('captchaCode'); . $inputCaptcha . '=>' . $sessCaptcha
        if ($inputCaptcha === $sessCaptcha)
            true;
        else
            $this->errors .= "<p>" . langline("error_captchaCode_notmath") . "</p>";
        ######### End Check Captcha
        ######### Check Phone
        $n = str_replace("+", "", post("Phone", true));
        $nn = str_replace(" ", "", $n);
        $numbers = $nn;
        if (!is_numeric($numbers)) {
            $this->set_message(langline("error_mobile_notnumeric"), "error");
        }
        ######### End Check Phone
        if ($this->form_validation->run() == FALSE || $this->errors != "") {
            $this->errors .= validation_errors();
            $this->set_message($this->errors, "error");
            $this->keep_data();
            redirect(site_url("users/login"));
        } else {
            //check login name and password
            $user = null;
            $_userlog["UR_Phone"] = $numbers;
            post("activecode") ? $_userlog["UR_ActiveCode"] = post("activecode", true) : 0;
            $user = $this->model->check_phone_password_rgst($_userlog, post("Password", true));

            if ($user && $user->UR_Status == 1) {
                if (post("activecode", TRUE) == $user->UR_ActiveCode) {
                    $this->model->update_rgst($user->UR_ID, array("UR_Status" => 2));
                    $remember = false;
                    if (post("remember"))
                        $remember = true;
                    $this->model->set_loggedin_user_rgst($user, $remember);
                    // Send SMS Config
                    //$numbers = $numbers;
                    $user = "USER";
                    $password = "*********";
                    $sender = "USER";
                    $mess = "مرحبا" . "%20" . "بك" . "%20" . "في" . "%20" . "ميدان" . "%20" . "البشائر" . "%20";
                    $mess .= "تم" . "%20" . "تفعيل" . "%20";
                    $mess .= "حسابك";
                    //$send = "http://sms4world.net/smspro/sendsms.php?user=" . $user . "&password=" . $password . "&numbers=" . $numbers . "&sender=" . $sender . "&message=" . $mess . "&lang=ar";
                    //$return_status = file_get_contents($send);
                    redirect(site_url() . "users/profile");
                } else {
                    $_qs = "?rt=1";
                    $this->set_message(langline("error_Type_Active_Code"), "warning");
                    $this->keep_data();
                    redirect(site_url() . "users/login" . $_qs);
                }
            } else if ($user && $user->UR_Status == 2) {
                $remember = false;
                if (post("remember"))
                    $remember = true;
                $this->model->update_rgst($user->UR_ID, array("UR_ResetPass" => 1));
                $this->model->set_loggedin_user_rgst($user, $remember);
                redirect(site_url() . "users/profile");
            } else if ($user && $user->UR_Status == 3) {
                $this->set_message(langline("error_Deactive_Account"), "warning");
                redirect(site_url() . "users/login");
            } else {
                if ($this->form_validation->run()) $this->set_message(langline("error_not_valid_phone_password"), "error");
                redirect(site_url() . "users/login");
            }
        }
    }

    public function profile()
    {
        $this->data['title'] = $this->site->site_name;
        $this->data['data'] = $this->model->get_rgst($_SESSION["mid"]);
        $this->data['content'] = "users/profile";
        $this->template->render("template", $this->data);
    }

    public function requests()
    {
        $this->data['title'] = $this->site->site_name;
        $result = $this->model->get_all_rqsts($_SESSION["mid"]);
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['content'] = "users/requests";
        $this->template->render("template", $this->data);
    }

    public function edit()
    {
        if (post()) {
            $this->_do_edit();
            exit;
        }
        $this->data['title'] = $this->site->site_name;
        $this->data['data'] = $this->model->get_rgst($_SESSION["mid"]);
        $this->data['content'] = "users/edit";
        $this->template->render("template", $this->data);
    }

    private function _do_edit()
    {
        //$this->_validate();
        if (!$this->errors) {
            $data['UR_nikName'] = post("nikName", true);
            $data['UR_FName'] = post("FName", true);
            $data['UR_SName'] = post("SName", true);
            $data['UR_TName'] = post("TName", true);
            $data['UR_QName'] = post("QName", true);
            $data['UR_Name'] = post("nikName", true) . ' ' . post("FName", true) . ' ' . post("SName", true) . ' ' . post("TName", true) . ' ' . post("QName", true);
            //$data['UR_UserName'] = 'NULL'; 
            //$data['UR_Phone'] = post("UR_Phone");
            $data['UR_Nationlaty'] = post("UR_Nationlaty");
            //$data['UR_Job'] = post("UR_Job");
            //$data['UR_JobPlace'] = post("UR_JobPlace");
            $data['UR_MemberID'] = post("UR_MemberID", true);
            $data['UR_NationalID'] = post("UR_NationalID", true);
            $data['UR_ResetPass'] = 0;
            if ($_FILES['UR_NationalIDImage']['name']) {
                if ($_FILES['UR_NationalIDImage']['size'] > 4248000) {
                    $this->errors .= "<p>" . langline("error_Maxfile_NotAllow") . "</p>"
                        . "<p>" . langline("error_Maxfile_Upload1") . round(($_FILES['UR_NationalIDImage']['size'] / 1000000), 1) . "MB، " . langline("error_Maxfile_Upload2") . "</p>";
                } else {
                    $upload_NationalIDImage = $this->do_upload("UR_NationalIDImage");
                    $data['UR_NationalIDImage'] = $upload_NationalIDImage ? $upload_NationalIDImage['file_name'] : '';
                    post("old_NationalIDImage")?unlink("uploads/requests/".post("old_NationalIDImage")):0;
                }
            }
            $id = $_SESSION['mid'];
            if ($rqst_id = $this->model->update_rgst($id, $data)) {
                //set action in user log  
                $this->set_message(langline("success_your_account_updated"), "success");
                redirect(site_url("users/edit"));
            } else {
                $this->errors .= "<p>" . langline("cannt_updated") . "</p>";
            }
        }

        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(site_url("users/edit"));
    }

    public function changepassword()
    {
        if (post()) {
            $this->_do_change_pass($_SESSION['captchaCode']);//$this->session->userdata('captchaCode')
            exit;
        }

        $this->data['title'] = $this->site->site_name;
        $this->data["captchaImg"] = $this->creat_cap(random_string('numeric', 4));
        $this->data['content'] = "users/changepassword";
        $this->template->render("template", $this->data);
    }

    private function _do_change_pass($sessCaptcha = 0)
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("captcha", "lang:captchaCode", "required|numeric|trim");
        $this->form_validation->set_rules('UserPassword', langline("users_user_password"), 'required|trim|max_length[32]|min_length[6]');
        $this->form_validation->set_rules('UserRePassword', langline("users_user_password_confirm"), 'required|matches[UserPassword]');

        ######### Check Captcha
        $inputCaptcha = post("captcha", true);
        $sessCaptcha = post("captchaCode");
        //$sessCaptcha = $this->session->userdata('captchaCode');
        if ($inputCaptcha <> $sessCaptcha)
            $this->errors .= "<p>" . langline("error_captchaCode_notmath") . "</p>";
        ######### End Check Captcha
        ######### End Check Phone
        if ($this->form_validation->run() == FALSE || $this->errors != "") {
            $this->errors .= validation_errors();
            $this->set_message($this->errors, "error");
            $this->keep_data();
            redirect(site_url() . "users/changepassword");
        } else {
            //check login name and password
            $password = $this->model->encrypt_password(post("UserPassword", true));
            $data['UR_Password'] = $password['hash'];
            $data['UR_Salt'] = $password['salt'];
            $data['UR_ResetPass'] = 0;
            $id = $_SESSION['mid'];
            $this->model->update_rgst($id, $data);
            $this->set_message(langline("success_password_changed"), "success");
            redirect(site_url() . "users/profile");

        }
    }

    public function forgetpassword($_stpn = "stp1")
    {
        if (post()) {
            $this->_do_forget_pass($_stpn, $_SESSION['captchaCode']);//$this->session->userdata('captchaCode')
            exit;
        }

        $this->data['title'] = $this->site->site_name;
        $this->data["captchaImg"] = $this->creat_cap(random_string('numeric', 4));
        $this->data['content'] = "users/forgetpassword";
        $this->template->render("template", $this->data);
    }

    private function _do_forget_pass($_stpn = "stp1", $sessCaptcha = 0)
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("Phone", "lang:UR_Phone", "required|trim");
        $this->form_validation->set_rules("NationalID", "lang:UR_NationalID", "required|numeric|trim");
        $this->form_validation->set_rules("captcha", "lang:captchaCode", "required|numeric|trim");
        if ($_stpn == "stp2") {
            $this->form_validation->set_rules('UserPassword', langline("users_user_password"), 'required|trim|max_length[32]|min_length[6]');
            $this->form_validation->set_rules('UserRePassword', langline("users_user_password_confirm"), 'required|matches[UserPassword]');
            $this->form_validation->set_rules("activecode", "lang:Active_Code", "required|trim");
        }

        ######### Check Captcha
        $inputCaptcha = post("captcha", true);
        $sessCaptcha = post("captchaCode");
        //$sessCaptcha = $this->session->userdata('captchaCode');
        if ($inputCaptcha <> $sessCaptcha)
            $this->errors .= "<p>" . langline("error_captchaCode_notmath") . "</p>";
        ######### End Check Captcha
        ######### Check Phone
        $n = str_replace("+", "", post("Phone", true));
        $nn = str_replace(" ", "", $n);
        $numbers = $nn;
        if (!is_numeric($numbers)) {
            $this->set_message(langline("error_mobile_notnumeric"), "error");
        }
        ######### End Check Phone
        if ($this->form_validation->run() == FALSE || $this->errors != "") {
            $this->errors .= validation_errors();
            $this->set_message($this->errors, "error");
            $this->keep_data();
            redirect(site_url() . "users/forgetpassword/$_stpn");
        } else {
            //check login name and password
            $user = null;
            $_userlog["UR_Phone"] = $numbers;
            $_userlog["UR_NationalID"] = post("NationalID", true);
            post("activecode") ? $_userlog["UR_ActiveCode"] = post("activecode", true) : 0;
            //post("activecode") ? $_userlog["UR_ActiveCode"] = post("activecode", true) : 0;
            $user = $this->model->check_phone_NationalID_rgst($_userlog);

            if ($user && $user->UR_Status != 3 && $user->UR_ResetPass == 0) {
                // Generate ActiveCode
                $_ActiveCode = random_string('numeric', 6);
                $data['UR_ActiveCode'] = $_ActiveCode;
                $data['UR_ResetPass'] = 1; // Set column to Reset password
                // Send SMS Config
                $username = "USER";
                $password = "*********";
                $sender = "USER";
                // %20
                $mess = "مرحبا" . "%20" . "بك" . "%20" . "في" . "%20" . "ميدان" . "%20" . "البشائر" . "%20";
                $mess .= "،رمز" . "%20" . "التفعيل" . "%20" . "لكلمة" . "%20" . "المرور" . "%20";
                $mess .= $_ActiveCode;
                //$send = "http://sms4world.net/smspro/sendsms.php?user=" . $username . "&password=" . $password . "&numbers=" . $numbers . "&sender=" . $sender . "&message=" . $mess . "&lang=ar";
                //$return_status = file_get_contents($send);
                // set ActiveCode in user Record
                if ($ur_id = $this->model->update_rgst($user->UR_ID, $data)) {
                    //$_qs = "?npss=1";
                    $this->set_message(langline("error_Type_Active_Code"), "warning");
                    $this->keep_data();
                    redirect(site_url() . "users/forgetpassword/stp2");
                }
            } else if ($user && $user->UR_ResetPass == 1 && $_stpn = "stp2") {
                $password = $this->model->encrypt_password(post("UserPassword", true));
                $data['UR_Password'] = $password['hash'];
                $data['UR_Salt'] = $password['salt'];
                $data['UR_ResetPass'] = 0;
                $this->model->update_rgst($user->UR_ID, $data);
                $remember = false;
                if (post("remember"))
                    $remember = true;
                $this->model->set_loggedin_user_rgst($user, $remember);
                $this->set_message(langline("success_password_changed"), "success");
                redirect(site_url() . "users/profile");
            } else {
                if ($user && $user->UR_Status == 3) $this->set_message(langline("error_Deactive_Account"), "warning");
                if ($this->form_validation->run()) $this->set_message(langline("cannt_updated"), "error");
                redirect(site_url() . "users/forgetpassword/$_stpn");
            }
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(site_url());
    }

    function refresh()
    {
        echo $this->creat_cap(random_string('numeric', 4));
    }


    private function creat_cap($_word)
    {
        $config = array(
            'word' => random_string('numeric', 5),
            'img_path' => 'captcha_images/',
            'img_url' => base_url() . 'captcha_images/',
            'img_width' => '150',
            'img_height' => 37,
            'word_length' => 10,
            'font_size' => 16,
            'expiration' => 7200,
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => '',
                'text' => array(0, 0, 0),
                'grid' => ''
            )
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode', $captcha['word']);
        //session_destroy();

        // Send captcha image to view
        return $captcha['image'];
    }


    private function do_upload($file_name = "userfile")
    {
        $this->load->library("upload");
        $this->load->library('image_lib');
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/requests";
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $config['max_size'] = '4400000'; //  Max upload size  5 MB
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $image_data =   $this->upload->data();
                $configer =  array(
                    'image_library'   => 'gd2',
                    'source_image'    =>  $image_data['full_path'],
                    'maintain_ratio'  =>  TRUE,
                    'width'           =>  1000,
                    'height'          =>  1000,
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
                return $image_data;
            }
        } else {
            return false;
        }
    }

}
