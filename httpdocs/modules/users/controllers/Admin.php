<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->lang->load("users", $this->languages[$this->user_lang]);
        $this->lang->load("permissions/permissions", $this->languages[$this->user_lang]);
        $this->load->model("permissions/Permissions_model");
        $this->model = new Users_model();
    }
     
    function index($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }

        $filter = get("filter");
        $this->data['title'] = langline("users_page_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($filter, $offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_users");
        $this->data['content'] = "users/list";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("users"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("user_name", langline("users_user_name"), "required|trim|max_length[150]");

        if (post("hidden_user_username") and post("hidden_user_username") != post("user_username")) {
            $this->form_validation->set_rules("user_username", langline("users_login_name"), "required|max_length[50]|is_unique[users.user_username]", array('is_unique' => langline("error_username_already_exist")));
        } else {
            
        }

        if (post("hidden_user_email") and post("hidden_user_email") != post("user_email")) {
            $this->form_validation->set_rules("user_email", langline("users_user_email"), "required|trim|max_length[150]|valid_email|is_unique[users.user_email]", array('is_unique' => langline("error_email_already_exist")));
        } else {
            $this->form_validation->set_rules("user_email", langline("users_user_email"), "required|trim|max_length[150]|valid_email");
        }

        if (!post("hidden_user_username")) {
            $this->form_validation->set_rules("user_password", langline("users_user_password"), "required|trim|trim|max_length[50]");
            $this->form_validation->set_rules("confirm_password", langline("users_user_password_confirm"), "required|trim|max_length[50]|matches[user_password]");
        } else {
            $this->form_validation->set_rules("user_password", langline("users_user_password"), "trim|trim|max_length[50]");
            $this->form_validation->set_rules("confirm_password", langline("users_user_password_confirm"), "trim|max_length[50]|matches[user_password]");
        }

        $this->form_validation->set_rules("user_phone", langline("users_user_phone"), "required|trim|max_length[20]");

        $this->form_validation->set_rules("user_about", langline("users_user_about"), "trim|max_length[150]");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add() {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add();
            exit;
        } 
        
        $this->data['title'] = langline("users_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "users/add";
        $this->data['permissions'] = $this->Permissions_model->list_modules_permission();
        $this->template->render("template", $this->data);
    }

    private function _do_add() {
        $this->_validate();
        $upload_data = $this->do_upload("user_avatar","users",TRUE);
        if (!$this->errors) {
            $data['user_name'] = post("user_name", true);
            $data['user_username'] = post("user_username", true);
            $password = $this->model->encrypt_password(post("user_password"));
            $data['user_password'] = $password['hash'];
            $data['user_salt'] = $password['salt'];
            $data['user_email'] = post("user_email", true);
            $data['user_phone'] = post("user_phone", true);
            $data['user_about'] = post("user_about", true);
            $data['user_status'] = post("user_status") ? 1 : 0;
            $data['user_created'] = date("Y-m-d H:i:s");

            if ($upload_data) {
                $data['user_avatar'] = $upload_data['file_name'];
            }

            if ($user_id = $this->model->insert($data)) {
                $this->set_message(langline("success_user_created"), "success");
                redirect(admin_url("users"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("users/add"));
    }

    function show($id = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }
        $this->data['title'] = langline("users_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "users/show";
        $this->data['data'] = $this->model->get($id);

        $this->template->render("template", $this->data);
    }

    function edit($id = 0) {
        if (!check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        if (post()) {
            $this->_do_update($id);
            exit;
        }
        $this->data['title'] = langline("users_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "users/edit";
        $this->data['data'] = $this->model->get($id);
        $this->data['permissions'] = $this->Permissions_model->list_modules_permission($id);

        $this->template->render("template", $this->data);
    }

    function account() {
        if (!$this->user->user_id) {
            redirect(admin_url("permissions/denied"));
        }

        if (post()) {
            $this->_update_account();
            exit;
        }

        $this->data['title'] = langline("users_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "users/edit_profile";
        $this->data['data'] = $this->model->get($this->user->user_id);
        $this->template->render("template", $this->data);
    }

    function _update_account() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("user_name", langline("users_user_name"), "required|trim|max_length[150]");

        if (post("hidden_user_email") and post("hidden_user_email") != post("user_email")) {
            $this->form_validation->set_rules("user_email", langline("users_user_email"), "required|trim|max_length[150]|valid_email|is_unique[users.user_email]", array('is_unique' => langline("error_email_already_exist")));
        } else {
            $this->form_validation->set_rules("user_email", langline("users_user_email"), "required|trim|max_length[150]|valid_email");
        }

        $this->form_validation->set_rules("user_phone", langline("users_user_phone"), "required|trim|max_length[20]");
        $this->form_validation->set_rules("user_about", langline("users_user_about"), "trim|max_length[500]");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            $this->keep_data();
            $this->set_message($this->errors, "error");
        } else {
            $upload_data = $this->do_upload("user_avatar","users",TRUE);
            $data['user_email'] = post("user_email", true);
            $data['user_phone'] = post("user_phone", true);
            $data['user_about'] = post("user_about", true);
            $data['user_notify_by_email'] = (bool) post("notify_by_email", true);
            $data['user_notify_by_sms'] = (bool) post("notify_by_sms", true);

            if ($upload_data) {
                $data['user_avatar'] = $upload_data['file_name'];
            }

            $this->model->update($this->user->user_id, $data);

            $this->set_message(langline("success_your_account_updated"), "success");
        }

        redirect(admin_url("users/account"));
    }

    function ajaxChangeAccountPassword() {
        if ($this->is_ajax()) {
            $user_id = $this->user->user_id;
            $this->load->library("form_validation");
            $this->form_validation->set_rules("change_password[old]", "lang:users_old_password", "required|trim");
            $this->form_validation->set_rules("change_password[new]", "lang:users_new_password", "required|trim");
            $this->form_validation->set_rules("change_password[confirm]", "lang:users_user_password_confirm", "required|trim|matches[change_password[new]]");
            if ($this->form_validation->run() == false) {
                $this->jsonData['html'] = validation_errors();
            } else {
                $_password = post("change_password");
                if ($this->model->change_account_password($user_id, $_password['old'], $_password['new'])) {
                    $this->jsonData['success'] = 1;
                    $this->jsonData['html'] = langline("success_password_changed");
                } else {
                    $this->jsonData['success'] = 0;
                    $this->jsonData['html'] = langline("error_invalid_old_password");
                }
            }
            echo json_encode($this->jsonData);
        } else {
            show_404();
        }
    }

    private function _do_update($id) {
        $this->_validate();

        $upload_data = $this->do_upload("user_avatar","users",TRUE);

        if (!$this->errors) {

            $data['user_name'] = post("user_name", true);
            $data['user_username'] = post("user_username", true);

            if (post("user_password")) {
                $password = $this->model->encrypt_password(post("user_password"));

                $data['user_password'] = $password['hash'];
                $data['user_salt'] = $password['salt'];
            } 
            $data['user_email'] = post("user_email", true);
            $data['user_phone'] = post("user_phone", true);
            $data['user_about'] = post("user_about", true);

            if ($upload_data) { 
                $data['user_avatar'] = $upload_data['file_name'];
                if(is_file("uploads/users/".  post("user_avatar_old"))) unlink("uploads/users/".  post("user_avatar_old"));
                if(is_file("uploads/users/thumb/".  post("user_avatar_old"))) unlink("uploads/users/thumb/".  post("user_avatar_old"));  
            } 

            $this->model->update($id, $data);
            $this->_update_permissions($id); 

            $this->set_message(langline("success_user_created"), "success");
            redirect(admin_url("users"));
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("users/edit/$id"));
    }

    function _update_permissions($id = 0) {

        $this->load->model('components/Components_model');
        $modules = $this->Components_model->list_modules();
        $data['permission_user'] = $id;
        $supervision = post("perm_supervision_action");
        $show = post("perm_show_action");
        $add = post("perm_add_action");
        $edit = post("perm_edit_action");
        $delete = post("perm_delete_action");
        if ($modules) {
            $this->Permissions_model->delete_user_permissions($id);
            foreach ($modules as $key => $item) {
                $data['permission_module'] = $item->module_id;
                $data['permission_supervision'] = (bool) isset($supervision[$item->module_id]) ? $supervision[$item->module_id] : 0;
                $data['permission_show'] = (bool) isset($show[$item->module_id]) ? $show[$item->module_id] : 0;
                $data['permission_add'] = (bool) isset($add[$item->module_id]) ? $add[$item->module_id] : 0;
                $data['permission_edit'] = (bool) isset($edit[$item->module_id]) ? $edit[$item->module_id] : 0;
                $data['permission_delete'] = (bool) isset($delete[$item->module_id]) ? $delete[$item->module_id] : 0;
                $this->Permissions_model->update($data);
            }
            log_add_user_action($this->user->user_id, $id, "edit", "log_edit_permissions", "permissions");
        }
    }

    function delete($id) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->model->update($id, array("user_status" => 0));
        $this->set_message(langline("success_user_deleted"), "success");
        redirect(admin_url("users"));
    } 

    function login() {
        if ($this->admin_logged_in()) {
            redirect($this->prev_page());
        }

        if (post()) {
            $this->_do_login();
            exit;
        }
        $this->data['title'] = langline("login_page_title") . " - " . $this->site->site_name;
        $this->template->render("users/login", $this->data);
    }

    private function _do_login() {
        $this->load->library("form_validation");
        $login_type = config_item("admin.login_type");
        if ($login_type == "username") {
            $this->form_validation->set_rules("username", "lang:users_login_name", "required|trim|max_length[50]");//|xss_clean
        } else {
            $this->form_validation->set_rules("email", "lang:users_email", "required|valid_email|trim|max_length[100]");
        }
        $this->form_validation->set_rules("password", "lang:user_password", "required|trim");
        if ($this->form_validation->run() == false) {
            $this->set_message(validation_errors(), "error");
            $this->keep_data();
            redirect(admin_url("login")); 
        } else {
            //check login name and password
            $user = null;
            if ($login_type == "username") {
                $user = $this->model->check_username_password(post("username", true), post("password", true), true);
            } else {
                $user = $this->model->check_email_password(post("email", true), post("password", true), true);
            }
            if ($user) {
                //die(var_dump($user));
                $remember = false;
                if (post("remember"))
                    $remember = true;
                $this->model->set_loggedin_user($user, $remember);
                log_add_user_action($user->user_id, 0, "show", "log_logged_in");

                if ($this->prev_page() == site_url()) {
                    redirect(admin_url("admin"));
                } else {
                    redirect($this->prev_page());
                }
            } else {
                if ($login_type == "username") {
                    $this->set_message(langline("error_not_valid_username_password"), "error");
                } else {
                    $this->set_message(langline("error_not_valid_email_password"), "error");
                }
                $this->keep_data();
                redirect(site_url("cpsite/lg"));
            }
        }
    }

    function logout() {
        if ($this->user) {
            log_add_user_action($this->user->user_id, $this->user->user_id, "delete", "log_logged_out");
        }

        $this->model->logout();
        redirect(site_url("cpsite/lg"));
    }
    
    private function do_upload($file_name = "userfile") {
        $this->load->library("upload");
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/pages";
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                return $this->upload->data();
            }
        } else {
            return false;
        }
    }

}
