<?php

class Users_Model extends CI_Model
{

    private $table = "users";
    private $table_rgst = "usersregister";

    function __construct()
    {
        parent:: __construct();
    }

    function get_all($filter = array(), $offset = 0, $limit = 0)
    {

        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }

        if ($filter) {
            if ($filter['name']) {
                $this->db->like("a.user_name", $filter['name']);
            }

            if ($filter['email']) {
                $this->db->where("a.user_email", $filter['email']);
            }

            if ($filter['login']) {
                $this->db->like("a.user_username", $filter['login']);
            }
        }

        $Q = $this->db->select("a.*")
            ->group_by("a.user_id")->get_where($this->table . " a", array("user_status" => 1));

        if ($filter) {

            if ($filter['name']) {
                $this->db->like("a.user_name", $filter['name']);
            }

            if ($filter['email']) {
                $this->db->where("a.user_email", $filter['email']);
            }

            if ($filter['group']) {
                $this->db->like("b.group_name", $filter['group']);
            }

            if ($filter['login']) {
                $this->db->like("a.user_username", $filter['login']);
            }
        }

        $total = $this->db->select("a.*")
            ->group_by("a.user_id")->get_where($this->table . " a", array("user_status" => 1))
            ->num_rows();

        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;

        $data['total'] = $total;

        return $data;
    }

    function change_account_password($user_id, $old, $new)
    {
        // check old password
        $Q = $this->db->get_where($this->table, array("user_id" => $user_id));
        if ($Q->num_rows() > 0) {
            $user = $Q->row();
            if (!password_verify($old, $user->user_password)) {
                return false;
            } else {
                // the old password is correct . then change to new password.
                $new_password = $this->encrypt_password($new);
                $this->db->update($this->table, array("user_password" => $new_password['hash'], "user_salt" => $new_password['salt']), array("user_id" => $user_id));
            }
        }
        return true;
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db->update($this->table, $data, array("user_id" => $id));
        return $this->db->affected_rows();
    }

    function join_to_group($user, $groups)
    {
        // remove user from all groups
        $this->db->delete("users_groups", array("ug_user_id" => $user));
        // join to a new groups
        if ($groups and is_array($groups)) {
            foreach ($groups as $group) {
                $this->db->insert("users_groups", array("ug_user_id" => $user, "ug_group_id" => $group));
            }
        }
        return $this->db->affected_rows();
    }

    function check_username($str)
    {
        $Q = $this->db->get_where($this->table, array("user_username" => $str), 1);
        return ($Q->num_rows() > 0) ? true : false;
    }

    function check_email($str)
    {
        $Q = $this->db->get_where($this->table, array("user_email" => $str), 1);
        return ($Q->num_rows() > 0) ? true : false;
    }

    function get($id = 0)
    {
        $Q = $this->db->select("a.*")
            ->group_by("a.user_id")
            ->get_where($this->table . " a", array("a.user_id" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function delete($id)
    {
        $this->db->query("DELETE a,b FROM {$this->table} a
                          LEFT JOIN users_groups b ON b.ug_user_id=a.user_id
                          WHERE a.user_id=$id");
        return $this->db->affected_rows();
    }

    function list_active_users($ignore = array())
    {
        if (count($ignore) > 0) {
            $this->db->where_not_in("user_id", $ignore);
        }
        $Q = $this->db->get_where($this->table, array("user_status" => 1));
        return $Q->result();
    }

    ####################### login methods ######################

    /**
     * @return bool
     */
    function is_logged_in()
    {
        $logged_in = $this->session->userdata("logged_in");
        if ($logged_in && $logged_in['logged_in'] == true) {
            $user_data = $this->session->userdata("logged_in");
            return $this->get($user_data['id']);
        } else {

            // check if user checked before remember me and he still remembered by system.
            if ($token = $this->input->cookie(config_item("remember_me_token"))) {
                //check if his remember token is valid or not
                $valid = $this->db->select("user_rememberToken")->get_where($this->table, array("user_rememberToken" => $token), 1);

                if ($valid->num_rows()) {
                    $user = $this->get($valid->row()->user_id);
                    $this->set_loggedin_user($user, true);
                    return $user;
                }
            }
        }
        return false;
    }

    function check_username_password($username, $password)
    {
        $Q = $this->db->select("a.*")
            ->group_by("a.user_id")
            ->get_where($this->table . " a", array("a.user_username" => $username));

        $user = false;

        if ($Q->num_rows() > 0) {
            $user = $Q->row();
            if (!password_verify($password, $user->user_password)) {
                return false;
            }
        }

        return $user;
    }

    function check_email_password($email, $password, $admin = false)
    {
        if ($admin) {
            $this->db->where("b.group_inAdmin", 1);
        } else {
            $this->db->where("b.group_inAdmin", 0);
        }

        $Q = $this->db->select("a.*,group_concat(b.group_id) as groups_id,group_concat(b.group_name) as groups_name")
            ->join("users_groups", "users_groups.ug_user_id=a.user_id", "left")
            ->join("groups b", "users_groups.ug_group_id=b.group_id", "left")
            ->group_by("a.user_id")->get_where($this->table . " a", array("a.user_email" => $email));
        $user = false;
        if ($Q->num_rows() > 0) {
            $user = $Q->row();
            if (!password_verify($password, $user->user_password)) {
                return false;
            }
        }

        return $user;
    }

    /**
     * @param string $password
     * @param int $cost (optional)
     * @return array  [hash,salt]
     */
    function encrypt_password($password, $cost = 10)
    {
        // Create a random salt
        $salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        $options = array("salt" => $salt, "cost" => $cost);
        $hash = password_hash($password, PASSWORD_BCRYPT, $options);
        return array("hash" => $hash, "salt" => $salt);
    }

    /**
     * @param object $user user data
     * @param bool $remember if true it will set cookie to remember user session
     */
    function set_loggedin_user($user, $remember = false)
    {
        // destroy any old sessions

        $data['id'] = $user->user_id;
        $data['username'] = $user->user_username;
        $data['email'] = $user->user_email;
        $data['level'] = $user->user_level;
        $data['logged_in'] = true;

        if ($remember) {
            $data['remember_me'] = true;
            $token = random_string();
            $cookie = array('name' => config_item("remember_me_token"), 'value' => $token, 'expire' => 1209600, // Two weeks
                'domain' => site_url(), 'path' => '/');
            // set_cookie(config_item("remember_me_token"),$token,1209600,site_url(),"/");
            $this->input->set_cookie($cookie);

            // set token value in users table for this user
            $this->db->update($this->table, array("user_rememberToken" => $token), array("user_id" => $user->user_id));
        }

        $permissions = $this->list_user_permissions($user->user_id);

        $data['permissions'] = $permissions;

        $this->session->set_userdata(array("logged_in" => $data));
    }

    function get_user_permissions($user)
    {

    }

    function list_user_permissions($user)
    {
        $Q = $this->db->select("a.*,b.*,c.module_name")
            ->join("permissions b", "a.user_id=b.permission_user")
            ->join("modules c", "c.module_id=b.permission_module")
            ->get_where("{$this->table} a", array("a.user_id" => $user));

        return ($Q->num_rows() > 0) ? $Q->result() : false;
    }

    function logout()
    {
        if ($this->is_logged_in()) {
            $user = $this->session->userdata("logged_in");

            if ($user['remember_me'] == true) {
                delete_cookie(config_item("remember_me_token"));
                $this->db->update($this->table, array("user_rememberToken" => ""), array("user_id" => $user['id']));
            }
            // destroy user session
//            $this->session->unset_userdata();
            $this->session->sess_destroy();
        }
        return;
    }

    // Register Users

    function get_rgst($id = 0)
    {
        $Q = $this->db->get_where($this->table_rgst . " a", array("a.UR_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert_rgst($data)
    {
        $this->db->insert($this->table_rgst, $data);
        return $this->db->insert_id();
    }

    function update_rgst($id, $data)
    {
        $this->db->update($this->table_rgst, $data, array("UR_ID" => $id));
        return $this->db->affected_rows();
    }

    function check_email_rgst($str)
    {
        $Q = $this->db->get_where($this->table_rgst, array("UR_Email" => $str), 1);
        return ($Q->num_rows() > 0) ? true : false;
    }

    function check_mobile_rgst($str)
    {
        $Q = $this->db->get_where($this->table_rgst, array("UR_Phone" => $str), 1);
        return ($Q->num_rows() > 0) ? true : false;
    }

    function check_nationid($str)
    {
        $Q = $this->db->get_where($this->table_rgst, array("UR_NationalID" => $str), 1);
        return ($Q->num_rows() > 0) ? true : false;
    }

    function check_memberid($str)
    {
        $Q = $this->db->get_where($this->table_rgst, array("UR_MemberID" => $str), 1);
        return ($Q->num_rows() > 0) ? true : false;
    }

    function check_email_password_rgst($email, $password)
    {

        $Q = $this->db->get_where($this->table_rgst . " a", array("a.UR_Status" => 2, "a.UR_Email" => $email));
        $user = false;
        if ($Q->num_rows() > 0) {
            $user = $Q->row();
            if (!password_verify($password, $user->UR_Password)) {
                return false;
            }
        }

        return $user;
    }

    function check_phone_password_rgst($_login_array, $password)
    {
        $Q = $this->db->get_where($this->table_rgst . " a", $_login_array); // "a.UR_Status" => 2 ,
        $user = false;
        if ($Q->num_rows() > 0) {
            $user = $Q->row();
            if (!password_verify($password, $user->UR_Password)) { // activecode
                return false;
            }
        }

        return $user;
    }

    function check_phone_NationalID_rgst($_col_array)
    {
        $Q = $this->db->get_where($this->table_rgst, $_col_array);
        $user = false;
        if ($Q->num_rows() > 0) {
            $user = $Q->row();
        }

        return $user;
    }

    function set_loggedin_user_rgst($user, $remember = false)
    {
        // destroy any old sessions

        $data['mid'] = $user->UR_ID;
        $data['mfname'] = $user->UR_Name;
        $data['musername'] = $user->UR_UserName;
        $data['Phone'] = $user->UR_Phone;
        $data['mlevel'] = "member";
        $data['mlogged_in'] = true;

        if ($remember) {
            $data['remember_me'] = true;
            $token = random_string();
            $cookie = array('name' => config_item("remember_me_token"), 'value' => $token, 'expire' => 1209600, // Two weeks
                'domain' => site_url(), 'path' => '/');
            // set_cookie(config_item("remember_me_token"),$token,1209600,site_url(),"/");
            $this->input->set_cookie($cookie);

            // set token value in users table for this user
            $this->db->update($this->table_rgst, array("UR_RememberToken" => $token), array("UR_ID" => $user->UR_ID));
        }

        $this->session->set_userdata($data);
    }

    function get_all_rqsts($id, $offset = 0, $limit = 100)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $_arr["rqst_Deleted"] = 0;
        $_arr["rqst_URID"] = $id;
        $Q = $this->db
            ->select("a.*,r.*")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("requests a", $_arr);
        $total = $this->db
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("requests a", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

}
