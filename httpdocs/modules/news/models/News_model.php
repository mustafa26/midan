<?php

class News_Model extends CI_Model {

    private $table = "news";

    function __construct() {
        parent :: __construct();
    }

    function get_all($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("nws_From", "desc");
        $Q = $this->db->get_where($this->table, array("nws_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("nws_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_home($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("nws_From", "desc");
        $Q = $this->db->get_where($this->table, array("nws_Deleted" => 0, "nws_Active" => 1));
        $total = $this->db->get_where($this->table, array("nws_Deleted" => 0, "nws_Active" => 1))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_marq($offset = 0, $limit = 10) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("nws_From", "desc");
        $Q = $this->db->get_where($this->table, array("nws_Deleted" => 0, "nws_Active" => 1, "nws_Marq" => 1));
        $total = $this->db->get_where($this->table, array("nws_Deleted" => 0, "nws_Active" => 1, "nws_Marq" => 1))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get($id = 0) {
        $Q = $this->db->get_where($this->table, array("nws_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $this->db->update($this->table, $data, array("nws_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id, $data) {
        $this->db->update($this->table, $data, array("nws_ID" => $id));
        return $this->db->affected_rows();
    }

}
