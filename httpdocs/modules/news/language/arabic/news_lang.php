<?php

$lang['news_nws_title'] = "الأخبار";
$lang['news_nws_header'] = "الأخبار";
$lang['news_link'] = "الأخبار";
$lang['cats_name'] = "الاسم";

$lang['nws_Title'] = "عنوان الخبر";
$lang['nws_SubTitle'] = "عنوان الخبر الفرعي";

$lang['nws_Content'] = "الشروط";
$lang['nws_Gifts'] = "الجوائز";
$lang['nws_RaceNum'] = "عدد الأشواط";

$lang['nws_MetaDescr'] = "وصف محتوى الخبر";
$lang['nws_MetaKeywords'] = "الكلمات الدليلية";
$lang['nws_HeaderPhoto'] = "صورة بنر الخبر";
$lang['Tab_Settings'] = "إعدادات الخبر";
$lang['Name'] = "الاسم";
$lang['project_status'] = "الحالة";
$lang['nws_Marq'] = "عرض في الشريط الأخباري";
$lang['project_description'] = "وصف";
$lang['project_sub_tasks_num'] = "%s مهمة";
$lang['Lable_From'] = "تاريخ النشر"; 
$lang['task_duration'] = "الفترة الزمنية";
$lang['Table_Compitations'] = "جدول الأخبار";
$lang['Time_period'] = "المدة الزمنية";
$lang['Num_Races'] = "عدد الأشواط ";
$lang['Races'] = "أشواط";
$lang['More'] = "التفاصيل";
$lang['Gifts'] = "الجوائز";
$lang['Request_now'] = "تسجيل بالخبر";
 
//Messages
$lang['success_categories_created'] = "تم إنشاء التصنيف بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات التصنيف بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات التصنيف ";
$lang['success_project_deleted'] = "تم حذف العنصر بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف التصنيف , حاول مرة أخرى بعد تحديث الخبر";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_news'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_event'] = "تحديث بيانات الخبر";
$lang['log_delete_project'] = "حذف مشروع";
