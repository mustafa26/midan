<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("News_Model");
        $this->lang->load("news", $this->languages[$this->user_lang]);
        $this->model = new News_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("news_status");
    }

    function index($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("news_nws_title") . " - " . $this->site->site_name;
        $limit = 10;
        $result = $this->model->get_all($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_news");
        $this->data['content'] = "news/index";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("news/index"), $result['total'], $limit, 4);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        foreach ($this->languages as $key => $value) {
            $this->form_validation->set_rules("nws_Title[$key]", langline("nws_Title") . $value, "required|trim|max_length[255]");
            $this->form_validation->set_rules("nws_SubTitle[$key]", langline("nws_SubTitle") . $value, "required|trim|max_length[255]");
            $this->form_validation->set_rules("nws_Content[$key]", langline("nws_Content") . $value, "required");
        }

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add($fid = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add($fid);
            exit;
        }

        $this->data['title'] = langline("news_nws_title") . " - " . $this->site->site_name;
        $this->data['content'] = "news/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add($fid = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("nws_HeaderPhoto");
        if (!$this->errors) {
            $data['nws_Title'] = serialize(post("nws_Title"));
            $data['nws_SubTitle'] = serialize(post("nws_SubTitle"));
            $data['nws_Content'] = serialize(post("nws_Content"));
            $data['nws_MetaDescr'] = serialize(post("nws_MetaDescr"));
            $data['nws_MetaKeywords'] = serialize(post("nws_MetaKeywords"));
            $data['nws_Created_By'] = $this->user->user_id;
            $data['nws_Created'] = date("Y-m-d H:i:s");
            $data['nws_From'] = post("nws_From");
            $data['nws_Active'] = post("nws_Active") ? 1 : 0;
            $data['nws_Marq'] = post("nws_Marq") ? 1 : 0;

            if ($upload_data) {
                $data['nws_HeaderPhoto'] = $upload_data['file_name'];
            }
            if ($project_id = $this->model->insert($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("news"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("news/add"));
    }

    function edit($id = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }

        $this->data['title'] = langline("news_nws_title") . " - " . $this->site->site_name;
        $this->data['content'] = "news/edit";
        $this->data['event'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("nws_HeaderPhoto");
        if (!$this->errors) {
            $data['nws_Title'] = serialize(post("nws_Title"));
            $data['nws_SubTitle'] = serialize(post("nws_SubTitle"));
            $data['nws_Content'] = serialize(post("nws_Content"));
            $data['nws_MetaDescr'] = serialize(post("nws_MetaDescr"));
            $data['nws_MetaKeywords'] = serialize(post("nws_MetaKeywords"));
            $data['nws_From'] = post("nws_From");
            $data['nws_Active'] = post("nws_Active") ? 1 : 0;
            $data['nws_Marq'] = post("nws_Marq") ? 1 : 0;

            if ($upload_data) {
                $data['nws_HeaderPhoto'] = $upload_data['file_name'];
                if(is_file("uploads/news/".  post("nws_HeaderPhoto_old"))) unlink("uploads/news/".  post("nws_HeaderPhoto_old"));
                if(is_file("uploads/news/thumb/".  post("nws_HeaderPhoto_old"))) unlink("uploads/news/thumb/".  post("nws_HeaderPhoto_old"));
            }

            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_news");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("news"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("news/edit/$id"));
    }

    function delete($id = 0) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['nws_deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("news"));
    }

    private function do_upload($file_name = "userfile", $file_module = "news", $resize = true, $file_type = "", $allowed_types = 'gif|jpg|jpeg|png') {
        $this->load->library("upload");
        $this->load->library('image_lib');
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/" . $file_type;
            $config['allowed_types'] = $allowed_types;
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $_img_file = $this->upload->data();
                if ($resize) {
                    $config_thumb['image_library'] = 'gd2';
                    $config_thumb['source_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/$file_type" . $_img_file["file_name"];
                    $config_thumb['create_thumb'] = FALSE;
                    $config_thumb['maintain_ratio'] = TRUE;
                    $config_thumb['width'] = 250;
                    $config_thumb['height'] = 250;
                    $config_thumb['new_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/thumb";
                    $this->image_lib->initialize($config_thumb);
                    $this->image_lib->resize();
                }
                return $_img_file;
            }
        } else {
            return false;
        }
    }

}
