<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class News extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("News_Model", 'model');
    }

    public function index($offset = 0) {
        $this->data['title'] = $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_home($offset, $limit);
        $this->data['content'] = "news/index";
        $this->data['total'] = $result['total'];
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("news/index"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    public function details($id = 0) {
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "news/details";
        $this->data['event'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    } 

}
