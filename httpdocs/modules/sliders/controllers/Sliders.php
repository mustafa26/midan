<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pages extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Pages_Model", 'model'); 
    }

    public function index($id = 0) {
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "sites/index";
        $result = $this->gmodel->get_all_module($id,3);
        $this->data['media'] = $result['data'];
        $this->data['site'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    public function contact() {
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "sites/contact";
        $this->template->render("template", $this->data);
    }
    
    public function under() {
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "sites/none";
        $this->template->render("template", $this->data);
    } 
}
