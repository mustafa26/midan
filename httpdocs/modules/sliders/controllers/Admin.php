<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("Sliders_Model");
        $this->lang->load("sliders", $this->languages[$this->user_lang]);
        $this->model = new Sliders_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("sliders_status");
    }

    function index($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }

        $this->data['title'] = langline("sliders_slid_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_sliders");
        $this->data['content'] = "sliders/index";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("sliders"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("slid_Name[ar]", langline("slid_Name") . "(عربي)", "required|trim|max_length[255]");
        $this->form_validation->set_rules("slid_Name[en]", langline("slid_Name") . "(English)", "required|trim|max_length[255]");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add() {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add();
            exit;
        }

        $this->data['title'] = langline("sliders_slid_title") . " - " . $this->site->site_name;
        $this->data['content'] = "sliders/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add() {
        $this->_validate();
        $upload_data = $this->do_upload("slid_Img", "sliders");
        if (!$this->errors) {
            $data['slid_Name'] = serialize(post("slid_Name"));
            if ($upload_data) {
                $data['slid_Img'] = $upload_data['file_name'];
            }
            $data['slid_CreatedBy'] = $this->user->user_id;
            $data['slid_Created'] = date("Y-m-d H:i:s");
            $data['slid_Status'] = post("slid_status");

            if ($project_id = $this->model->insert($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("sliders"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("sliders?open=form"));
    }

    function edit($id = 0) {
        if (!check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }
        $cat = $this->model->get($id);
        $this->data['title'] = langline("sliders_slid_title") . " - " . $this->site->site_name;
        log_add_user_action($this->user->user_id, $id, "show", "log_show_sliders");
        $this->data['content'] = "sliders/edit";
        $this->data['data'] = $cat;
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("slid_Img", "sliders");
        if (!$this->errors) {
            $data['slid_Name'] = serialize(post("slid_Name"));

            if ($upload_data) {
                $data['slid_Img'] = $upload_data['file_name'];
                unlink("uploads/sliders/" . post("slid_Img_old"));
                unlink("uploads/sliders/" . post("slid_Img_old"));
            }
            $data['slid_Status'] = post("slid_status");
            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_sliders");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("sliders"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("sliders/edit/$id"));
    }

    function delete($id) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['slid_deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("sliders"));
    }

    function do_upload($file_name = "userfile", $file_module = "", $resize = false, $file_type = "", $allowed_types = "") {
        $this->load->library("upload");
        $this->load->library('image_lib');
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/" . $file_type;
            $config['allowed_types'] = $allowed_types != "" ? $allowed_types : 'gif|jpg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $config['max_size'] = '3000000';
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $_img_file = $this->upload->data();
                if ($resize) {
                    $config_thumb['image_library'] = 'gd2';
                    $config_thumb['source_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/$file_type" . $_img_file["file_name"];
                    $config_thumb['create_thumb'] = TRUE;
                    $config_thumb['maintain_ratio'] = TRUE;
                    $config_thumb['width'] = 250;
                    $config_thumb['height'] = 250;
                    $config_thumb['new_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/thumb";
                    $this->image_lib->initialize($config_thumb);
                    $this->image_lib->resize();
                }
                return $_img_file;
            }
        } else {
            return false;
        }
    }

}
