<?php
/*********************************************
 *  Project     : projects_management
 *  File        : users_lang.php
 *  Created at  : 5/24/15 - 2:19 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
$lang['projects_page_title']           = "Projects";
$lang['projects_page_header']          = "Projects";
$lang['projects_link']                 = "Projects";
$lang['project_name']                  = "Project Name";
$lang['project_manager']               = "Project Manager";
$lang['project_groups']                = "Project Groups";
$lang['project_duration']              = "Project Duration";
$lang['project_status']                = "Status";
$lang['project_description']           = "Description";
$lang['project_add_task_btn']          = "Add Task";
$lang['project_add_task']              = "Add New Task";
$lang['project_sub_tasks']             = "Tasks";
$lang['project_sub_tasks_num']         = "%s Task";
$lang['project_attaches']              = "Attached files";
$lang['project_show_task']              ="Task Details";
$lang['status_all_percent']            = "Full Percent";
$lang['status_remaining_percent']      = "Remaining Percent";
$lang['status_project_percent']        = "Manager Percent";
$lang['status_task_percent']           = "Work Percent";
$lang['text_no_groups_selected']       = "No Groups Selected";


//Messages

$lang['success_project_created']        = "Project Created Successfully";
$lang['success_project_updated']        = "Project Updated Successfully";
$lang['success_project_deleted']        = "Project Deleted Successfully";
$lang['error_project_not_deleted']      = "Error: project Not Deleted Try again after refresh";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated']         = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project']   = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project']    = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project']     = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task']        = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed']    = "Project %s  status changed to Delayed because Project tasks not completed in time line";

$lang['log_show_projects']      = "Show Projects";
$lang['log_add_project']      = "Add a new Project";
$lang['log_edit_project']      = "Update Project";
$lang['log_delete_project']      = "Delete Project";