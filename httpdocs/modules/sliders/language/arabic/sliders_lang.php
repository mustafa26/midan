<?php

$lang['sliders_slider_title'] = "الصفحات";
$lang['sliders_slider_header'] = "الصفحات";
$lang['sliders_link'] = "الصفحات";
$lang['sliders_Name'] = "الاسم";
$lang['slider_Url'] = "الرابط";
$lang['pages_link'] = "صور الشريط المتحرك";
$lang['slider_Title'] = "عنوان الصفحة";
$lang['slider_SubTitle'] = "عنوان الصفحة الفرعي";
$lang['slider_Content'] = "محتوى الصفحة";    
$lang['slider_MetaDescr'] = "وصف محتوى الصفحة";
$lang['slider_MetaKeywords'] = "الكلمات الدليلية";
$lang['slider_HeaderPhoto'] = "الصورة";
$lang['Tab_Settings'] = "إعدادات الصفحة";
$lang['Name'] = "الاسم";
$lang['project_status'] = "الحالة";
$lang['project_description'] = "وصف";
$lang['project_sub_tasks_num'] = "%s مهمة";
$lang['link_multi_media'] = "وسائط";

//Messages
$lang['success_categories_created'] = "تم إنشاء التصنيف بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات التصنيف بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات التصنيف ";
$lang['success_project_deleted'] = "تم حذف التصنيف بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف التصنيف , حاول مرة أخرى بعد تحديث الصفحة";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_sites'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_site'] = "تحديث بيانات الصفحة";
$lang['log_delete_project'] = "حذف مشروع";
