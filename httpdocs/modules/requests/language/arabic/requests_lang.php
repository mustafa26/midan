<?php

$lang['events_event_title'] = "السباقات";
$lang['event_title'] = "السباق";
$lang['requests_request_title'] = "نماذج الطلبات";
$lang['requests_request_header'] = "نماذج الطلبات";
$lang['requests_link'] = "الطلبات";
$lang['cats_name'] = "اسم الطلب ";
$lang['Sort'] = "الترتيب";
$lang['All'] = "الكل";

$lang['Name'] = "الاسم";
$lang['project_status'] = "الحالة";
$lang['project_sub_tasks_num'] = "%s مهمة";
$lang['Lable_From'] = "من";
$lang['Lable_To'] = "الى";
$lang['task_duration'] = "الفترة الزمنية";
$lang['Time_period'] = "المدة الزمنية";
$lang['Num_Races'] = "عدد الأشواط ";
$lang['Races'] = "أشواط";
$lang['Race'] = "الشوط";
$lang['More'] = "التفاصيل";
$lang['Date'] = "التاريخ";
$lang['Date_Race'] = "تاريخ السباق";

// Form Request
$lang['Request'] = "تسجيل الهجن";
$lang['rqst_Info'] = "معلومات";
$lang['rqst_EzabaInfo'] = "معلومات العزبة";
$lang['rqst_Camel'] = "المطية";
$lang['rqst_Owner'] = "المالك";
$lang['rqst_Name'] = "اسم المطية";
$lang['rqst_NumCard'] = "رقم الشريحة";
$lang['rqst_NumCard_'] = "برجاء كتابة الرقم الخاص بالشريحة للمطية";
$lang['rqst_NumRace'] = "رقم الشوط";
$lang['rqst_Image'] = "صورة للمطية";
$lang['rqst_PermitImage'] = "صورة التصريح (برنت) من الاتحاد المعني";
$lang['AgeGroup'] = "الفئة";
$lang['rqst_AgeGroup'] = "الفئة العمرية ";
$lang['rqst_AgeGroup_2'] = "الحجائج (سنتان)";
$lang['rqst_AgeGroup_3'] = "اللقايا (ثلاث سنوات)";
$lang['rqst_AgeGroup_4'] = "اليداع أو الجذع (أربع سنوات)";
$lang['rqst_AgeGroup_5'] = "الثنايا (خمس سنوات)";
$lang['rqst_AgeGroup_6'] = "الرباع والحايل والحول والزمول  (ست سنوات فأكثر)";
$lang['rqst_Gender'] = "جنس المطية"; 
$lang['rqst_Gender_01'] = "ذكر";
$lang['rqst_Gender_02'] = "أنثى"; 
$lang['rqst_Gender_1'] = "أبكار";
$lang['rqst_Gender_2'] = "جعدان";
$lang['rqst_Gender_3'] = "حول";
$lang['rqst_Gender_4'] = "زمول";
$lang['rqst_Gender_01'] = "ذكر";
$lang['rqst_Gender_02'] = "أنثى";

$lang['rqst_NameCamel'] = "إسم الناقة";
$lang['rqst_OwnerName'] = "اسم المالك";
$lang['rqst_OwnerName_'] = "اسم صاحب المطية";
$lang['rqst_memberNumber'] = "رقم المشارك";
$lang['Request_now'] = "تسجيل بالسباق";

$lang['Day'] = "يوم";
$lang['Month'] = "شهر";
$lang['Year'] = "سنة";

// Country
$lang['oman'] = "سلطنة عُمان"; 
$lang['uea'] = "الإمارات العربية المتحدة";  
$lang['ksa'] = "المملكة العربية السعودية"; 
$lang['ksa'] = "ممكلة البحرين"; 
$lang['qatar'] = "قطر"; 
$lang['kiwat'] = "الكويت"; 

$lang['rqst_Created'] = "تاريخ الإضافة";
$lang['rqst_OwnerBD'] = "تاريخ الميلاد";
$lang['rqst_OwnerBD_'] = "تاريخ ميلاد صاحب المطية";  
$lang['rqst_OwnerID'] = "البطاقة الشخصية(الرقم المدني)";
$lang['rqst_OwnerID_'] = "رقم البطاقة الشخصية لصاحب المطية";
$lang['rqst_OwnerIDExp'] = "تاريخ الإنتهاء";
$lang['rqst_OwnerIDExp_'] = "تاريخ انتهاء البطاقة الشخصية";
$lang['rqst_OwnerNationlaty'] = "الجنسية";
$lang['rqst_OwnerIDImage'] = "صورة من البطاقة الشخصية";
$lang['UR_MemberIDImag'] = "صورة بطاقة المشارك"; 
$lang['rqst_OwnerImage'] = "صورة شخصية للمالك";
$lang['rqst_OwnerPhone'] = "رقم الهاتف ";
$lang['rqst_OwnerPhone_'] = "رقم الهاتف الخلوي";
$lang['rqst_OwnerEmail'] = "البريد الإلكتروني";
$lang['rqst_BankInfo'] = "المعلومات البنكية";
$lang['rqst_Bank'] = "اسم البنك";
$lang['rqst_OC'] = "رقم الحساب";
$lang['rqst_OC_'] = "رقم حساب البنك";
$lang['rqst_IBAN'] = "رقم الحوالة الدولية IBAN ";
$lang['rqst_ImageOC'] = "ارفاق صورة لشهادة الحساب";
$lang['rqst_Status'] = "الحالة";
$lang['rqst_Created_By'] = "صاحب الاضافة";
$lang['rqst_Select_File'] = "أختر ملف";

$lang['statusNew'] = "جديد";
$lang['statusAccept'] = "مقبول";
$lang['statusNonAccept'] = "مرفوض"; 
$lang['View_Orders'] = "أستعراض طلبات الأشتراك";
$lang['rqst_Send_SMS'] = "ارسال SMS";


// Country
$lang['oman'] = "سلطنة عُمان"; 
$lang['uea'] = "الإمارات العربية المتحدة";  
$lang['ksa'] = "المملكة العربية السعودية"; 
$lang['ksa'] = "ممكلة البحرين"; 
$lang['qatar'] = "قطر"; 
$lang['kiwat'] = "الكويت";
// SMS Templates
$lang['SMS_T1'] = "عدم إرفاق بطاقة مشارك";
$lang['SMS_T2'] = "عدم ارفاق تصريح الأتحاد (برنت)";
$lang['SMS_T3'] = "تصريح الأتحاد منتهي";
$lang['SMS_T4'] = "البيانات المسجلة لاتتطابق مع المرفقات";

//Messages
$lang['error_Maxfile_NotAllow'] = "<b>خطأ :</b>  حجم الصورة تجاوز الحد الأقصى وهو 4MB";
$lang['error_Maxfile_Allow'] = "<b>ملاحظة :</b>  يجب أن لا يتجاوز حجم الصورة عن  4MB.";
$lang['error_Maxfile_Upload1'] = "<b>ملاحظة :</b>  حجم الصورة المضافة هو :  ";
$lang['error_Maxfile_Upload2'] = " يرجى تصغير حجم الصورة وإعادة رفعها. ";

$lang['success_categories_created'] = "تم إنشاء الطلب  بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات الطلب  بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات الطلب  ";
$lang['success_project_deleted'] = "تم حذف العنصر بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف الطلب  , حاول مرة أخرى بعد تحديث السباق";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_requests'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_request'] = "تحديث بيانات السباق";
$lang['log_delete_project'] = "حذف مشروع";

$lang['NumRace_1'] = "الأول";
$lang['NumRace_2'] = "الثاني";
$lang['NumRace_3'] = "الثالث";
$lang['NumRace_4'] = "الرابع";
$lang['NumRace_5'] = "الخامس";
$lang['NumRace_6'] = "السادس";
$lang['NumRace_7'] = "السابع";
$lang['NumRace_8'] = "الثامن";
$lang['NumRace_9'] = "التاسع"; 
$lang['NumRace_10'] = "العاشر"; 
$lang['NumRace_11'] = "الحادي عشر"; 
$lang['NumRace_12'] = "الثاني عشر"; 
$lang['NumRace_13'] = "الثالث عشر"; 
$lang['NumRace_14'] = "الرابع عشر"; 
$lang['NumRace_15'] = "الخامس عشر"; 
$lang['NumRace_16'] = "السادس عشر"; 
$lang['NumRace_17'] = "السابع عشر"; 
$lang['NumRace_18'] = "الثامن عشر"; 
$lang['NumRace_19'] = "التاسع عشر";
$lang['NumRace_20'] = "العشرين";
