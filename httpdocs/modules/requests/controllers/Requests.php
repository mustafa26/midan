<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Requests extends Public_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Requests_Model", 'model');
        $this->load->model("races/Races_Model");
        $this->lang->load("requests", $this->languages[$this->user_lang]);
        $this->load->model("events/Events_Model");
        if (!isset($_SESSION['mlogged_in'])) redirect(site_url("users/login"));
    }

    public function index()
    {
        $this->data['title'] = $this->site->site_name;
        $result = $this->model->get_all_user_rqsts($_SESSION["mid"]);
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['content'] = "requests/index";
        $this->template->render("template", $this->data);
    }

    public function printreq()
    {
        $this->data['title'] = $this->site->site_name;
        $result = $this->model->get_all_user_rqsts($_SESSION["mid"]);
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        view("printreq", $this->data);
    }


    public function tracks($id = 0)
    {
        $this->data['title'] = $this->site->site_name;
        $this->data['event'] = $this->Events_Model->get($id);
        $this->data['races'] = $this->Races_Model->get_all($id, 0, 40);
        $this->data['content'] = "requests/tracks";
        $this->template->render("template", $this->data);
    }


    public function event($id = 0)
    {
        !$_SESSION['mlogged_in'] ? redirect(site_url("users/login")) : "";
        if (post()) {
            $this->_do_add($id);
            exit;
        }

        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "requests/request";
        $this->data['event'] = $this->Events_Model->get($id);
        $this->data['races'] = $this->Races_Model->get_all($id, 0, 40);
        $this->template->render("template", $this->data);
    }


    private function _validate($_edit = false)
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("rqst_Name", langline("rqst_Name"), "required|trim|max_length[255]");
        $this->form_validation->set_rules("rqst_NumCard", langline("rqst_NumCard"), "required|trim|min_length[15]|max_length[15]");
        $this->form_validation->set_rules("rqst_NumRace", langline("rqst_NumRace"), "required");

        //$this->form_validation->set_rules('rqst_Image', 'lang:rqst_Image', 'required');
        //$this->form_validation->set_rules('rqst_PermitImage', 'lang:rqst_PermitImage', 'required');
        if (empty($_FILES['rqst_PermitImage']['name']) && !$_edit) {
            $this->form_validation->set_rules('rqst_PermitImage', 'lang:rqst_PermitImage', 'required');
        } elseif ($_FILES['rqst_PermitImage']['size'] > 4248000) {
            $this->errors .= "<p>" . langline("error_Maxfile_NotAllow") . "</p>"
                . "<p>" . langline("error_Maxfile_Upload1") . round(($_FILES['rqst_PermitImage']['size'] / 1000000), 1) . "MB، " . langline("error_Maxfile_Upload2") . "</p>";
        }
        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    private function _do_add($id = 0)
    {
        $this->_validate();
        if (!$this->errors) {
            $data['rqst_URID'] = $_SESSION['mid'];
            $data['rqst_FID'] = $id;
            $data['rqst_Type'] = 'Request';
            $data['rqst_Name'] = post("rqst_Name");
            $data['rqst_NumCard'] = post("rqst_NumCard");
            $data['rqst_NumRace'] = post("rqst_NumRace");
            //$data['rqst_Image'] = post("rqst_Image");
            //$data['rqst_PermitImage'] = post("rqst_PermitImage");
            $data['rqst_AgeGroup'] = post("rqst_AgeGroup");
            $data['rqst_Gender'] = post("rqst_Gender");
            //$_DOB = "";
            //$_DOB = post("rqst_OwnerBD_y") . '-' . post("rqst_OwnerBD_m") . '-' . post("rqst_OwnerBD_d"); //0000-00-00
            //$data['rqst_OwnerBD'] = $_DOB; //post("rqst_OwnerBD");
            //$data['rqst_OwnerID'] = post("rqst_OwnerID");
            //$data['rqst_OwnerIDImage'] = post("rqst_OwnerIDImage");
            //$_IDExp = "";
            //$_IDExp = post("rqst_OwnerIDExp_y") . '-' . post("rqst_OwnerIDExp_m") . '-' . post("rqst_OwnerIDExp_d"); //0000-00-00
            //$data['rqst_OwnerIDExp'] = $_IDExp; //post("rqst_OwnerIDExp");
            //$data['rqst_OwnerNationlaty'] = post("rqst_OwnerNationlaty");
            //$data['rqst_OwnerImage'] = post("rqst_OwnerImage");
            //$data['rqst_OwnerPhone'] = post("rqst_OwnerPhone");
            //$data['rqst_OwnerEmail'] = post("rqst_OwnerEmail");
            //$data['rqst_Bank'] = post("rqst_Bank");
            //$data['rqst_OC'] = post("rqst_OC");
            //$data['rqst_IBAN'] = post("rqst_IBAN");
            //$data['rqst_ImageOC'] = post("rqst_ImageOC");
            $data['rqst_Status'] = 1;
            $data['rqst_eventDate'] = post("eventDate");
            $data['rqst_Created'] = date("Y-m-d H:i:s");
            /*
                        $upload_data = $this->do_upload("rqst_Image");
                        $data['rqst_Image'] = $upload_data ? $upload_data['file_name'] : '';*/
            $upload_data = $this->do_upload("rqst_PermitImage");
            $data['rqst_PermitImage'] = $upload_data ? $upload_data['file_name'] : '';
            //$upload_data = $this->do_upload("rqst_OwnerIDImage");
            //$data['rqst_OwnerIDImage'] = $upload_data ? $upload_data['file_name'] : '';
            //$upload_data = $this->do_upload("rqst_OwnerImage");
            //$data['rqst_OwnerImage'] = $upload_data ? $upload_data['file_name'] : '';
            //$upload_data = $this->do_upload("rqst_ImageOC");
            //$data['rqst_ImageOC'] = $upload_data ? $upload_data['file_name'] : '';
            if ($rqst_id = $this->model->insert($data)) {
                // Send SMS Config
                $numbers = $_SESSION['Phone'];
                $user = "USER";
                $password = "*********";
                $sender = "USER";
                // %20
                $_mess = "تم تسجيل المطية مبدئيا برقم الشريحة:";
                $_mess .= post("rqst_NumCard")."%20";
                $_mess .= "وسيتم مراجعة الطلب.";
                $mess = str_replace(" ", "%20", $_mess);
                //$send = "http://sms4world.net/smspro/sendsms.php?user=" . $user . "&password=" . $password . "&numbers=" . $numbers . "&sender=" . $sender . "&message=" . $mess . "&lang=ar";
                //$return_status = file_get_contents($send);
                //set action in user log  
                $this->set_message(langline("success_categories_created"), "success");
                redirect(site_url("users/requests"));
            }
        }

        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(site_url("requests/event/" . $id));
    }


    public function edit($id = 0)
    {
        !$_SESSION['mlogged_in'] ? redirect(site_url("users/login")) : "";
        if (post()) {
            $this->_do_edit($id);
            exit;
        }

        $this->data['title'] = $this->site->site_name;
        $events = $this->model->get($id);
        $this->data['event'] = $events;
        //$this->data['event'] = $this->Events_Model->get($id);
        $this->data['races'] = $this->Races_Model->get_all($events->rqst_FID, 0, 40);
        $this->data['content'] = "requests/edit";
        $this->template->render("template", $this->data);
    }

    private function _do_edit($id = 0)
    {
        $this->_validate(true);
        if (!$this->errors) {
            $data['rqst_Name'] = post("rqst_Name");
            $data['rqst_NumCard'] = post("rqst_NumCard");
            $data['rqst_NumRace'] = post("rqst_NumRace");
            $data['rqst_Status'] = 1;

            if ($_FILES['rqst_PermitImage']['name']) {
                $upload_data = $this->do_upload("rqst_PermitImage");
                $data['rqst_PermitImage'] = $upload_data ? $upload_data['file_name'] : '';
                post("old_PermitImage")?unlink("uploads/requests/".post("old_PermitImage")):0;
            }

            if ($rqst_id = $this->model->update($id, $data)) {
                //set action in user log
                $this->set_message(langline("success_project_updated"), "success");
                redirect(site_url("requests"));
            } else {
                $this->errors .= "<p>" . langline("success_project_updated_error") . "</p>";
            }
        }

        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(site_url("requests/edit/" . $id));
    }

    function delete($id)
    {
        //$this->model->update($id, array("rqst_Deleted" => 1));
        $events = $this->model->get($id);
        $this->model->delete($id);
        $this->set_message(langline("success_project_deleted"), "success");
        redirect(site_url("requests"));
    }

    public function result($id = 0)
    {
        !$_SESSION['mlogged_in'] ? redirect(site_url("users/login")) : "";
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "requests/result";
        $this->data['event'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    public function getcard($cardid = 0)
    {
        $_res = $this->model->getbycard($cardid);
        if ($_res && $_res->rqst_NumCard)
            echo('
        <div class="alert alert-warning">
            <strong>تنبية!</strong> رقم الشريحة مسجل من قبل وبالتالي لايمكن قبول الطلب.
        </div>');
    }


    public function ezab($fid = 0)
    {
        !$_SESSION['mlogged_in'] ? redirect(site_url("users/login")) : "";
        if (post()) {
            $this->_do_add_ezab();
            exit;
        }
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "requests/request_ezab";
        $this->template->render("template", $this->data);
    }

    private function _validate_ezab()
    {
        $this->load->library("form_validation");
        //$this->form_validation->set_rules("rqst_Name", langline("rqst_Name") , "required|trim|max_length[255]");
        //$this->form_validation->set_rules("rqst_Email", langline("rqst_Email") , "required|trim|max_length[255]");
        //$this->form_validation->set_rules("rqst_Phone", langline("rqst_Phone") , "required|trim|max_length[255]");
        $this->form_validation->set_rules("rqst_EzbaName", langline("rqst_EzbaName"), "required|trim|max_length[255]");
        $this->form_validation->set_rules("rqst_Location", langline("rqst_Location"), "required|trim|max_length[255]");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    private function _do_add_ezab()
    {
        $this->_validate_ezab();
        if (!$this->errors) {
            $data['rqst_URID'] = $_SESSION['mid'];
            //$data['rqst_Name'] = post("rqst_Name");
            //$data['rqst_Email'] = post("rqst_Email");
            //$data['rqst_Phone'] = post("rqst_Phone");
            $data['rqst_EzbaName'] = post("rqst_EzbaName");
            $data['rqst_Location'] = post("rqst_Location");
            $data['rqst_Notes'] = post("rqst_Notes");
            $data['rqst_Created'] = date("Y-m-d H:i:s");

            if ($rqst_id = $this->model->insert_ezab($data)) {
                //set action in user log
                $this->set_message(langline("success_categories_created"), "success");
                redirect(site_url("requests/result"));
            }
        }

        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(site_url("requests/ezab/"));
    }

    private function do_upload($file_name = "userfile")
    {
        $this->load->library("upload");
        $this->load->library('image_lib');
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/requests";
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $config['max_size'] = '4400000'; // Max upload size  5 MB
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $image_data =   $this->upload->data();
                $configer =  array(
                    'image_library'   => 'gd2',
                    'source_image'    =>  $image_data['full_path'],
                    'maintain_ratio'  =>  TRUE,
                    'width'           =>  1000,
                    'height'          =>  1000,
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
                return $image_data;
            }
        } else {
            return false;
        }
    }

}
