<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("Requests_Model");
        $this->load->model("events/Events_Model");
        $this->load->model("races/Races_Model");
        $this->load->helper("requests");
        $this->lang->load("requests", $this->languages[$this->user_lang]);
        $this->model = new Requests_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("requests_status");
    }

    function index($fid = 0, $offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($fid, $offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['content'] = "requests/index";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/index/" . $fid), $result['total'], $limit, 5);
        $this->template->render("template", $this->data);
    }

    function allrqsts($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_rqsts($offset, $limit);
        $resall = $this->Events_Model->get_all(0, 200);
        $event = get("eventid") != 0 ? $this->Events_Model->get(get("eventid")) : 0;
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['content'] = "requests/allrqsts";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['res_all'] = $resall['data'];
        $this->data['res_total'] = $resall['total'];
        $this->data['event'] = $event;
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/allrqsts/"), $result['total'], $limit, 4);
        $this->template->render("template", $this->data);
    }

    function allrequests($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_requests($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['content'] = "requests/index_requests";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/allrequests/"), $result['total'], $limit, 4);
        $this->template->render("template", $this->data);
    }

    function lastlist($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 30;
        $result = $this->model->get_all_lastlist($offset, $limit);
        $resall = $this->Events_Model->get_all_home(0, 200);
        $event = get("eventid") != 0 ? $this->Events_Model->get(get("eventid")) : 0;
        $race = get("rqst_NumRace") != 0 ? $this->Races_Model->get(get("rqst_NumRace")) : 0;
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['content'] = "requests/lastlist";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['res_all'] = $resall['data'];
        $this->data['res_total'] = $resall['total'];
        $this->data['event'] = $event;
        $this->data['race'] = $race;
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $_suffix_url = (get("Race") || get("eventid")) ? "?" : "";
        get("Race") ? $_suffix_url.="Race=" . get("Race") : "";
        (get("Race") && get("eventid")) ? $_suffix_url.="&" : "";
        get("eventid") ? $_suffix_url.="eventid=" . get("eventid") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/lastlist/"), $result['total'], $limit, 4, $_suffix_url); //
        $this->template->render("template", $this->data);
    }

    function lastlistcon($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }

        if (post("add")) {
            $this->_do_Addlastlistcon();
            exit;
        } elseif (post("edit") && is_numeric(post("id"))) {
            $this->_do_Updatelastlistcon(post("id"));
            exit;
        }

        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 30;
        $result = $this->model->get_all_lastlist($offset, $limit);
        $resall = $this->Events_Model->get_all_home(0, 200);
        $event = get("eventid") != 0 ? $this->Events_Model->get(get("eventid")) : 0;
        $race = get("rqst_NumRace") != 0 ? $this->Races_Model->get(get("rqst_NumRace")) : 0;
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['res_all'] = $resall['data'];
        $this->data['res_total'] = $resall['total'];
        $this->data['event'] = $event;
        $this->data['race'] = $race;
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $_suffix_url = (get("Race") || get("eventid")) ? "?" : "";
        get("Race") ? $_suffix_url.="Race=" . get("Race") : "";
        (get("Race") && get("eventid")) ? $_suffix_url.="&" : "";
        get("eventid") ? $_suffix_url.="eventid=" . get("eventid") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/lastlistcon/"), $result['total'], $limit, 4, $_suffix_url); //
        $this->data['content'] = "requests/lastlistcon";
        $this->template->render("template", $this->data);
    }

    function toexcel($offset = 0) {
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=list_".random_string().".xls");
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 200;
        $result = $this->model->get_all_lastlist($offset, $limit);
        $resall = $this->Events_Model->get_all(0, 200);
        $event = get("eventid") != 0 ? $this->Events_Model->get(get("eventid")) : 0;
        $race = get("rqst_NumRace") != 0 ? $this->Races_Model->get(get("rqst_NumRace")) : 0;
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['content'] = "requests/lastlist";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['res_all'] = $resall['data'];
        $this->data['res_total'] = $resall['total'];
        $this->data['event'] = $event;
        $this->data['race'] = $race;
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $_suffix_url = (get("Race") || get("eventid")) ? "?" : "";
        get("Race") ? $_suffix_url.="Race=" . get("Race") : "";
        (get("Race") && get("eventid")) ? $_suffix_url.="&" : "";
        get("eventid") ? $_suffix_url.="eventid=" . get("eventid") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/lastlist/"), $result['total'], $limit, 4, $_suffix_url); //
        //$this->template->render("template", $this->data); 
        $this->template->renderview("requests/printlist", $this->data);
    }

    function printlist($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 200;
        $result = $this->model->get_all_lastlist($offset, $limit);
        $resall = $this->Events_Model->get_all(0, 200);
        $event = get("eventid") != 0 ? $this->Events_Model->get(get("eventid")) : 0;
        $race = get("rqst_NumRace") != 0 ? $this->Races_Model->get(get("rqst_NumRace")) : 0;
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['content'] = "requests/lastlist";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['res_all'] = $resall['data'];
        $this->data['res_total'] = $resall['total'];
        $this->data['event'] = $event;
        $this->data['race'] = $race;
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $_suffix_url = (get("Race") || get("eventid")) ? "?" : "";
        get("Race") ? $_suffix_url.="Race=" . get("Race") : "";
        (get("Race") && get("eventid")) ? $_suffix_url.="&" : "";
        get("eventid") ? $_suffix_url.="eventid=" . get("eventid") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/lastlist/"), $result['total'], $limit, 4, $_suffix_url); //
        //$this->template->render("template", $this->data); 
        $this->template->renderview("requests/printlist", $this->data);
    }

    function ezab($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_ezab();
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['content'] = "requests/ezab";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/ezab/"), $result['total'], $limit, 4);
        $this->template->render("template", $this->data);
    }

    function events() {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $result = $this->model->get_all();
        $this->data['content'] = "requests/index";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['pagination'] = "";
        $this->template->render("template", $this->data);
    }

    function view($id = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }

        if (post()) {
            $this->_do_update($id);
            exit;
        }

        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $this->data['content'] = "requests/view";
        $_row = $this->model->get($id);
        $this->data['row'] = $_row;
        $resall = $this->Events_Model->get_all(0, 200);
        $this->data['races'] = $this->Races_Model->get_all($_row->rqst_FID, 0, 40);
        $this->data['res_all'] = $resall['data'];
        $this->data['res_total'] = $resall['total'];
        $this->template->render("template", $this->data);
    }

    function ezabview($id = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }

        if (post()) {
            $this->ezab_do_update($id);
            exit;
        }

        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $this->data['content'] = "requests/ezabview";
        $this->data['row'] = $this->model->get_ezab($id);
        $this->template->render("template", $this->data);
    }

    private function _do_Addlastlistcon() {

        if (!$this->errors) {
            //$data['Lsort'] = post("Lsort");
            //$data['winner_sort'] = post("wsrt");
            $data['Cname'] = post("Cname");
            $data['Oname'] = post("Oname");
            $data['CID'] = post("CID");
            $data['MID'] = post("MID");
            //$data['RaceNum'] = post("RaceNum");
            $data['event_ID'] = post("event_ID");
            $data['RaceNum'] = post("rqst_NumRace");
            //$data['event_Date'] = post("event_Date");
            //$data['CGroup'] = post("CGroup");
            $data['DateCreated'] = date("Y-m-d H:i:s");

            if ($this->model->insertlastlistcon($data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, 0, "add", "log_edit_requests");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("requests/lastlistcon"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("requests/lastlistcon"));
    }

    private function _do_Updatelastlistcon($id = 0) {
        if (!$this->errors) {
            $data['winner_sort'] = post("wsrt");
            $data['Cname'] = post("Cname");
            $data['Oname'] = post("Oname");
            $data['CID'] = post("CID");
            $data['MID'] = post("MID");
            $data['event_ID'] = post("event_ID");
            $data['RaceNum'] = post("rqst_NumRace");
            //$data['event_Date'] = post("event_Date");
            $data['CGroup'] = post("CGroup"); 
            $data['RaceTime'] = post("RaceTime");
            $data['Car'] = post("Car");

            if ($this->model->updatelastlistcon($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_requests");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("requests/lastlistcon$_qstr"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("requests/lastlistcon$_qstr"));
    }

    private function _do_update($id = 0) {
        if (!$this->errors) {
            $data['rqst_Confirm_By'] = $this->user->user_id;
            $data['rqst_Status'] = post("rqst_Status");
            $data['rqst_FID'] = post("rqst_FID");
            $data['rqst_Name'] = post("rqst_Name");
            $data['rqst_NumCard'] = post("rqst_NumCard");
            //$data['rqst_Gender'] = post("rqst_Gender");
            $data['rqst_AgeGroup'] = post("rqst_AgeGroup");
            $data['rqst_NumRace'] = post("rqst_NumRace");
            if ((post("rqst_Status") == 2 || post("rqst_Status") == 3) && post("send_sms") == 1) { // && post("rqst_SentSMS") == 0
                // Send SMS Config 
                $numbers = post("UR_Phone");
                $user = "USER";
                $password = "*********";
                $sender = "USER";
                if (post("rqst_Status") == 2) {
                    $mess = "مرحبا" . "%20" . "بك،" . "%20" . "تم" . "%20" . "قبول" . "%20";
                } elseif (post("rqst_Status") == 3) {
                    $mess = "مرحبا" . "%20" . "بك،" . "%20" . "تم" . "%20" . "رفض" . "%20";
                    $mess .="الطلب" . "%20". "بسبب". "%20";
                    $mess .= str_replace(" ", "%20", post("messagesms")). "%20";
                }

                $mess .="المطية" . "%20" . "برقم" . "%20" . "شريحة:";
                $mess .= post("rqst_NumCard");
                //$send = "http://sms4world.net/smspro/sendsms.php?user=USER&password=*********&numbers=".$numbers."&sender=Durar&message=".$mess."&lang=ar";
                //$send = "http://sms4world.net/smspro/sendsms.php?user=" . $user . "&password=" . $password . "&numbers=" . $numbers . "&sender=" . $sender . "&message=" . $mess . "&lang=ar";
                //$return_status = file_get_contents($send);
                if ($return_status)
                    $data['rqst_SentSMS'] = post("rqst_SentSMS") + 1;
            }

            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_requests");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("requests/allrequests"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("requests/view/$id"));
    }

    private function ezab_do_update($id = 0) {
        if (!$this->errors) {
            $data['rqst_Confirm_By'] = $this->user->user_id;
            $data['rqst_Status'] = post("rqst_Status");
            if ($this->model->update_ezab($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_requests");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("requests/ezab"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("requests/ezabview/$id"));
    }

    function delete($fid = 0, $id = 0) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['rqst_Deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("requests/index/$fid"));
    }

    function deletectrl($id = 0) {
        if (!check_user_permission(current_module(), "delete")) {
            redirect(admin_url("permissions/denied"));
        }

        if ($this->model->deletectrl($id)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("requests/lastlistcon"));
    }

    function ezabdelete($id = 0) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['rqst_Deleted'] = 1;
        $this->model->delete_ezab($id, $data);
        if ($this->model->delete_ezab($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("requests/ezab"));
    }

    public function getcard($cardid = 0)
    {
        $_res = $this->model->getbycard($cardid);
        if ($_res && $_res->rqst_NumCard)
            echo('
        <div class="alert alert-warning">
            <strong>تنبية!</strong> رقم الشريحة مسجل من قبل وبالتالي لايمكن قبول الطلب.
        </div>');
    }

}
