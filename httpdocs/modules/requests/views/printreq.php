<html>
<head>
    <title>Print Request</title>

    <style type="text/css">
        .myOtherTable {
            background-color: #FFFFE0;
            border-collapse: collapse;
            color: #000;
            font-size: 18px;
            direction: rtl;

        }

        .myOtherTable th {
            background-color: #BDB76B;
            color: #683b11;
            font-variant: small-caps;
        }

        .myOtherTable td, .myOtherTable th {
            padding: 5px;
            border: 0;
            border: 1px solid #BDB76B;
        }

        .myOtherTable td {
            font-family: Georgia, Garamond, serif;
            height: auto;
        }
    </style>

</head>
<body onload="print()">
<div align="center">
    <img src="https://midanalbashir.gov.om/templates/site/default/assets/lib/img/2018/img/logo.png" alt="">
    <br>
    <?php if ($_SESSION['mlogged_in']): ?>
        <table class="myOtherTable" width="1000">
            <thead>
            <tr>
                <th width="3%">م</th>
                <th width="20%">المالك اسم </th>
                <th width="10%">اسم المطية</th>
                <th width="15%">رقم الشريحة</th>
                <th width="10%">رقم الشوط</th>
                <th width="10%">الفئة العمرية</th>
                <th width="10%">جنس المطية</th>
                <th width="10%">حالة الطلب</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            if ($data):
                foreach ($data as $row):
                    ?>
                    <tr>
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td>
                            <?php echo $_SESSION['mfname']; ?>
                        </td>
                        <td>
                            <?php echo $row->rqst_Name; ?>
                        </td>
                        <td>
                            <?php echo $row->rqst_NumCard; ?>
                        </td>
                        <td>
 الشوط : <?php echo $row->rac_Name; ?>
                        </td>
                        <td>
                            <?php echo $row->rac_Age; ?>
                        </td>
                        <td>
                            <?php echo $row->rac_Gender; ?>
                        </td>
                        <td>
                            <?php echo $row->rqst_Status == 1 ? "<span class='btn yellow'>" . langline("statusNew") . "</span>" : ($row->rqst_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>") ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
            else:
                ?>
                <tr>
                    <td colspan="7"><p class="note note-info">لم تقم بتسجيل مطية بعد.</p></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>

    <?php else: ?>
        <?php redirect(site_url("users/login")); ?>
    <?php endif; ?>
</div>
</body>
</html>