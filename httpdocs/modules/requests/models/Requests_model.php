<?php

class Requests_Model extends CI_Model
{

    private $table = "requests";
    private $table_ezab = "requests_ezab";

    function __construct()
    {
        parent:: __construct();
    }

    function get_all($fid = 0, $offset = 0, $limit = 0)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $_arr["rqst_FID"] = $fid;
        $_arr["rqst_Deleted"] = 0;
        get("status") ? $_arr["rqst_Status"] = get("status") : '';
        // $w = get("MemberID") ? " AND b.UR_MemberID=".get("MemberID"):'';//
        get("MemberID") ? $_arr["UR_MemberID"] = get("MemberID") : '';
        get("NumCard") ? $_arr["rqst_NumCard"] = get("NumCard") : '';
        $this->db->order_by("rqst_Created", "desc");
        $Q = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty,b.UR_MemberID")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->get_where("{$this->table} a", $_arr);
        $total = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->get_where("{$this->table} a", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_member($mid = 0, $offset = 0, $limit = 0)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $_arr["rqst_URID"] = $mid;
        $_arr["rqst_Deleted"] = 0;
        get("status") ? $_arr["rqst_Status"] = get("status") : '';
        $this->db->order_by("rqst_Created", "desc");
        $Q = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty,b.UR_MemberID,r.*")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("{$this->table} a", $_arr);
        $total = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty,r.*")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("{$this->table} a", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }


    function get_all_requests($offset = 0, $limit = 0)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        //$_arr["rqst_FID"] = $fid;
        $_arr["rqst_Deleted"] = 0;
        get("status") ? $_arr["rqst_Status"] = get("status") : '';
        // $w = get("MemberID") ? " AND b.UR_MemberID=".get("MemberID"):'';//
        get("NationalID") ? $_arr["UR_NationalID"] = get("NationalID") : '';
        get("MemberID") ? $_arr["UR_MemberID"] = get("MemberID") : '';
        get("NumCard") ? $_arr["rqst_NumCard"] = get("NumCard") : '';
        $this->db->order_by("rqst_Created", "desc");
        $Q = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty,b.UR_MemberID,r.*")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("{$this->table} a", $_arr);
        $total = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty,b.UR_MemberID,r.*")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("{$this->table} a", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }


    function get_all_subscribers($NumRace = 0)
    {
        $_arr["a.rqst_NumRace"] = $NumRace;
        $_arr["a.rqst_Status"] = 2;
        $_arr["a.rqst_Deleted"] = 0;
        $this->db->order_by("a.rqst_Created"); //, "desc"
        $Q = $this->db
            ->select("a.*,b.UR_Name,b.UR_MemberID,b.UR_Nationlaty")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->get_where("{$this->table} a", $_arr);
        $total = $this->db
            ->select("a.*,b.UR_Name,b.UR_MemberID,b.UR_Nationlaty")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->get_where("{$this->table} a", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }
    //$this->db->select('*, COUNT(CID) as total');
    //$this->db->group_by('CID');
    function get_all_lastlist($offset = 0, $limit = 0)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->select('*, COUNT(CID) as total');
        $this->db->group_by('CID');
        $this->db->order_by("DateCreated");
        //$this->db->order_by("event_ID,RaceNum");
        $_arr["event_ID <>"] = 0;//rqst_Gender rqst_AgeGroup  eventid
        get("NumCard") ? $_arr["trim(CID)"] = trim(get("NumCard")) : 0;
        get("Cname") ? $this->db->like('Cname', trim(get("Cname")), 'both') : 0;
        get("MemberID") ? $_arr["trim(MID)"] = trim(get("MemberID")) : 0;
        get("rqst_NumRace") ? $_arr["RaceNum"] = trim(get("rqst_NumRace")) : 0;
        get("eventid") ? $_arr["event_ID"] = trim(get("eventid")) : 0;
        $this->db->join("races", "rac_ID=RaceNum", "left");
        $Q = $this->db->get_where("las_list", $_arr);
        get("Cname") ? $this->db->like('Cname', trim(get("Cname")), 'both') : 0;
        $this->db->select('*, COUNT(CID) as total');
        $this->db->group_by('CID');
        $this->db->join("races", "rac_ID=RaceNum", "left");
        $total = $this->db->get_where("las_list", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }


    function get_all_rqsts($offset = 0, $limit = 0)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        //$_arr["rqst_FID"] = $fid;rqst_Gender rqst_AgeGroup  eventid
        get("Gender") ? $_arr["rqst_Gender"] = get("Gender") : 0;
        get("AgeGroup") ? $_arr["rqst_AgeGroup"] = get("AgeGroup") : 0;
        get("Race") ? $_arr["rqst_NumRace"] = get("Race") : 0;
        get("eventid") ? $_arr["rqst_FID"] = get("eventid") : 0;
        $_arr["rqst_Deleted"] = 0;
        get("status") ? $_arr["rqst_Status"] = get("status") : '';
        $this->db->order_by("rqst_Created", "desc");
        $Q = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty,b.UR_MemberID,r.*")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("{$this->table} a", $_arr);
        $total = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty,b.UR_MemberID,r.*")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("{$this->table} a", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_ezab($offset = 0, $limit = 0)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        //$_arr["rqst_FID"] = $fid;
        $_arr["rqst_Deleted"] = 0;
        get("status") ? $_arr["rqst_Status"] = get("status") : '';
        $Q = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->get_where("requests_ezab a", $_arr);
        $total = $this->db
            ->select("a.*,b.UR_Name,b.UR_Phone,b.UR_NationalID,b.UR_Nationlaty")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->get_where("requests_ezab a", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get($id = 0)
    {
        $Q = $this->db
            ->select("a.*,b.*,r.*")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("{$this->table} a", array("rqst_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function getbycard($id = 0)
    {
        $Q = $this->db
            ->select("rqst_NumCard")
            ->get_where($this->table, array("rqst_NumCard" => $id,"rqst_Deleted" => 0));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }


    function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function insertlastlistcon($data)
    {
        $this->db->insert("las_list", $data);
        return $this->db->insert_id();
    }

    function updatelastlistcon($id, $data)
    {
        $this->db->update("las_list", $data, array("id" => $id));
        return $this->db->affected_rows();
    }

    function update($id, $data)
    {
        $this->db->update($this->table, $data, array("rqst_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id)
    {
        $this->db->delete($this->table,array("rqst_ID"=>$id));
        return $this->db->affected_rows();
    }

    function deletectrl($id)
    {
        $this->db->query("DELETE FROM `las_list` WHERE id=" . $id);
        return $this->db->affected_rows();
    }

    function get_ezab($id = 0)
    {
        $Q = $this->db
            ->select("a.*,b.*")
            ->join("usersregister b", "b.UR_ID=a.rqst_URID", "left")
            ->get_where("requests_ezab a", array("rqst_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert_ezab($data)
    {
        $this->db->insert($this->table_ezab, $data);
        return $this->db->insert_id();
    }

    function update_ezab($id, $data)
    {
        $this->db->update($this->table_ezab, $data, array("rqst_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete_ezab($id, $data)
    {
        $this->db->update($this->table_ezab, $data, array("rqst_ID" => $id));
        return $this->db->affected_rows();
    }


    function get_all_user_rqsts($id, $offset = 0, $limit = 100)
    {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $_arr["rqst_Deleted"] = 0;
        $_arr["rqst_URID"] = $id;
        $Q = $this->db
            ->select("a.*,r.*")
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("requests a", $_arr);
        $total = $this->db
            ->join("races r", "r.rac_ID=a.rqst_NumRace", "left")
            ->get_where("requests a", $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

}



/*

INSERT INTO `las_list`(`Cname`, `Oname`, `CID`, `MID`, `Nationlaty`, `RaceNum`, `event_ID`,`DateCreated`) SELECT `a`.`rqst_Name`, `b`.`UR_Name`,`a`.`rqst_NumCard`, `b`.`UR_MemberID`, `b`.`UR_Nationlaty`, `a`.`rqst_NumRace`, `a`.`rqst_FID`, `a`.`rqst_Created` FROM `requests` `a` LEFT JOIN `usersregister` `b` ON `b`.`UR_ID`=`a`.`rqst_URID` WHERE `rqst_Status` =2 ORDER BY `rqst_Created` ASC

UPDATE `usersregister` SET `UR_Name`= CONCAT(`UR_FName`, " ", `UR_SName`, " ", `UR_TName`, " ", `UR_QName`) WHERE 1



 * INSERT INTO `las_list_group`(`RaceNum`,`event_ID`,  `CGroup`) 
SELECT DISTINCT `RaceNum` , `event_ID`, `CGroup` FROM `las_list` WHERE `event_ID`='8' ORDER BY `RaceNum`
 * 
 * 
 * 
 * SELECT a.`rqst_Name`,b.UR_Name,a.`rqst_NumCard`,b.UR_MemberID,a.`rqst_NumRace`,a.`rqst_FID`,`rqst_eventDate`,c.`CGroup`,COUNT(a.`rqst_ID`) FROM `requests` a


LEFT OUTER JOIN `usersregister` b
ON b.UR_ID=a.`rqst_URID`


LEFT OUTER JOIN `las_list_group` c
ON c.event_ID = a.`rqst_FID` AND c.RaceNum = a.`rqst_NumRace`

WHERE a.`rqst_Status`=2 AND a.`rqst_Deleted`=0 

GROUP BY a.`rqst_ID` ORDER BY a.`rqst_FID`,a.`rqst_NumRace`
 * 
 * 
 * 
 * 
 * 
 * 
 * INSERT INTO `las_list` (`Cname`, `Oname`, `CID`, `MID`, `RaceNum`, `event_ID`, `event_Date`, `CGroup`, `idcount`) 

SELECT a.`rqst_Name`,b.UR_Name,a.`rqst_NumCard`,b.UR_MemberID,a.`rqst_NumRace`,a.`rqst_FID`,`rqst_eventDate`,c.`CGroup` ,COUNT(a.`rqst_ID`) 
FROM `requests` a

LEFT OUTER JOIN `usersregister` b
ON b.UR_ID=a.`rqst_URID`

LEFT OUTER JOIN `las_list_group` c
ON c.event_ID = a.`rqst_FID` AND c.RaceNum = a.`rqst_NumRace`

WHERE a.`rqst_Status`=2 AND a.`rqst_Deleted`=0 

GROUP BY a.`rqst_ID`
ORDER BY a.`rqst_FID`,a.`rqst_NumRace`



SELECT `Cname`,`CID`, count(`CID`) as cnt FROM las_list GROUP BY `CID` HAVING cnt > 1


 *  */