<?php
 
function resize( $image_file_path, $new_image_file_path, $max_width=480, $max_height=1600 )
{
	error_reporting(1);
	if(substr($image_file_path,-3)=="jpg"){ 
		$img = ImageCreateFromJPEG ( $image_file_path );
	}elseif(substr($image_file_path,-3)=="gif"){ 
		$img =ImageCreateFromGIF($image_file_path) ;
	}
	$FullImage_width = imagesx ($img);	// Original image width
	$FullImage_height = imagesy ($img);	// Original image height
	
	$new_width = $max_width;
	$new_height = $max_height;
	
	// --Start Full Creation, Copying--
	// now, before we get silly and 'resize' an image that doesn't need it...
	if ( $new_width == $FullImage_width && $new_height == $FullImage_height )
		copy ( $image_file_path, $new_image_file_path );
	
	//$full_id = ImageCreate( $new_width , $new_height );		//create an image
	//ImageCopyResized( $full_id, $img, 0,0,  0,0, $new_width, $new_height, $FullImage_width, $FullImage_height );
	$full_id= imagecreatetruecolor($new_width, $new_height);
	imagecopyresampled($full_id, $img, 0, 0, 0, 0, $new_width, $new_height, $FullImage_width, $FullImage_height);
	$full = ImageJPEG( $full_id, $new_image_file_path, 80);
	ImageDestroy( $full_id );
}

function resizeProp( $image_file_path, $new_image_file_path, $max_width=480, $max_height=1600 )
{
	error_reporting(1);
	if(substr($image_file_path,-3)=="jpg"){ 
		$img = ImageCreateFromJPEG ( $image_file_path );
	}elseif(substr($image_file_path,-3)=="gif"){ 
		$img =ImageCreateFromGIF($image_file_path) ;
	}
	$FullImage_width = imagesx ($img);	// Original image width
	$FullImage_height = imagesy ($img);	// Original image height
	
	// now we check for over-sized images and pare them down
	// to the dimensions we need for display purposes
	$ratio =  ( $FullImage_width > $max_width ) ? (real)($max_width / $FullImage_width) : 1 ;
	$new_width = ((int)($FullImage_width * $ratio));	//full-size width
	$new_height = ((int)($FullImage_height * $ratio));	//full-size height
	//check for images that are still too high
	$ratio =  ( $new_height > $max_height ) ? (real)($max_height / $new_height) : 1 ;
	$new_width = ((int)($new_width * $ratio));	//mid-size width
	$new_height = ((int)($new_height * $ratio));	//mid-size height
	
	// --Start Full Creation, Copying--
	// now, before we get silly and 'resize' an image that doesn't need it...
	if ( $new_width == $FullImage_width && $new_height == $FullImage_height )
		copy ( $image_file_path, $new_image_file_path );
	
	//$full_id = ImageCreate( $new_width , $new_height );		//create an image
	//ImageCopyResized( $full_id, $img, 0,0,  0,0, $new_width, $new_height, $FullImage_width, $FullImage_height );
	$full_id= imagecreatetruecolor($new_width, $new_height);
	imagecopyresampled($full_id, $img, 0, 0, 0, 0, $new_width, $new_height, $FullImage_width, $FullImage_height);
	$full = ImageJPEG( $full_id, $new_image_file_path, 80);
	ImageDestroy( $full_id );
}