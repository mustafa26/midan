<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("Events_Model");
        $this->load->model("races/Races_Model");
        $this->lang->load("events", $this->languages[$this->user_lang]);
        $this->model = new Events_model();
        $this->load->helper("events");
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("events_status");
    }

    function index($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("events_event_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_events");
        $this->data['content'] = "events/index";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("events"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    function requests($fid = 0, $offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("events_event_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_requests($fid,$offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_events");
        $this->data['content'] = "events/index";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("events"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        foreach ($this->languages as $key => $value) {
            $this->form_validation->set_rules("event_Title[$key]", langline("event_Title") . $value, "required|trim|max_length[255]");
            $this->form_validation->set_rules("event_SubTitle[$key]", langline("event_SubTitle") . $value, "required|trim|max_length[255]");
            $this->form_validation->set_rules("event_Content[$key]", langline("event_Content") . $value, "required");
        }

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add($fid = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add($fid);
            exit;
        }

        $this->data['title'] = langline("events_event_title") . " - " . $this->site->site_name;
        $this->data['content'] = "events/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add($fid = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("event_HeaderPhoto");
        if (!$this->errors) {
            $data['event_Title'] = serialize(post("event_Title"));
            $data['event_SubTitle'] = serialize(post("event_SubTitle"));
            $data['event_Content'] = serialize(post("event_Content"));
            $data['event_Gifts'] = serialize(post("event_Gifts"));
            $data['event_MetaDescr'] = serialize(post("event_MetaDescr"));
            $data['event_MetaKeywords'] = serialize(post("event_MetaKeywords"));
            $data['event_Created_By'] = $this->user->user_id;
            $data['event_Created'] = date("Y-m-d H:i:s");
            $data['event_RaceNum'] = post("event_RaceNum");
            $data['event_AgeGroup'] = post("event_AgeGroup");
            $data['event_From'] = post("event_From");
            $data['event_To'] = post("event_To");
            $data['event_Active'] = post("event_Active") ? 1 : 0;
            $data['event_Year'] = post("event_Year");

            if ($upload_data) {
                $data['event_HeaderPhoto'] = $upload_data['file_name'];
            }
            if ($project_id = $this->model->insert($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("events"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("events/add"));
    }

    function edit($id = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }

        $this->data['title'] = langline("events_event_title") . " - " . $this->site->site_name;
        $this->data['content'] = "events/edit";
        $this->data['event'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("event_HeaderPhoto");
        if (!$this->errors) {
            $data['event_Title'] = serialize(post("event_Title"));
            $data['event_SubTitle'] = serialize(post("event_SubTitle"));
            $data['event_Content'] = serialize(post("event_Content"));
            $data['event_Gifts'] = serialize(post("event_Gifts"));
            $data['event_MetaDescr'] = serialize(post("event_MetaDescr"));
            $data['event_MetaKeywords'] = serialize(post("event_MetaKeywords"));
            $data['event_RaceNum'] = post("event_RaceNum");
            $data['event_AgeGroup'] = post("event_AgeGroup");
            $data['event_From'] = post("event_From");
            $data['event_To'] = post("event_To");
            $data['event_Active'] = post("event_Active") ? 1 : 0;
            $data['event_Year'] = post("event_Year");

            if ($upload_data) {
                $data['event_HeaderPhoto'] = $upload_data['file_name'];
                if(is_file("uploads/events/".  post("event_HeaderPhoto_old"))) unlink("uploads/events/".  post("event_HeaderPhoto_old"));
                if(is_file("uploads/events/thumb/".  post("event_HeaderPhoto_old"))) unlink("uploads/news/events/".  post("event_HeaderPhoto_old"));
            }

            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_events");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("events"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("events/edit/$id"));
    }

    function delete($id = 0) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['event_deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("events"));
    }


    public function toword($id = 0) {
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=list_".random_string().".xls");
        $event = $this->model->get($id);
        $this->data['races'] = $this->Races_Model->get_all($id, 0, 40);
        //$lstlstgroup = $this->model->get_las_list_group($id);
        //$this->data['lstlstgroup'] = $lstlstgroup;
        $this->data['event'] = $event;
        $this->data['title'] = $this->site->site_name.' | '._t($event->event_Title, 'arabic');
        $this->load->view("toword", $this->data);
    }


    public function toexcel($id = 0) {
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=list_".random_string().".xls");
        $event = $this->model->get($id);
        $this->data['races'] = $this->Races_Model->get_all($id, 0, 40);
        //$lstlstgroup = $this->model->get_las_list_group($id);
        //$this->data['lstlstgroup'] = $lstlstgroup;
        $this->data['event'] = $event;
        $this->data['title'] = $this->site->site_name.' | '._t($event->event_Title, 'arabic');
        $this->load->view("toword", $this->data);
    }


    public function toprint($id = 0) {
        $event = $this->model->get($id);
        $this->data['races'] = $this->Races_Model->get_all($id, 0, 40);
        //$lstlstgroup = $this->model->get_las_list_group($id);
        //$this->data['lstlstgroup'] = $lstlstgroup;
        $this->data['event'] = $event;
        $this->data['title'] = $this->site->site_name.' | '._t($event->event_Title, 'arabic');
        $this->load->view("toprint", $this->data);
    }


    private function do_upload($file_name = "userfile", $file_module = "events", $resize = true, $file_type = "", $allowed_types = "") {
        $this->load->library("upload");
        $this->load->library('image_lib');
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/" . $file_type;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $_img_file = $this->upload->data();
                if ($resize) {
                    $config_thumb['image_library'] = 'gd2';
                    $config_thumb['source_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/$file_type" . $_img_file["file_name"];
                    $config_thumb['create_thumb'] = FALSE;
                    $config_thumb['maintain_ratio'] = TRUE;
                    $config_thumb['width'] = 250;
                    $config_thumb['height'] = 250;
                    $config_thumb['new_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/thumb";
                    $this->image_lib->initialize($config_thumb);
                    $this->image_lib->resize();
                }
                return $_img_file;
            }
        } else {
            return false;
        }
    }

}
