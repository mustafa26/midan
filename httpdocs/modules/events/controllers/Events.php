<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Events extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Events_Model", 'model');
        $this->load->model("races/Races_Model");
    }

    public function index($id = 0, $offset = 0) {
        $this->data['title'] = $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_home($offset, $limit);
        $this->data['content'] = "events/index";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("events"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    public function details($id = 0) {
        $event = $this->model->get($id);
        $this->data['title'] = $this->site->site_name.' | '._t($event->event_Title, 'arabic');
        $this->data['event'] = $event;
        $this->data['races'] = $this->Races_Model->get_all_subsrbrs($id, 0, 40);
		$this->data['content'] = "events/details";
        $this->template->render("template", $this->data);
    }

    public function subscribers($id = 0) {
        $event = $this->model->get($id);
        $this->data['races'] = $this->Races_Model->get_all_subsrbrs($id, 0, 40);
        //$lstlstgroup = $this->model->get_las_list_group($id);
        //$this->data['lstlstgroup'] = $lstlstgroup;
        $this->data['event'] = $event;
        $this->data['title'] = $this->site->site_name.' | '._t($event->event_Title, 'arabic');
		$this->data['content'] = "events/subscribers";
        $this->template->render("template", $this->data);
    }

    public function results($id = 0) {
        $event = $this->model->get($id);
        $this->data['races'] = $this->Races_Model->get_all_subsrbrs($id, 0, 40);
        //$lstlstgroup = $this->model->get_las_list_group($id);
        $this->data['title'] = $this->site->site_name.' | '._t($event->event_Title, 'arabic');
        //$this->data['lstlstgroup'] = $lstlstgroup;
        $this->data['event'] = $event;
		$this->data['content'] = "events/results";
        $this->template->render("template", $this->data);
    }
	
    public function statistics() {
        $this->data['title'] = $this->site->site_name.' | الاحصائيات';
        $this->data['total'] = $this->model->get_statistices_total(); 
        $this->data['total_3'] = $this->model->get_st_tev(9);
        $this->data['total_4'] = $this->model->get_st_tev(10);
        $this->data['total_5'] = $this->model->get_st_tev(11);
        $this->data['total_6'] = $this->model->get_st_tev(12);
        $this->data['total_7'] = $this->model->get_st_tev(13);
        $this->data['total_8'] = $this->model->get_st_tev(14);
		$this->data['content'] = "events/statistices";
        $this->template->render("template", $this->data);
    }

    public function statistic($id=0) {
        $this->data['title'] = $this->site->site_name.' | الاحصائيات';
        $event = $this->model->get($id);
        $sbyev = $this->model->get_statistices_bev($id); 
        $this->data['stc_dtls'] = $sbyev;
        $this->data['event'] = $event;
		$this->data['content'] = "events/statistice";
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("rqst_Name", langline("event_Title") . $value, "required|trim|max_length[255]");
        $this->form_validation->set_rules("rqst_NumCard", langline("event_SubTitle") . $value, "required|trim|max_length[255]");
        $this->form_validation->set_rules("rqst_NumRace", langline("event_Content") . $value, "required");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    public function request($id = 0) {
        if (post()) {
            $this->_do_add($id);
            exit;
        }
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "events/request";
        $this->data['event'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }
    
    public function requestresult($id = 0) { 
        $this->data['title'] = $this->site->site_name;
        $this->data['content'] = "events/requestresult";
        $this->data['event'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    private function _do_add($id=0) {
        $this->_validate();
        if (!$this->errors) {

            $data['rqst_FID'] = $id;
            $data['rqst_Type'] = 'Request';
            $data['rqst_Name'] = post("rqst_Name");
            $data['rqst_NumCard'] = post("rqst_NumCard");
            $data['rqst_NumRace'] = post("rqst_NumRace");
            //$data['rqst_Image'] = post("rqst_Image");
            //$data['rqst_PermitImage'] = post("rqst_PermitImage");
            $data['rqst_AgeGroup'] = post("rqst_AgeGroup");
            $data['rqst_OwnerName'] = post("rqst_OwnerName");
            $data['rqst_OwnerBD'] = post("rqst_OwnerBD");
            $data['rqst_OwnerID'] = post("rqst_OwnerID");
            //$data['rqst_OwnerIDImage'] = post("rqst_OwnerIDImage");
            $data['rqst_OwnerIDExp'] = post("rqst_OwnerIDExp");
            $data['rqst_OwnerNationlaty'] = post("rqst_OwnerNationlaty");
            //$data['rqst_OwnerImage'] = post("rqst_OwnerImage");
            $data['rqst_OwnerPhone'] = post("rqst_OwnerPhone");
            $data['rqst_OwnerEmail'] = post("rqst_OwnerEmail");
            $data['rqst_Bank'] = post("rqst_Bank");
            $data['rqst_OC'] = post("rqst_OC");
            $data['rqst_IBAN'] = post("rqst_IBAN");
            //$data['rqst_ImageOC'] = post("rqst_ImageOC");
            $data['rqst_Status'] = 0;
            $data['rqst_Created'] = date("Y-m-d H:i:s");

            $upload_data = $this->do_upload("rqst_Image");
            $data['rqst_Image'] = $upload_data ? $upload_data['file_name'] : '';
            $upload_data = $this->do_upload("rqst_PermitImage");
            $data['rqst_PermitImage'] = $upload_data ? $upload_data['file_name'] : '';
            $upload_data = $this->do_upload("rqst_OwnerIDImage");
            $data['rqst_OwnerIDImage'] = $upload_data ? $upload_data['file_name'] : '';
            $upload_data = $this->do_upload("rqst_OwnerImage");
            $data['rqst_OwnerImage'] = $upload_data ? $upload_data['file_name'] : '';
            $upload_data = $this->do_upload("rqst_ImageOC");
            $data['rqst_ImageOC'] = $upload_data ? $upload_data['file_name'] : '';
            if ($rqst_id = $this->model->insert_request($data)) {
                //set action in user log  
                $this->set_message(langline("success_categories_created"), "success");
                redirect(site_url("events/requestresult"));
            }
        }

        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(site_url("events/request/".$id));
    }

    private function do_upload($file_name = "userfile") {
        $this->load->library("upload");
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/requests";
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                return $this->upload->data();
            }
        } else {
            return false;
        }
    }

}
