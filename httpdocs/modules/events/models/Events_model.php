<?php

class Events_Model extends CI_Model {

    private $table = "events"; 

    function __construct() {
        parent :: __construct();
    }

    function get_all($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->select('e.*, COUNT(r.rac_ID) as total_races');
        $this->db->group_by('e.event_ID');
        $this->db->order_by("e.event_Created","DESC");
        $this->db->join("races r", "r.rac_FID=e.event_ID AND r.rac_Status=1", "left");
        //$this->db->join("races", "rac_ID=RaceNum", "left");
        $Q = $this->db->get_where("$this->table e", array("e.event_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("event_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_las_list_group($eventid = 0) { 
        $Q = $this->db->get_where("las_list_group", array("event_ID" => $eventid));
        $total = $this->db->get_where("las_list_group", array("event_ID" => $eventid))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }
	
	
    function get_las_list($RaceNum=0,$event_ID=0) {
		$this->db->select('*, COUNT(CID) as total');		
		$this->db->order_by("DateCreated","ASC");
 		$this->db->group_by('CID');
        $Q = $this->db->get_where("las_list", array("event_ID" => $event_ID,"RaceNum" => $RaceNum)); 
		$this->db->select('*, COUNT(CID) as total');
 		$this->db->group_by('CID'); 
        $total = $this->db->get_where("las_list", array("event_ID" => $event_ID,"RaceNum" => $RaceNum))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }
	
    function get_results($RaceNum=0,$event_ID=0) {
		$this->db->select('*, COUNT(CID) as total');
 		$this->db->group_by('CID'); 
 		$this->db->order_by('winner_sort'); 
        $Q = $this->db->get_where("las_list", array("winner_sort <>" => 0,"event_ID" => $event_ID,"RaceNum" => $RaceNum)); 
		$this->db->select('*, COUNT(CID) as total');
 		$this->db->group_by('CID'); 
        $total = $this->db->get_where("las_list", array("winner_sort <>" => 0,"event_ID" => $event_ID,"RaceNum" => $RaceNum))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_st_tev($event_ID=0) {
        $total = $this->db->get_where("las_list", array("event_ID" => $event_ID))->num_rows();
        return $total;
    }

    function get_stby_Nationlaty($Nationlaty="") {
        $total = $this->db->get_where("las_list", array("Nationlaty" => $Nationlaty))->num_rows();
        return $total;
    }

    function get_statistices_total() { 
 		$this->db->group_by('CID');   
        $total = $this->db->get("las_list")->num_rows();  
        return $total;
    }
    function get_statistices_bev($event_ID) {
		//$Q = $this->db->query("SELECT COUNT(DISTINCT `CID`) as total,`RaceNum` FROM `las_list` WHERE `event_ID` = $event_ID GROUP BY `RaceNum`");
        //$data = $Q->num_rows() > 0 ? $Q->result() : false;
        //return $data;
        $this->db->select('*, COUNT(CID) as total');
        $this->db->group_by('RaceNum');
        $this->db->order_by('RaceNum');
        $this->db->join("races r", "rac_ID=RaceNum", "left");
        $Q = $this->db->get_where("las_list", array("event_ID" => $event_ID));
        $this->db->select('*, COUNT(CID) as total');
        $this->db->group_by('RaceNum');
        $this->db->order_by('RaceNum');
        $this->db->join("races r", "rac_ID=RaceNum", "left");
        $total = $this->db->get_where("las_list", array("event_ID" => $event_ID))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_home($offset = 0, $limit = 0) {
        $this->db->order_by("event_Created");
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $_where["event_Year"] = date("Y");
        $_where["event_Active"] = 1;
        $_where["event_Deleted"] = 0;
        $Q = $this->db->get_where($this->table, $_where);
        $total = $this->db->get_where($this->table, $_where)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }
 

    function get($id = 0) {
        $Q = $this->db->get_where($this->table, array("event_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
     

    function update($id, $data) {
        $this->db->update($this->table, $data, array("event_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id, $data) {
        $this->db->update($this->table, $data, array("event_ID" => $id));
        return $this->db->affected_rows();
    }

}
