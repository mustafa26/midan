<?php

$lang['events_event_title'] = "السباقات";
$lang['events_event_header'] = "السباقات";
$lang['events_link'] = "السباقات";
$lang['cats_name'] = "اسم التصنيف";

$lang['event_Title'] = "عنوان السباق";
$lang['event_SubTitle'] = "عنوان السباق الفرعي";

$lang['event_Content'] = "الشروط"; 
$lang['event_Gifts'] = "الجوائز";  
$lang['event_RaceNum'] = "عدد الأشواط";    

$lang['event_MetaDescr'] = "وصف محتوى السباق";
$lang['event_MetaKeywords'] = "الكلمات الدليلية";
$lang['event_HeaderPhoto'] = "صورة بنر السباق";
$lang['Tab_Settings'] = "إعدادات السباق";
$lang['Name'] = "الاسم";
$lang['project_status'] = "الحالة";
$lang['project_description'] = "وصف";
$lang['project_sub_tasks_num'] = "%s مهمة";
$lang['Lable_From'] = "من";
$lang['Lable_To'] = "الى";
$lang['task_duration'] = "الفترة الزمنية";
$lang['Table_Compitations'] = "جدول السباقات";
$lang['Time_period'] = "المدة الزمنية";
$lang['Num_Races'] = "عدد الأشواط ";
$lang['Races'] = "أشواط";
$lang['More'] = "التفاصيل";

$lang['View_Orders'] = "أستعراض طلبات الأشتراك";


//Messages

$lang['success_project_created']        = "Project Created Successfully";
$lang['success_project_updated']        = "Project Updated Successfully";
$lang['success_project_deleted']        = "Project Deleted Successfully";
$lang['error_project_not_deleted']      = "Error: project Not Deleted Try again after refresh";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated']         = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project']   = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project']    = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project']     = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task']        = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed']    = "Project %s  status changed to Delayed because Project tasks not completed in time line";

$lang['log_show_projects']      = "Show Projects";
$lang['log_add_project']      = "Add a new Project";
$lang['log_edit_project']      = "Update Project";
$lang['log_delete_project']      = "Delete Project";