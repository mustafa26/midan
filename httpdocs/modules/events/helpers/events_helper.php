<?php

function las_list_subscribers($RaceNum = 0)
{
    $ins = &get_instance();
    $ins->load->model("requests/Requests_Model");
    $_las_list = $ins->Requests_Model->get_all_subscribers($RaceNum);
    if ($_las_list['data']):
        ?>
        <tr>
            <th scope="row" style="background-color: #ca9e66; color:#fff;"> م</th>
            <th scope="row" style="background-color: #ca9e66; color:#fff;">اسم الناقة</th>
            <th scope="row" style="background-color: #ca9e66; color:#fff;"> اسم المالك</th>
            <th scope="row" style="background-color: #ca9e66; color:#fff;">رقم الشريحة</th>
            <th scope="row" style="background-color: #ca9e66; color:#fff;">رقم المشارك</th>
            <th scope="row" style="background-color: #ca9e66; color:#fff;">الدولة</th>
        </tr>
        <?php
        $n = 1;
        foreach ($_las_list['data'] as $key => $value) :
            ?>
            <tr>
                <th scope="row" style="background-color: #fff;"><?php echo $n ?></th>
                <th scope="row" style="background-color: #ebcaa0;"><?php echo $value->rqst_Name ?></th>
                <th scope="row" style="background-color: #fff;"><?php echo $value->UR_Name ?></th>
                <th scope="row" style="background-color: #ebcaa0;"><?php echo $value->rqst_NumCard ?></th>
                <th scope="row" style="background-color: #fff;"><?php echo $value->UR_MemberID ?></th>
                <th scope="row" style="background-color: #fff;"><?php echo langline($value->UR_Nationlaty) ?></th>
            </tr>
            <?php
            $n++;
        endforeach;
        ?>
    <?php
    endif;
}