<?php

$lang['members_member_title'] = "المشاركون";
$lang['requests_request_title'] = "نماذج الطلبات";
$lang['requests_request_header'] = "نماذج الطلبات";
$lang['members_link'] = "الطلبات";
$lang['cats_name'] = "اسم التصنيف";
$lang['Sort'] = "الترتيب";
$lang['All'] = "الكل";

$lang['UR_Name'] = "الاسم";
$lang['users_nikName'] = "اللقب";
$lang['users_nikName_1'] = "صاحب السمو السيد";
$lang['users_nikName_2'] = "السيد";
$lang['users_nikName_3'] = "الدكتور";
$lang['users_nikName_4'] = "الشيخ";
$lang['users_nikName_5'] = "الفاضل";

$lang['users_FName'] = "الاسم الأول";
$lang['users_SName'] = "الاسم الثاني";
$lang['users_TName'] = "الاسم الثالث";
$lang['users_QName'] = "القبيلة";

$lang['project_status'] = "الحالة";
$lang['project_sub_tasks_num'] = "%s مهمة";
$lang['Lable_From'] = "من";
$lang['Lable_To'] = "الى";
$lang['task_duration'] = "الفترة الزمنية";
$lang['Time_period'] = "المدة الزمنية";
$lang['Num_Races'] = "عدد الأشواط ";
$lang['Races'] = "أشواط";
$lang['More'] = "التفاصيل";

// Form Request
$lang['Request'] = "تسجيل الهجن";
$lang['rqst_Info'] = "معلومات";
$lang['rqst_EzabaInfo'] = "معلومات العزبة";
$lang['rqst_Camel'] = "المطية";
$lang['rqst_Owner'] = "المالك";
$lang['rqst_Name'] = "اسم المطية";
$lang['rqst_NumCard'] = "رقم الشريحة";
$lang['rqst_NumCard_'] = "برجاء كتابة الرقم الخاص بالشريحة للمطية";
$lang['rqst_NumRace'] = "رقم الشوط";
$lang['rqst_Image'] = "صورة للمطية";
$lang['rqst_PermitImage'] = "صورة التصريح (برنت) من الاتحاد المعني";
$lang['Events'] = "السباقات";
$lang['AgeGroup'] = "الفئة";
$lang['Race'] = "الشوط";
$lang['rqst_AgeGroup'] = "الفئة العمرية ";
$lang['rqst_AgeGroup_2'] = "الحجائج (سنتان)";
$lang['rqst_AgeGroup_3'] = "اللقايا (ثلاث سنوات)";
$lang['rqst_AgeGroup_4'] = "اليداع أو الجذع (أربع سنوات)";
$lang['rqst_AgeGroup_5'] = "الثنايا (خمس سنوات)";
$lang['rqst_AgeGroup_6'] = "الرباع والحايل والحول والزمول  (ست سنوات فأكثر)";
$lang['rqst_Gender'] = "جنس المطية";
$lang['rqst_Gender_01'] = "ذكر";
$lang['rqst_Gender_02'] = "أنثى";
$lang['rqst_Gender_1'] = "أبكار";
$lang['rqst_Gender_2'] = "جعدان";
$lang['rqst_Gender_3'] = "حول";
$lang['rqst_Gender_4'] = "زمول";
$lang['rqst_Gender_01'] = "ذكر";
$lang['rqst_Gender_02'] = "أنثى";

$lang['rqst_NameCamel'] = "إسم الناقة";
$lang['rqst_OwnerName'] = "اسم المالك";
$lang['Request_now'] = "تسجيل بالسباق";

$lang['Day'] = "يوم";
$lang['Month'] = "شهر";
$lang['Year'] = "سنة";
$lang['rqst_Created'] = "تاريخ الإضافة";
$lang['rqst_OwnerID'] = "البطاقة الشخصية(الرقم المدني)";

$lang['UR_BD'] = "تاريخ الميلاد";
$lang['UR_ID'] = "رقم البطاقة الشخصية";
$lang['UR_IDExp'] = "تاريخ الإنتهاء";
$lang['UR_Nationlaty'] = "الجنسية";
$lang['UR_IDImage'] = "صورة من البطاقة الشخصية";
$lang['UR_Image'] = "صورة شخصية";
$lang['UR_Phone'] = "رقم الهاتف ";
$lang['btn_change_password'] = " كلمة المرور";
$lang['UR_Email'] = "البريد الإلكتروني";

$lang['UR_Job'] = "الوظيفة";
$lang['UR_JobPlace'] = "مكان العمل";
$lang['UR_NationalID'] = " البطاقة الشخصية(الرقم المدني)"; 
$lang['UR_MemberID'] = "رقم المشارك";
$lang['UR_NationalIDExp'] = "تاريخ الأنتهاء"; 
$lang['UR_NationalIDImage'] = "صورة بطاقة المشارك";
$lang['UR_Address'] = "العنوان";

$lang['UR_Status'] = "الحالة";
$lang['UR_Created_By'] = "صاحب الاضافة";
$lang['UR_Select_File'] = "أختر ملف";
$lang['statusNew'] = "جديد";
$lang['statusAccept'] = "مقبول";
$lang['statusNonAccept'] = "مرفوض";
$lang['statusProcessingStill'] = "قيد التسجيل";
$lang['UR_Created'] = "تاريخ الإضافة"; 
$lang['View_Orders'] = "أستعراض طلبات الأشتراك";


// Country
$lang['oman'] = "سلطنة عُمان"; 
$lang['uea'] = "الإمارات العربية المتحدة";  
$lang['ksa'] = "المملكة العربية السعودية"; 
$lang['ksa'] = "ممكلة البحرين"; 
$lang['qatar'] = "قطر"; 
$lang['kiwat'] = "الكويت";

//Messages
$lang['success_categories_created'] = "تم إنشاء التصنيف بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات التصنيف بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات التصنيف ";
$lang['success_project_deleted'] = "تم حذف العنصر بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف التصنيف , حاول مرة أخرى بعد تحديث السباق";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_requests'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_request'] = "تحديث بيانات السباق";
$lang['log_delete_project'] = "حذف مشروع";
