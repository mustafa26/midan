<?php

$lang['events_event_title'] = "السباقات";
$lang['events_event_header'] = "السباقات";
$lang['events_link'] = "السباقات";
$lang['cats_name'] = "اسم التصنيف";
$lang['Sort'] = "الترتيب"; 

$lang['event_Title'] = "عنوان السباق";
$lang['event_SubTitle'] = "عنوان السباق الفرعي";

$lang['event_Content'] = "الشروط";
$lang['event_Gifts'] = "الجوائز";
$lang['event_RaceNum'] = "عدد الأشواط";

$lang['event_MetaDescr'] = "وصف محتوى السباق";
$lang['event_MetaKeywords'] = "الكلمات الدليلية";
$lang['event_HeaderPhoto'] = "صورة بنر السباق";
$lang['Tab_Settings'] = "إعدادات السباق";
$lang['Name'] = "الاسم";
$lang['project_status'] = "الحالة";
$lang['project_description'] = "وصف";
$lang['project_sub_tasks_num'] = "%s مهمة";
$lang['Lable_From'] = "من";
$lang['Lable_To'] = "الى";
$lang['task_duration'] = "الفترة الزمنية";
$lang['Table_Compitations'] = "جدول السباقات";
$lang['Time_period'] = "المدة الزمنية";
$lang['Num_Races'] = "عدد الأشواط ";
$lang['Races'] = "أشواط";
$lang['More'] = "التفاصيل";
$lang['Gifts'] = "الجوائز";
$lang['Request_now'] = "تسجيل بالسباق";

// Form Request
$lang['Request'] = "تسجيل الهجن";
$lang['rqst_Name'] = "اسم المطية";
$lang['rqst_NumCard'] = "الرقم الشريحة";
$lang['rqst_NumCard_'] = "برجاء كتابة الرقم الخاص بالشريحة للمطية";
$lang['rqst_NumRace'] = "رقم الشوط";
$lang['rqst_Image'] = "صورة للمطية";
$lang['rqst_PermitImage'] = "صورة التصريح من الاتحاد المعني";
$lang['rqst_AgeGroup'] = "الفئة العمرية للمطية ";
$lang['rqst_AgeGroup_2'] = "الحجائج (سنتان)";
$lang['rqst_AgeGroup_3'] = "اللقايا (ثلاث سنوات)";
$lang['rqst_AgeGroup_4'] = "اليداع أو الجذع (أربع سنوات)";
$lang['rqst_AgeGroup_5'] = "الثنايا (خمس سنوات)";
$lang['rqst_AgeGroup_6'] = "الرباع والحايل والحول  (ست سنوات فأكثر)";
$lang['rqst_OwnerName'] = "اسم المالك";
$lang['rqst_OwnerName_'] = "اسم صاحب المطية";
$lang['rqst_OwnerBD'] = "تاريخ الميلاد";
$lang['rqst_OwnerBD_'] = "تاريخ ميلاد صاحب المطية";
$lang['rqst_OwnerID'] = "رقم البطاقة الشخصية";
$lang['rqst_OwnerID_'] = "رقم البطاقة الشخصية لصاحب المطية";
$lang['rqst_OwnerIDExp'] = "تاريخ الإنتهاء";
$lang['rqst_OwnerIDExp_'] = "تاريخ انتهاء البطاقة الشخصية";
$lang['rqst_OwnerNationlaty'] = "الجنسية";
$lang['rqst_OwnerIDImage'] = "صورة من البطاقة الشخصية";
$lang['rqst_OwnerImage'] = "صورة شخصية للمالك";
$lang['rqst_OwnerPhone'] = "رقم الهاتف ";
$lang['rqst_OwnerPhone_'] = "رقم الهاتف الخلوي";
$lang['rqst_OwnerEmail'] = "البريد الإلكتروني";
$lang['rqst_BankInfo'] = "المعلومات البنكية";
$lang['rqst_Bank'] = "اسم البنك";
$lang['rqst_OC'] = "رقم الحساب";
$lang['rqst_OC_'] = "رقم حساب البنك";
$lang['rqst_IBAN'] = "رقم الحوالة الدولية IBAN ";
$lang['rqst_ImageOC'] = "ارفاق صورة لشهادة الحساب";
$lang['rqst_Status'] = "الحالة";
$lang['rqst_Created_By'] = "صاحب الاضافة";
$lang['rqst_Select_File'] = "أختر ملف"; 


$lang['View_Orders'] = "أستعراض طلبات الأشتراك";
//Messages
$lang['success_categories_created'] = "تم إنشاء التصنيف بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات التصنيف بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات التصنيف ";
$lang['success_project_deleted'] = "تم حذف التصنيف بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف التصنيف , حاول مرة أخرى بعد تحديث السباق";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_events'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_event'] = "تحديث بيانات السباق";
$lang['log_delete_project'] = "حذف مشروع";
