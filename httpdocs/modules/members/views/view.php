<!-- END PAGE HEADER-->
<div class="portlet box green form col-10">
    <div class="portlet-body">
        <div class="form-body">
            <h3 class="form-section"><?php echo langline("Personal_Info") ?></h3>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("UR_Name") ?>:</label>

                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_Name ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <?php /*
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Email") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><?php echo $row->UR_Email ?></p>
                            </div>
                        </div>
                    </div>
                     */
                ?>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("UR_Phone") ?>:</label>

                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_Phone ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("UR_Created") ?>:</label>

                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_Created ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("UR_NationalID") ?>:</label>

                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_NationalID ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("UR_MemberID") ?>:</label>

                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_MemberID ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("UR_Nationlaty") ?>:</label>

                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo langline($row->UR_Nationlaty) ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("UR_NationalIDImage") ?>:</label>

                        <div class="col-md-9">
                            <p class="form-control-static">
                                <img
                                    src="<?php echo $row->UR_NationalIDImage ? $upload_path . '/requests/' . $row->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                                    width="400"/>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <?php echo langline("Send_SMS") ?>:
                            <br>
                            <span id="charNum" style="color: red"></span>
                        </label>

                        <div class="col-md-9">
                            <p class="form-control-static">

                            <form action="<?php echo admin_url("members/sender") ?>" class="form-horizontal"
                                  method="post" id="sms_form" accept-charset="utf-8">
                                <textarea maxlength="160" name="messagesms" id="messagesms" class="form-control"
                                          required></textarea>
                                <button type="submit" name="send_sms" id="send_sms"
                                        class="btn blue"><?php echo langline("Send") ?></button>
                                <input name="mobnumber" id="mobnumber" type="hidden"
                                       value="<?php echo $row->UR_Phone ?>">
                            </form>
                            </p>
                            <div id="response_send"></div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>

    var maxLength = 160;
    $('#sms_txt').keyup(function () {
        var length = $(this).val().length;
        var length = maxLength - length;
        $('#charNum').text("متبقي : " + length + "حرف");
        //$('#charNum').text(length);
    });
    //#################

        $('#sms_form').submit(function () {
            // show that something is loading
            $('#response_send').html("<b>Loading response...</b>");
            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url("members/sender") ?>',
                data: $(this).serialize()
                //data: form_data
            })
                .done(function (data) {
                    // show the response
                    $('#response_send').html(data);
                })
                .fail(function () {
                    // just in case posting your form failed
                    alert("Posting failed.");
                });
            // to prevent refreshing the whole page page
            return false;
        });
</script>


