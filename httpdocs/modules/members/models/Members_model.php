<?php

class Members_Model extends CI_Model {

    private $table = "usersregister";

    function __construct() {
        parent :: __construct();
    }

    function get_all($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $_arr["UR_Deleted"] = 0;
        get("status") ? $_arr["UR_Status"] = get("status") : '';
        get("MemberID") ? $_arr["UR_MemberID"] = get("MemberID") : '';
        get("NationalID") ? $_arr["UR_NationalID"] = get("NationalID") : '';
        get("Phone") ? $_arr["UR_Phone"] = get("Phone") : '';
        $Q = $this->db->get_where($this->table, $_arr);
        $total = $this->db->get_where($this->table, $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_home($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $_arr["UR_Deleted"] = 0;
        $_arr["UR_Status"] = 2;
        $Q = $this->db->get_where($this->table, $_arr);
        $total = $this->db->get_where($this->table, $_arr)->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get($id = 0) {
        $Q = $this->db->get_where($this->table, array("UR_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function getbyphone($id = 0)
    {
        $Q = $this->db
            ->select("UR_Phone")
            ->get_where($this->table, array("UR_Phone" => $id,"UR_Deleted" => 0));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function getbynationalid($id = 0)
    {
        $Q = $this->db
            ->select("UR_NationalID")
            ->get_where($this->table, array("UR_NationalID" => $id,"UR_Deleted" => 0));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $this->db->update($this->table, $data, array("UR_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id) {
        $this->db->delete($this->table,array("UR_ID"=>$id));
        //$this->db->update($this->table, $data, array("UR_ID" => $id));
        return $this->db->affected_rows();
    }

}
