<?php

class Admin extends Admin_Controller
{

    private $model;
    private $_username = "DURAR";
    private $_password = "123456789";

    function __construct()
    {
        parent:: __construct();
        $this->load->model("Members_Model");
        $this->load->model("users/Users_model");
        $this->load->model("newsletter/Newsletter_model");
        $this->load->model("requests/Requests_Model");
        $this->load->model("events/Events_Model");
        $this->load->model("races/Races_Model");
        $this->load->helper("members");
        $this->lang->load("members", $this->languages[$this->user_lang]);
        $this->model = new Members_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("members_status");
    }

    function index($offset = 0)
    {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("members_UR_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_members");
        $this->data['content'] = "members/index";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("members/index/"), $result['total'], $limit, 4);
        $this->template->render("template", $this->data);
    }

    function requests($mid = 0, $offset = 0)
    {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }

        if (post()) {
            $this->_do_requests($mid);
            exit;
        }
        $this->data['title'] = langline("requests_link") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->Requests_Model->get_all_member($mid, $offset, $limit);
        $this->data['events'] = $this->Events_Model->get_all_home();
        log_add_user_action($this->user->user_id, 0, "show", "log_show_requests");
        $this->data['content'] = "members/requests";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['offset'] = $offset + 1;
        $_suffx = get("status") ? "?status=" . get("status") : "";
        $this->data['pagination'] = $this->pagination->pager(admin_url("requests/index/" . $mid), $result['total'], $limit, 5);
        $this->template->render("template", $this->data);
    }

    private function _do_requests($mid=0)
    {
        if (!$this->errors) {
            $data['rqst_Name'] = post("rqst_Name");
            $data['rqst_NumCard'] = post("rqst_NumCard");
            $data['rqst_FID'] = post("rqst_FID");
            $data['rqst_NumRace'] = post("rqst_NumRace");
            $data['rqst_URID'] = post("rqst_URID");
            $data['rqst_Confirm_By'] = $this->user->user_id;
            $data['rqst_Status'] = 2;
            $data['rqst_Deleted'] = 0;
            $data['rqst_Created'] = date("Y-m-d H:i:s");

            if ($_FILES['PermitImage']['name']) {
                if ($_FILES['PermitImage']['size'] > 4248000) {
                    $this->errors .= "<p>" . langline("error_Maxfile_NotAllow") . "</p>"
                        . "<p>" . langline("error_Maxfile_Upload1") . round(($_FILES['PermitImage']['size'] / 1000000), 1) . "MB، " . langline("error_Maxfile_Upload2") . "</p>";
                } else {
                    $upload_NationalIDImage = $this->do_upload("PermitImage");
                    $data['rqst_PermitImage'] = $upload_NationalIDImage ? $upload_NationalIDImage['file_name'] : '';
                }
            }
            if ($this->Requests_Model->insert($data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, 0, "add", "log_add_members");
                $this->set_message(langline("success_user_created"), "success");
                redirect(admin_url("members/requests/$mid"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("members/requests/$mid"));
    }
    function view($id = 0)
    {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("members_UR_title") . " - " . $this->site->site_name;
        $this->data['upload_path'] = site_url("uploads");
        $this->data['row'] = $this->model->get($id);
        view("view", $this->data);
    }

    function add()
    {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add();
            exit;
        }

        $this->data['title'] = langline("members_UR_title") . " - " . $this->site->site_name;
        $this->data['content'] = "members/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('Phone', langline("UR_Phone"), 'required|trim|max_length[15]|min_length[11]|numeric');
        $_res = $this->model->getbyphone(post("Phone"));
        if ($_res && $_res->UR_Phone) $this->errors .= "<p> رقم الهاتف مسجل من قبل وبالتالي لايمكن قبول الطلب.</p>";
        $_res = $this->model->getbynationalid(post("NationalID"));
        if ($_res && $_res->UR_NationalID) $this->errors .= "<p> الرقم المدني مسجل من قبل وبالتالي لايمكن قبول الطلب.</p>";
        if ($this->form_validation->run() == FALSE || $this->errors != "") {
            $this->errors .= validation_errors();
            $this->keep_data();
            $this->set_message($this->errors, "error");
            redirect(admin_url("members/add"));
        } else {
            $data['UR_nikName'] = post("nikName");
            $data['UR_FName'] = post("FName");
            $data['UR_SName'] = post("SName");
            $data['UR_TName'] = post("TName");
            $data['UR_QName'] = post("QName");
            $data['UR_Name'] = post("nikName", true) . ' ' . post("FName", true) . ' ' . post("SName", true) . ' ' . post("TName", true) . ' ' . post("QName", true);
            $data['UR_Phone'] = post("Phone");
            $data['UR_NationalID'] = post("NationalID");
            $data['UR_MemberID'] = post("MemberID");
            $data['UR_Nationlaty'] = post("Nationlaty");
            $data['UR_Confirm_By'] = $this->user->user_id;
            $data['UR_Status'] = 2;
            $data['UR_Deleted'] = 0;
            $data['UR_Created'] = date("Y-m-d H:i:s");

            if (post("UserPassword", true)) {
                //check login name and password
                $password = $this->Users_model->encrypt_password(post("UserPassword", true));
                $data['UR_Password'] = $password['hash'];
                $data['UR_Salt'] = $password['salt'];
                $data['UR_ResetPass'] = 0;
            }

            if ($_FILES['NationalIDImage']['name']) {
                if ($_FILES['NationalIDImage']['size'] > 4248000) {
                    $this->errors .= "<p>" . langline("error_Maxfile_NotAllow") . "</p>"
                        . "<p>" . langline("error_Maxfile_Upload1") . round(($_FILES['NationalIDImage']['size'] / 1000000), 1) . "MB، " . langline("error_Maxfile_Upload2") . "</p>";
                } else {
                    $upload_NationalIDImage = $this->do_upload("NationalIDImage");
                    $data['UR_NationalIDImage'] = $upload_NationalIDImage ? $upload_NationalIDImage['file_name'] : '';
                    post("old_NationalIDImage") ? unlink("uploads/requests/" . post("old_NationalIDImage")) : 0;
                }
            }
            if ($this->model->insert($data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, 0, "add", "log_add_members");
                $this->set_message(langline("success_user_created"), "success");
                redirect(admin_url("members"));
            }
        }
    }


    function edit($id = 0)
    {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }

        $this->data['title'] = langline("members_UR_title") . " - " . $this->site->site_name;
        $this->data['content'] = "members/edit";
        $this->data['row'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0)
    {
        if (!$this->errors) {
            $data['UR_Confirm_By'] = $this->user->user_id;
            $data['UR_nikName'] = post("nikName");
            $data['UR_FName'] = post("FName");
            $data['UR_SName'] = post("SName");
            $data['UR_TName'] = post("TName");
            $data['UR_QName'] = post("QName");
            $data['UR_Name'] = post("nikName", true) . ' ' . post("FName", true) . ' ' . post("SName", true) . ' ' . post("TName", true) . ' ' . post("QName", true);
            $data['UR_Phone'] = post("Phone");
            $data['UR_NationalID'] = post("NationalID");
            $data['UR_MemberID'] = post("MemberID");
            $data['UR_Nationlaty'] = post("Nationlaty");
            $data['UR_Status'] = post("Status");

            if (post("UserPassword", true)) {
                //check login name and password
                $password = $this->Users_model->encrypt_password(post("UserPassword", true));
                $data['UR_Password'] = $password['hash'];
                $data['UR_Salt'] = $password['salt'];
                $data['UR_ResetPass'] = 0;
            }

            if ($_FILES['NationalIDImage']['name']) {
                if ($_FILES['NationalIDImage']['size'] > 4248000) {
                    $this->errors .= "<p>" . langline("error_Maxfile_NotAllow") . "</p>"
                        . "<p>" . langline("error_Maxfile_Upload1") . round(($_FILES['NationalIDImage']['size'] / 1000000), 1) . "MB، " . langline("error_Maxfile_Upload2") . "</p>";
                } else {
                    $upload_NationalIDImage = $this->do_upload("NationalIDImage");
                    $data['UR_NationalIDImage'] = $upload_NationalIDImage ? $upload_NationalIDImage['file_name'] : '';
                    post("old_NationalIDImage") ? unlink("uploads/requests/" . post("old_NationalIDImage")) : 0;
                }
            }
            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_members");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("members"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("members/edit/$id"));
    }

    function delete($id = 0)
    {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['UR_deleted'] = 1;
        if ($this->model->delete($id)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("members"));
    }


    function sender()
    {
        if ($this->is_ajax()) {
            $this->_ajax_sender();
            return;
        }

        show_error("Ajax Requiest only supported connect to servr support to solve this problem", 500);
    }

    private function _ajax_sender()
    {
        $errors = "";
        $jsondata = array(
            "success" => 0,
            "html" => "",
            "redir" => ""
        );

        // check if user existes

        $this->load->library("form_validation");
        $this->form_validation->set_rules("messagesms", "lang:label_email", "required");
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_error_delimiters("", "");
            $errors .= validation_errors();
        }
        if (!$errors) {
            $user = $this->_username;
            $password = $this->_password;
            $sender = $this->_username;
            $numbers = post("mobnumber");
            $messagesms = post("messagesms");
            $mess = str_replace(" ", "%20", $messagesms);
            $this->Newsletter_model->insert_sms(array("sms_To" => $numbers, "sms_Message" => $messagesms, "sms_CreatedBy" => $this->user->user_id, "sms_Created" => date("Y-m-d H:i:s")));
            // %20
            $send = "http://sms4world.net/smspro/sendsms.php?user=" . $user . "&password=" . $password . "&numbers=" . $numbers . "&sender=" . $sender . "&message=" . $mess . "&lang=ar";
            $return_status = file_get_contents($send);
            $errors = substr($return_status, 0, 1) == 1 ? langline("Success_Sms") : langline("Error");
            //$errors = langline("Success_Sms");
            //save email into database.
            //$data['mn_email'] = post("messagesms");
            //$id = $this->model->save_email($data);
        }
        $jsondata['html'] = $errors;
        echo $errors;
    }

    function load_ajax_races($_FID=0)
    {
        $races = $this->Races_Model->get_all_byevent($_FID);
        ?>
        <select name="rqst_NumRace" name="rqst_NumRace" class="form-control input-medium">
            <?php if ($races): foreach ($races["data"] as $v): ?>
                <option
                    value="<?php echo $v->rac_ID; ?>"><?php echo $v->rac_Sort; ?>
                    - <?php langline("Race") ?> :
                    <?php echo $v->rac_Name; ?> | <?php echo langline("rqst_AgeGroup") ?> : <?php echo $v->rac_Age; ?>
                    | <?php echo langline("rqst_Gender") ?> : <?php echo $v->rac_Gender; ?></option>
            <?php
            endforeach; endif;
            ?>
        </select>
    <?php
    }

    public function getphone($phone = 0)
    {
        $_res = $this->model->getbyphone($phone);
        if ($_res && $_res->UR_Phone)
            echo('
        <div class="alert alert-danger">
        <strong>خطأ!</!</strong> رقم الهاتف مسجل من قبل وبالتالي لايمكن قبول الطلب.
        </div>');
    }

    public function getnationalid($nationalid = 0)
    {
        $_res = $this->model->getbynationalid($nationalid);
        if ($_res && $_res->UR_NationalID)
            echo('
        <div class="alert alert-danger">
        <strong>خطأ!</strong> الرقم المدني مسجل من قبل وبالتالي لايمكن قبول الطلب.
        </div>');
    }

    private function do_upload($file_name = "userfile")
    {
        $this->load->library("upload");
        $this->load->library('image_lib');
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/requests";
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $config['max_size'] = '4400000'; //  Max upload size  5 MB
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $image_data = $this->upload->data();
                $configer = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'maintain_ratio' => TRUE,
                    'width' => 1000,
                    'height' => 1000,
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
                return $image_data;
            }
        } else {
            return false;
        }
    }

}
