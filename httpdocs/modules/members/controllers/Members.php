<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Members extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Members_Model", 'model');
    }

    public function index($offset = 0) {
        $this->data['title'] = $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_home($offset, $limit);
        $this->data['content'] = "members/index";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("members"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

}
