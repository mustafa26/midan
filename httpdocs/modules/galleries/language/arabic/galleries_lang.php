<?php 
$lang['galleries_gallery_title'] = "مكتبة الصور";
$lang['galleries_gallery_header'] = "مكتبة الصور";
$lang['galleries_link'] = "مكتبة الصور";
$lang['Heritage_Village'] = "القرية التراثية";
$lang['cats_name'] = "اسم التصنيف";
$lang['gallery_Title'] = "عنوان الصفحة";
$lang['gallery_SubTitle'] = "عنوان الصفحة الفرعي";
$lang['gallery_Content'] = "محتوى الصفحة";    
$lang['gallery_MetaDescr'] = "وصف محتوى الصفحة";
$lang['gallery_MetaKeywords'] = "الكلمات الدليلية";
$lang['gallery_HeaderPhoto'] = "صورة";
$lang['Select_File'] = "اختر ملف"; 
$lang['Tab_Settings'] = "إعدادات الصفحة";
$lang['Name'] = "الاسم";
$lang['project_status'] = "الحالة";
$lang['project_description'] = "وصف";
$lang['project_sub_tasks_num'] = "%s مهمة";

//Messages
$lang['success_categories_created'] = "تم إنشاء التصنيف بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات التصنيف بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات التصنيف ";
$lang['success_project_deleted'] = "تم حذف التصنيف بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف التصنيف , حاول مرة أخرى بعد تحديث الصفحة";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_galleries'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_page'] = "تحديث بيانات الصفحة";
$lang['log_delete_project'] = "حذف مشروع";
