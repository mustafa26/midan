<?php

/* * *******************************************
 *  Project     : Galleries Management
 *  File        : Galleries_model.php
 *  Created at  : 11/10/15 - 12:12 PM  
 * ******************************************* */

class Galleries_Model extends CI_Model {

    private $table_cats = "galleries_cats";
    private $table = "galleries";

    function __construct() {
        parent :: __construct();
    }

    /**
     * @param int $group
     * @param int $offset
     * @param int $limit
     * @return mixed
     */
    function get_all_cats($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("cat_Year,cat_created","DESC");
        $Q = $this->db->get_where($this->table_cats, array("cat_deleted" => 0));
        $total = $this->db->get_where($this->table_cats, array("cat_deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_home_cats($t="",$offset = 0, $limit = 100)
    { //,$y=0
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("cat_created", "DESC");
        $Q = $this->db->get_where($this->table_cats, array("cat_Type" => $t,"cat_status" => 1,"cat_deleted" => 0));//,"cat_Year" => $y
        $total = $this->db->get_where($this->table_cats, array("cat_Type" => $t,"cat_status" => 1,"cat_deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_cats($id = 0) {
        $Q = $this->db->get_where($this->table_cats, array("cat_id" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert_cats($data) {
        $this->db->insert($this->table_cats, $data);
        return $this->db->insert_id();
    }

    function update_cats($id, $data) {
        $this->db->update($this->table_cats, $data, array("cat_id" => $id));
        return $this->db->affected_rows();
    }

    function delete_cats($id = 0, $data) {
        $this->db->update($this->table_cats, $data, array("cat_id" => $id));
        return $this->db->affected_rows();
    }

    function get_all($id = 0, $offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
		$this->db->order_by("gallery_Created","DESC");
        $Q = $this->db->get_where($this->table, array("gallery_ParentID" => $id,"gallery_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("gallery_ParentID" => $id,"gallery_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get($id = 0) {
        $Q = $this->db->get_where($this->table, array("gallery_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $this->db->update($this->table, $data, array("gallery_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id = 0, $data) {
        $this->db->update($this->table, $data, array("gallery_ID" => $id));
        return $this->db->affected_rows();
    }

}
