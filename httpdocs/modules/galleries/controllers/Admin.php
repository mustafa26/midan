<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("Galleries_Model");
        $this->lang->load("galleries", $this->languages[$this->user_lang]);
        $this->model = new Galleries_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("galleries_status");
    }

    function index($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("galleries_gallery_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_cats($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_galleries");
        $this->data['content'] = "galleries/cats";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("galleries"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    private function _validate_cats() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("cat_name[ar]", langline("cat_name") . "(عربي)", "required|trim|max_length[255]");
        $this->form_validation->set_rules("cat_name[en]", langline("cat_name") . "(English)", "required|trim|max_length[255]");


        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add() {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add();
            exit;
        }

        $this->data['title'] = langline("galleries_gallery_title") . " - " . $this->site->site_name;
        $this->data['content'] = "galleries/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add() {
        $this->_validate_cats();
        $upload_data = $this->do_upload("cat_image");
        if (!$this->errors) {
            $data['cat_name'] = serialize(post("cat_name"));
            if ($upload_data) {
                $data['cat_image'] = $upload_data['file_name'];
            }
            $data['cat_created_by'] = $this->user->user_id;
            $data['cat_Year'] = post("cat_Year");
            $data['cat_Type'] = post("cat_Type");
            $data['cat_created'] = date("Y-m-d H:i:s");
            $data['cat_status'] = post("cat_status");

            if ($project_id = $this->model->insert_cats($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("galleries"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("galleries?open=form"));
    }

    function edit($id = 0) {
        if (!check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }
        $cat = $this->model->get_cats($id);
        $this->data['title'] = langline("galleries_gallery_title") . " - " . $this->site->site_name;
        log_add_user_action($this->user->user_id, $id, "show", "log_show_galleries");
        $this->data['content'] = "galleries/edit_cat";
        $this->data['data'] = $cat;
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0) {
        $this->_validate_cats();
        $upload_data = $this->do_upload("cat_image");
        if (!$this->errors) {
            $data['cat_name'] = serialize(post("cat_name"));
            $data['cat_Year'] = post("cat_Year");
            $data['cat_Type'] = post("cat_Type");
            if ($upload_data) {
                $data['cat_image'] = $upload_data['file_name'];
            }
            $data['cat_status'] = post("cat_status");
            if ($this->model->update_cats($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_galleries");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("galleries"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("galleries/edit/$id"));
    }

    function delete($id) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['cat_deleted'] = 1;
        if ($this->model->delete_cats($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("galleries"));
    }

    function cats($id = 0, $offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("galleries_gallery_title") . " - " . $this->site->site_name;
        $limit = 20;
        $cat = $this->model->get_cats($id);
        $result = $this->model->get_all($id, $offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_galleries");
        $this->data['content'] = "galleries/index";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['cats'] = $cat;
        $this->data['pagination'] = $this->pagination->pager(admin_url("galleries/cats/$id/"), $result['total'], $limit, 5);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        foreach ($this->languages as $key => $value) {
            $this->form_validation->set_rules("gallery_Title[$key]", langline("gallery_Title") . $value, "required|trim|max_length[255]");
        }

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function addgallery($fid = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_addgallery($fid);
            exit;
        }

        $this->data['title'] = langline("galleries_gallery_title") . " - " . $this->site->site_name;
        $cat = $this->model->get_cats($fid);
        $this->data['cats'] = $cat;
        $this->data['content'] = "galleries/add";
        $this->template->render("template", $this->data);
    }

    private function _do_addgallery($fid = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("gallery_HeaderPhoto");
        if (!$this->errors) {
            $data['gallery_ParentID'] = $fid;
            $data['gallery_Title'] = serialize(post("gallery_Title"));
            $data['gallery_Created_By'] = $this->user->user_id;
            $data['gallery_Created'] = date("Y-m-d H:i:s");
            $data['gallery_Active'] = post("gallery_Active") ? 1 : 0;

            if ($upload_data) {
                $data['gallery_HeaderPhoto'] = $upload_data['file_name'];
            }
            if ($project_id = $this->model->insert($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("galleries/cats/$fid"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("galleries/addgallery/$fid"));
    }

    function editgallery($fid = 0, $id = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_updategallery($fid, $id);
            exit;
        }

        $this->data['title'] = langline("galleries_gallery_title") . " - " . $this->site->site_name;
        $cat = $this->model->get_cats($fid);
        $this->data['cats'] = $cat;
        $this->data['content'] = "galleries/edit";
        $this->data['gallery'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    private function _do_updategallery($fid = 0, $id = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("gallery_HeaderPhoto");
        if (!$this->errors) {
            $data['gallery_Title'] = serialize(post("gallery_Title"));
            $data['gallery_Active'] = post("gallery_Active") ? 1 : 0;

            if ($upload_data) {
                $data['gallery_HeaderPhoto'] = $upload_data['file_name'];
                if (is_file("uploads/galleries/" . post("gallery_HeaderPhoto_old")))
                    unlink("uploads/galleries/" . post("gallery_HeaderPhoto_old"));
                if (is_file("uploads/galleries/thumb/" . post("gallery_HeaderPhoto_old")))
                    unlink("uploads/galleries/thumb/" . post("gallery_HeaderPhoto_old"));
            }

            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_galleries");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("galleries/cats/$fid"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("galleries/editgallery/$fid/$id"));
    }

    function deletegallery($fid = 0, $id = 0) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['gallery_Deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("galleries/cats/$fid"));
    }

    private function do_upload($file_name = "userfile", $file_module = "galleries", $resize = true, $file_type = "", $allowed_types = 'gif|jpg|jpeg|png') {
        $this->load->library("upload");
        $this->load->library('image_lib');
        # TODO Resize Image
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/" . $file_type;
            $config['allowed_types'] = $allowed_types;
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $config['max_size'] = '3000000'; //3MB
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $_img_file = $this->upload->data();
                $config_thumb['image_library'] = 'gd2';
                $config_thumb['source_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/$file_type" . $_img_file["file_name"];
                $config_thumb['create_thumb'] = FALSE;
                $config_thumb['maintain_ratio'] = TRUE;
                $config_thumb['width'] = 1100;
                $config_thumb['height'] = 800;
                $config_thumb['new_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/";
                $this->image_lib->initialize($config_thumb);
                $this->image_lib->resize();
                if ($resize) {
                    $config_thumb['image_library'] = 'gd2';
                    $config_thumb['source_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/$file_type" . $_img_file["file_name"];
                    $config_thumb['create_thumb'] = FALSE;
                    $config_thumb['maintain_ratio'] = TRUE;
                    $config_thumb['width'] = 250;
                    $config_thumb['height'] = 250;
                    $config_thumb['new_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/thumb/";
                    $this->image_lib->initialize($config_thumb);
                    $this->image_lib->resize();
                }
                return $_img_file;
            }
        } else {
            return false;
        }
    }

}
