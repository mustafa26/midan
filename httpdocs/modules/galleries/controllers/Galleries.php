<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Galleries extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Galleries_Model", 'model');
    }

    public function index($t = "g", $y = 0) {
        $this->data['title'] = $this->site->site_name;
        $result = $this->model->get_all_home_cats($t);//, $y
        $this->data['data'] = $result['data'];
        //if ($y) {
         //   $this->data['year'] = $y;
            $this->data['content'] = "galleries/cats";
       // } else
       //     $this->data['content'] = "galleries/index";
        $this->template->render("template", $this->data);
    }

    public function view($t = "g", $id = 0) {
        $this->data['title'] = $this->site->site_name;
        $result = $this->model->get_all($id, 0, 100);
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['cats'] = $this->model->get_cats($id);
        $this->data['content'] = "galleries/all";
        $this->template->render("template", $this->data);
    }

}
