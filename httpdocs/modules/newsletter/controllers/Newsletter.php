<?php

/**
 * news Letter Controller
 */
class Newsletter extends MY_Controller {

    function __construct() { 
        parent::__construct();
        $this->lang->load("newsletter", $this->languages[$this->user_lang]);
        $this->load->helper('email');
        $this->load->model("Newsletter_model", "model");
    }

    /**
     * join to news letter
     */
    
    function index() {
        redirect(site_url());
    }

    function register() {
        if ($this->is_ajax()) {
            $this->_ajax_register();
            return;
        }

        show_error("Ajax Requiest only supported connect to servr support to solve this problem", 500);
    }

    private function _ajax_register() {
        $errors = "";
        $jsondata = array(
            "success" => 0,
            "html" => "",
            "redir" => ""
        );

        // check if user existes

        $this->load->library("form_validation");
        //$this->form_validation->set_rules("name", "lang:label_name", "required|trim|max_lenght[50]");
        $this->form_validation->set_rules("email", "lang:label_email", "required|valid_email|trim");
        if ($this->model->check_email(post("email"))) {
            $errors .= langline("error_email_alredy_existes");
        }
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_error_delimiters("", "");
            $errors .= validation_errors();
        }
        if (!$errors) {
            //save email into database.
            $data['mn_email'] = post("email");
            $id = $this->model->save_email($data);
            $jsondata['success'] = 1;
            $errors = langline("success_thanks_to_join_newsletter");

            // send email to user.
            //$vars['email'] = post("email");
            //$vars['id'] = $id;
            //$message = view("emails/join_success", $vars, true);
            //send_email(post("email"), $this -> site_name . " " . langline("newsletter"), $message);
            //$this->sendmail(post('email'), $this->site_name, $message, array($this->site_email, $this->site_name));
        }
        $jsondata['html'] = $errors;
        echo $errors;
    }

    function unsubscribe($id = 0, $email = "") {
        if ($id && $email) {
            $do = $this->model->unsubscribe($id, $email);
            if (!$do) {
                redirect(base());
            }
            $data['title'] = $this->site_name;
            $data['content'] = "unsubscribe_success";
            $data['email'] = $email;
            $this->template->render("template", $data);
        } else {
            redirect(base());
        }
    }

}
