<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Admin Panel For News Module
 */
class Admin extends Admin_Controller {

    var $jsondata;
    var $errors = "";

    function __construct() {
        parent::__construct();
        $this->lang->load("newsletter", $this->languages[$this->user_lang]);
        $this->load->model("Newsletter_model", "model");
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("pages_status");

        $this->jsondata['success'] = 0;
        $this->jsondata['html'] = "";
        $this->jsondata['redir'] = base() . "newsletter/admin/index/" . segment(4);
    }

    function index($offset=0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = $this->site->site_name;
        //log_add_user_action($this->user->user_id, 0, "show", "log_show_news"); 
        $limit = 20;
        $result = $this->model->get_all($offset, $limit);
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("news"), $result['total'], $limit, 3);
        $this->data['content'] = "newsletter/all";
        $this->template->render("template", $this->data);
    }

    function new_email($fid = 0, $offset = 0) {

        if (post() || $this->session->userdata("email_data")) {
            if (post()) {

                $this->session->set_userdata(array("email_data" => post()));
            }
            $this->send_emails($fid, $offset);
            //exit ;
        } else {

            $mtype = get("mtype") == "newsletter" ? "newsletter" : "users";
            $data['title'] = $this->site->site_name . " {lang:title_send_newsletter}";
            $data['content'] = "admin/new_email";
            $data['type'] = $mtype;
            $data['email_theme'] = view("emails/newsletter", '', TRUE);
            $this->template->render("template", $data);
        }
    }

    function send_emails($fid = 0, $offset = 0) {

        $email = $this->session->userdata("email_data");
        $subject = $email['subject'];
        $message = $email['message'];
        $from = array($this->site_email, $this->site->site_name);

        $mtype = "newsletter";
        // check members type list
        // this line to get only newsletter or users from url
        $mtype = get("mtype") == "newsletter" ? "newsletter" : "users";

        $users = array();

        $limit = 100;

        if ($mtype == "newsletter") {
            $users = $this->model->newsletter_list($offset, $limit);
        } else {
            $users = $this->model->registerd_list($offset, $limit);
        }
        if ($users) {

            foreach ($users['data'] as $user) {

                /*
                  $this -> email -> to($user->email);
                  $this -> email -> subject($subject);
                  $this -> email -> message($message);
                  $this -> email -> send(); */
                $this->sendmail($user->email, $subject, $message, $from);
                $offset++;
            }
        }

        if ($count >= $users['total'])
            $this->session->set_userdata("email_data", FALSE);
        $result = array(
            "sent" => $offset,
            "total" => $users['total'],
            //"errors"=>$this -> email -> print_debugger(),
            "offset" => $offset
        );

        $data['title'] = $this->site->site_name . "| {lang:title_new_email} ";
        $data['data'] = (object) $result;
        $data['content'] = "admin/sent_result";
        $this->template->render("template", $data);
    }

    private function _do_action() {

        $action = post("action");

        switch ($action) {
            case 'enable' :
                $this->model->update_newsletter_status(post("item_id"), 1);
                set_message("<p>{lang:success_items_enabled}</p>", "success");
                break;

            case 'disable' :
                $this->model->update_newsletter_status(post("item_id"), 0);
                set_message("<p>{lang:success_items_disabled}</p>", "success");
                break;

            case 'delete' :
                $this->model->delete_newsletter_members(post("item_id"));
                set_message("<p>{lang:success_items_deleted}</p>", "success");
                break;
        }
    }

    function delete($id = 0) {
        $this->model->delete($id); 
        redirect(admin_url("newsletter"));
    }
    function active($id = 0) {
        $this->model->active($id); 
        redirect(admin_url("newsletter"));
    }
    function inactive($id = 0) {
        $this->model->inactive($id); 
        redirect(admin_url("newsletter"));
    }
    
    function delete_old($fid = 0, $id = 0) {
        $this->model->delete_newsletter_members(array($id));
        set_message("<p>{lang:success_items_deleted}</p>", "success");
        $mtype = "newsletter";
        // check members type list
        // this line to get only newsletter or users from url
        $mtype = get("mtype") == "users" ? "users" : "newsletter";
        redirect(base() . "newsletter/admin/index/$fid?mtype=$mtype");
    }

    private function _validate() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("name", "lang:label_name", "required|trim|max_lenght[150]");
        $this->form_validation->set_rules("email", "lang:label_email", "required|trim|valied_email|max_lenght[150]");

        if (post("email") && post("email") != post("hidden_email")) {
            if ($this->_check_email()) {
                $this->errors .= "<div class='message error'><p>" . langline("error_email_alredy_existes") . "</p></div>";
            }
        }

        if ($this->form_validation->run() == FALSE) {
            $this->errors .= "<div class='message error'>" . validation_errors() . "</p></div>";
            return FALSE;
        }
        return TRUE;
    }

    function add($fid = 0) {

        if (post()) {
            $this->is_ajax() ? $this->_ajax_add($fid, $id) : $this->_do_add($fid, $id);
            exit;
        }

        $data['title'] = $this->site->site_name . "| {lang:title_add_newsletter_member}";
        $data['content'] = "admin/add";
        $this->template->render("template", $data);
    }

    function edit($fid = 0, $id = 0) {

        if (post()) {
            $this->is_ajax() ? $this->_ajax_update($fid, $id) : $this->_do_update($fid, $id);
            exit;
        }

        $mtype = get("mtype") == "users" ? "users" : "newsletter";
        $data['title'] = $this->site->site_name . "| {lang:title_edit_newsletter_member}";
        $data['data'] = $this->model->get_member($id);
        $data['content'] = "admin/edit";
        $this->template->render("template", $data);
    }

    private function _ajax_add($fid = 0) {
        $this->_validate();
        if (!$this->errors) {
            $data['mn_name'] = post('name');
            $data['mn_email'] = post('email');
            $data['mn_created'] = strtotime("now");
            $data['mn_active'] = post("active");
            $data['mn_active_code'] = random_string();
            if ($this->model->insert_member($data)) {
                $this->jsondata['success'] = 1;
                $this->errors .= "<div class='message success'><p>" . langline("success_data_saved") . "</p></div>";
                $action = get('cat') ? "?cat=" . get("cat") : "";
                $this->jsondata['redir'] = base() . "newsletter/admin/index/$fid";
                set_message("<p>" . langline("success_data_saved") . "</p>", "success");
            } else {
                $this->errors = "<div class='message error'><p>" . langline("error_data_not_saved") . "</p></div>";
                set_message($this->errors, "error");
            }
        }
        $this->jsondata['html'] = $this->errors;
        echo json_encode($this->jsondata);
    }

    private function _do_add($fid = 0) {
        $this->_validate();

        if (!$this->errors) {

            $data['mn_name'] = post('name');
            $data['mn_email'] = post('email');
            $data['mn_created'] = strtotime("now");
            $data['mn_active'] = post("active");
            $data['mn_active_code'] = random_string();

            if ($this->model->insert_member($data)) {
                set_message(langline("success_data_saved"), "success");
                redirect(base() . "newsletter/admin/index/$fid");
            } else {
                set_message("<p>" . langline("error_data_not_saved") . "</p>", "error");
            }
        } else {
            set_message($this->errors, "error");
            $_POST = post();
            redirect(base() . "newsletter/admin/index/$fid");
        }
    }

    private function _ajax_update($fid = 0, $id = 0) {

        $this->_validate();

        if (!$this->errors) {

            $data['mn_name'] = post('name');
            $data['mn_email'] = post('email');
            $data['mn_active'] = post("active");
            if ($this->model->update_member($id, $data)) {

                $this->jsondata['success'] = 1;
                $this->errors .= "<p>" . langline("success_data_saved") . "</p>";
                $action = get('cat') ? "?cat=" . get("cat") : "";
                $this->jsondata['redir'] = base() . "newsletter/admin/index/$fid";
            } else {
                $this->errors .= "<div class='message error'><p>" . langline("error_data_not_saved") . "</p></div>";
            }
        }
        $this->jsondata['html'] = $this->errors;
        set_message($this->errors, "success");
        echo json_encode($this->jsondata);
    }

    private function _do_update($fid = 0, $id = 0) {
        $this->_validate();
        if (!$this->errors) {
            $data['mn_name'] = post('name');
            $data['mn_email'] = post('email');
            $data['mn_active'] = post("active");
            if ($this->model->update_member($id, $data)) {

                set_message("<p>" . langline("success_data_saved") . "</p>", "success");

                redirect(base() . "newsletter/admin/index/$fid");
            } else {
                set_message("<p>" . langline("error_data_not_saved") . "</p>", "error");
            }
        } else {
            set_message($this->errors);
            $_POST = post();

            redirect(base() . "newsletter/admin/edit/$fid/$id");
        }
    }

    function _check_email() {

        return $this->model->check_email(post("email"));
    }
            

}
