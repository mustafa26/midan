<?php
$lang['title_newsletter']					="القائمة البريدية";
$lang['title_newsletter_members']			="مشتركي القائمة البريدية";
$lang['title_add_newsletter_member']		="إضافة مشترك للقائمة البريدية";
$lang['title_edit_newsletter_member']		="تعديل بيانات مشترك في القائمة البريدية";
$lang['error_email_alredy_existes']			="خطأ : البريد الالكتروني مسجل من قبل";
$lang['success_thanks_to_join_newsletter']	="شكرا على انضمامك للقائمة البريدية ";
$lang['label_email']						="البريد الالكتروني";
$lang['thank_you_for_join_us']				="نشكرك على انضمامك لقائمتنا البريدية";
$lang['your_email_unsubscribed']			="تم إلغاء اشتراكك في القائمة البريدية ";
$lang['label_join_date']					="تاريخ الانضمام";
$lang['title_members_list']					="قائمة الأعضاء";
$lang['title_send_letter']					="مراسلة";
$lang['title_send_newsletter']				="إرسال قائمة بريدية";
