<?php

/**
 * News letter Model
 */
class Newsletter_model extends CI_Model {

    private $modeltbl = "newsletter";
    private $modelsms = "sms";

    function __construct() {
        parent::__construct();
    }

    function get_all($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("mn_created", "desc");
        $Q = $this->db->get_where($this->modeltbl, array("mn_Deleted" => 0));
        $total = $this->db->get_where($this->modeltbl, array("mn_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function check_email($email) {
        $Q = $this->db->get_where($this->modeltbl, array("mn_email" => $email));
        return ($Q->num_rows() > 0) ? TRUE : FALSE;
    }

    function save_email($data) {

        $data['mn_created'] = strtotime("now");
        $this->db->insert($this->modeltbl, $data);
        return $this->db->insert_id();
    }

    /**
     * get all news letter members
     * display for Admin users only
     *
     */
    function newsletter_members($offset = 0, $limit = 0) {

        $Q = $this->db->get($this->modeltbl, $limit, $offset);
        $total = $this->db->select("count(*) as num_rows")->get($this->modeltbl)->num_rows();
        $data['data'] = $Q->result();
        $data['total'] = $total;
        return ($Q->num_rows() > 0) ? $data : FALSE;
    }

    /**
     * get all news letter members
     * display for Admin users only
     *
     */
    function newsletter_list($offset = 0, $limit = 0) {
        $this->db->select("mn_id as id,mn_name as name,mn_email as email,mn_created as created,mn_active as active");
        $Q = $this->db->get($this->modeltbl, $limit, $offset);
        $total = $this->db->select("mn_id")->get($this->modeltbl)->num_rows();
        $data['data'] = $Q->result();
        $data['total'] = $total;
        return ($Q->num_rows() > 0) ? $data : FALSE;
    }

    /**
     * get all news letter members
     * display for Admin users only
     *
     */
    function registerd_list($offset = 0, $limit = 0) {
        $this->db->select("id,first_name as name ,email,created_on as created,active");
        $Q = $this->db->get("users", $limit, $offset);
        $total = $this->db->select("id")->get("users")->num_rows();
        $data['data'] = $Q->result();
        $data['total'] = $total;
        return ($Q->num_rows() > 0) ? $data : FALSE;
    }

    /**
     * Get news letter member
     * @param (int) $id member id
     * @return (object) stdClassObject
     */
    function get_member($id = 0) {
        $Q = $this->db->get_where($this->modeltbl, array("mn_id" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : FALSE;
    }

    function insert_member($data) {
        $this->db->insert($this->modeltbl, $data);
        return $this->db->insert_id();
    }

    /**
     * Update member
     */
    function update_member($id = 0, $data) {
        $this->db->update($this->modeltbl, $data, array("mn_id" => $id));
        return $this->db->affected_rows();
    }

    function update_newsletter_status($ids = array(), $status = 1) {
        if (is_array($ids)) {
            foreach ($ids as $id) {
                $this->db->update($this->modeltbl, array("mn_active" => $status), array("mn_id" => $id));
            }
            return TRUE;
        }
        return FALSE;
    }

    function delete($id = 0) {
        $this->db->update($this->modeltbl, array("mn_Deleted" => 1), array("mn_id" => $id));
        return TRUE;
    }
    function active($id = 0) {
        $this->db->update($this->modeltbl, array("mn_active" => 1), array("mn_id" => $id));
        return TRUE;
    }
    function inactive($id = 0) {
        $this->db->update($this->modeltbl, array("mn_active" => 0), array("mn_id" => $id));
        return TRUE;
    }

    function delete_newsletter_members($ids = array()) {
        if (is_array($ids)) {
            foreach ($ids as $id) {
                $this->db->delete($this->modeltbl, array("mn_id" => $id));
            }
            return TRUE;
        }
        return FALSE;
    }

    function unsubscribe($id, $email) {
        $this->db->delete($this->modeltbl, array(
            "id" => $id,
            "email" => $email
        ));
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    function check_id($id) {
        $Q = $this->db->get_where($this->modeltbl, array("id" => $email));
        return ($Q->num_rows() > 0) ? TRUE : FALSE;
    }

    function admin_search($word = "", $offset = 0, $limit = 30) {
        helper("email");
        //check if user search by email
        $field = valid_email($word) ? "mn_email" : "mn_name";
        $this->db->like($field, $word);
        $Q = $this->db->get($this->modeltbl);
        $this->db->like($field, $word);
        $total = $this->db->select("count(*) as num_rows")->get($this->modeltbl)->num_rows();
        $data['data'] = $Q->result();
        $data['total'] = $total;
        return ($Q->num_rows() > 0) ? $data : FALSE;
    }


    function insert_sms($data) {
        $this->db->insert($this->modelsms, $data);
        return $this->db->insert_id();
    }

}
