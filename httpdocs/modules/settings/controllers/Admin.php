<?php

/*********************************************
 *  Project     : Projects Management
 *  File        : admin.php
 *  Created at  : 5/12/15 - 1:10 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
class Admin extends Admin_Controller
{
    private $model;
    private $rules = array();

    function __construct()
    {
        parent:: __construct();
        $this->load->model("Settings_model");
        $this->model = new Settings_model();
        $this->lang->load("settings", $this->languages[$this->user_lang]);
        if(!check_user_permission(current_module(),"show")){
            redirect(admin_url("permissions/denied"));
        }

    }

    function index()
    {

        $this->data['title'] ="";//$this->site->site_name. "| Settings";

        $this->data['data'] = $this->model->all();

        $this->data['content'] = "settings/list";
        log_add_user_action($this->user->user_id,0,"edit","log_show_settings");
        $this->template->render("template", $this->data);

    }

    function setDefaultSettings($id){

        if($this->is_ajax() && check_user_permission(current_module(),"edit")){
            $this->model->set_default_settings($id);
            log_add_user_action($this->user->user_id,$id,"edit","log_set_default_settings");

            $this->jsonData['success']=1;
            $this->jsonData['html']=langline("success_settings_updated");
            echo json_encode($this->jsonData);
            exit;
        }

        show_404();
    }


    private function _validate()
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("settings_name", langline("setting_name"), "required|trim");
        $this->form_validation->set_rules("site_url", langline("settings_site_url"), "required|trim|max_length[150]");
        $this->form_validation->set_rules("site_name[{$this->user_lang}]", langline("settings_site_name"), "required|trim|max_length[150]");
        $this->form_validation->set_rules("site_email", langline("settings_site_email"), "required|trim|max_length[150]");
        $this->form_validation->set_rules("mail_func", langline("settings_mail_function"), "required");

        if (post("mail_func") == "smtp") {

            $this->form_validation->set_rules("smtp_server", langline("settings_smtp_server"), "required|trim|max_length[150]");
            $this->form_validation->set_rules("smtp_port", langline("settings_smtp_port"), "required|trim|max_length[4]");
            $this->form_validation->set_rules("smtp_user", langline("settings_smtp_user"), "required|trim|max_length[150]");
            $this->form_validation->set_rules("smtp_password", langline("settings_password"), "required|trim|max_length[150]");

        }

        $this->form_validation->set_rules("social_facebook", langline("settings_social_facebook"), "trim|max_length[150]");
        $this->form_validation->set_rules("social_twitter", langline("settings_social_twitter"), "trim|max_length[150]");
        $this->form_validation->set_rules("social_youtube", langline("settings_social_youtube"), "trim|max_length[150]");
        $this->form_validation->set_rules("social_gplus", langline("settings_social_gplus"), "trim|max_length[150]");
        $this->form_validation->set_rules("social_linkedin", langline("settings_social_linkedin"), "trim|max_length[150]");
        $this->form_validation->set_rules("social_instagram", langline("settings_social_instagram"), "trim|max_length[150]");
        $this->form_validation->set_rules("contact_email", langline("settings_contact_email"), "trim|max_length[150]");
        $this->form_validation->set_rules("contact_phones", langline("settings_contact_phones"), "trim|max_length[150]");
        $this->form_validation->set_rules("contact_info", langline("settings_contact_information"), "trim|max_length[150]");

        $this->form_validation->set_rules("seo_description", langline("settings_meta_description"), "trim|max_length[255]");
        $this->form_validation->set_rules("seo_tags", langline("settings_meta_tags"), "trim|max_length[200]");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();

            return false;
        }
        return true;

    }


    function add()
    {
        if(!check_user_permission(current_module(),"add")){
            redirect(admin_url("permissions/denied"));
        }
        if(post()){
            $this->_do_add();
            exit;
        }
        $this->data['title'] = "Add settings";

        $this->data['content'] = "settings/add";

        $this->template->render("template", $this->data);
    }


    private function _do_add()
    {

        $this->_validate();

        $upload_data =  $this->do_upload("site_logo");
        // no errors found in validation
        if (!$this->errors) {

            $data['setting_name']       = post("settings_name");
            $data['site_url']           = post("site_url");
            $data['site_name']          = serialize(post("site_name"));
            $data['site_email']         = post("site_email");

            if($upload_data){
                $data['site_logo']         = $upload_data['file_name'];
            }

            $data['mail_function']      = post("mail_func");
            $data['smtp_server']        = post("smtp_server");
            $data['smtp_port']          = post("smtp_port");
            $data['smtp_user']          = post("smtp_user");
            $data['smtp_password']      = post("smtp_password");
            $data['site_closed']        = post("site_closed");
            $data['closed_message']     = post("closed_message");
            $data['is_default']         = post("is_default");
            $data['meta_description']   = post("seo_description");
            $data['meta_tags']          = post("seo_tags");
            $data['contact_email']      = post("contact_email");
            $data['contact_phones']     = post("contact_phones");
            $data['contact_information']= post("contact_information");
            $data['default_language']   = post("default_language");
            $data['social_facebook']    = post("social_facebook");
            $data['social_twitter']     = post("social_twitter");
            $data['social_youtube']     = post("social_youtube");
            $data['social_gplus']       = post("social_gplus");
            $data['social_instagram']   = post("social_instagram");
            $data['social_linkedin']    = post("social_linkedin");
            $data['created']            = date("Y-m-d H:i:s");

            if ($id=$this->model->insert($data)) {
                log_add_user_action($this->user->user_id,$id,"add","log_add_settings");
                $this->set_message(langline("success_data_inserted"), "success");
                redirect(admin_url("settings"));
            }

        }
        else {
            $this->keep_data();
            $this->set_message($this->errors, "error");
            redirect(admin_url("settings/add"));
        }
    }


    function edit($id = 0)
    {
        if(!check_user_permission(current_module(),"edit")){
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_update($id);
            exit;
        }

        $this->data['title']    = "Add settings";
        $this->data['data']     = $this->model->get($id);
        $this->data['content']  = "settings/edit";
        $this->template->render("template", $this->data);
    }


    private function _update($id = 0)
    {
        $this->_validate();

        $upload_data =  $this->do_upload("site_logo");
        // no errors found in validation
        if (!$this->errors) {
            $data['setting_name']       = post("settings_name");
            $data['site_url']           = post("site_url");
            $data['site_name']          = serialize(post("site_name"));
            $data['site_email']         = post("site_email");

            if($upload_data){
                $data['site_logo']         = $upload_data['file_name'];
            }

            $data['mail_function']      = post("mail_func");
            $data['smtp_server']        = post("smtp_server");
            $data['smtp_port']          = post("smtp_port");
            $data['smtp_user']          = post("smtp_user");
            $data['smtp_password']      = post("smtp_password");
            $data['site_closed']        = post("site_closed");
            $data['closed_message']     = post("closed_message");

//            $data['is_default']         = post("is_default");

            $data['meta_description']   = post("seo_description");
            $data['meta_tags']          = post("seo_tags");
            $data['contact_email']      = post("contact_email");
            $data['contact_phones']     = post("contact_phones");
            $data['contact_information']= post("contact_information");
            $data['default_language']   = post("default_language");
            $data['social_facebook']    = post("social_facebook");
            $data['social_twitter']     = post("social_twitter");
            $data['social_youtube']     = post("social_youtube");
            $data['social_gplus']       = post("social_gplus");
            $data['social_instagram']   = post("social_instagram");
            $data['social_linkedin']    = post("social_linkedin");

            $this->model->update($id,$data);
            log_add_user_action($this->user->user_id,$id,"edit","log_edit_settings");
                $this->set_message(langline("success_data_inserted"), "success");
                redirect(admin_url("settings"));
        }
        else {

            $this->keep_data();

            $this->set_message($this->errors, "error");

            redirect(admin_url("settings/edit/$id"));
        }
    }

    function delete($id=0){
        if(!check_user_permission(current_module(),"delete") || !check_user_permission(current_module(),"edit")){
            redirect(admin_url("permissions/denied"));
        }
        if($this->model->is_default($id)){
            $this->set_message(langline("error_settings_is_default"),"error");
            redirect(admin_url("settings"));
        }else{

            $this->model->delete($id);
            log_add_user_action($this->user->user_id,$id,"delete","log_delete_settings");
            $this->set_message(langline("success_settings_deleted"),"success");
            redirect(admin_url("settings"));

        }
    }

    function do_upload($file_name = "userfile")
    {
        $data = false;

        $this->load->library("upload");

        if(!empty($_FILES[$file_name]['name'])){
            $config['upload_path']      = rtrim(APPPATH.$this->config->item("upload_dir"), "/") ;

//            die($config['upload_path']);

            $config['allowed_types']    = 'gif|jpg|png';
            $config['encrypt_name']     = true;
            $config['file_ext_tolower'] = true;
            $this -> upload -> initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                return $this->upload->data();
            }
        }else{
            return false;
        }

    }
}