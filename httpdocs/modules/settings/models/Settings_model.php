<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : settings_model.php
 *  Created at  : 5/12/15 - 1:14 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 class Settings_model extends CI_Model{

     private $table="settings";

     function __construct(){
         parent :: __construct();
     }

     function all(){
         $Q=$this->db->get($this->table);
         return $Q->num_rows() > 0 ?$Q->result():false;
     }

     function get($id=0){
         $Q=$this->db->get_where($this->table,array("id"=>$id));
         return $Q->row();
     }

     function site(){
         $Q=$this->db->select("id,site_name,site_email,site_closed,closed_message,site_logo,meta_tags,meta_description,default_theme,default_language")
             ->get_where($this->table,array("is_default"=>1),1);
         return $Q->num_rows()?$Q->row():false;
     }

     function email(){
         $Q=$this->db->select("mail_function,smtp_server,smtp_username,smtp_password,smtp_port")
             ->get_where($this->table,array("is_default"=>1),1);
         return $Q->num_rows()?$Q->row():false;
     }

     function contact(){
         $Q=$this->db->select("contact_email,contact_phones,contact_information")
             ->get_where($this->table,array("is_default"=>1),1);
         return $Q->num_rows()?$Q->row():false;
     }

     function set_default_settings($id=0){
         $this->db->update($this->table,array("is_default"=>0));
         $this->db->update($this->table,array("is_default"=>1),array("id"=>$id));
         return $this->db->affected_rows();
     }

     function get_default_settings(){
         $Q=$this->db->get_where($this->table,array("is_default"=>1));
         return $Q->num_rows()>0?$Q->row():false;
     }

     function insert($data){
         $this->db->insert($this->table,$data);
         return $this->db->insert_id();
     }
     function update($id,$data){
         $this->db->update($this->table,$data,array("id"=>$id));
         return $this->db->affected_rows();
     }

     function is_default($id){
         $Q=$this->db->get_where($this->table,array("is_default"=>1,"id"=>$id),1);
         return $Q->num_rows()>0?true:false;
     }

     function delete($id){
         $this->db->delete($this->table,array("id"=>$id));
         return $this->db->affected_rows();
     }


 }