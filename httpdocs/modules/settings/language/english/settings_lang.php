<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : settings_lang.php
 *  Created at  : 5/14/15 - 9:27 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
$lang['settings_page_title']                = "Settings";
$lang['settings_name']                      = "Settings Name";
$lang['settings_tab_general']               = "General";
$lang['settings_tab_email']                 = "Email";
$lang['settings_tab_social']                = "Social";
$lang['settings_tab_contact']               = "Contact info";
$lang['settings_tab_seo']                   = "SEO";
$lang['settings_site_name']                 = "Site Name";
$lang['settings_site_email']                = "Site Email";
$lang['settings_site_url']                  = "Site URL";
$lang['settings_site_logo']                 = "Site Logo";
$lang['settings_site_closed']               = "put site down?";
$lang['settings_created_at']                = "Created at";
$lang['settings_last_update']               = "Last Update";
$lang['settings_closed_message']            = "Closed Message";
$lang['settings_is_default']                = "Default Settings?";
$lang['settings_meta_description']          = "Meta Description";
$lang['settings_meta_tags']                 = "Meta Tags";
$lang['settings_default_language']          = "Default Language";
$lang['settings_default_theme']             = "Default Theme";
$lang['settings_mail_function']             = "Sending Method";
$lang['settings_mail_func_option']          = "mail";
$lang['settings_smtp_func_option']          = "SMTP";
$lang['settings_smtp_server']               = "SMTP Server";
$lang['settings_smtp_port']                 = "SMTP Port";
$lang['settings_smtp_user']                 = "SMTP User";
$lang['settings_password']                  = "SMTP Password";
$lang['settings_social_facebook']           = "Facebook Link";
$lang['settings_social_twitter']            = "Twitter Link";
$lang['settings_social_youtube']            = "Youtube Link";
$lang['settings_social_gplus']              = "Google Plus Link";
$lang['settings_social_linkedin']           = "Linked in link";
$lang['settings_social_instagram']          = "Instagram";
$lang['settings_contact_email']             = "Contact Email";
$lang['settings_contact_phones']            = "Contact Phones";
$lang['settings_contact_information']       = "Contact Information";
$lang['settings_seo_description']           = "SEO Description";
$lang['settings_seo_tags']                  = "SEO Tags";

// messages
$lang['error_settings_is_default']          = "Error: Not allowed to delete Default Settings";
$lang['success_settings_deleted']          = "Settings deleted successfully";

$lang['log_show_settings']                  = "Show All Available Settings";
$lang['log_edit_settings']                  = "Update Settings Row";
$lang['log_add_settings']                  = "Add a new Settings Row";
$lang['log_delete_settings']                  = "Delete Settings Row";
$lang['log_set_default_settings']                  = "Change Default Settings Row";