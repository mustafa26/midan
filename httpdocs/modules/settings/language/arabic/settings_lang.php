<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : settings_lang.php
 *  Created at  : 5/14/15 - 9:27 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
$lang['settings_page_title']                = "الإعدادات";
$lang['settings_name']                      = "إسم الإعدادات";
$lang['settings_tab_general']               = "عام";
$lang['settings_tab_email']                 = "البريد الإلكتروني";
$lang['settings_tab_social']                = "التواصل الاجتماعي";
$lang['settings_tab_contact']               = "بيانات الاتصال";
$lang['settings_tab_seo']                   = "محركات البحث (SEO)";
$lang['settings_site_name']                 = "اسم الموقع";
$lang['settings_site_email']                = "البريد الإلكتروني";
$lang['settings_site_url']                  = "رابط الموقع";
$lang['settings_site_logo']                 = "شعار الموقع";
$lang['settings_site_closed']               = "إغلاق الموقع؟";
$lang['settings_closed_message']            = "رسالة الإغلاق";
$lang['settings_is_default']                = "افتراضي";
$lang['settings_created_at']                = "انشئ في";
$lang['settings_last_update']               = "آخر تحديث";
$lang['settings_meta_description']          = "وصف";
$lang['settings_meta_tags']                 = "وسوم";
$lang['settings_default_language']          = "اللغة الافتراضية";
$lang['settings_default_theme']             = "التصميم الافتراضي";
$lang['settings_mail_function']             = "دالة المراسلة";
$lang['settings_mail_func_option']          = "mail";
$lang['settings_smtp_func_option']          = "SMTP";
$lang['settings_smtp_server']               = "SMTP خادم";
$lang['settings_smtp_port']                 = "SMTP منفذ";
$lang['settings_smtp_user']                 = "SMTP اسم المستخدم";
$lang['settings_password']                  = "SMTP كلمة المرور";
$lang['settings_social_facebook']           = "فيس بوك";
$lang['settings_social_twitter']            = "تويتر";
$lang['settings_social_youtube']            = "يوتيوب";
$lang['settings_social_gplus']              = "جوجل+";
$lang['settings_social_linkedin']           = "لينكدإن";
$lang['settings_social_instagram']          = "انستجرام";
$lang['settings_contact_email']             = "البريد الإلكتروني";
$lang['settings_contact_phones']            = "أرقام الهاتف/النقال";
$lang['settings_contact_information']       = "معلومات إضافية";
$lang['settings_seo_description']           = "وصف ";
$lang['settings_seo_tags']                  = "وسوم";

// messages
$lang['error_settings_is_default']          = "<b>خطأ: </b>غير مسموح بحذف الإعدادات الافتراضية";
$lang['success_settings_deleted']          = "تم حذف الإعدادات بنجاح";

$lang['log_show_settings']                  = "استعراض جميع الإعدادات المتاحة";
$lang['log_edit_settings']                  = "تحديث إعدادات النظام";
$lang['log_add_settings']                  = "إضافة سجل إعدادات جديد";
$lang['log_delete_settings']                  = "حذف سجل إعدادات";
$lang['log_set_default_settings']                  = "تغيير الإعدادات الافتراضية للنظام";