<?php
/*********************************************
 *  Project     : projects_management
 *  File        : Log.php
 *  Created at  : 7/15/15 - 11:13 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 class Admin extends Admin_Controller{
     private $model;

     function __construct(){
         parent :: __construct();
         $this->load->model("Log_model");
         $this->model= New Log_model();
         $this->lang->load("log",$this->languages[$this->user_lang]);

     }

     function index($offset=0){

         if(!check_user_permission(current_module(),"show")){
             redirect(admin_url("permissions/denied"));
         }
         log_add_user_action($this->user->user_id,0,"show","log_show_user_log");
         $this->data['title']       = langline("log_page_title")." - ".$this->site->site_name;
         $limit                     = 30;
         $results                   = $this -> model->get_all(0,$offset,$limit);
         $this->data['data']        = $results['data'];
         if($results){

             foreach($results['data'] as $lang){
                 $this->lang->load($lang->ul_module."/{$lang->ul_module}",$this->languages[$this->user_lang]);
             }
         }

         $this->data['content']     = "log/list";
         $events_color=array("show"=>"info","add"=>"success","edit"=>"warning","delete"=>"danger");
         $this->data['events_color']    = $events_color;
         $this->data['modules_list']     = $this->Components_model->list_modules();
         $this->template->render("template",$this->data);
     }

     function user($user=0){

         $this->load->model("users/Users_model");
         $user_data                 = $this->Users_model->get($user);
         $this->data['title']       = sprintf(langline("log_user_page_title"),$user_data->user_name)." - ".$this->site->site_name;
         $results                   = $this->model->get_all($user);
         $this->data['data']        = $results['data'];

         if($results['data']){

             foreach($results['data'] as $lang){
                 $this->lang->load($lang->ul_module."/{$lang->ul_module}",$this->languages[$this->user_lang]);
             }
         }

         $events_color=array("show"=>"info","add"=>"success","edit"=>"warning","delete"=>"danger");
         $this->data['events_color']    = $events_color;
         $this->data['user_data']       = $user_data;
         $this->data['content']         = "log/list";
         $this->data['modules_list']     = $this->Components_model->list_modules();
         $this->template->render("template",$this->data);
     }
 }