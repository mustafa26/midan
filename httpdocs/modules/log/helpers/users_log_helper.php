<?php 

 if(!function_exists("log_add_user_action")){

     /**
      * @param $user user ID
      * @param $item_id Current item ID
      * @param $action_type (add,edit,delete,show)
      * @param $description action description
      * @param $module (optional) Module name
      * @return int insert ID
      */
     function log_add_user_action($user,$item_id,$action_type,$description,$module=""){

         $ins=& get_instance();

         $ins->load->model("log/Log_model");
         $log=new Log_model();

         $data['ul_user']       = $user;
         $data['ul_action']     = $action_type;
         $data['ul_message']    = $description;
         $data['ul_module']     = $module!=""?$module:current_module();
         $data['ul_action_id']  = $item_id;
         $data['ul_created']    = date("Y-m-d H:i:s");

         return $log->log_add_action($data);
     }
 }