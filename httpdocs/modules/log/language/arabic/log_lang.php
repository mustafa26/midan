<?php
/*********************************************
 *  Project     : projects_management
 *  File        : log_lang.php
 *  Created at  : 7/26/15 - 10:53 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 $lang['log_user_page_title']       = "فهرس الأحداث التي قام بها %s";
 $lang['log_event_date']            = "الوقت";
 $lang['log_user_avatar']           = "صورة";
 $lang['log_user_name']             = "الاسم";
 $lang['log_event_description']     = "الإجراء";
 $lang['log_event_type']            = "نوع الإجراء";
 $lang['log_no_events']             = "لا يوجد أي إجراءات حتى الآن";
 $lang['log_action_add']            = "إضافة";
 $lang['log_action_edit']           = "تحديث";
 $lang['log_action_delete']         = "حذف";
 $lang['log_action_show']           = "استعراض";
 $lang['log_show_user_log']         = "عرض فهرس مستخدم";
 $lang['log_show_all_users_log']    = "عرض فهرس جميع المستخدمين";
