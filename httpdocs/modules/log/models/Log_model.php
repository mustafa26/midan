<?php
/*********************************************
 *  Project     : projects_management
 *  File        : Log_model.php
 *  Created at  : 7/15/15 - 11:11 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 class Log_model extends CI_Model{

     private $table="users_log";

     public function __construct(){
         parent :: __construct();
     }

     function get_all($user=0,$offset=0,$limit=0){
         if($user){
             $this->db->where("a.ul_user",$user);
         }

         if($offset || $limit){
             $this->db->limit($limit,$offset);
         }

        $Q = $this->db->select("a.*,users.user_name,users.user_username,modules.module_name,modules.module_icon_class,modules.module_title")
                 ->join("users","users.user_id=a.ul_user")
                 ->join("modules","modules.module_name=a.ul_module")
                 ->group_by("a.ul_id")
                 ->order_by("a.ul_id","desc")
                 ->get("{$this->table} a");

         if($user){
             $this->db->where("a.ul_user",$user);
         }

         $total = $this->db->select("a.*,users.user_name,users.user_username,modules.module_name,modules.module_title")
                         ->join("users","users.user_id=a.ul_user")
                         ->join("modules","modules.module_id=a.ul_module")
                         ->group_by("a.ul_id")
                         ->order_by("a.ul_id","desc")
                         ->get("{$this->table} a");

         $data['data']=$Q->num_rows()>0?$Q->result():false;
         $data['total']=$total;
         return $data;
     }

     function log_add_action($data=array()){
         $this->db->insert($this->table,$data);
         return $this->db->insert_id();
     }

}