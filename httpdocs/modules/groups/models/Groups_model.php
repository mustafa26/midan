<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : Users_model.php
 *  Created at  : 5/21/15 - 2:27 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 class Groups_model extends CI_Model{
     private $table="groups";

     function __construct(){
         parent :: __construct();
     }

     /**
      * @param int $group
      * @param int $offset
      * @param int $limit
      * @return mixed
      */
     function get_all($user=0,$offset=0,$limit=0){

         if($offset||$limit){
             $this->db->limit($limit,$offset);
         }

         if($user){
             $this->db->where("b.ug_user_id",$user);
         }

         $Q=$this->db->select("a.*,c.*,count(b.ug_user_id) as members_num")
             ->join("users_groups b","b.ug_group_id=a.group_id","left")
             ->join("users c","c.user_id=b.ug_user_id","left")
             ->group_by("a.group_id")
             ->get($this->table." a");

         if($user){
             $this->db->where("b.ug_user_id",$user);
         }

         $total=$this->db->select("a.*,c.*,count(b.ug_user_id) as members_num")
             ->join("users_groups b","b.ug_group_id=a.group_id","left")
             ->join("users c","c.user_id=b.ug_user_id","left")
             ->group_by("a.group_id")
             ->get($this->table." a")->num_rows();
         $data['data']=$Q->num_rows()>0?$Q->result():false;
         $data['total']=$total;
         return $data;
     }

     function get_group($id=0){
         $Q=$this->db->get_where($this->table,array("group_id"=>$id));
         return $Q->row();
     }

     function insert($data){
         $this->db->insert($this->table,$data);
         return $this->db->insert_id();
     }

     function update($id,$data){

         $this->db->update($this->table,$data,array("group_id"=>$id));
         return $this->db->affected_rows();
     }

     function update_super_group($id){

         $this->db->update($this->table,array("group_isSuper"=>0));
         $this->db->update($this->table,array("group_isSuper"=>1),array("group_id"=>$id));
         return $this->db->affected_rows();

     }
     function is_super($id){
         $Q=$this->db->get_where($this->table,array("group_isSuper"=>1,"group_id"=>$id),1);
         return $Q->num_rows()>0?true:false;
     }

     function delete($id){
         $this->db->delete($this->table,array("group_id"=>$id));
         return $this->db->affected_rows();
     }

     function list_groups(){
         $Q=$this->db->get($this->table);
         return ($Q->num_rows() > 0)?$Q->result():false;
 }

 }