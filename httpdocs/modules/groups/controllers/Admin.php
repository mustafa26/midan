<?php

/*********************************************
 *  Project     : Projects Management
 *  File        : Users.php
 *  Created at  : 5/6/15 - 9:28 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : php.ahmedfathi@gmail.com
 *  site        : http://durar-it.com
 *********************************************/
class Admin extends Admin_Controller
{
    private $model;

    function __construct()
    {
        parent:: __construct();
        $this->load->model("Groups_model")->model("permissions/Permissions_model");
        $this->model = new Groups_model();
        $this->lang->load("groups", $this->languages[$this->user_lang]);
        $this->lang->load("permissions/permissions", $this->languages[$this->user_lang]);
        if(!check_user_permission(current_module(),"show")){
            redirect(admin_url("permissions/denied"));
        }
    }

    function index($offset = 0)
    {
        $limit = 20;

        $user_id = get("user") ? get("user") : 0;// optional

        $result = $this->model->get_all($user_id, $offset, $limit);
        log_add_user_action($this->user->user_id,0,"show","log_show_groups");
        $this->data['title'] = langline("groups_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "groups/list";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("groups"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    private function _validate()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("group_name", langline("groups_group_name"), "required|trim|max_length[50]");
        $this->form_validation->set_rules("group_description", langline("groups_group_description"), "trim|max_length[150]");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }
        return true;
    }

    function add()
    {
        if(!check_user_permission(current_module(),"add")){
            redirect(admin_url("permissions/denied"));
        }

        if (post()) {
            $this->_do_add();
            exit;
        }

        $this->data['title'] = langline("groups_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "groups/add";
        $this->data['permissions'] = $this->Permissions_model->list_modules_permission();
        $this->template->render("template", $this->data);

    }

    private function _do_add()
    {

        $this->_validate();

        if (!$this->errors) {

            $data['group_name'] = post("group_name", true);
            $data['group_description'] = post("group_description", true);
            $data['group_inAdmin'] = (bool)post("in_admin");
            $data['group_created'] = date("Y-m-d H:i:s");
            $data['group_status'] = 1;
            if ($group = $this->model->insert($data)) {
                log_add_user_action($this->user->user_id,$group->group_id,"add","log_add_group");
                // insert group permissions
                $this->_update_permissions($group);
                $this->set_message(langline("success_group_created"), "success");
                redirect(admin_url("groups"));
            }
        }

        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("groups/add"));
    }

    function _update_permissions($group=0)
    {

                $this->load->model('components/Components_model');
                $modules=$this->Components_model->list_modules();
                $data['permission_group']   = $group;
	            $supervision      = post("perm_supervision_action");
                $show      = post("perm_show_action");
                $add       = post("perm_add_action");
                $edit      = post("perm_edit_action");
                $delete    = post("perm_delete_action");
            if($modules){

                $this->Permissions_model->delete_group_permissions($group);

                foreach($modules as $key=>$item){
                    $data['permission_module']     = $item->module_id;
	                $data['permission_supervision']= (bool)isset($supervision[$item->module_id])?$supervision[$item->module_id]:0;
                    $data['permission_show']       = (bool)isset($show[$item->module_id])?$show[$item->module_id]:0;
                    $data['permission_add']        = (bool)isset($add[$item->module_id])?$add[$item->module_id]:0;
                    $data['permission_edit']       = (bool)isset($edit[$item->module_id])?$edit[$item->module_id]:0;
                    $data['permission_delete']     = (bool)isset($delete[$item->module_id])?$delete[$item->module_id]:0;
                    $this->Permissions_model->update($data);
                }

                log_add_user_action($this->user->user_id,$group,"edit","log_edit_permissions","permissions");
            }

    }

    function edit($id = 0)
    {
        if(!check_user_permission(current_module(),"edit")){
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }
        $this->data['title'] = langline("groups_page_title") . " - " . $this->site->site_name;
        $this->data['data'] = $this->model->get_group($id);
        $this->data['permissions'] = $this->Permissions_model->list_modules_permission($id);
        $this->data['content'] = "groups/edit";
        $this->template->render("template", $this->data);

    }

    private function _do_update($id)
    {

        $this->_validate();

        if (!$this->errors) {


            $data['group_name'] = post("group_name", true);
            $data['group_description'] = post("group_description", true);
            $data['group_inAdmin'] = (bool)post("in_admin");
            $data['group_status'] = 1;

            $this->model->update($id, $data);
            log_add_user_action($this->user->user_id,$id,"edit","log_edit_group");
            $this->_update_permissions($id);

            $this->set_message(langline("success_group_updated"), "success");

            redirect(admin_url("groups"));
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");

        redirect(admin_url("groups/edit/$id"));
    }

    function setSuperGroup($id)
    {

        if ($this->is_ajax() and check_user_permission(current_module(),"edit")) {

            $this->model->update_super_group($id);
            log_add_user_action($this->user->user_id,$id,"edit","log_change_super_group");
            $this->jsonData['success'] = 1;
            $this->jsonData['html'] = langline("success_super_group_updated");
            echo json_encode($this->jsonData);
            exit;
        }

        show_404();

    }

    function delete($id = 0)
    {
        if(!check_user_permission(current_module(),"delete") || !check_user_permission(current_module(),"edit")){
            redirect(admin_url("permissions/denied"));
        }
        if ($this->model->is_super($id)) {
            $this->set_message(langline("error_group_is_super"), "error");
        }
        else {
            $this->model->delete($id);
            log_add_user_action($this->user->user_id,$id,"delete","log_delete_group");
            $this->set_message(langline("success_group_deleted"), "success");

        }
        redirect(admin_url("groups"));
    }
}