<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : groups_lang.php
 *  Created at  : 5/21/15 - 2:52 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 $lang['groups_page_title']="Groups";
$lang['groups_link']     ="Groups";
$lang['groups_group_name']="Group Name";
$lang['groups_group_description']="Description";
$lang['groups_page_header']     ="Groups";
$lang['groups_group_in_admin']   ="Is Admin Group";
$lang['groups_super_group']      = "Super Group";
$lang['groups_group_members']      = "Group Members";
$lang['groups_no_members']              = "No Members";
$lang['groups_members_count']           = "%s Member";
$lang['groups_created_at']         ="Created at";
$lang['groups_last_update']         ="Last Update";
$lang['groups_permissions']      = "Permissions";
$lang['success_group_created']   ="Group Created Successfully";
$lang['success_group_updated']   ="Group updated Successfully";
$lang['success_group_deleted']   ="Group Deleted Successfully";
$lang['error_group_is_super']   ="Error: this group Is super Group, not allowed to delete it";

$lang['log_show_groups']                = "show Users Groups";
$lang['log_add_groups']                = "Add Users Group";
$lang['log_edit_groups']                = "Update Users Group";
$lang['log_delete_groups']                = "Delete Users Group";
$lang['log_change_super_group']               ="Change Super Group";