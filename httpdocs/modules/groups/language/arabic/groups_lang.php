<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : groups_lang.php
 *  Created at  : 5/21/15 - 2:52 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
$lang['groups_page_title']              = "المجموعات";
$lang['groups_link']                    = "المجموعات";
$lang['groups_group_name']              = "اسم المجموعة";
$lang['groups_group_description']       = "الوصف";
$lang['groups_page_header']             = "المجموعات";
$lang['groups_permissions']             = "الصلاحيات";
$lang['groups_group_in_admin']          = "يمكن دخول لوحة التحكم";
$lang['groups_no_members']              = "لا يوجد أعضاء";
$lang['groups_members_count']           = "%s عضو";
$lang['groups_super_group']             = "المجموعة الرئيسية";
$lang['groups_group_members']           = "أعضاء المجموعة";
$lang['groups_created_at']              ="أنشئت في";
$lang['groups_last_update']             ="آخر تحديث";
$lang['success_group_created']          = "تم إنشاء المجموعة بنجاح";
$lang['success_group_updated']          = "تم تحديث بيانات المجموعة بنجاح";
$lang['success_group_deleted']          = "تم حذف المجموعة بنجاح";
$lang['error_group_is_super']           = "<b>خطأ :</b> لا يمكنك حذف المجموعة الرئيسية";

$lang['log_show_groups']                = "عرض مجموعات المستخدمين";
$lang['log_add_group']                  = "إضافة مجموعة المستخدمين";
$lang['log_edit_group']                 = "تحديث مجموعة المستخدمين";
$lang['log_delete_group']               = "حذف مجموعة المستخدمين";
$lang['log_change_super_group']               ="تغيير الموجموع الرئيسية";


