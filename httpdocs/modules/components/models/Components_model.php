<?php

/* * *******************************************
 *  Project     : projects_management
 *  File        : Components_model.php
 *  Created at  : 5/31/15 - 10:11 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 * ******************************************* */

class Components_model extends CI_Model {

    private $table = "modules";

    function __construct() {

        parent::__construct();
    }

    function get($id = 0) {
        $Q = $this->db->get_where($this->table, array("module_id" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function get_by_name($name = "") {
        $Q = $this->db->get_where($this->table, array("module_name" => $name));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function get_all($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }

        $this->db->order_by("module_sort");
        $Q = $this->db->get($this->table);

        $total = $this->db->get($this->table)->num_rows();
        $data['data'] = $Q->result();
        $data['total'] = $total;

        return $Q->num_rows() ? $data : false;
    }

    function get_all_parents($parent = 0, $offset = 0, $limit = 0) {

        if ($offset || $limit) {

            $this->db->limit($limit, $offset);
        }

        $this->db->order_by("module_sort");
        $Q = $this->db->get_where($this->table, array("module_parent" => $parent));

        $total = $this->db->get_where($this->table, array("module_parent" => $parent))->num_rows();

        $data['data'] = $Q->result();

        $data['total'] = $total;

        return $Q->num_rows() ? $data : false;
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $this->db->update($this->table, $data, array("module_id" => $id));
        return $this->db->affected_rows();
    }

    function list_modules($ignore = 0) {

        if ($ignore) {
            $this->db->where("module_id !=", $ignore);
        }
        $this->db->order_by("module_sort");
        $Q = $this->db->get_where($this->table, array("module_status" => 1));
        return $Q->result();
    }

    function delete($id) {
        $this->db->query("DELETE a,b FROM {$this->table} a LEFT JOIN permissions b ON  a.module_id=b.permission_module WHERE a.module_id={$id}");
        return $this->db->affected_rows();
    }

}
