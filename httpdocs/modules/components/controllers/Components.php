<?php

/* * *******************************************
 *  Project     : projects_management
 *  File        : modules.php
 *  Created at  : 5/31/15 - 9:52 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 * ******************************************* */

class Components extends Admin_Controller {

    private $model;

    function __construct() {

        parent:: __construct();
        $this->load->model("Components_model");
        $this->model = new Components_model();

        $this->lang->load("components", $this->languages[$this->user_lang]);
    }

    function index($offset = 0) {
        $limit = 20;
        $parent = get("parent") | 0;
        $results = $this->model->get_all_parents($parent, $offset, $limit);
        $this->data['title'] = langline("modules_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "modules/list";
        $this->data['pagination'] = $this->pagination->pager(site_url("modules"), $results['total'], $limit, 2);
        $this->data['data'] = $results['data'];
        $this->template->render("template", $this->data);
    }

    function _validate() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("title[]", "lang:com_title_arabic", "trim|max_length[100]");
        $this->form_validation->set_rules("title[]", "lang:com_title_arabic", "trim|max_length[100]");
        $this->form_validation->set_rules("description[]", "lang:com_description", "trim|max_length[255]");
        $this->form_validation->set_rules("description[]", "lang:com_description", "trim|max_length[255]");

        if ($this->user_lang == "ar") {
            $this->form_validation->set_rules("title[]", "lang:com_title_arabic", "required");
        } else if ($this->user_lang = "en") {
            $this->form_validation->set_rules("title[]", "lang:com_title_english", "required");
        }

        $this->form_validation->set_rules("name", "lang:com_name", "required|trim|max_length[50]");
        $this->form_validation->set_rules("parent", "lang:com_parent", "trim|max_length[255]");
        $this->form_validation->set_rules("icon_class", "lang:icon_class", "max_length[100]");
        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }
        return false;
    }

    function add() {

        if (post()) {
            $this->_do_add();
            exit;
        }

        $this->data['title'] = langline("modules_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "modules/add";
        $this->data['modules'] = $this->model->list_modules();
        $this->template->render("template", $this->data);
    }

    function _do_add() {
        $this->_validate();
        if (!$this->errors) {
            $icon = "";

            if ($file = $this->do_upload("module_icon")) {
                $icon = $file['file_name'];
            }
            $data['module_sort'] = post("module_sort");
            $data['module_title'] = serialize(post("title"));
            $data['module_description'] = serialize(post("description"));
            $data['module_name'] = post("name");
            $data['module_parent'] = post("parent");
            $data['module_icon'] = $icon;
            $data['module_icon_class'] = post("icon_class");
            $data['module_show'] = (bool) post("show");
            $data['module_status'] = (bool) post("status");
            $data['module_css'] = post("module_css");
            $data['module_created'] = date("Y-m-d H:i:s");

            if ($this->model->insert($data)) {
                $this->set_message(langline("success_module_created"), "success");
                redirect(site_url("components"));
            }
        } else {
            $this->keep_data();
            $this->set_message($this->errors, "error");
            redirect(site_url("components/add"));
        }
    }

    function edit($id = 0) {

        if (post()) {
            $this->_do_update($id);
            exit;
        }

        $this->data['title'] = langline("modules_page_title") . " - " . $this->site->site_name;
        $this->data['content'] = "modules/edit";
        $this->data['data'] = $this->model->get($id);
        ;
        $this->data['modules'] = $this->model->list_modules();
        $this->template->render("template", $this->data);
    }

    function _do_update($id = 0) {
        $this->_validate();
        if (!$this->errors) {

            $icon = "";

            if ($file = $this->do_upload("module_icon")) {
                $icon = $file['file_name'];
            }

            $data['module_sort'] = post("module_sort");
            $data['module_title'] = serialize(post("title"));
            $data['module_description'] = serialize(post("description"));
            $data['module_name'] = post("name");
            $data['module_parent'] = post("parent");
            $data['module_icon'] = $icon;
            $data['module_icon_class'] = post("icon_class");
            $data['module_css'] = post("module_css");
            $data['module_show'] = (bool) post("show");
            $data['module_status'] = (bool) post("status");

            $this->model->update($id, $data);
            $this->set_message(langline("success_module_created"), "success");
            redirect(site_url("components"));
        } else {
            $this->keep_data();
            $this->set_message($this->errors, "error");
            redirect(site_url("components/edit/$id"));
        }
    }

    function delete($id) {

        $id = intval($id) | 0;
        if ($this->model->delete($id)) {

            $this->set_message(langline("success_module_deleted"), "success");
        } else {

            $this->set_message(langline("error_module_not_deleted"), "error");
        }

        $qs = get() ? "?" . http_build_query(get()) : ""; // append query string to redirect link

        redirect(site_url("components" . $qs));
    }

    private function do_upload($file_name = "userfile") {

        $this->load->library("upload");

        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/");

//            die($config['upload_path']);

            $config['allowed_types'] = 'gif|jpg|png|icon';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                return $this->upload->data();
            }
        } else {
            return false;
        }
    }

}
