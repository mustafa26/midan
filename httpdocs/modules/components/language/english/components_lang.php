<?php

/* * *******************************************
 *  Project     : projects_management
 *  File        : english.php
 *  Created at  : 5/31/15 - 11:33 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 * ******************************************* */
$lang['com_page_title'] = "Components";
$lang['com_page_header'] = "Components Management";
$lang['com_link'] = "Components";
$lang['com_list_all'] = "List";
$lang['com_title'] = "Module Title";
$lang['com_title_arabic'] = "Module Title (Arabic)";
$lang['com_title_english'] = "Module Title (English)";
$lang['com_name'] = "Module Name";
$lang['com_name_hint'] = "Module folder name";
$lang['com_description'] = "Description";
$lang['com_description_arabic'] = "Description (Arabic)";
$lang['com_description_english'] = "Description (English)";
$lang['com_parent'] = "Parent Module";
$lang['com_url'] = "Queries";
$lang['com_url_hint'] = "?name=value OR name/value";
$lang['com_icon'] = "Module Icon";
$lang['com_image'] = "OR Upload Icon Image";
$lang['com_no_parent'] = "No Parent Module";
$lang['com_show'] = "Appear in Menu";
$lang['com_status'] = "Module Status";
$lang['com_sub_modules'] = "Sub Modules";
$lang['com_css'] = "Css Style";
$lang['com_Sort'] = "Sort";
$lang['success_module_deleted'] = "Module Deleted Successfully";
$lang['error_module_not_deleted'] = "Error:No Modules Deleted, try again after refresh.";
