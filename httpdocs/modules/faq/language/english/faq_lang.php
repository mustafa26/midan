<?php

/* * *******************************************
 *  Project     : requests_management
 *  File        : users_lang.php
 *  Created at  : 5/24/15 - 2:19 PM 
 *  site        : http://durar-it.com
 * ******************************************* */

$lang['requests_event_title'] = "الأحداث";
$lang['requests_event_header'] = "الأحداث";
$lang['requests_link'] = "الأحداث";
$lang['cats_name'] = "اسم التصنيف";
$lang['event_Title'] = "عنوان الحدث";
$lang['event_SubTitle'] = "عنوان الحدث الفرعي";
$lang['event_Content'] = "محتوى الحدث";    
$lang['event_MetaDescr'] = "وصف محتوى الحدث";
$lang['event_MetaKeywords'] = "الكلمات الدليلية";
$lang['event_HeaderPhoto'] = "صورة بنر الحدث";
$lang['Tab_Settings'] = "إعدادات الحدث";
$lang['Name'] = "الاسم";
$lang['project_status'] = "الحالة";
$lang['project_description'] = "وصف";
$lang['project_sub_tasks_num'] = "%s مهمة";

//Messages
$lang['success_categories_created'] = "تم إنشاء التصنيف بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات التصنيف بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات التصنيف ";
$lang['success_project_deleted'] = "تم حذف التصنيف بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف التصنيف , حاول مرة أخرى بعد تحديث الحدث";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_requests'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_event'] = "تحديث بيانات الحدث";
$lang['log_delete_project'] = "حذف مشروع";
