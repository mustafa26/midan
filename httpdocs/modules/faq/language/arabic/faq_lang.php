<?php
 

$lang['faq_request_title'] = "الأحداث";
$lang['faq_request_header'] = "الأحداث";
$lang['faq_link'] = "نماذج الطلبات";
$lang['cats_name'] = "اسم";
$lang['request_Title'] = "عنوان الحدث";
$lang['request_SubTitle'] = "عنوان الحدث الفرعي";
$lang['request_Content'] = "محتوى الحدث";    
$lang['request_MetaDescr'] = "وصف محتوى الحدث";
$lang['request_MetaKeywords'] = "الكلمات الدليلية";
$lang['request_HeaderPhoto'] = "صورة بنر الحدث";
$lang['Tab_Settings'] = "إعدادات الحدث";
$lang['Name'] = "الاسم";
$lang['project_status'] = "الحالة";
$lang['project_description'] = "وصف";
$lang['project_sub_tasks_num'] = "%s مهمة"; 

//Messages
$lang['success_categories_created'] = "تم إنشاء التصنيف بنجاح";
$lang['success_project_updated'] = "تم تحديث بيانات التصنيف بنجاح";
$lang['success_project_updated_error'] = "خطأ في تحديث بيانات التصنيف ";
$lang['success_project_deleted'] = "تم حذف التصنيف بنجاح";
$lang['error_project_not_deleted'] = "<b>خطأ : </b> خطأ لم يتم حذف التصنيف , حاول مرة أخرى بعد تحديث الحدث";

// notifications
$lang['notify_user_is_project_manager'] = "أسند إليك  %s مشروع %s";
$lang['notify_project_updated'] = "مشروع %s تم تحديثه بواسطة %s";
$lang['notify_your_group_in_project'] = "تم إسناد مشروع %s إلى فريق عملك بواسطة %s";
$lang['notify_commented_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_project'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_replayed_in_task'] = "تم إضافة تعليق على مشروع%s بواسطة %s";
$lang['notify_your_project_delayed'] = "تم تحويل مشروع %s إلى حالة التأخير بسبب تأخر اكتمال مهام المشروع";

// log messages
$lang['log_show_requests'] = "استعراض المشاريع";
$lang['log_add_catgory'] = "إضافة تصنيف جديد";
$lang['log_edit_catgory'] = "تحديث بيانات تصنيف";
$lang['link_edit_request'] = "تحديث بيانات الحدث";
$lang['log_delete_project'] = "حذف مشروع";
