<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("Faq_Model");
        $this->lang->load("faq", $this->languages[$this->user_lang]);
        $this->model = new Faq_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("faq_status");
    }

    function index($fid = 0, $offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("faq_faq_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($fid,$offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_faq");
        $this->data['content'] = "faq/index";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("faq"), $result['total'], $limit, 4);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        foreach ($this->languages as $key => $value) {
            $this->form_validation->set_rules("faq_Title[$key]", langline("faq_Title") . $value, "required|trim|max_length[255]");
            $this->form_validation->set_rules("faq_SubTitle[$key]", langline("faq_SubTitle") . $value, "required|trim|max_length[255]");
            $this->form_validation->set_rules("faq_Content[$key]", langline("faq_Content") . $value, "required");
        }

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function edit($id = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }

        $this->data['title'] = langline("faq_faq_title") . " - " . $this->site->site_name;
        $this->data['content'] = "faq/edit";
        $this->data['event'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("faq_HeaderPhoto");
        if (!$this->errors) {
            $data['faq_Title'] = serialize(post("faq_Title"));
            $data['faq_SubTitle'] = serialize(post("faq_SubTitle"));
            $data['faq_Content'] = serialize(post("faq_Content"));
            $data['faq_MetaDescr'] = serialize(post("faq_MetaDescr"));
            $data['faq_MetaKeywords'] = serialize(post("faq_MetaKeywords"));
            $data['faq_Active'] = post("faq_Active") ? 1 : 0;


            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_faq");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("faq"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("faq/edit/$id"));
    }

    function delete($fid = 0, $id = 0) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['cat_deleted'] = 1;
        if ($this->model->delete_cats($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("faq/cats/$fid"));
    }

}
