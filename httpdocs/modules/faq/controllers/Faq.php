<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Faq extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Faq_Model", 'model');
        $this->load->model("pages/Pages_Model");
    }

    public function index($fid = 0, $offset = 0) {
        $this->data['title'] = $this->site->site_name;
        //$limit = 20;
        //$result = $this->model->get_all_home($offset, $limit);
        $this->data['page'] = $this->Pages_Model->get(18);
        $this->data['content'] = "faq/index";
        //$this->data['data'] = $result['data'];
        //$this->data['pagination'] = $this->pagination->pager(admin_url("events"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

}
