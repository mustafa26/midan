<?php 

class Faq_Model extends CI_Model {

    private $table = "faq";

    function __construct() {
        parent :: __construct();
    }

    function get_all($fid = 0, $offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $Q = $this->db->get_where($this->table, array("faq_FID" => $fid, "faq_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("faq_FID" => $fid, "faq_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get($id = 0) {
        $Q = $this->db->get_where($this->table, array("faq_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $this->db->update($this->table, $data, array("faq_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id = 0) {
        $this->db->update($this->table, $data, array("faq_ID" => $id));
        return $this->db->affected_rows();
    }

}
