<?php
/*********************************************
 *  Project     : projects_management
 *  File        : Permissions.php
 *  Created at  : 6/3/15 - 9:46 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 class Admin extends Admin_Controller{
     private $model;
     function __construct(){
         parent :: __construct();
         $this->load
             ->model("Permissions_model")
             ->model("groups/Groups_model")
             ->helper("permissions");
         $this->lang->load("permissions",$this->languages[$this->user_lang]);
         $this->model= new Permissions_model();

     }

     function index($group=0){

         if(!check_user_permission(current_module(),"show")){
             redirect(admin_url("permissions/denied"));
         }

         if(post()){
             $this->_update($group);
             exit;
         }

         $this->data['title']           = langline("perm_page_title")." - ".$this->app->app_name;
         $this->data['content']         = "permissions/list";
         $this->data['data']            = $this->model->list_modules_permission($group);
         $this->data['group']           = $group;
         $this->data['groups']          = $this->Groups_model->list_groups();

         $this->template->render("template",$this->data);
     }

     function denied(){
         $this->data['title']           = langline("perm_page_title")." - ".$this->app->app_name;
         $this->data['content']         = "permissions/denied";
         $this->template->render("template",$this->data);
     }

     private function _update($group=0){
         if(post("group")){
             $group=post("group");
         }

         $this->load->model('components/Components_model');
         $modules=$this->Components_model->list_modules();

         if(!$group){
             $this->keep_data();
             $this->set_message(langline("error_no_group_selected"),"error");
         }else{
             $data['permission_group']=$group;
	         $supervision      = post("perm_supervision_action");
             $show      = post("perm_show_action");
             $add       = post("perm_add_action");
             $edit      = post("perm_edit_action");
             $delete    = post("perm_delete_action");
             if($modules){
                 $this->model->delete_group_permissions($group);
                 foreach($modules as $key=>$item){
                     $data['permission_module']     = $item->module_id;
                     $data['permission_supervision']       = (bool)isset($supervision[$item->module_id])?$supervision[$item->module_id]:0;
                     $data['permission_show']       = (bool)isset($show[$item->module_id])?$show[$item->module_id]:0;
                     $data['permission_add']       = (bool)isset($add[$item->module_id])?$add[$item->module_id]:0;
                     $data['permission_edit']       = (bool)isset($edit[$item->module_id])?$edit[$item->module_id]:0;
                     $data['permission_delete']     = (bool)isset($delete[$item->module_id])?$delete[$item->module_id]:0;
                     $this->model->update($data);
                 }
                 log_add_user_action($this->user->user_id,0,"edit","log_edit_permissions");

                 $this->set_message(langline("success_permissions_updated"),"success");
                 $this->keep_data();
             }

         }

         redirect(admin_url("permissions/$group"));
     }
 }