<?php

/*********************************************
 *  Project     : projects_management
 *  File        : Permissions_model.php
 *  Created at  : 6/3/15 - 2:55 PM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
class Permissions_model extends CI_Model
{

    private $table = "permissions";

    function __construct()
    {
        parent:: __construct();
    }

    function get_user_permissions($user=0){
    $Q=$this->db->select("a.*,b.*")
        ->join("users b","b.user_id=a.permission_user")
        ->join("modules","modules.module_id=a.permission_module")
        ->get_where($this->table,array("b.user_id"=>$user));
        return  $Q->num_rows()>0?$Q->result():false;
    }

    function list_modules_permission($user = 0, $parent = 0)
    {


       /* if ($user) {

            $this->db->where("", $user);
        }*/


        $Q = $this->db->select("a.*,b.*")
            ->join("permissions b", "a.module_id=b.permission_module and b.permission_user={$user}", "left")
            ->group_by("a.module_id")
            ->get_where("modules a",
                array(
                    "a.module_status" => 1,
                    "a.module_parent" => $parent));

        return ($Q->num_rows() > 0) ? $Q->result() : false;

    }


    function delete_user_permissions($id=0){
        //delete all current group permissions
        $this->db->delete($this->table,array("permission_user"=>$id));
        return $this->db->affected_rows();
    }

    function update($data){
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

}