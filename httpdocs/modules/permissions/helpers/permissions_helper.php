<?php 

function permission_sub_modules($user=0,$parent=0){
    $ins=get_instance();
    $ins->load->model("permissions/Permissions_model");

    return $ins->Permissions_model->list_modules_permission($user,$parent);
}

function get_user_permissions($user){

    $ins=get_instance();

    $ins->load->model("permissions/Permissions_model");

    return $ins->Permissions_model->get_user_permissions($user);
}

/**
 * @param $module
 * @param $action
 */
function check_user_permission($module=null,$action=null){

    $loggedin=get_instance()->session->userdata("logged_in");

    $actions=array(
        "supervision"=>"permission_supervision",
        "show"=>"permission_show",
        "add"=>"permission_add",
        "edit"=>"permission_edit",
        "delete"=>"permission_delete"
    );

    $permissions=$loggedin['permissions'];

    if(isset($loggedin)){
            if($permissions){
                foreach($permissions as $k=>$v){

                   if($v->module_name===$module && $v->$actions[$action]==1){
                       return true;
                   }
                }
            }


    }
    return false;

}
