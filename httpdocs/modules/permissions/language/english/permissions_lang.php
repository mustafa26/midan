<?php
/*********************************************
 *  Project     : projects_management
 *  File        : permissions.php
 *  Created at  : 6/3/15 - 9:39 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 $lang['perm_page_title']               = "Permissions";
 $lang['perm_page_header_title']        = "Permissions";
 $lang['perm_add_action']               = "Add";
 $lang['perm_supervision_action']       = "Supervision";
 $lang['perm_show_action']              = "Show";
 $lang['perm_edit_action']              = "Edit";
 $lang['link_permissions']              = "Permissions";
 $lang['perm_delete_action']            = "Delete";
$lang['perm_denied_link']               = "Denied";
$lang['perm_denied_message']            ='<h3>Oops!  Something went wrong.</h3>
                                            <p>
                                               this page my be deleted or you don\'t have permissions to access it.<br><br>
                                            </p>';
 $lang['select_group']                  = "Select Group";
 $lang['full_permissions']              = "Full Permissions";
 $lang['success_permissions_updated']   = "Permissions Updated Successfully";
 $lang['error_permissions_not_updated'] = "<b>Error:</b> Can't update permissions, \r\n Try again after Refresh this page";
 $lang['error_no_group_selected']       = "<b>Error:</b> No Group Selected";
$lang['log_edit_permissions']           ="تحديث الصلاحيات";
$lang['log_add_permissions']           ="إضافة الصلاحيات";

