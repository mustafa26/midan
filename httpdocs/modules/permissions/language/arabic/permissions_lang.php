<?php
/*********************************************
 *  Project     : projects_management
 *  File        : permissions.php
 *  Created at  : 6/3/15 - 9:39 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : ahmedfathi@durar-it.com
 *  site        : http://durar-it.com
 *********************************************/
 $lang['perm_page_title']               = "الصلاحيات";
 $lang['perm_page_header_title']        = "الصلاحيات";
 $lang['perm_supervision_action']              = "الإشراف";
 $lang['perm_show_action']              = "عرض";
$lang['perm_add_action']               = "إضافة";
 $lang['perm_edit_action']              = "تعديل";
 $lang['perm_delete_action']            = "حذف";
 $lang['link_permissions']              = "الصلاحيات";
$lang['perm_denied_link']               ="غير مصرح";
$lang['perm_denied_message']            ='<h3>خطأ! يبدو أن هناك خطأ ما .</h3>
                                            <p>
                                               ربما تكون هذه الصفحة محذوفة أو أنك غير مصرح لك بالدخول إلى هذه الصفحة.<br><br>
                                            </p>';


 $lang['select_group']                  = "اختر مجموع";
 $lang['full_permissions']              = "الصلاحيات كاملة";
 $lang['success_permissions_updated']   = "تم تحديث الصلاحيات بنجاح";
 $lang['error_permissions_not_updated'] = "<b>خطأ :</b> لم يتم تحديث الصلاحيات, \r\n حاول مرة أخرى بعد تحديث الصفحة";
 $lang['error_no_group_selected']       = "<b>خطأ :</b> لم يتم تحديد المجموعة";
$lang['log_edit_permissions']           ="تحديث الصلاحيات";
$lang['log_add_permissions']           ="إضافة الصلاحيات";

