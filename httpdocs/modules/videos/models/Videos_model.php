<?php

class Videos_Model extends CI_Model {

    private $table_cats = "videos_cats";
    private $table = "videos";

    function __construct() {
        parent:: __construct();
    }

    function get_all_cats($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("cat_created", "DESC");
        $Q = $this->db->get_where($this->table_cats, array("cat_deleted" => 0));
        $total = $this->db->get_where($this->table_cats, array("cat_deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_home_cats($t = "", $y = 0, $offset = 0, $limit = 100) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("cat_created", "DESC");
        $Q = $this->db->get_where($this->table_cats, array("cat_Type" => $t, "cat_status" => 1, "cat_deleted" => 0));//, "cat_Year" => $y
        $total = $this->db->get_where($this->table_cats, array("cat_Type" => $t, "cat_status" => 1, "cat_deleted" => 0))->num_rows();
        $data['data'] = $total > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_cats($id = 0) {
        $Q = $this->db->get_where($this->table_cats, array("cat_id" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert_cats($data) {
        $this->db->insert($this->table_cats, $data);
        return $this->db->insert_id();
    }

    function update_cats($id, $data) {
        $this->db->update($this->table_cats, $data, array("cat_id" => $id));
        return $this->db->affected_rows();
    }

    function delete_cats($id = 0, $data) {
        $this->db->update($this->table_cats, $data, array("cat_id" => $id));
        return $this->db->affected_rows();
    }

    function get_all($id = 0, $offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("vd_Created", "DESC");
        $Q = $this->db->get_where($this->table, array("vd_ParentID" => $id, "vd_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("vd_ParentID" => $id, "vd_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_home($fid = 0, $offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("vd_Created", "DESC");
        $Q = $this->db->get_where($this->table, array("vd_ParentID" => $fid, "vd_Active" => 1, "vd_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("vd_ParentID" => $fid, "vd_Active" => 1, "vd_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_year($year = 0, $offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("vd_Created", "DESC");
        $Q = $this->db->get_where($this->table, array("vd_Active" => 1, "vd_Year" => $year, "vd_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("vd_Active" => 1, "vd_Year" => $year, "vd_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get($id = 0) {
        $Q = $this->db->get_where($this->table, array("vd_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $this->db->update($this->table, $data, array("vd_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id, $data) {
        $this->db->update($this->table, $data, array("vd_ID" => $id));
        return $this->db->affected_rows();
    }

}
