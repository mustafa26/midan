<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("Videos_Model");
        $this->lang->load("videos", $this->languages[$this->user_lang]);
        $this->model = new Videos_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("vd_status");
    }


    function index($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("videos_gallery_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_cats($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_videos");
        $this->data['content'] = "videos/cats";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("videos/index"), $result['total'], $limit, 4);
        $this->template->render("template", $this->data);
    }

    private function _validate_cats() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("cat_name[ar]", langline("cat_name") . "(عربي)", "required|trim|max_length[255]");
        $this->form_validation->set_rules("cat_name[en]", langline("cat_name") . "(English)", "required|trim|max_length[255]");


        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function addcat() {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add_cat();
            exit;
        } 
    }

    private function _do_add_cat() {
        $this->_validate_cats();
        $upload_data = $this->do_upload("cat_image");
        if (!$this->errors) {
            $data['cat_name'] = serialize(post("cat_name"));
            if ($upload_data) {
                $data['cat_image'] = $upload_data['file_name'];
            }
            $data['cat_created_by'] = $this->user->user_id;
            $data['cat_Year'] = post("cat_Year");
            $data['cat_Type'] = post("cat_Type");
            $data['cat_created'] = date("Y-m-d H:i:s");
            $data['cat_status'] = post("cat_status");

            if ($project_id = $this->model->insert_cats($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("videos"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("videos?open=form"));
    }

    function editcat($id = 0) {
        if (!check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update_cat($id);
            exit;
        }
        $cat = $this->model->get_cats($id);
        $this->data['title'] = langline("videos_gallery_title") . " - " . $this->site->site_name;
        log_add_user_action($this->user->user_id, $id, "show", "log_show_videos");
        $this->data['content'] = "videos/edit_cat";
        $this->data['data'] = $cat;
        $this->template->render("template", $this->data);
    }

    private function _do_update_cat($id = 0) {
        $this->_validate_cats();
        $upload_data = $this->do_upload("cat_image");
        if (!$this->errors) {
            $data['cat_name'] = serialize(post("cat_name"));
            if ($upload_data) {
                $data['cat_image'] = $upload_data['file_name'];
            }
            $data['cat_status'] = post("cat_status");
            $data['cat_Year'] = post("cat_Year");
            $data['cat_Type'] = post("cat_Type");
            if ($this->model->update_cats($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_videos");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("videos"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("videos/editcat/$id"));
    }

    function delete_cat($id) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['cat_deleted'] = 1;
        if ($this->model->delete_cats($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("videos"));
    }

    function cats($id = 0,$offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['title'] = langline("vd_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($id,$offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_videos");
        $this->data['content'] = "videos/index";
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("videos/cats/$id"), $result['total'], $limit, 5);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        foreach ($this->languages as $key => $value) {
            $this->form_validation->set_rules("vd_Title[$key]", langline("vd_Title") . $value, "required|trim|max_length[255]");
        }

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add($fid = 0) {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add($fid);
            exit;
        }

        $this->data['title'] = langline("vd_title") . " - " . $this->site->site_name;
        $this->data['content'] = "videos/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add($fid = 0) {
        $this->_validate();
        if (!$this->errors) {
            $upload_data_img = $this->do_upload("vd_Photo");
            $upload_data_vdo = $this->do_upload("vd_File");
            
            $data['vd_ParentID'] = $fid;
            $data['vd_Title'] = serialize(post("vd_Title"));
            $data['vd_Date'] = serialize(post("vd_Date"));
            $data['vd_Year'] = date("Y");
            $data['vd_YouTube'] = post("vd_YouTube");
            //$data['vd_File'] = post("vd_File");
            $data['vd_Active'] = post("vd_Active") ? 1 : 0;
            $data['vd_Created'] = date("Y-m-d H:i:s");
            if ($upload_data_img) {
                $data['vd_Photo'] = $upload_data_img['file_name'];
            }

            if ($upload_data_vdo) {
                $data['vd_File'] = $upload_data_vdo['file_name'];
            }

            if ($this->model->insert($data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, 0, "add", "log_edit_videos"); 
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("videos/cats/".$fid));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("videos/add/".$fid));
    }

    function edit($fid = 0,$id = 0) {
        if (!check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($fid,$id);
            exit;
        }

        $this->data['title'] = langline("vd_title") . " - " . $this->site->site_name;
        $this->data['content'] = "videos/edit";
        $this->data['video'] = $this->model->get($id);
        $this->template->render("template", $this->data);
    }

    private function _do_update($fid = 0,$id = 0) {
        $this->_validate();
        if (!$this->errors) {
            $upload_data_img = $this->do_upload("vd_Photo");
            $upload_data_vdo = $this->do_upload("vd_File");
            $data['vd_Title'] = serialize(post("vd_Title"));
            $data['vd_Date'] = serialize(post("vd_Date"));
            $data['vd_YouTube'] = post("vd_YouTube");
            $data['vd_Active'] = post("vd_Active") ? 1 : 0;

            if ($upload_data_img) {
                $data['vd_Photo'] = $upload_data_img['file_name'];
            }

            if ($upload_data_vdo) {
                $data['vd_File'] = $upload_data_vdo['file_name'];
            } 
            
            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_videos");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("videos/cats/".$fid));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("videos/edit/$id"));
    }

    function delete($fid = 0, $id = 0) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['vd_Deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("videos/cats/$fid"));
    }

    private function do_upload($file_name = "userfile") {
        $this->load->library("upload");
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/videos";
            $config['allowed_types'] = 'gif|jpg|png|mp4';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                return $this->upload->data();
            }
        } else {
            return false;
        }
    }

}
