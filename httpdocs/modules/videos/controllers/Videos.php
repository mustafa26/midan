<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Videos extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Videos_Model", 'model');
    }

    public function index($t = "Racing", $y = 0) {
        $this->data['title'] = $this->site->site_name;
        $result = $this->model->get_all_home_cats($t);//$y
        $this->data['data'] = $result['data'];
      //  if ($y) {
      //      $this->data['year'] = $y;
            $this->data['content'] = "videos/all_year";
      //  } else
       //     $this->data['content'] = "videos/index";
        $this->template->render("template", $this->data);
    }

    public function all($t = "Racing",$fid = 0, $offset = 0) {
        $this->data['title'] = $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all_home($fid, $offset, $limit);
        $this->data['cats'] = $this->model->get_cats($fid);
        $this->data['data'] = $result['data'];
        $this->data['total'] = $result['total'];
        $this->data['pagination'] = $this->pagination->pager(site_url("videos/all/$t/$fid"), $result['total'], $limit, 5);
        $this->data['content'] = "videos/all";
        $this->template->render("template", $this->data);
    }

    public function v() {
        $this->data['title'] = $this->site->site_name;
        //$limit = 20;
        //$result = $this->model->get_all_home($offset, $limit);
        $this->data['content'] = "videos/index";
        //$this->data['data'] = $result['data'];
        //$this->data['pagination'] = $this->pagination->pager(admin_url("events"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

}
