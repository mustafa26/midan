<?php

class Admin extends Admin_Controller {

    private $model;

    function __construct() {
        parent:: __construct();
        $this->load->model("Pdf_Model");
        $this->lang->load("pdf", $this->languages[$this->user_lang]);
        $this->model = new Pdf_model();
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }
        $this->data['status'] = config_item("pdf_status");
    }

    function index($offset = 0) {
        if (!check_user_permission(current_module(), "show")) {
            redirect(admin_url("permissions/denied"));
        }

        $this->data['title'] = langline("pdf_pdf_title") . " - " . $this->site->site_name;
        $limit = 20;
        $result = $this->model->get_all($offset, $limit);
        log_add_user_action($this->user->user_id, 0, "show", "log_show_pdf");
        $this->data['content'] = "pdf/index";
        $this->data['data'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("pdf"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

    private function _validate() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("pdf_Name[ar]", langline("pdf_Name") . "(عربي)", "required|trim|max_length[255]");
        $this->form_validation->set_rules("pdf_Name[en]", langline("pdf_Name") . "(English)", "required|trim|max_length[255]");

        if ($this->form_validation->run() == false) {
            $this->errors = validation_errors();
            return false;
        }

        return true;
    }

    function add() {
        if (!check_user_permission(current_module(), "add")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_add();
            exit;
        }

        $this->data['title'] = langline("pdf_pdf_title") . " - " . $this->site->site_name;
        $this->data['content'] = "pdf/add";
        $this->template->render("template", $this->data);
    }

    private function _do_add() {
        $this->_validate();
        $upload_data = $this->do_upload("pdf_Img", "pdf");
        if (!$this->errors) {
            $data['pdf_Name'] = serialize(post("pdf_Name"));
            if ($upload_data) {
                $data['pdf_Img'] = $upload_data['file_name'];
            }
            $data['pdf_CreatedBy'] = $this->user->user_id;
            $data['pdf_Created'] = date("Y-m-d H:i:s");
            $data['pdf_Status'] = post("pdf_status");

            if ($project_id = $this->model->insert($data)) {
                //set action in user log
                log_add_user_action($this->user->user_id, 0, "add", "log_add_project");
                $this->set_message(langline("success_categories_created"), "success");
                redirect(admin_url("pdf"));
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("pdf?open=form"));
    }

    function edit($id = 0) {
        if (!check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }
        if (post()) {
            $this->_do_update($id);
            exit;
        }
        $cat = $this->model->get($id);
        $this->data['title'] = langline("pdf_pdf_title") . " - " . $this->site->site_name;
        log_add_user_action($this->user->user_id, $id, "show", "log_show_pdf");
        $this->data['content'] = "pdf/edit";
        $this->data['data'] = $cat;
        $this->template->render("template", $this->data);
    }

    private function _do_update($id = 0) {
        $this->_validate();
        $upload_data = $this->do_upload("pdf_Img", "pdf");
        if (!$this->errors) {
            $data['pdf_Name'] = serialize(post("pdf_Name"));

            if ($upload_data) {
                $data['pdf_Img'] = $upload_data['file_name'];
                unlink("uploads/pdf/" . post("pdf_Img_old"));
                unlink("uploads/pdf/" . post("pdf_Img_old"));
            }
            $data['pdf_Status'] = post("pdf_status");
            if ($this->model->update($id, $data)) {
                // join project to selected groups
                log_add_user_action($this->user->user_id, $id, "edit", "log_edit_pdf");
                $this->set_message(langline("success_project_updated"), "success");
                redirect(admin_url("pdf"));
            } else {
                $this->errors .= langline("success_project_updated_error");
            }
        }
        $this->keep_data();
        $this->set_message($this->errors, "error");
        redirect(admin_url("pdf/edit/$id"));
    }

    function delete($id) {
        if (!check_user_permission(current_module(), "delete") || !check_user_permission(current_module(), "edit")) {
            redirect(admin_url("permissions/denied"));
        }

        $data['pdf_deleted'] = 1;
        if ($this->model->delete($id, $data)) {
            $this->set_message(langline("success_project_deleted"), "success");
        } else {
            $this->set_message(langline("error_project_not_deleted"), "error");
        }
        log_add_user_action($this->user->user_id, $id, "delete", "log_delete_project");
        redirect(admin_url("pdf"));
    }

    function do_upload($file_name = "userfile", $file_module = "", $resize = false, $file_type = "", $allowed_types = "") {
        $this->load->library("upload");
        $this->load->library('image_lib');
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/" . $file_type;
            $config['allowed_types'] = $allowed_types != "" ? $allowed_types : 'pdf';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $config['max_size'] = '3000000';
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $_img_file = $this->upload->data();
                if ($resize) {
                    $config_thumb['image_library'] = 'gd2';
                    $config_thumb['source_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/$file_type" . $_img_file["file_name"];
                    $config_thumb['create_thumb'] = TRUE;
                    $config_thumb['maintain_ratio'] = TRUE;
                    $config_thumb['width'] = 250;
                    $config_thumb['height'] = 250;
                    $config_thumb['new_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/thumb";
                    $this->image_lib->initialize($config_thumb);
                    $this->image_lib->resize();
                }
                return $_img_file;
            }
        } else {
            return false;
        }
    }

}
