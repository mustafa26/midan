<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pdf extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Pdf_Model", 'model');
    }

    public function index($offset = 0) {
        $this->data['title'] = $this->site->site_name;
        $limit = 20;
        $this->data['content'] = "pdf/index";
        $result = $this->model->get_all();
        $this->data['pdf'] = $result['data'];
        $this->data['pagination'] = $this->pagination->pager(admin_url("pdf/index"), $result['total'], $limit, 3);
        $this->template->render("template", $this->data);
    }

}
