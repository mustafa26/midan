<?php

/* * *******************************************
 *  Project     : Pages Management
 *  File        : sliders_model.php
 *  Created at  : 11/10/15 - 12:12 PM  
 * ******************************************* */

class Pdf_Model extends CI_Model {

    private $table = "pdf";

    function __construct() {
        parent :: __construct();
    }

    /**
     * @param int $group
     * @param int $offset
     * @param int $limit
     * @return mixed
     */
    function get_all($offset = 0, $limit = 0) {
        if ($offset || $limit) {
            $this->db->limit($limit, $offset);
        }
        $Q = $this->db->get_where($this->table, array("pdf_Deleted" => 0));
        $total = $this->db->get_where($this->table, array("pdf_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get_all_active($offset = 0, $limit = 14) {
        $this->db->limit($limit, $offset);
        $Q = $this->db->get_where($this->table, array("pdf_Deleted" => 0,"pdf_Status" => 1));
        $total = $this->db->get_where($this->table, array("pdf_Deleted" => 0,"pdf_Status" => 1))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }

    function get($id = 0) {
        $Q = $this->db->get_where($this->table, array("pdf_ID" => $id));
        return ($Q->num_rows() > 0) ? $Q->row() : false;
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_ID();
    }

    function update($id, $data) {
        $this->db->update($this->table, $data, array("pdf_ID" => $id));
        return $this->db->affected_rows();
    }

    function delete($id = 0, $data) {
        $this->db->update($this->table, $data, array("pdf_ID" => $id));
        return $this->db->affected_rows();
    }

}
