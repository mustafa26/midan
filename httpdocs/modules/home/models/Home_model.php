<?php

/**
 * Home Model 
 */
class Home_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get() {
        return FALSE;
    }

    function get_settings() {
        //$Q=$this->db->get('module_home');
        //return $Q->first_row();
    }

    function get_settings_ads() {
        $Q = $this->db->query("SELECT * FROM `settings` WHERE `ads_from` <= DATE_FORMAT(NOW(),'%Y-%m-%d') AND `ads_to` >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        return ($Q->num_rows() > 0) ? True : FALSE;
    }

    function get_all_pages($id = 0) {
        $Q = $this->db->get_where("pages", array("page_ParentID" => $id, "page_Deleted" => 0));
        $total = $this->db->get_where("pages", array("page_ParentID" => $id, "page_Deleted" => 0))->num_rows();
        $data['data'] = $Q->num_rows() > 0 ? $Q->result() : false;
        $data['total'] = $total;
        return $data;
    }
	
	function get_winners(){
        $this->db->order_by("a.RaceNum");
        $this->db->join("events e", "e.event_ID=a.event_ID", "left");
        $this->db->join("races r", "r.rac_ID=a.RaceNum", "left");
        $Q = $this->db->get_where("las_list a", array("a.Car" => 1));
        $data = $Q->num_rows() > 0 ? $Q->result() : false;
        return $data;
	}
    function get_race_today(){
        $Q = $this->db->query("SELECT * FROM `events` WHERE `event_From` = DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $data = $Q->num_rows() > 0 ? $Q->result() : false;
        return $data;
    }

    function get_races(){
        $Q = $this->db->query("SELECT * FROM `events` WHERE `event_Year` = DATE_FORMAT(NOW(),'%Y')");
        $data = $Q->num_rows() > 0 ? $Q->result() : false;
        return $data;
    }

    function get_all_photos(){
        $Q = $this->db->get_where("galleries", array("gallery_Deleted" => 0));
        $total = $Q->num_rows();
        return $total;
    }

    function get_all_videos(){
        $Q = $this->db->get_where("videos", array("vd_Active" => 1,"vd_Deleted" => 0));
        $total = $Q->num_rows();
        return $total;
    }
	function get_all_news(){
        $Q = $this->db->get_where("news", array("nws_Deleted" => 0));
		$total = $Q->num_rows();
		return $total;		
	}
	 

}
