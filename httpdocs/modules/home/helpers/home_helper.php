<?php
/**
 *
 * get main Items
 * @param void ()
 *
 */

function main_menu()
{
    $ins = &get_instance();
    $ins->load->model("items/Items_model");
    $items = $ins->Items_model->get_all(0, 0, 10, "asc", array("mi_parent" => "0", "mi_atmenu" => "1", "mi_show" => "1"));
    $_html = '';
    $_html .= '<div class="menu_area">
                    <div id="menu">
                        <ul id="nav"> 
       ';
    if ($items) {
        $n = 0;
        foreach ($items['data'] as $val) {
            $_title = explode("::", json_decode($val->mi_title)->arabic);
            $_subitems = $ins->Items_model->get_all($val->mi_id, 0, 10);
            if ($_subitems['total'] && $val->mi_id != 8) {
                $_html .= ' <div class="menu_align">
                            <li><a href="#">
                            <span class="menu_item">
                            <span class="menu_image">
                            <img src="{base_url}uploads/items/' . $val->mi_icon . '" width="203" height="80" />
                            </span>
                            <span class="menu_title_a" style="background-color:' . $val->mi_bgcolor . ';">
                            <span class="menu_title_ar">' . $_title[0] . '</span>
                            <span class="menu_title_en">' . $_title[1] . '</span>
                            </span>
                            </span>
                            </a>
                                    <ul>	 ';
                foreach ($_subitems['data'] as $v) {
                    $_html .= '
                         <li><a href="{base}' . $v->mi_name . '/index/' . $v->mi_id . '" style="background-color:' . $val->mi_bgcolor . ';">' . json_decode($v->mi_title)->arabic . '</a></li>
                             ';
                }
                $_html .= '</ul>
                                    </li>	
                    </div>';
            } else {
                $_html .= '
                                        <div class="menu_align"> 
                                        <a href="{base}' . $val->mi_name . '">
                                    <span class="menu_item">
                                <span class="menu_image">
                                <img src="{base_url}uploads/items/' . $val->mi_icon . '" width="203" height="80" />
                                </span>
                                <span class="menu_title_a" style="background-color:' . $val->mi_bgcolor . ';">
                                <span class="menu_title_ar">' . $_title[0] . '</span>
                                <span class="menu_title_en">' . $_title[1] . '</span>
                                </span>
                                </span>
                                </a> </div>';

            }
            $n++;
        }
    }

    $_html .= '
                                </ul>
                    </div>
                    </div>';
    return $_html;
}

/**
 *
 * get News
 * @param void ()
 *
 */

function get_winners()
{
    global $themepath;
    $_html = '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated">
    <div class="col-md-12 shortcut light-brown-border">
        <h1>
        <i><img src="' . $themepath . '/assets/lib/img/2018/img/sections/car-icon.png" alt=""/></i>
   الفائزون بالسيارات
</h1>
        <div id="myCarousel" class="carousel slide">
            <div class="icon"><img src="' . $themepath . '/assets/lib/img/2018/img/sections/camel.png"></div>
            <div class="carousel-inner">';
    $ins = &get_instance();
    $ins->load->model("home/Home_model");
    $winners = $ins->Home_model->get_winners();
    $_html .= '';
    if ($winners) {
        $n = 1;
        foreach ($winners as $val) {
            $_html .= '<div class="item ' . ($n == 1 ? 'active' : '') . '">
                            <div class="row-fluid">
                               <center class="col-sm-12 brown-txt titl">
                               ' . $val->Oname . '
                               </center>
                               <div class="col-sm-12 con">
                                <img src="' . $themepath . '/assets/lib/img/2018/img/sections/date.png" width="16" height="" alt="" style="margin-left:8px"/>   ' . _t($val->event_SubTitle, "arabic") . '
                               </div>
                                <div class="col-sm-12 con">
                             <img src="' . $themepath . '/assets/lib/img/2018/img/sections/list.png" width="16" height="" alt="" style="margin-left:8px"/>    الشوط ' . $val->rac_Name . ' / ' . _t($val->event_Title, "arabic") . '
                               </div>
                               <div class="col-sm-12 con">
                              <img src="' . $themepath . '/assets/lib/img/2018/img/sections/cup.png" width="16" height="" alt="" style="margin-left:8px"/>   المركز ' . langline("NumRace_" . $val->winner_sort) . '
                               </div>
                               <div class="col-sm-12 con">
                                <img src="' . $themepath . '/assets/lib/img/2018/img/sections/camel3.png" width="16" height="" alt="" style="margin-left:8px"/>   اسم الناقة : ' . $val->Cname . '
                               </div>
                            </div><!--/row-fluid-->
                        </div>
						 ';
            $n++;
        }
    } else $_html .= '<br><br><br><center><span class="text-danger">عذرا .. لايوجد بيانات</span></center>';
    $_html .= '</div>
            <a class="right carousel-control" href="#myCarousel" data-slide="prev">‹</a>
            <a class="left carousel-control" href="#myCarousel" data-slide="next">›</a>
        </div>
    </div>
</div>';
    echo $_html;
}

function get_race_today($upload_path = "")
{
    global $themepath;
    $_html = '
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated">
    <div class="col-md-12 shortcut light-brown-border">
        <h1><i><img src="' . $themepath . '/assets/lib/img/2018/img/sections/race-icon.png" alt=""/></i> سباقات
            اليوم</h1>

        <div id="myCarousel115" class="carousel slide">
            <div class="carousel-inner">';
    $ins = &get_instance();
    $ins->load->model("home/Home_model");
    $races = $ins->Home_model->get_race_today();
    if ($races) {
        $n = 1;
        foreach ($races as $val) {
            $_html .= '<div class="item ' . ($n == 1 ? 'active' : '') . '">
                   	<div class="row-fluid">
						<center>
							<a href="' . site_url("events/subscribers/" . $val->event_ID) . '">
							<img src="' . $upload_path . '/events/' . $val->event_HeaderPhoto . '" border="0">
							</a>
						</center>
						 <div class="col-sm-12 no-margin no-padding">
							  <center style="font-family:\'bein\'; color:#96291f; margin-top: 3px;margin-bottom: 6px;">
							  ' . _t($val->event_Title, "arabic") . '
							  </center>
						</div>
                <div class="more">
                    <a href="' . site_url("events/subscribers/" . $val->event_ID) . '" >استعراض </a>
                </div>
					</div>
                 </div> 
				 ';
            $n++;
        }
    } else $_html .= '<br><br><br><center><span class="text-danger">عذرا .. لايوجد بيانات</span></center>';
    $_html .= '
            </div>
            <a class="right carousel-control" href="#myCarousel115" data-slide="prev">‹</a>
            <a class="left carousel-control" href="#myCarousel115" data-slide="next">›</a>
        </div>
    </div>
</div>';
    echo $_html;
}




function get_result_races()
{
    global $themepath;
    $_html = '
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated">
    <div class="col-md-12 shortcut light-brown-border">
        <h1><i><img src="'.$themepath.'/assets/lib/img/2018/img/sections/daily-icon.png" alt=""/></i> نتائج
            السباقات </h1>

        <div id="myCarousel7" class="carousel slide">

            <div class="icon"><img src="'.$themepath.'/assets/lib/img/2018/img/sections/camel.png"></div>
            <div class="carousel-inner">';
    $ins = &get_instance();
    $ins->load->model("home/Home_model");
    $races = $ins->Home_model->get_races();
    if ($races) {
        $n = 1;
        foreach ($races as $val) {
            $_html .= '
                <div class="item ' . ($n == 1 ? 'active' : '') . '">
                    <div class="row-fluid">
                        <div class="col-sm-12 no-margin no-padding">
                            <center class="titl">' . _t($val->event_Title, "arabic") . '</center>
                            <div class="con col-sm-12"><img
                                    src="'.$themepath.'/assets/lib/img/2018/img/sections/date.png">
                                 ' . _t($val->event_SubTitle, "arabic") . '
                            </div>
                            <div class="con col-sm-12">
                               <img src="'.$themepath.'/assets/lib/img/2018/img/sections/race.png"> عدد
                                الأشواط : ' . $val->event_RaceNum . ' أشواط
                            </div>

                        </div>
                    </div>
                    <div class="more">
                        <a href="'.site_url("events/results/" . $val->event_ID).'">
                        النتائج
                        </a>
                    </div>
                </div>
				 ';
            $n++;
        }
    } else $_html .= '<br><br><br><center><span class="text-danger">عذرا .. لايوجد بيانات</span></center>';
    $_html .= '
            </div>
            <a class="right carousel-control" href="#myCarousel7" data-slide="prev">‹</a>
            <a class="left carousel-control" href="#myCarousel7" data-slide="next">›</a>
        </div>
    </div>
</div>';
    echo $_html;
}



function get_statistics_races()
{
    global $themepath;
    $_html = '
    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 wow fadeInUp animated">
        <div class="stastics-bg">
            <form class="form">
                <select id="urlSelect" onchange="window.location = jQuery(\'#urlSelect option:selected\').val();"
                        class="form-control">
                    <option disabled="disabled" selected="selected">إختر الشوط</option>';
    $ins = &get_instance();
    $ins->load->model("home/Home_model");
    $races = $ins->Home_model->get_races();
    if ($races) {
        $n = 1;
        foreach ($races as $val) {
            $_html .= '
                <option value="'.site_url("events/statistic/" . $val->event_ID).'">' . _t($val->event_Title, "arabic") . '</option>
				 ';
            $n++;
        }
    } else $_html .= '<br><br><br><center><span class="text-danger">عذرا .. لايوجد بيانات</span></center>';
    $_html .= '
                </select>
            </form>
        </div>
    </div>';
    echo $_html;
}



function last_news($_limit = 0)
{
    $_html = '';
    $ins = &get_instance();
    $ins->load->model("news/News_model");
    $news = $ins->News_model->get_news_home($_limit);
    $_html .= '<div class="shurtcuts_right">
                    <div class="shirtcuts_title">أخر الأخبــار</div>';
    if ($news) {
        $n = 1;
        foreach ($news as $val) {
            $_html .= '<div class="shurtcuts_news">
                            <div class="shurtcuts_image"><a href="{base}news/view/' . $val->mn_id . '"><img src="{base_url}uploads/news/_thumb/' . $val->mn_photo . '" width="95" height="69" border="0" /></a></div>
                            <div class="shirtcuts_txt">
                            <div class="news_title"><a href="{base}news/view/' . $val->mn_id . '">' . _t($val->mn_title, "arabic") . '</a></Div>
                            <div class="shurtcut_detials"><a href="{base}news/view/' . $val->mn_id . '">' . word_limiter(strip_tags(_t($val->mn_content, "arabic")), 32) . '</a>
                            </div>
                            </div>
                                <div class="articals_reads_home">القراءات : ' . $val->mn_visited . ' قراءة</div>
                            </div>';
            if ($n != 1) $_html .= '<div class="shurtcuts_hr"><hr /></div>';
            $n++;
        }
    }
    $_html .= '</div>';
    return $_html;
}


/**
 *
 * get Articals
 * @param void ()
 *
 */


function last_articals($_limit = 0)
{
    $_html = '';
    $ins = &get_instance();
    $ins->load->model("articles/Articles_model");
    $arts = $ins->Articles_model->get_articles_home($_limit);

    $_html .= '<div class="shurtcuts_left">
                        <div class="shirtcuts_title">أخر المقــالات</div>  
        ';
    if ($arts) {
        foreach ($arts as $val) {
            $_html .= '<div class="shurtcuts_artical">
                                <div class="shurtcut_artical">
                                <a href="{base}articles/view/' . $val->mn_id . '">' . _t($val->mn_title, "arabic") . '</a>
                                <div class="articals_reads_home">القراءات : ' . $val->mn_visited . ' قراءة</div>
                                </div>
                            </div>
                            <div class="shurtcuts_hr"><hr /></div>';
        }
    }
    $_html .= '</div>';
    return $_html;
}


/**
 *
 * get Videos
 * @param (str) $userlang ... language
 *
 */


function view_videos_html($userlang = "arabic")
{
    $_html = '';
    $i = 1;
    $ins = &get_instance();
    $ins->load->model("videos/videos_model");
    $videos = $ins->videos_model->get_all(9, 0, 4);

    foreach ($videos['data'] as $key => $v) {
        $t = unserialize($v->mv_title);
        $_html1 .= '<li><a href="#" title="[link title]">' . $i . '</a></li>';
        $_html2 .= '<div class="sliderkit-panel">
												<div class="video_area">
													<a href="#inline' . $i . '" rel="inline-600-450" class="pirobox" title="Inline content">
													<span><img src="{base_url}uploads/videos/' . $v->mv_img . '" width="278" height="187" border="0" title="' . $t[$userlang] . '" /></span>
													<div class="video_play"><img src="{themepath}/images/videos/video_play.png" width="44" height="44" border="0" />
													</div> </a>
												</div>
											</div>
											
		<div id="inline' . $i . '" style="display:none; background:white; ">
		<div class="video">';
        if ($v->mv_typevideo == "tube") {
            $_html2 .= '<iframe width="520" height="320" src="http://www.youtube.com/embed/' . $v->mv_src . '" frameborder="0" allowfullscreen></iframe>';
        } else {
            $_html2 .= '<object type="application/x-shockwave-flash" width="520" height="320"  wmode="transparent" data="{themepath}/videos/hwplayer-43.swf?file={base_url}uploads/videos/' . $v->mv_src . '"></object>';
        }
        $_html2 .= '</div><div class="video_detials"> ' . json_decode($v->mv_dsrip)->$userlang . '</div></div> ';
        $i++;
    }
    $_html .= '<div class="home_culmin">
						<div class="home_culmin_title_videos">
							<a href="video_album.html">ألبوم الفيديوهات</a>
						</div>
						<div class="home_culm_area">
							<div class="home_culm">
								<!-- Start contentslider-std -->
								<div class="sliderkit contentslider-std">
									<div class="sliderkit-btn sliderkit-go-btn sliderkit-go-prev">
										<a href="#" title="Previous"><span>Previous</span></a>
									</div>
									<div class="sliderkit-btn sliderkit-go-btn sliderkit-go-next">
										<a href="#" title="Next"><span>Next</span></a>
									</div>
									<div class="sliderkit-nav">
										<div class="sliderkit-nav-clip">
											<ul>
												' . $_html1 . '
											</ul>
										</div>
									</div>
									<div class="sliderkit-panels">';

    $_html .= $_html2 . '
									</div>
								</div>
								<!-- // end of contentslider-std -->

							</div>
						</div>
					</div>';

    return $_html;
}

/**
 *
 * get Videos
 * @param (str) $userlang ... language
 *
 */


function view_images_html($userlang = "arabic")
{
    $_html = '';
    $i = 1;
    $x = 1;
    $ins = &get_instance();
    $ins->load->model("gallery/gallery_model");
    $gallery = $ins->gallery_model->get_all(3, 0, 24);
    $_html .= '
<div class="home_culmin">
<div class="home_culmin_title_album"><a href="{base}gallery">ألبــوم الصور</a></div>
<div class="home_culm_area">
<div class="home_culm">
<div id="showcase2" class="showcase">';
// 8
    foreach ($gallery['data'] as $key => $v) {
        $t = unserialize($v->mg_title);
        if ($i == 1) $_html .= '
		<!--client-->
		<div class="showcase-slide">
		<div class="showcase-content">';
        $_html .= '
<div class="image">  <!--  rel="gallery"  class="pirobox_gall"-->
<a href="{base}gallery/view/' . $v->mg_fid . '" title="' . $t[$userlang] . '">
<img src="{base_url}uploads/gallery/thumb/' . $v->mg_photo . '" width="63" height="63" border="0" />
</a>
</div>';
        if ($i == 8 || $x == $gallery['total']) {
            $_html .= '</div></div>';
            $i = 0;
        }
        $i++;
        $x++;
    }
    $_html .= '
</div></div> 
</div>
</div>';

    return $_html;
}
/**
 *
 * get Videos
 * @param (str) $userlang ... language
 *
 */
  
	 