<?php
$lang['title_home_page']="الرئيسية";
$lang['text_home_page']="Home Page";
$lang['label_home_page_name']="Home Page Name";
$lang['label_welcome_message']="Welcome Message";
$lang['label_welcome_title']="Message Title";




$lang['NumRace_1'] = "الأول";
$lang['NumRace_2'] = "الثاني";
$lang['NumRace_3'] = "الثالث";
$lang['NumRace_4'] = "الرابع";
$lang['NumRace_5'] = "الخامس";
$lang['NumRace_6'] = "السادس";
$lang['NumRace_7'] = "السابع";
$lang['NumRace_8'] = "الثامن";
$lang['NumRace_9'] = "التاسع"; 
$lang['NumRace_10'] = "العاشر"; 
$lang['NumRace_11'] = "الحادي عشر"; 
$lang['NumRace_12'] = "الثاني عشر"; 
$lang['NumRace_13'] = "الثالث عشر"; 
$lang['NumRace_14'] = "الرابع عشر"; 
$lang['NumRace_15'] = "الخامس عشر"; 
$lang['NumRace_16'] = "السادس عشر"; 
$lang['NumRace_17'] = "السابع عشر"; 
$lang['NumRace_18'] = "الثامن عشر"; 
$lang['NumRace_19'] = "التاسع عشر";
$lang['NumRace_20'] = "العشرين";

