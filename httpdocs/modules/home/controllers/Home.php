<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Home extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("events/Events_Model", 'model');
        $this->load->helper("home");
    }

    public function index() {
        $this->data['title'] = $this->site->site_name;
        $this->data['total'] = $this->model->get_statistices_total();
        $this->data['total_oman'] = $this->model->get_stby_Nationlaty("oman");
        $this->data['total_uea'] = $this->model->get_stby_Nationlaty("uea");
        $this->data['total_ksa'] = $this->model->get_stby_Nationlaty("ksa");
        //$this->data['total_kba'] = $this->model->get_stby_Nationlaty("kba");
        $this->data['total_qatar'] = $this->model->get_stby_Nationlaty("qatar");
        //$this->data['total_kiwat'] = $this->model->get_stby_Nationlaty("kiwat");
        $this->data['content'] = "home/index";
        $this->template->render("template", $this->data);
    } 

}
