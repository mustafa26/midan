<?php

(defined('BASEPATH')) OR exit('No direct script access allowed'); 

class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->append_lang("home_admin");
    }

    /**
     * Display all payment methods
     * @access public .
     * @return void.
     */
    
    public function index() {
        $this->is_logged();
        $data['title'] = $this->site_name . " | " . langline('title_contact'); 
        $data['content'] = "admin/home"; 
        $this->crud->set_theme('flexigrid');  
        $this->crud->set_table('module_home'); 
        $this->crud->set_subject(langline("text_home_page"));  
        $this->crud->required_fields('home_title'); 
        $this->crud->edit_fields("home_title", "welcome_title", "welcome_photo", "welcome_message");
        $this->crud->set_field_upload("welcome_photo", "uploads/images");

        $this->crud->unset_add();
        $this->crud->unset_delete();
        $this->crud->unset_export();

        $this->crud->columns("home_title", "welcome_title", "welcome_message");
        //fields settings. validation rules .
        $this->crud->set_rules("home_title", "lang:label_method_name", "required|trim|max_lignth[100]");

        /*         * Display Settings... this settings to use the active language * */
        $this->crud->display_as("home_title", langline("label_home_page_name"));
        $this->crud->display_as("welcome_message", langline("label_welcome_message"));
        $this->crud->display_as("welcome_title", langline("label_welcome_title"));

        $output = $this->crud->render();
        // get javascript links from CRUD library.
        foreach ($output->js_files as $file)
            $this->js($file);

        // get css links from CRUD library.
        foreach ($output->css_files as $file)
            $this->css($file);

        $data['data'] = $output->output;

        $data['content'] = "admin/home.tpl";

        $data = array_merge((array) $output, $data);

        $this->template->render('template', $data);
    }

}
