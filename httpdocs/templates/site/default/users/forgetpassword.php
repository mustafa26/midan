<?php if (isset($_SESSION['mlogged_in'])): ?>
    <?php redirect(site_url("users/profile")); ?>
<?php else: ?>
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/demo.css">

    <div class="col-md-12">
        <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
            <h3>تسجيل دخول</h3>

            <h3>
                <?php echo theme_messages(); ?>
            </h3>
            <form class="form-signin col-md-10" action="" method="post">
                <div class="form-group col-sm-12 pull-left">
                    <label for="inputEmail" class="col-md-4 col-sm-4"><i
                            class="fa fa-male"></i> <?php echo langline("UR_Phone") ?></label>
                    <input type="tel" id="phone" name="Phone" class="login-field col-md-4 col-sm-8"
                           placeholder="<?php echo langline("UR_Phone") ?>" required autofocus
                           value="<?php echo input_value("Phone") ?>">
                </div>
                <div class="form-group col-sm-12 pull-left">
                    <label for="inputEmail" class="col-md-4 col-sm-4"><i
                            class="fa fa-male"></i> <?php echo langline("UR_NationalID") ?></label>
                    <input type="tel" id="nationalid" name="NationalID" class="login-field col-md-4 col-sm-8"
                           placeholder="<?php echo langline("UR_NationalID") ?>" required autofocus
                           value="<?php echo input_value("NationalID") ?>">
                </div>
                <?php if (segment(3) == "stp2") { ?>
                    <div class="form-group col-sm-12 pull-left">
                        <label for="inputPassword" class="col-md-4 col-sm-4"><i
                                class="fa fa-lock"></i> <?php echo langline("users_new_password") ?></label>
                        <input type="password" id="UserPassword" name="UserPassword"
                               class="login-field col-md-4 col-sm-8"
                               placeholder="<?php echo langline("users_user_password") ?>" required>
                    </div>
                    <div class="form-group col-sm-12 pull-left">
                        <label for="ReUR_Password"
                               class="col-md-4 col-sm-4 col-xs-12"><i
                                class="fa fa-lock"></i> <?php echo langline("users_user_password_confirm") ?></label>
                        <input type="password" name="UserRePassword" id="UserRePassword"
                               class="login-field col-md-4 col-sm-8"
                               placeholder="<?php echo langline("users_user_password_confirm") ?>" required>
                    </div>
                    <div class="form-group col-sm-12 pull-left">
                        <label for="activecode" class="col-md-4 col-sm-4"><i class="fa fa-lock"></i>
                            <?php echo langline("Active_Code") ?>
                            <?php echo langline("Send_Active_Code") ?>
                        </label>
                        <input type="number" name="activecode" id="activecode" class="login-field col-md-4 col-sm-8"
                               placeholder="<?php echo langline("Active_Code") ?>" required>
                    </div>
                <?php } ?>


                <div class="form-group col-sm-12 pull-left">
                    <label for="captcha"
                           class="col-md-4 col-sm-4 col-xs-12"><i
                            class="fa fa-lock"></i> <?php echo langline("Retype_IMGCode") ?></label>
                    <input name="captcha" id="captcha" type="text" class="login-field col-md-4 col-sm-8" value=""
                           placeholder="<?php echo langline("ImageCode") ?>" required>

                    <p class="login-field col-md-4 col-sm-8">
                        <span id="captImg"><?php echo $captchaImg; ?></span>
                        <a href="javascript:void(0);" class="refreshCaptcha">
                            <img src="<?php echo base_url() . 'assets/images/refresh.png'; ?>"/>
                        </a>
                    </p>
                </div>
                <div class="form-group">
                    <button class="btn btn-lg btn-default col-md-offset-4 col-sm-offset-4 btn-submit btn-primary"
                            type="submit"><?php echo langline("Continue") ?> > <?php echo langline("login") ?></button>
                </div>
                <input name="captchaCode" value="<?php echo $this->session->userdata('captchaCode')?>" type="hidden">
            </form>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo site_url() ?>assets/buildfalg/js/intlTelInput.js"></script>
    <script>
        // $("#phone").intlTelInput();
        $("#phone").intlTelInput({
            initialCountry: "om",
            onlyCountries: ['om', 'ae', 'bh', 'sa', 'qa', 'kw'],
            utilsScript: "build/js/utils.js"
        });
    </script>
<?php endif; ?>
<script>
    $(document).ready(function () {
        $('.refreshCaptcha').on('click', function () {
            $.get('<?php echo site_url('users/refresh'); ?>', function (data) {
                $('#captImg').html(data);
            });
        });
    });
</script>
