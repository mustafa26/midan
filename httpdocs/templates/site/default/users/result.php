
<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3> <?php echo langline("success_user_created") ?></h3> 
        <h3>
            <?php echo theme_messages(); ?>
        </h3>
        <h4>
            فضلا .. 
            <br><br>
            ستصلك رسالة تفعيل علي هاتفك
            <br>
            قم بالدخول لحساب لإستكمال تفعيل حسابك
            <br><br>
            <a href="<?php echo site_url("users/login?rt=1") ?>" class="btn btn-lg btn-primary  btn-submit">
                أضغظ هنا
            </a>
            <!-- فضلا قم بفحص بريدك الالكتروني لإكمال عملية التسجيل.. -->
        </h4>
    </div> 
</div>