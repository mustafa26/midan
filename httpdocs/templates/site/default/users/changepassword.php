<?php if (isset($_SESSION['mlogged_in'])): ?>
    <div class="col-md-12">
        <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
            <h3>تسجيل دخول</h3>

            <h3>
                <?php echo theme_messages(); ?>
            </h3>

            <form class="form-signin col-md-10" action="" method="post">

                <div class="form-group col-sm-12 pull-left">
                    <label for="inputPassword" class="col-md-4 col-sm-4"><i
                            class="fa fa-lock"></i> <?php echo langline("users_new_password") ?></label>
                    <input type="password" id="UserPassword" name="UserPassword"
                           class="login-field col-md-4 col-sm-8"
                           placeholder="<?php echo langline("users_user_password") ?>" required>
                </div>
                <div class="form-group col-sm-12 pull-left">
                    <label for="ReUR_Password"
                           class="col-md-4 col-sm-4 col-xs-12"><i
                            class="fa fa-lock"></i> <?php echo langline("users_user_password_confirm") ?></label>
                    <input type="password" name="UserRePassword" id="UserRePassword"
                           class="login-field col-md-4 col-sm-8"
                           placeholder="<?php echo langline("users_user_password_confirm") ?>" required>
                </div>
                <div class="form-group col-sm-12 pull-left">
                    <label for="captcha"
                           class="col-md-4 col-sm-4 col-xs-12"><i
                            class="fa fa-lock"></i> <?php echo langline("Retype_IMGCode") ?></label>
                    <input name="captcha" id="captcha" type="text" class="login-field col-md-4 col-sm-8" value=""
                           placeholder="<?php echo langline("ImageCode") ?>" required>

                    <p class="login-field col-md-4 col-sm-8">
                        <span id="captImg"><?php echo $captchaImg; ?></span>
                        <a href="javascript:void(0);" class="refreshCaptcha">
                            <img src="<?php echo base_url() . 'assets/images/refresh.png'; ?>"/>
                        </a>
                    </p>
                </div>
                <div class="form-group">
                    <button class="btn btn-lg btn-default col-md-offset-4 col-sm-offset-4 btn-submit btn-primary"
                            type="submit"><?php echo langline("btn_save") ?></button>
                </div>
                <input name="captchaCode" value="<?php echo $this->session->userdata('captchaCode')?>" type="hidden">
            </form>
        </div>
    </div>
<?php else: ?>
    <?php redirect(site_url("users/login")); ?>
<?php endif; ?>