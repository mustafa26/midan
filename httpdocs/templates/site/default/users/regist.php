<link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/intlTelInput.css">
<link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/demo.css">

<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3>تسجيل عضو جديد</h3>

        <h3>
            <?php echo theme_messages(); ?>
        </h3>

        <?php
        $_action = "";
        $_attributes = array(
            "id" => "formdata",
            "method" => "post",
            "class" => "form-signin col-md-8",
            "enctype" => "multipart/form-data");
        echo form_open($_action, $_attributes);
        ?>
        <div class="form-group col-sm-12 pull-left">
            <label for="phone" class="col-md-4 col-sm-4 col-xs-12"><span
                    style="color:red">* </span><?php echo langline("UR_Phone") ?></label>
            <input type="tel" maxlength="16" name="UR_Phone" id="phone" value="<?php echo input_value("UR_Phone") ?>"
                   required>
        </div>
        <div class="form-group col-sm-12 pull-left">
            <label for="UR_Password" class="col-md-4 col-sm-4 col-xs-12"><span
                    style="color:red">* </span><?php echo langline("users_user_password") ?></label>
            <input type="password" name="UR_Password" id="UR_Password" class="login-field col-md-8 col-sm-8 col-xs-12"
                   placeholder="<?php echo langline("users_user_password") ?>" required>
        </div>
        <div class="form-group col-sm-12 pull-left">
            <label for="ReUR_Password" class="col-md-4 col-sm-4 col-xs-12"><span
                    style="color:red">* </span><?php echo langline("users_user_password_confirm") ?></label>
            <input type="password" name="ReUR_Password" id="ReUR_Password"
                   class="login-field col-md-8 col-sm-8 col-xs-12"
                   placeholder="<?php echo langline("users_user_password_confirm") ?>" required>
        </div>
        <div class="form-group col-sm-12 pull-left">
            <label class="col-md-4 col-sm-4"><span style="color:red">* </span><?php echo langline("users_nikName") ?>
            </label>
            <select name="nikName" id="nikName" class="login-field col-md-8 col-sm-8 col-xs-12"> <option
                    value="<?php echo langline("users_nikName_5"); ?>"  <?php echo input_value("nikName") == langline("users_nikName_5") ? 'selected' : '' ?>><?php echo langline("users_nikName_5"); ?></option>
                <option
                    value="<?php echo langline("users_nikName_1"); ?>" <?php echo input_value("nikName") == langline("users_nikName_1") ? 'selected' : '' ?>><?php echo langline("users_nikName_1"); ?></option>
                <option
                    value="<?php echo langline("users_nikName_2"); ?>" <?php echo input_value("nikName") == langline("users_nikName_2") ? 'selected' : '' ?>><?php echo langline("users_nikName_2"); ?></option>
                <option
                    value="<?php echo langline("users_nikName_3"); ?>" <?php echo input_value("nikName") == langline("users_nikName_3") ? 'selected' : '' ?>><?php echo langline("users_nikName_3"); ?></option>
                <option
                    value="<?php echo langline("users_nikName_4"); ?>" <?php echo input_value("nikName") == langline("users_nikName_4") ? 'selected' : '' ?>><?php echo langline("users_nikName_4"); ?></option>
                </select>
            <label class="col-md-4 col-sm-4"><span style="color:red">* </span><?php echo langline("users_user_name") ?>
            </label>

            <input type="text" id="FName" name="FName" class="login-field col-md-2 col-sm-8"
                   placeholder="<?php echo langline("users_FName") ?>" value="<?php echo input_value("FName") ?>">
            <input type="text" id="SName" name="SName" class="login-field col-md-2 col-sm-8"
                   placeholder="<?php echo langline("users_SName") ?>" value="<?php echo input_value("SName") ?>">
            <input type="text" id="TName" name="TName" class="login-field col-md-2 col-sm-8"
                   placeholder="<?php echo langline("users_TName") ?>" value="<?php echo input_value("TName") ?>">
            <input type="text" id="QName" name="QName" class="login-field col-md-2 col-sm-8"
                   placeholder="<?php echo langline("users_QName") ?>" value="<?php echo input_value("QName") ?>">
        </div>
        <?php /*
             * <div class="form-group">
              <label for="UR_Phone" class="col-md-4 col-sm-4"><?php echo langline("UR_Phone"); ?></label>
              <input type="number" name="UR_Phone" id="UR_Phone" class="login-field col-md-8 col-sm-8" placeholder="<?php echo langline("UR_Phone") ?>" value="<?php echo input_value("UR_Phone") ?>"  >
              </div>
             */
        ?>
        <div class="form-group col-sm-12 pull-left">
            <label for="UR_Nationlaty" class="col-md-4 col-sm-4 col-xs-12"><span
                    style="color:red">* </span><?php echo langline("UR_Nationlaty"); ?></label>
            <select name="UR_Nationlaty" id="UR_Nationlaty" class="login-field col-md-8 col-sm-8 col-xs-12">
                <option value="oman" <?php echo input_value("UR_Nationlaty") == "oman" ? 'selected' : '' ?> >سلطنة
                    عُمان
                </option>
                <option <?php echo input_value("UR_Nationlaty") == "uea" ? 'selected' : '' ?> value="uea">الإمارات
                    العربية المتحدة
                </option>
                <option <?php echo input_value("UR_Nationlaty") == "ksa" ? 'selected' : '' ?> value="ksa">المملكة
                    العربية السعودية
                </option>
                <option <?php echo input_value("UR_Nationlaty") == "kba" ? 'selected' : '' ?> value="kba">ممكلة
                    البحرين
                </option>
                <option <?php echo input_value("UR_Nationlaty") == "qatar" ? 'selected' : '' ?> value="qatar">دولة قطر
                </option>
                <option <?php echo input_value("UR_Nationlaty") == "kiwat" ? 'selected' : '' ?> value="kiwat">دولة الكويت
                </option>
            </select>
        </div>

        <?php /*?><div class="form-group col-sm-12 pull-left">
                <label for="UR_BD" class="col-md-4 col-sm-4 col-xs-12"><?php echo langline("UR_BD") ?></label>
                <div class="col-sm-8 col-md-8 col-lg-8 pull-left">
                    <select name="UR_BD_d" id="UR_BD_d" style="width:90px;"> 
                        <option value=""><?php echo langline("Day") ?></option>
                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i; ?>" >
                                <?= $i < 10 ? '0' . $i : $i; ?>
                            </option>
                        <?php } ?>
                    </select> &nbsp;&nbsp; 
                    <select name="UR_BD_m" id="UR_BD_m" style="width:90px;"> 
                        <option value=""><?php echo langline("Month") ?></option>
                        <?php
                        $y_l = date("Y", time());
                        for ($i = 1; $i <= 12; $i++) {
                            ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i; ?>" >
                                <?= $i < 10 ? '0' . $i : $i; ?>
                            </option>
                        <?php } ?>
                    </select> 
                    &nbsp;&nbsp;
                    <select name="UR_BD_y" id="UR_BD_y" style="width:90px;"> 
                        <option value=""><?php echo langline("Year") ?></option> 
                        <?php for ($i = 1920; $i <= $y_l - 15; $i++) { ?>
                            <option value="<?= $i; ?>" >
                                <?= $i ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <!--
                <input type="text" name="rqst_OwnerBD" id="weeklyDatePicker" placeholder="<?php //echo langline("rqst_OwnerBD_")  ?>" value="<?php //echo input_value("rqst_OwnerBD")  ?>" class="col-md-8 col-sm-8 col-xs-12 login-field" />
                -->
            </div><?php */
        ?>

        <?php /*?>            <div class="form-group col-sm-12 pull-left">
                <label for="UR_Image" class="col-md-4 col-sm-4 col-xs-12">صورة شخصية</label>
                <input type="file" name="UR_Image" id="UR_Image" data-text="أختر ملف" class="login-field col-md-8 col-sm-8 col-xs-12"  />
            </div><?php */
        ?>
        <div class="form-group col-sm-12 pull-left">
            <label for="UR_NationalID" class="col-md-4 col-sm-4 col-xs-12"><span
                    style="color:red">* </span><?php echo langline("UR_NationalID") ?></label>
            <input type="text" name="UR_NationalID" id="UR_NationalID" class="login-field col-md-8 col-sm-8 col-xs-12"
                   placeholder="<?php echo langline("UR_NationalID") ?>"
                   value="<?php echo input_value("UR_NationalID") ?>">
        </div>
        <?php /*?>
            <div class="form-group col-sm-12 pull-left">
                <label for="UR_NationalIDExp" class="col-md-4 col-sm-4 col-xs-12">تاريخ الإنتهاء</label> 
                <div class="col-sm-8 col-md-8 col-lg-8 pull-left">
                    <select name="UR_NationalIDExp_d" id="UR_NationalIDExp_d" style="width:90px;"> 
                        <option value=""><?php echo langline("Day") ?></option>
                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i; ?>" >
                                <?= $i < 10 ? '0' . $i : $i; ?>
                            </option>
                        <?php } ?>
                    </select> &nbsp;&nbsp; 
                    <select name="UR_NationalIDExp_m" id="UR_NationalIDExp_m" style="width:90px;"> 
                        <option value=""><?php echo langline("Month") ?></option>
                        <?php
                        for ($i = 1; $i <= 12; $i++) {
                            ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i; ?>" >
                                <?= $i < 10 ? '0' . $i : $i; ?>
                            </option>
                        <?php } ?>
                    </select> 
                    &nbsp;&nbsp;
                    <select name="UR_NationalIDExp_y" id="UR_NationalIDExp_y" style="width:90px;"> 
                        <option value=""><?php echo langline("Year") ?></option> 
                        <?php for ($i = $y_l; $i <= $y_l + 10; $i++) { ?>
                            <option value="<?= $i; ?>" >
                                <?= $i ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <!--
                <input type="text" name="UR_NationalIDExp" id="UR_NationalIDExp" placeholder="تاريخ الإنتهاء" class="col-md-8 col-sm-8 col-xs-12 login-field"/> 
                -->
            </div><?php */
        ?>

        <div class="form-group col-sm-12 pull-left">
            <label for="UR_NationalID" class="col-md-4 col-sm-4 col-xs-12"><span
                    style="color:red">* </span><?php echo langline("UR_MemberID") ?></label>
            <input type="text" name="UR_MemberID" id="UR_MemberID" class="login-field col-md-8 col-sm-8 col-xs-12"
                   placeholder="<?php echo langline("UR_MemberID") ?>" value="<?php echo input_value("UR_MemberID") ?>"
                   required>
        </div>
        <div class="form-group col-sm-12 pull-left">
            <label for="UR_MemberIDImag" class="col-md-4 col-sm-4 col-xs-12">
                <span style="color:red">* </span><?php echo langline("UR_MemberIDImag") ?>
                <br>

                <div class="alert alert-warning">
                    <?php echo langline("error_Maxfile_Allow") ?>
                </div>
            </label>
            <input type="file" name="UR_NationalIDImage" id="UR_NationalIDImage" data-text="أختر ملف"
                   class="login-field col-md-8 col-sm-8 col-xs-12"/>
        </div>
        <?php /*?>            <div class="form-group col-sm-12 pull-left">
                <label for="UR_Job" class="col-md-4 col-sm-4 col-xs-12">المهنة الحالية</label>
                <input name="UR_Job" id="UR_Job" type="text" class="login-field col-md-8 col-sm-8 col-xs-12" value="<?php echo input_value("UR_Job") ?>">
            </div>
            <div class="form-group col-sm-12 pull-left">
                <label for="UR_JobPlace" class="col-md-4 col-sm-4 col-xs-12">مكان العمل</label>
                <input name="UR_JobPlace" id="UR_JobPlace" type="text" class="login-field col-md-8 col-sm-8 col-xs-12" value="<?php echo input_value("UR_JobPlace") ?>">
            </div>
            <div class="form-group col-sm-12 pull-left">
                <label for="UR_Address" class="col-md-4 col-sm-4 col-xs-12">العنوان</label>
                <textarea name="UR_Address" id="UR_Address" cols="" rows="7" class="login-field col-md-8 col-sm-8 col-xs-12"><?php echo input_value("UR_Address") ?></textarea>
            </div> <?php */
        ?>
        <div class="form-group col-sm-12 pull-left">
            <label for="captcha" class="col-md-4 col-sm-4 col-xs-12"><span
                    style="color:red">* </span><?php echo langline("Retype_IMGCode") ?></label>
            <input name="captcha" id="captcha" type="text" class="login-field col-md-4 col-sm-4 col-xs-12" value="">

            <p class="login-field col-md-4 col-sm-4 col-xs-12">
                <span id="captImg"><?php echo $captchaImg; ?></span>
                <a href="javascript:void(0);" class="refreshCaptcha">
                    <img src="<?php echo base_url() . 'assets/images/refresh.png'; ?>"/>
                </a>
            </p>
        </div>
        <div class="form-group col-sm-12 pull-left">
            <button class="btn btn-lg btn-primary col-md-offset-4 col-sm-offset-4 btn-submit" type="submit">تسجيل جديد
            </button>
        </div>
        <input name="captchaCode" value="<?php echo $this->session->userdata('captchaCode')?>" type="hidden">
        <?php
        echo form_close();
        ?>
    </div>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo site_url() ?>assets/buildfalg/js/intlTelInput.js"></script>
<script>
    // $("#phone").intlTelInput();

    $("#phone").intlTelInput({
        initialCountry: "om",
        onlyCountries: ['om', 'ae', 'bh', 'sa', 'qa', 'kw'],
        utilsScript: "build/js/utils.js"
    });
</script>

<script>
    $(document).ready(function () {
        $('.refreshCaptcha').on('click', function () {
            $.get('<?php echo site_url('users/refresh'); ?>', function (data) {
                $('#captImg').html(data);
            });
        });
    });
</script>
