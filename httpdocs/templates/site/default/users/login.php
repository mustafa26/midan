<?php
//echo $_SESSION['captchaCode'];
if (isset($_SESSION['mlogged_in'])): ?>
    <?php redirect(site_url("users/profile")); ?>
<?php else: ?>
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/demo.css">

    <div class="col-md-12">
        <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
            <h3>تسجيل دخول</h3>

            <h3>
                <?php echo theme_messages(); ?>
            </h3>

            <form class="form-signin col-md-10" action="" method="post">
                <div class="form-group col-sm-12 pull-left">
                    <label for="inputEmail" class="col-md-4 col-sm-4"><i
                            class="fa fa-male"></i> <?php echo langline("UR_Phone") ?></label>
                    <input type="tel" id="phone" name="Phone" class="login-field col-md-4 col-sm-8"
                           placeholder="<?php echo langline("UR_Phone") ?>" required autofocus
                           value="<?php echo input_value("Phone") ?>">
                </div>
                <div class="form-group col-sm-12 pull-left">
                    <label for="inputPassword" class="col-md-4 col-sm-4" style="font-family: Ariel !important;"><i
                            class="fa fa-lock"></i> <?php echo langline("users_user_password") ?></label>
                    <input type="password" id="Password" name="Password" class="login-field col-md-4 col-sm-8"
                           placeholder="<?php echo langline("users_user_password") ?>" required>
                </div>
                <?php if (is_numeric(get("rt")) && get("rt") == 1) { ?>
                    <div class="form-group col-sm-12 pull-left">
                        <label for="activecode" class="col-md-4 col-sm-4"><i class="fa fa-lock"></i>
                            <?php echo langline("Active_Code") ?>
                            <?php echo langline("Send_Active_Code")?>
                        </label>
                        <input type="number" name="activecode" id="activecode" class="login-field col-md-4 col-sm-8"
                               placeholder="<?php echo langline("Active_Code") ?>" required style="font-family: Ariel !important;">
                    </div>
                <?php } ?>
                <div class="form-group col-sm-12 pull-left">
                    <label for="captcha"
                           class="col-md-4 col-sm-4 col-xs-12"><?php echo langline("Retype_IMGCode") ?></label>
                    <input name="captcha" id="captcha" type="text" class="login-field col-md-4 col-sm-8" value="" style="font-family: Ariel !important;">

                    <p class="login-field col-md-4 col-sm-8">
                        <span id="captImg"><?php echo $captchaImg; ?></span>
                        <a href="javascript:void(0);" class="refreshCaptcha">
                            <img src="<?php echo base_url() . 'assets/images/refresh.png'; ?>"/>
                        </a>
                    </p>
                </div>
                <div class="form-group">
                    <button class="btn btn-lg btn-default col-md-offset-4 col-sm-offset-4 btn-submit btn-primary"
                            type="submit"><?php echo langline("login") ?></button>
                    &nbsp; &nbsp;

                    <?php if (!is_numeric(get("rt")) && get("rt") != 1) { ?>
                        <a href="<?php echo site_url("users/regist") ?>"
                           class="btn btn-lg btn-default btn-submit btn-new"><?php echo langline("login_new") ?></a>
                        <div class="col-md-offset-4">
<!--                            <input type="checkbox" value="remember-me" class=" col-sm-offset-4">  تذكر دخولي-->
                            <a href="<?php echo site_url("users/forgetpassword/stp1") ?>" class="col-md-6 col-sm-offset-0"><u><?php echo langline("forget_password") ?></u></a>
                            <br>
                        </div>
                    <?php } ?>
                </div>
                <input name="captchaCode" value="<?php echo $this->session->userdata('captchaCode')?>" type="hidden">
            </form>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo site_url() ?>assets/buildfalg/js/intlTelInput.js"></script>
    <script>
        // $("#phone").intlTelInput();
        $("#phone").intlTelInput({
            initialCountry: "om",
            onlyCountries: ['om', 'ae', 'bh', 'sa', 'qa', 'kw'],
            utilsScript: "build/js/utils.js"
        });
    </script>
<?php endif; ?>
<script>
    $(document).ready(function () {
        $('.refreshCaptcha').on('click', function () {
            $.get('<?php echo site_url('users/refresh'); ?>', function (data) {
                $('#captImg').html(data);
            });
        });
    });
</script>
