<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3>
           <span> <?php echo langline("Profile_Info"); ?></span>
            <a href="<?php echo site_url("users/edit"); ?>" class="btn btn-lg btn-default btn-primary btn-submit btn-new pull-right"  style="margin-right: 10px;"><?php echo langline("Edit")?></a>
            <a href="<?php echo site_url("requests"); ?>" class="btn btn-lg btn-default btn-primary btn-submit btn-new pull-right"><?php echo langline("Requests")?></a>
        </h3>
        <h3>
            <?php echo theme_messages(); ?>
        </h3>
        <?php if ($_SESSION['mlogged_in']): ?>  
            <div class="form-signin" style="float:right;">
                <div class="form-group row">
                    <label for="UR_Name" class="col-md-4 col-sm-4 col-xs-5"><?php echo langline("users_user_name") ?></label>
                    <?php echo $data->UR_Name ?>
                </div>
                <div class="form-group row">
                    <label for="UR_Phone" class="col-md-4 col-sm-4 col-xs-5"><?php echo langline("UR_Phone"); ?></label>
                    <?php echo $data->UR_Phone ?>+
                </div>            
                <div class="form-group row">
                    <label for="UR_Nationlaty" class="col-md-4 col-sm-4 col-xs-5"><?php echo langline("UR_Nationlaty"); ?></label>
                    <?php echo langline($data->UR_Nationlaty) ?>
                </div>
                <div class="form-group row">
                    <label for="UR_NationalID" class="col-md-4 col-sm-4 col-xs-5">رقم البطاقة الشخصية</label>
                    <?php echo $data->UR_NationalID ?> 
                </div>
                <div class="form-group row">
                    <label for="UR_NationalIDImage" class="col-md-4 col-sm-4 col-xs-5">صورة البطاقة</label>
                    <img src="<?php echo $data->UR_NationalIDImage ? $upload_path . '/requests/' . $data->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="200" /> 
                </div>

                <a href="<?php echo site_url("users/edit"); ?>" class="btn btn-lg green pull-right"  style="margin-right: 10px;"><?php echo langline("link_edit")?></a>
                <a href="<?php echo site_url("users/changepassword"); ?>" class="btn btn-lg btn-default btn-submit btn-new pull-right"  style="margin-right: 10px;"><?php echo langline("btn_change_password")?></a>

            </div>
        <?php else: ?>
            <?php redirect(site_url("users/login")); ?>
        <?php endif; ?>

    </div> 
</div>
