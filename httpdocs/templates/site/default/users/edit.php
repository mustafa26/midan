<link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/intlTelInput.css">
<link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/demo.css">

<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3>
           <span> <?php echo langline("Edit"); ?> </span>
            <a href="<?php echo site_url("users/profile"); ?>" class="btn btn-lg btn-default btn-primary btn-submit btn-new pull-right" style="margin-right: 10px;"><?php echo langline("Profile_Info")?></a>
            <a href="<?php echo site_url("requests"); ?>" class="btn btn-lg btn-default btn-primary btn-submit btn-new pull-right" ><?php echo langline("Requests")?></a>
        </h3> 
        <?php if ($_SESSION['mlogged_in']): ?>  
        <h3 class="col-md-8 green-border green-text">
                <?php echo theme_messages(); ?>
            </h3>
            <form class="form-signin" action="" method="post" enctype="multipart/form-data" id="formdata">  
                <?php /*
                  <div class="form-group">
                  <label for="UR_Email" class="col-md-4 col-sm-4 col-xs-12">البريد الإلكتروني</label>
                  <?php echo $data->UR_Email ?>
                  </div>
                 * 
                 */ ?>
                <div class="form-group row">
                    <label for="UR_Name" class="col-md-4 col-sm-4 col-xs-12"><?php echo langline("users_user_name") ?></label>
					
<div class="col-md-8 col-sm-8 col-xs-12 no-padding">
    <input type="text" id="nikName" name="nikName" class="login-field col-md-12 col-sm-12 col-xs-12" placeholder="<?php echo langline("users_nikName") ?>" value="<?php echo $data->UR_nikName ?>"> 
					<input type="text" id="FName" name="FName" class="login-field col-md-6 col-sm-6 col-xs-6" placeholder="<?php echo langline("users_FName") ?>" value="<?php echo $data->UR_FName ?>" required>
					<input type="text" id="SName" name="SName" class="login-field col-md-6 col-sm-6 col-xs-6" placeholder="<?php echo langline("users_SName") ?>" value="<?php echo $data->UR_SName ?>" required>
					<input type="text" id="TName" name="TName" class="login-field col-md-6 col-sm-6 col-xs-6" placeholder="<?php echo langline("users_TName") ?>" value="<?php echo $data->UR_TName ?>" required>
					<input type="text" id="QName" name="QName" class="login-field col-md-6 col-sm-6 col-xs-6" placeholder="<?php echo langline("users_QName") ?>" value="<?php echo $data->UR_QName ?>" required>  
                </div>
                </div>
                <div class="form-group row">
                    <label for="UR_Phone" class="col-md-4 col-sm-4 col-xs-5"><?php echo langline("UR_Phone"); ?></label>
                    <input type="tel" class="login-field col-md-8 col-sm-8  col-xs-7" placeholder="<?php echo langline("UR_Phone") ?>" value="<?php echo $data->UR_Phone ?>" readonly >
                </div>            
                <div class="form-group row">
                    <label for="UR_Nationlaty" class="col-md-4 col-sm-4 col-xs-5"><?php echo langline("UR_Nationlaty"); ?></label>
                    <div class="col-md-8 col-sm-8 col-xs-7 no-padding">
                    <select name="UR_Nationlaty" id="UR_Nationlaty" class="form-control ">
                        <option value="oman" <?php echo $data->UR_Nationlaty == "oman" ? "selected" : "" ?> >سلطنة عُمان</option>
                        <option value="uea" <?php echo $data->UR_Nationlaty == "uea" ? "selected" : "" ?>>الإمارات العربية المتحدة</option>
                        <option value="ksa" <?php echo $data->UR_Nationlaty == "ksa" ? "selected" : "" ?>>المملكة العربية السعودية</option>
                        <option value="kba" <?php echo $data->UR_Nationlaty == "kba" ? "selected" : "" ?>>ممكلة البحرين</option>
                        <option value="qatar" <?php echo $data->UR_Nationlaty == "qatar" ? "selected" : "" ?>>دولة قطر</option>
                        <option value="kiwat" <?php echo $data->UR_Nationlaty == "kiwat" ? "selected" : "" ?>>دولة الكويت</option>
                    </select>
                     </div>
                </div>
                <?php /*?><div class="form-group">
                    <?php $_UR_BD = explode('-', $data->UR_BD) ?> 
                    <label for="UR_BD" class="col-md-4 col-sm-4 col-xs-12"><?php echo langline("UR_BD") ?></label>
                    <select name="UR_BD_d" id="UR_BD_d" style="width:90px;"> 
                        <option value=""><?php echo langline("Day") ?></option>
                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i; ?>" <?php echo $_UR_BD[2] == $i ? 'selected' : '' ?>>
                                <?= $i < 10 ? '0' . $i : $i; ?>
                            </option>
                        <?php } ?>
                    </select> &nbsp;&nbsp; 
                    <select name="UR_BD_m" id="UR_BD_m" style="width:90px;"> 
                        <option value=""><?php echo langline("Month") ?></option>
                        <?php
                        $y_l = date("Y", time());
                        for ($i = 1; $i <= 12; $i++) {
                            ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i; ?>" <?php echo $_UR_BD[1] == $i ? 'selected' : '' ?>>
                                <?= $i < 10 ? '0' . $i : $i; ?>
                            </option>
                        <?php } ?>
                    </select> 
                    &nbsp;&nbsp;
                    <select name="UR_BD_y" id="UR_BD_y" style="width:90px;"> 
                        <option value=""><?php echo langline("Year") ?></option> 
                        <?php for ($i = 1920; $i <= $y_l - 15; $i++) { ?>
                            <option value="<?= $i; ?>" <?php echo $_UR_BD[0] == $i ? 'selected' : '' ?>>
                                <?= $i ?>
                            </option>
                        <?php } ?>
                    </select>
                    <!--
                    <input type="text" name="rqst_OwnerBD" id="weeklyDatePicker" placeholder="<?php echo langline("rqst_OwnerBD_") ?>" value="<?php //echo input_value("rqst_OwnerBD") ?>" class="col-md-8 col-sm-8 col-xs-12 login-field" />
                    -->
                </div> 
                <div class="form-group">
                    <label for="UR_Image" class="col-md-4 col-sm-4 col-xs-12">صورة شخصية</label>
                    <input type="file" name="UR_Image" id="UR_Image" data-text="أختر ملف" class="login-field col-md-8 col-sm-8 col-xs-12" />
                    <img src="<?php echo $data->UR_Image ? $upload_path . '/requests/' . $data->UR_Image : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="200" /> 
                </div><?php */?>
                <div class="form-group row">
                <label for="UR_NationalID" class="col-md-4 col-sm-4 col-xs-12">رقم البطاقة الشخصية (الرقم المدني)</label>
                    <input type="text" name="UR_NationalID" id="UR_NationalID" class="login-field col-md-8 col-sm-8 col-xs-12" placeholder="رقم البطاقة الشخصية" required value="<?php echo $data->UR_NationalID ?>">
                </div>
                <?php /*?><div class="form-group">
                    <?php $_UR_NationalIDExp = explode('-', $data->UR_NationalIDExp) ?> 
                    <label for="UR_NationalIDExp" class="col-md-4 col-sm-4 col-xs-12">تاريخ الإنتهاء</label> 
                    <select name="UR_NationalIDExp_d" id="UR_NationalIDExp_d" style="width:90px;"> 
                        <option value=""><?php echo langline("Day") ?></option>
                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i; ?>" <?php echo $_UR_NationalIDExp[2] == $i ? 'selected' : '' ?>>
                                <?= $i < 10 ? '0' . $i : $i; ?>
                            </option>
                        <?php } ?>
                    </select> &nbsp;&nbsp; 
                    <select name="UR_NationalIDExp_m" id="UR_NationalIDExp_m" style="width:90px;"> 
                        <option value=""><?php echo langline("Month") ?></option>
                        <?php
                        for ($i = 1; $i <= 12; $i++) {
                            ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i; ?>" <?php echo $_UR_NationalIDExp[1] == $i ? 'selected' : '' ?>>
                                <?= $i < 10 ? '0' . $i : $i; ?>
                            </option>
                        <?php } ?>
                    </select> 
                    &nbsp;&nbsp;
                    <select name="UR_NationalIDExp_y" id="UR_NationalIDExp_y" style="width:90px;"> 
                        <option value=""><?php echo langline("Year") ?></option> 
                        <?php for ($i = $y_l; $i <= $y_l + 10; $i++) { ?>
                            <option value="<?= $i; ?>" <?php echo $_UR_NationalIDExp[0] == $i ? 'selected' : '' ?>>
                                <?= $i ?>
                            </option>
                        <?php } ?>
                    </select>
                    <!--
                    <input type="text" name="UR_NationalIDExp" id="UR_NationalIDExp" placeholder="تاريخ الإنتهاء" class="col-md-8 col-sm-8 col-xs-12 login-field"/> 
                    -->
                </div><?php */?>
                <div class="form-group row">
                <label for="UR_NationalID" class="col-md-4 col-sm-4 col-xs-12"><?php echo langline("UR_MemberID")?></label>
                <input type="text" name="UR_MemberID" id="UR_MemberID" class="login-field col-md-8 col-sm-8 col-xs-12" placeholder="<?php echo langline("UR_MemberID")?>"  value="<?php echo $data->UR_MemberID ?>" required>
            </div>
            <div class="form-group  row">
                <label for="UR_MemberIDImag" class="col-md-4 col-sm-4 col-xs-12">
                   <?php echo langline("UR_MemberIDImag")?>
					<br>
					<div class="alert alert-warning">
						<?php echo langline("error_Maxfile_Allow") ?>
					</div> 
                   </label> 
                    <input type="file" name="UR_NationalIDImage" id="UR_NationalIDImage" data-text="أختر ملف" class="login-field col-md-8 col-sm-8 col-xs-12" />
                    <img src="<?php echo $data->UR_NationalIDImage ? $upload_path . '/requests/' . $data->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="200" /> 
                </div>
<?php /*?>                <div class="form-group">
                    <label for="UR_Job" class="col-md-4 col-sm-4 col-xs-12">المهنة الحالية</label>
                    <input name="UR_Job" id="UR_Job" type="text" class="login-field col-md-8 col-sm-8 col-xs-12" value="<?php echo $data->UR_Job ?>">
                </div>
                <div class="form-group">
                    <label for="UR_JobPlace" class="col-md-4 col-sm-4 col-xs-12">مكان العمل</label>
                    <input name="UR_JobPlace" id="UR_JobPlace" type="text" class="login-field col-md-8 col-sm-8 col-xs-12" value="<?php echo $data->UR_JobPlace ?> ">
                </div>
                <div class="form-group">
                    <label for="UR_Address" class="col-md-4 col-sm-4 col-xs-12">العنوان</label>
                    <textarea name="UR_Address" id="UR_Address" cols="" rows="7" class="login-field col-md-8 col-sm-8 col-xs-12"><?php echo $data->UR_Address ?></textarea>
                </div> <?php */?>
                <div class="form-group row">          
                    <button class="btn btn-lg btn-primary col-md-offset-4 col-sm-offset-4 btn-submit" type="submit">حفظ</button>
                </div>
            </form>  
        <?php else: ?>
            <?php redirect(site_url("users/login")); ?>
        <?php endif; ?>

    </div> 
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo site_url() ?>assets/buildfalg/js/intlTelInput.js"></script> 
<script>
    // $("#phone").intlTelInput();

    $("#phone").intlTelInput({ 
        initialCountry: "om",
        onlyCountries: ['om', 'ae', 'bh', 'sa', 'qa', 'kw'],
        utilsScript: "build/js/utils.js"
    });
</script>
