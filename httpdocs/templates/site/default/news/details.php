<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><a href="<?php echo site_url("news") ?>"><?php echo langline("news_nws_title") ?></a></h3> 
        <h2><i class="fa fa-angle-left"></i> <?php echo _t($event->nws_Title, $user_lang); ?></h2>
           <div class="sharethis-inline-share-buttons"></div>
            <br>
        <a href="<?php echo $event->nws_HeaderPhoto ? $upload_path . '/news/' . $event->nws_HeaderPhoto : 'assets/lib/img/slider/slide2.png' ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo _t($event->nws_Title, $user_lang); ?>" data-footer="<?php echo $event->nws_From ?>" class="col-md-4 col-sm-6 col-xs-12 pull-right">
             <div style="width: 100%; float: right; height: 160px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url('<?php echo $event->nws_HeaderPhoto ? $upload_path . '/news/' . $event->nws_HeaderPhoto : 'assets/lib/img/slider/slide2.png' ?>" class="internal-img"></div>

        </a> 
        <?php echo _t($event->nws_Content, $user_lang); ?>
    </div>
</div> 


