<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><?php echo langline("news_nws_title") . '('.$total.' خبر)' ?></h3>  
        <?php if ($data): foreach ($data as $row): ?>   
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated competition news">
                    <div class="img-news" style="width: 100%; float: right; height: 160px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image:url(<?php echo $row->nws_HeaderPhoto ? $upload_path . '/news/' . $row->nws_HeaderPhoto : 'assets/lib/img/slider/slide2.png' ?>)"></div> 
                    <h2><?php echo _t($row->nws_Title, $user_lang); ?></h2>  
                    <span><?php echo langline("Lable_From") ?> :</span>  
                    <?php echo $row->nws_From ?>   
                    <div class="btn btn-default pull-right"><a href="<?php echo site_url("news/details/" . $row->nws_ID) ?>"><?php echo langline("More"); ?></a></div>
                </div>   
                <?php
            endforeach;
        else:
            ?>
            <?php echo langline("no_results") ?> 
        <?php endif; ?>
        <!-- Testimonials -->
    </div>
    <nav><?php echo $pagination ?></nav>
</div>