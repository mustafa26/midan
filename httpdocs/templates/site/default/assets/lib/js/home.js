$(function() {

	var todayDate = moment().startOf('day');
	var YM = todayDate.format('YYYY-MM');
	var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
	var TODAY = todayDate.format('YYYY-MM-DD');
	var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listWeek'
		},
		editable: false,
		eventLimit: true, // allow "more" link when too many events
		navLinks: true,
		events: [
			{
				title: 'الحجايج',
				url: 'https://midanalbashir.gov.om/events/details/9',
				start:  '2018-02-11T07:30:00'
			},
			{
				title: 'اللقايا',
				url: 'https://midanalbashir.gov.om/events/details/10',
				start:  '2018-02-12T07:30:00'
			},
			{
				title: 'الإيداع',
				url: 'https://midanalbashir.gov.om/events/details/11',
				start:  '2018-02-13T07:30:00'
			},
			{
				title: 'الثنايـا',
				url: 'https://midanalbashir.gov.om/events/details/12',
				start:  '2018-02-13T09:00:00'
			},
			{
				title: 'الحول - الزمول',
				url: 'https://midanalbashir.gov.om/events/details/13',
				start:  '2018-02-13T10:30:00'
			},
			{
				title: 'أشواط الرموز',
				url: 'https://midanalbashir.gov.om/events/details/14',
				start:  '2018-02-15T13:30:00'
			}
		]
	});
});