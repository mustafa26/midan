<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><?php echo langline("pages_link") ?></h3> <br>

        <div class="sharethis-inline-share-buttons"></div>
        <br>
        <?php if ($pdf): foreach ($pdf as $value): ?>
                <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInDown animated competition">
                    <img src="<?php echo site_url("assets/images/pdf.png") ?>"> 
                    <h1><?php echo _t($value->pdf_Name, $user_lang); ?></h1>  
                    <div class="btn btn-default pull-right">

                        <a href="<?php echo $value->pdf_Img ? $upload_path . "/pdf/" . $value->pdf_Img : '#' ?>" target="_blank">تفاصيل</a>
                    </div>
                </div>
                <?php
            endforeach;
        else:
            ?>
            <?php echo langline("no_results") ?>
        <?php endif; ?>
        <nav><?php echo $pagination ?></nav>
        <!-- Testimonials -->
    </div>
</div>