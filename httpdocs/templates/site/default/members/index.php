<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><?php echo langline("members_member_title") ?></h3>
        <?php if ($data): foreach ($data as $row): ?>  
                <div class="col-md-3 wow fadeInDown animated member">
                    <img src="<?php echo $row->UR_Image ? $upload_path . '/requests/' . $row->UR_Image : $themepath . '/assets/lib/img/member-default.jpg' ?>" width="100" height="100">
                    <div class="name"><?php echo $row->UR_Name; ?></div>
                </div>    
                <?php
            endforeach;
        else:
            echo langline("no_results");
        endif;
        ?>     
        <nav><?php echo $pagination ?></nav> 
    </div>
</div>