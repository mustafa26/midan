<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3>معرض الفيديوهات لمهرجان البشائر الثالث لعام 2018 (12 فيديو)</h3>

        <div class="col-sm-12">
           

              <div class="col-md-6 col-sm-12 videos">
<iframe width="100%" height="240" src="https://www.youtube.com/embed/78pnEerIooo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <h2 class="text-center">
                   فيديو | اليوم الرابع لمهرجان البشائر السنوي الثالث لسباقات الهجن العربية ٢٠١٨م
                </h2>

                <div class="time text-center">
                    14 فبراير 2018م
                </div>
            </div>

              <div class="col-md-6 col-sm-12 videos">
               <iframe width="100%" height="240" src="https://www.youtube.com/embed/3QJEXlVCSOU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                               <h2 class="text-center">
                  المنصة
                </h2>

                <div class="time text-center">
                    14 فبراير 2018م
                </div>
            </div>
            <div class="col-md-6 col-sm-12 videos">
                <iframe width="100%" height="240" src="https://www.youtube.com/embed/YyuUGyX11sU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <h2 class="text-center">
                    فيديو | اليوم الثالث لمهرجان البشائر السنوي الثالث لسباقات الهجن العربية ٢٠١٨م
                </h2>

                <div class="time text-center">
                    13 فبراير 2018م
                </div>
            </div>


            <div class="col-md-6 col-sm-12 videos">
             <iframe width="100%" height="240" src="https://www.youtube.com/embed/oSSTLz2PzRQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <h2 class="text-center">
                   المنصة
                </h2>

                <div class="time text-center">
                    13 فبراير 2018م
                </div>
            </div>

            <div class="col-md-6 col-sm-12 videos">
                <iframe width="100%" height="240" src="https://www.youtube.com/embed/mqhoX3-YtxM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <h2 class="text-center">
                    فيديو | اليوم الثاني لمهرجان البشائر السنوي الثالث لسباقات الهجن العربية ٢٠١٨م
                </h2>

                <div class="time text-center">
                    12 فبراير 2018م
                </div>
            </div>


            <div class="col-md-6 col-sm-12 videos">
              <iframe width="100%" height="240" src="https://www.youtube.com/embed/XwM7qItOpoU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <h2 class="text-center">
                   المنصة
                </h2>

                <div class="time text-center">
                    12 فبراير 2018م
                </div>
            </div>

            <div class="col-md-6 col-sm-12 videos">
               <iframe width="100%" height="240" src="https://www.youtube.com/embed/zto42JYLvjo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                <h2 class="text-center">
                    فيديو | اليوم الأول لمهرجان البشائر السنوي الثالث لسباقات الهجن العربية ٢٠١٨م
                </h2>

                <div class="time text-center">
                    11 فبراير 2018م
                </div>
            </div>


            <div class="col-md-6 col-sm-12 videos">
               <iframe width="100%" height="240" src="https://www.youtube.com/embed/MzDnoio_Ykk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <h2 class="text-center">
                   المنصة
                </h2>

                <div class="time text-center">
                    11 فبراير 2018م
                </div>
            </div>


            <div class="col-md-6 col-sm-12 videos">
                <iframe width="100%" height="240" src="https://www.youtube.com/embed/hhFpXXDQRxQ" frameborder="0"
                        allowfullscreen></iframe>

                <h2 class="text-center">
                    
                   المنصة
                </h2>

                <div class="time text-center">
                    10 فبراير 2018م
                </div>
            </div>

            <div class="col-md-6 col-sm-12 videos">
                <iframe width="100%" height="240" src="https://www.youtube.com/embed/ew-lm28t9no" frameborder="0"
                        allowfullscreen></iframe>

                <h2 class="text-center">
                    أغنية مهرجان البشائر السنوي الثالث لسباقات الهجن العربية 2018 بولاية أدم بمحافظة الداخلية
                </h2>

                <div class="time text-center">
                    6 نوفمبر 2018
                </div>
            </div>

            <div class="col-md-6 col-sm-12 videos">
                <iframe width="100%" height="240" src="https://www.youtube.com/embed/bJn8W8L-K8I" frameborder="0"
                        allowfullscreen></iframe>

                <h2 class="text-center">
                    الفيديو الترويجي لمهرجان البشائر السنوي الثالث لسباقات الهجن العربية 2018
                </h2>

                <div class="time text-center">
                    6 نوفمبر 2018
                </div>
            </div>

            <div class="col-md-6 col-sm-12 videos">
                <iframe width="100%" height="240" src="https://www.youtube.com/embed/dyio1Z5UE8s" frameborder="0"
                        allowfullscreen></iframe>

                <h2 class="text-center">
                    برنامج مجلس البشائر 2018 , الحلقة الأولى , تغطية , مهرجان البشائر السنوي 2018
                </h2>

                <div class="time text-center">
                    27 يناير 2018
                </div>
            </div>


        </div>

    </div>
