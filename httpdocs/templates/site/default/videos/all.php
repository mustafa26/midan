<div class="col-md-12">
<div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
    <h3>
        <a href="<?php echo site_url("videos/index/".segment(3)) ?>"><?php echo langline(segment(3)) ?></a>
        >
        <?php echo _t($cats->cat_name, 'ar') ?>
        (<?php echo $total ?> فيديو)
    </h3>
<div class="col-sm-12">
<?php if ($data): foreach ($data as $row): ?>

    <div class="col-md-6 col-sm-12 videos">
        <?php if($row->vd_YouTube){?>
            <iframe width="100%" height="240" src="https://www.youtube.com/embed/<?php echo $row->vd_YouTube; ?>" frameborder="0"
                    allowfullscreen></iframe>
        <?php }else{?>
            <video width="100%" height="240" controls poster="<?php echo $upload_path ?>/videos/<?php echo $row->vd_Photo; ?>">
                <source src="<?php echo $upload_path ?>/videos/<?php echo $row->vd_File; ?>" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        <?php }?>
        <h2 class="text-center" style="height: 40px;">
            <?php echo _t($row->vd_Title, 'ar'); ?>
        </h2>
        <div class="time text-center">
            <?php echo _t($row->vd_Date, 'ar'); ?>
        </div>
    </div>
<?php
endforeach;
else:
    ?>
    <p class="note note-info"><?php echo langline("no_results") ?></p>
<?php endif; ?>

</div>
<div class="col-sm-12">
    <hr/>
</div>

<nav><?php echo $pagination ?></nav>


</div>
