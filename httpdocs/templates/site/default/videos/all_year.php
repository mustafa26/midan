<!--lightbox-->
<link href="<?php echo $themepath ?>/assets/lib/css/ekko-lightbox.min.css" rel="stylesheet" />
<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3>
            <a href="<?php echo site_url("videos/index/".segment(3)) ?>"><?php echo langline(segment(3)) ?></a>
           <!--  > سنة <?php //echo $year ?> -->
        </h3>
        <div class="col-md-12 col-sm-12">
            <?php if ($data):
                foreach ($data as $row):
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated competition news">
                        <div class="img-news" style="width: 100%; float: right; height: 160px; background-repeat: no-repeat; background-position: center; background-size: contain; background-image:url(<?php echo $row->cat_image ? $upload_path . '/videos/' . $row->cat_image : 'assets/lib/img/slider/slide2.png' ?>)"></div>

                        <h2><?php echo _t($row->cat_name, $user_lang); ?></h2>
                        <div class="btn btn-default pull-right">
                            <a href="<?php echo site_url("videos/all/".segment(3)."/" . $row->cat_id) ?>">استعراض</a>
                        </div>
                    </div>
                    <?php
                endforeach;
            else:
                ?>

                <?php echo langline("no_results") ?>
<?php endif; ?>
        </div>
    </div>
</div>
