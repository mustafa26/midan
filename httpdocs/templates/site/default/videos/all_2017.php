
<div class="col-md-12">
<div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
<h3>معرض الفيديوهات لمهرجان البشائر الثاني لعام 2017 (42 فيديو)</h3>
<div class="col-sm-12">

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/dFhVwDAOQjs" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الخامس - فئة الحول - أشواط الرموز
    </h2>
    <div class="time text-center">
        9 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/_tdvY8E87bc" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الرابع - فئة الثنايا - أشواط الرموز
    </h2>
    <div class="time text-center">
        9 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/o7uoweWcmrQ" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الثالث - فئة الإيداع - أشواط الرموز
    </h2>
    <div class="time text-center">
        9 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/EzOC0qZ6JSo" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الثاني - فئة الثنايا - أشواط الرموز
    </h2>
    <div class="time text-center">
        9 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/qGDyef8gIHQ" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الأول - فئة الحجائج - أشواط الرموز
    </h2>
    <div class="time text-center">
        9 مارس 2017
    </div>
</div>

<div class="col-sm-12"><hr /></div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/pEOLeHDbrWk" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الثاني - فئة الحول - الزمول 6كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/5u4cIx4Hw-M" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الأول - فئة الحول - الزمول 6كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>
<div class="col-sm-12"><hr /></div>
<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/juejfDgb6sg" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الثاني - فئة الثنايـا 8كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/0jrikz731SI" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الأول- فئة الثنايـا 8كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>

<div class="col-sm-12"><hr /></div>
<div class="col-md-6 col-sm-12 videos">
    <div style="position:relative;height:0;padding-bottom:75.0%"><iframe src="https://www.youtube.com/embed/y0p4RDFAAlA?ecver=2" width="100%" height="240" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط السادس - فئة ايذاع 6كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <div style="position:relative;height:0;padding-bottom:75.0%"><iframe src="https://www.youtube.com/embed/FJ7VwRGA4n8?ecver=2" width="100%" height="240" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الخامس - فئة ايذاع 6كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <div style="position:relative;height:0;padding-bottom:75.0%"><iframe src="https://www.youtube.com/embed/3YZd0Cfzcyk?ecver=2" width="100%" height="240" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الرابع - فئة ايذاع 6كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <div style="position:relative;height:0;padding-bottom:75.0%"><iframe src="https://www.youtube.com/embed/H0UZKeAzWoc?ecver=2" width="100%" height="240" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الثالث - فئة ايذاع 6كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <div style="position:relative;height:0;padding-bottom:75.0%"><iframe src="https://www.youtube.com/embed/u7_ir-_sn3Y?ecver=2" width="100%" height="260" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الثاني - فئة ايذاع 6كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <div style="position:relative;height:0;padding-bottom:75.0%"><iframe src="https://www.youtube.com/embed/8zutNtPxCao?ecver=2" width="100%" height="260" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني للهجن 2017 - الشوط الأول - فئة ايذاع 6كلم
    </h2>
    <div class="time text-center">
        8 مارس 2017
    </div>
</div>

<div class="col-sm-12"><hr /></div>
<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/8Yx8yHL19MM" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط العشرون  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>



<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/H70KF5gQRjQ" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط التاسع عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/B4LVNOtL3Qo" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الثامن عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/YAWDOoya7Rk" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط السابع عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/IHCuyGzUUCI" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط السادس عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/SLckXetgzfI" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الخامس عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/LE_0IeamaRQ" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الرابع عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/QSTX885-0es" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الثالث عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/WAGkhfnIZ7Q" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الثاني عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/YzE56uxaDEs" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الحادي عشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/ApiuY0mkVxg" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط العاشر  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/0N6PfaUh1Og" frameborder="0" allowfullscreen></iframe>

    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط التاسع  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/lULdFveOaDk" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الثامن  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/ZjFN2dva1tw" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط السابع  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/dEF9ag0ZE6U" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط السادس  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/FL08G11EL7g" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الخامس  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/m9qrOtte0zk" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الرابع  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/SEPVDWA4RIw" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الثالث  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/BbPZmDztZQI" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الثاني  -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>


<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/MomUvP7R3_o" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">


        مهرجان البشائر السنوي الثاني
        للهجن 2017 -
        الشوط الأول -  فئة اللقايا 5كلم
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>
<div class="col-sm-12"><hr /></div>
<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/caVXO4e5Eg4" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        غدا انطلاق منافسات المهرجان السنوي الثاني لسباقات الهجن العربية
    </h2>
    <div class="time text-center">
        5 مارس 2017
    </div>
</div>
<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/IrqtkK7JaMY" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        الحلقة الثانية من برنامج الوثبة في البشائر 2017
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>

<div class="col-md-6 col-sm-12 videos">
    <iframe width="100%" height="240" src="https://www.youtube.com/embed/K33wNil6b14" frameborder="0" allowfullscreen></iframe>
    <h2 class="text-center">
        ميدان البشائر صرح تراثي يحتضن سباقات الهجن
    </h2>
    <div class="time text-center">
        7 مارس 2017
    </div>
</div>
</div>
<div class="col-sm-12"><hr /></div>
<h3>ارشيف الفيديوهات</h3>
<div class="col-sm-12">
    <div class="col-md-6 col-sm-12 videos">
        <video width="100%" height="240" controls poster="<?php echo $upload_path ?>/videos/video3.jpg">
            <source src="<?php echo $upload_path ?>/videos/5.mp4" type="video/mp4">
            <source src="<?php echo $upload_path ?>/videos/5.ogg" type="video/ogg">
            Your browser does not support the video tag.
        </video>
        <h2 class="text-center">
            تغطيه المركز الإعلامي للمهرجان لختام  ايام مهرجان حفل افتتاح ميدان البشاير
        </h2>
        <div class="time text-center">
            13 فبراير 2016
        </div>
    </div>

    <div class="col-md-6 col-sm-12 videos">
        <video width="100%" height="240" controls poster="<?php echo $upload_path ?>/videos/video3.jpg">
            <source src="<?php echo $upload_path ?>/videos/4.mp4" type="video/mp4">
            <source src="<?php echo $upload_path ?>/videos/4.ogg" type="video/ogg">
            Your browser does not support the video tag.
        </video>
        <h2 class="text-center">
            تغطيه المركز الإعلامي للمهرجان لختام  ايام مهرجان حفل افتتاح ميدان البشاير
        </h2>
        <div class="time text-center">
            13 فبراير 2016
        </div>
    </div>

    <div class="col-md-6 col-sm-12 videos">
        <video width="100%" height="240" controls poster="<?php echo $upload_path ?>/videos/video3.jpg">
            <source src="<?php echo $upload_path ?>/videos/video3.mp4" type="video/mp4">
            <source src="<?php echo $upload_path ?>/videos/video3.ogg" type="video/ogg">
            Your browser does not support the video tag.
        </video>
        <h2 class="text-center">مهرجان حفل إفتتاح ميدان البشائر<br>
            خلال الفترة 9-13 فبراير 2016</h2>
        <div class="time text-center">
            8 فبراير 2016
        </div>
    </div>

    <div class="col-md-6 col-sm-12 videos">
        <video width="100%" height="240" controls poster="<?php echo $upload_path ?>/videos/video.jpg">
            <source src="<?php echo $upload_path ?>/videos/video.mp4" type="video/mp4">
            <source src="<?php echo $upload_path ?>/videos/video.ogg" type="video/ogg">
            Your browser does not support the video tag.
        </video>
        <h2 class="text-center">ختام مناشط وفعاليات موسم سباقات الهجن بميدان البشائر لعام<br>
            2013م - 2014م </h2>
        <div class="time text-center">
            السبت 18 مايو 2014م
        </div>
    </div>

    <div class="col-md-6 col-sm-12 videos">
        <video width="100%" height="240" controls poster="<?php echo $upload_path ?>/videos/video2.jpg">
            <source src="<?php echo $upload_path ?>/videos/video2.mp4" type="video/mp4">
            <source src="<?php echo $upload_path ?>/videos/video2.ogg" type="video/ogg">
            Your browser does not support the video tag.
        </video>
        <h2 class="text-center">حفل تكريم المنجزين و الداعمين بميدان البشائر<br>
            2014 - 2015</h2>
        <div class="time text-center">
            السبت 18 مايو 2014م
        </div>
    </div>
    </video>
</div>

</div>
