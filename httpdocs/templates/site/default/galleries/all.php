<!--lightbox-->
<link href="<?php echo $themepath ?>/assets/lib/css/ekko-lightbox.min.css" rel="stylesheet" />  
<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3> 
            <?php
            $year = $cats->cat_Year;
            if (segment(3) == "hv"):
                ?>
                <a href="<?php echo site_url("galleries/index/" . segment(3)) ?>"><?php echo langline("Heritage_Village") ?></a> 
            <?php else: ?>
                <a href="<?php echo site_url("galleries") ?>"><?php echo langline("galleries_link") ?></a>
            <?php endif; ?> 
            <?php /* <a href="<?php echo site_url("galleries/index/".segment(3)."/$year") ?>">> سنة <?php echo $year ?> </a> */ ?>
            >
            <?php echo _t($cats->cat_name, 'ar') ?>
            (<?php echo $total ?> صورة)
        </h3>
        <div class="col-md-12 col-sm-12">

            <?php if ($data): foreach ($data as $row): ?> 

                    <a href="<?php echo $row->gallery_HeaderPhoto ? $upload_path . '/galleries/' . $row->gallery_HeaderPhoto : 'assets/lib/img/slider/slide2.png' ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo _t($row->gallery_Title, $user_lang); ?>" data-footer="<?php echo _t($row->gallery_Title, $user_lang); ?>" class="col-md-3 col-sm-6 col-xs-6 img-thumbnail">
                        <div style="width: 100%; float: right; height: 160px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url('<?php echo $row->gallery_HeaderPhoto ? $upload_path . '/galleries/thumb/' . $row->gallery_HeaderPhoto : 'assets/lib/img/slider/slide2.png' ?>');" alt="<?php echo _t($row->gallery_Title, $user_lang); ?>"></div>
                    </a>

                    <?php
                endforeach;
            else:
                ?>

                <?php echo langline("no_results") ?> 
            <?php endif; ?> 
        </div> 		
    </div> 
</div>
