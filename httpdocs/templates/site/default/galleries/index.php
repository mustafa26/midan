<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page"> 
        <?php if (segment(3) == "hv"): ?>
            <h3><?php echo langline("Heritage_Village") ?></h3>
        <?php else: ?>
            <h3><?php echo langline("galleries_link") ?></h3>
        <?php endif; ?>

        <div class="col-md-12 col-sm-12">
            <?php if (segment(3) == "hv"): ?> 
                <?php for ($i=2018;$i <= date("Y");$i++) : ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated competition news">
                        <div class="img-news"
                             style="width: 100%; float: right; height: 160px; background-repeat: no-repeat; background-position: center; background-size: contain; background-image:url(https://midanalbashir.gov.om/uploads/galleries/50e3e1ef399407c4ddd86f7b1e5c586d.png)"></div>

                        <h2>المهرجان السنوي لسباقات الهجن العربية بميدان البشاير <?php echo $i ?>م
                            <br><center>(القرية التراثية)</center></h2>

                        <div class="btn btn-default pull-right">
                            <a href="<?php echo site_url("galleries/index/hv/".$i) ?>">استعراض</a>
                        </div>
                    </div>
                <?php endfor; ?>
            <?php else: ?> 
                <?php for ($i=2017;$i <= date("Y");$i++) : ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated competition news">
                        <div class="img-news"
                             style="width: 100%; float: right; height: 160px; background-repeat: no-repeat; background-position: center; background-size: contain; background-image:url(https://midanalbashir.gov.om/uploads/galleries/50e3e1ef399407c4ddd86f7b1e5c586d.png)"></div>

                        <h2>المهرجان السنوي لسباقات الهجن العربية بميدان البشاير <?php echo $i ?>م</h2>

                        <div class="btn btn-default pull-right">
                            <a href="<?php echo site_url("galleries/index/g/".$i) ?>">استعراض</a>
                        </div>
                    </div>
                <?php endfor; ?>
            <?php endif; ?>
        </div>
    </div>
</div>