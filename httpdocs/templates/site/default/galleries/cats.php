<!--lightbox-->
<link href="<?php echo $themepath ?>/assets/lib/css/ekko-lightbox.min.css" rel="stylesheet"/>
<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3> 
            <?php 
            $t = "g";
            if (segment(3) == "hv"):
            $t = "hv"; ?>
            <a href="<?php echo site_url("galleries/index/hv") ?>"><?php echo langline("Heritage_Village") ?></a>
            <?php else: 
            $t = "g"; ?>
                <a href="<?php echo site_url("galleries") ?>"><?php echo langline("galleries_link") ?></a>
            <?php endif; ?>  
         <!--   > سنة <?php //echo $year ?>-->
        </h3>
        <div class="col-md-12 col-sm-12">
            <?php if ($data): foreach ($data as $row): ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated competition news">
                        <div class="img-news"
                             style="width: 100%; float: right; height: 160px; background-repeat: no-repeat; background-position: center; background-size: contain; background-image:url(<?php echo $row->cat_image ? $upload_path . '/galleries/' . $row->cat_image : 'assets/lib/img/slider/slide2.png' ?>)"></div>

                        <h2><?php echo _t($row->cat_name, $user_lang); ?></h2>

                        <div class="btn btn-default pull-right">
                            <a href="<?php echo site_url("galleries/view/$t/" . $row->cat_id) ?>">استعراض</a>
                        </div>
                    </div>
                    <?php
                endforeach;
            else:
                ?>

                <?php echo langline("no_results") ?>
            <?php endif; ?>
        </div>
    </div>
</div>
