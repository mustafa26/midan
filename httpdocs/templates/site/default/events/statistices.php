<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <!--    <h1>أرشيف مهرجان البشائر الثاني لعام 2017 م</h1>-->
        <div class="competition-d" style="direction: ltr">


            <link href="<?php echo $themepath ?>/assets/lib/css/canves.css" rel="stylesheet" type="text/css">
            <script type="text/javascript">
                window.onload = function () {
                    var chart = new CanvasJS.Chart("chartContainer",
                        {
                            theme: "theme2",
                            title: {
                                text: ""
                            },
                            data: [
                                {
                                    type: "pie",
                                    showInLegend: true,
                                    toolTipContent: "{y} - #percent %",
                                    yValueFormatString: "",
                                    legendText: "{indexLabel}",
                                    dataPoints: [
                                        {y: <?php echo $total_3?>, indexLabel: "الحجائج  <?php echo round((($total_3/$total)*100),2)?>% (<?php echo $total_3?>)مطية"},
                                        {y: <?php echo $total_4?>, indexLabel: "اللقايا  <?php echo round((($total_4/$total)*100),2)?>% (<?php echo $total_4?>)مطية"},
                                        {y: <?php echo $total_5?>, indexLabel: "الإيداع  <?php echo round((($total_5/$total)*100),2)?>% (<?php echo $total_5?>)مطية"},
                                        {y: <?php echo $total_6?>, indexLabel: "الثنايا  <?php echo round((($total_6/$total)*100),2)?>% (<?php echo $total_6?>)مطية"},
                                        {
                                            y: <?php echo $total_7?>,
                                            indexLabel: "الحول - الزمول  <?php echo round((($total_7/$total)*100),2)?>% (<?php echo $total_7?>)مطية"
                                        },
                                        {
                                            y: <?php echo $total_8?>,
                                            indexLabel: "أشواط الرموز  <?php echo round((($total_8/$total)*100),2)?>% (<?php echo $total_8?>)مطية"
                                        }
                                    ]
                                }
                            ]
                        });
                    chart.render();
                }
            </script>
            <script src="<?php echo $themepath ?>/assets/lib/js/canvasjs.min.js"></script>
            <center>
                <br>

                <div class="sharethis-inline-share-buttons"></div>
                <br>

                <h2 style="font-size: 20px;">
                    إجمالي عدد المطايا المسجلة: <?php echo $total; ?>
                </h2>
                <br>
                <br>
            </center>
            <div class="chart">

                <div id="chartContainer" style="height: 400px; width: 100%; position: relative;"></div>
                <br>
                <center class="redtxt">
                    يرجى التكرم بالنقر على الشكل اعلاه لمعرفة نسبة كل فئة
                </center>
                <div class="white-bg">
                </div>
            </div>
        </div>
        <!-- Testimonials -->
    </div>
</div>