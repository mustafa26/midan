<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><?php echo langline("Table_Compitations") ?></h3> <br>
        <center class="col-sm-12">
            <a href="<?php echo $upload_path . '/downloads/' ?>tables_2018.pdf" target="_blank">
                <div class="btn btn-success pdf-btn">
                    تحميل جدول السباقات
                    &nbsp; <img src="<?php echo $themepath ?>/assets/lib/img/download-icon.png">
                </div>
            </a>
            <br><br>
        </center>
        <div class="sharethis-inline-share-buttons"></div>
        <br>
        <?php if ($data): foreach ($data as $row): ?>
            <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInDown animated competition">
                <img src="<?php echo $upload_path . '/events/' . $row->event_HeaderPhoto ?>">

                <h1><?php echo _t($row->event_Title, $user_lang); ?></h1>
                <h4><strong><?php echo _t($row->event_SubTitle, $user_lang); ?></strong></h4>

                <h2>
                    <strong><?php echo langline("Time_period") ?>:</strong>
                    <span><?php echo langline("Lable_From") ?></span>
                    <?php echo $row->event_From ?>
                    <span><?php echo langline("Lable_To") ?></span>
                    <?php echo $row->event_To ?>
                </h2>

                <h2><strong><?php echo langline("Num_Races"); ?>
                        :</strong> <?php echo $row->event_RaceNum ?> <?php echo langline("Races"); ?></h2>

                <div class="btn btn-default pull-right">
                    <a href="<?php echo site_url("events/details/" . $row->event_ID) ?>">الأشوط</a>
                </div>
                    <div class="btn btn-default pull-right">
                    <a href="<?php echo site_url("events/results/" . $row->event_ID) ?>">النتائج</a>
                    </div>

                    <div class="btn btn-default pull-right">
                    <a href="<?php echo site_url("events/statistic/" . $row->event_ID) ?>">الاحصائيات</a>
                    </div>

                <?php /*
                    */
                ?>
                    <div class="btn btn-default pull-right">
                    <a href="<?php echo site_url("events/subscribers/" . $row->event_ID) ?>">المشاركون</a>
                    </div>
            </div>
        <?php
        endforeach;
        else:
            ?>
            <?php echo langline("no_results") ?>
        <?php endif; ?>
        <nav><?php echo $pagination ?></nav>
        <!-- Testimonials -->
    </div>
</div>