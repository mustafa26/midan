   <div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><?php echo langline("Table_Compitations") ?></h3>
<!--        <div class="btn btn-default pull-right">-->
<!--            <a href="--><?php //echo site_url("requests/tracks/" . $event->event_ID) ?><!--">--><?php //echo langline("Request_now"); ?><!--</a>-->
<!--        </div>-->

        <div class="competition-d">
            <h1><?php echo _t($event->event_Title, $user_lang); ?></h1>
            <h4><strong><?php echo _t($event->event_SubTitle, $user_lang); ?></strong></h4>  
            <div class="text-center">
                <img src="<?php echo $upload_path . '/events/' . $event->event_HeaderPhoto ?>">
            </div>

            <table border="1" cellpadding="3" cellspacing="2" style="width:100%">
                <?php
                if ($races['data']):
                    ?>
                    <tr>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"> م</th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;">الشوط</th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_1"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_2"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_3"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_4"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_5"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_6"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_7"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_8"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_9"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;"><?php echo langline("NumRace_10"); ?></th>
                        <th scope="row" style="background-color: #ca9e66; color:#fff;">عدد المسجلين</th>
                    </tr>
                    <?php
                    $n = 1; $total_count =0;
                    foreach ($races['data'] as $key => $row) :
                        $total_count +=$row->total;
                        ?>
                        <tr>
                            <td scope="row" style="background-color: #8e6235; color:#fff;"><?php echo $row->rac_Sort; ?></td>
                            <td scope="row" style="background-color: #ebcaa0;">
                                <?php echo $row->rac_Name .' - '. $row->rac_Age .' - '.$row->rac_Gender.' - '.$row->rac_ord?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord1; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord2; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord3; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord4; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord5; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord6; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord7; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord8; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord9; ?></td>
                            <td scope="row" style="background-color: #fff;"><?php echo $row->rac_ord10; ?></td>
                            <td scope="row" style="background-color: #ebcaa0;"><?php echo $row->total; ?></td>
                        </tr>
                        <?php
                        $n++;
                    endforeach;
                endif;
                ?>
                <tr>
                    <td scope="row" align="center" style="background-color: #ebcaa0;" colspan="12">الأجمالي : </td>
                    <td scope="row" style="background-color: #8e6235; color:#fff"><?php echo $total_count; ?></td>
                </tr>
            </table>
            <br>
            <div class="sharethis-inline-share-buttons"></div>
            <hr/>
<!--            <h2>-->
<!--                <strong>--><?php //echo langline("Time_period") ?><!--:</strong>-->
<!--                <span>--><?php //echo langline("Lable_From"); ?><!--</span>  -->
<!--                --><?php //echo $event->event_From ?><!--  -->
<!--                <span>--><?php //echo langline("Lable_To"); ?><!--</span> -->
<!--                --><?php //echo $event->event_To ?>
<!--            </h2> -->
            <p>
                <strong><?php echo langline("Num_Races"); ?> : </strong>
                <?php echo $event->event_RaceNum ?>
                <?php //echo langline("Races"); ?>
            </p>
            <hr/>
            <?php echo _t($event->event_Content, $user_lang) ?>
<!--            <h1>--><?php //echo _t($event->event_Gifts, $user_lang)?langline("Gifts"):""; ?><!--</h1>-->
<!--            --><?php //echo _t($event->event_Gifts, $user_lang) ?>
<?php /* ########################### */?>
<!--            <div class="btn btn-default pull-right">-->
<!--                <a href="--><?php //echo site_url("requests/tracks/" . $event->event_ID) ?><!--">--><?php //echo langline("Request_now"); ?><!--</a>-->
<!--            </div>-->
        </div>              
        <!-- Testimonials -->
    </div>
</div>