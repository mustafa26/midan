<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><?php echo langline("Table_Compitations") ?> &raquo; النتائج</h3>
        <div class="competition-d"> 
            <h1><?php echo _t($event->event_Title, $user_lang); ?></h1>
            <h4>
            <strong><?php echo _t($event->event_SubTitle, $user_lang); ?></strong> 
                &raquo; 
                <strong><?php echo langline("Num_Races"); ?> :</strong> 
                <?php echo $event->event_RaceNum ?> 
                 شوط 
            </h4>
            <div class="sharethis-inline-share-buttons"></div>
            <br>
 <table border="1" cellpadding="3" cellspacing="2" style="width:100%">

     <?php if ($races['data']):
         $n = 1;
         $total_count = 0;
         foreach ($races['data'] as $key => $row) :
             $total_count += $row->total;
             ?>
             <tr>
                 <th colspan="3" scope="col"
                     style="background-color: #673b10;text-align: center; color:#fff;">
                     <?php echo $row->rac_Sort; ?>
                     - <?php echo langline("event_Race") . " " . $row->rac_Name; ?>
                     .. <?php echo $row->rac_Age; ?> .. <?php echo $row->rac_Gender; ?> .. <?php echo $row->rac_ord; ?>
                 </th>
                 <td colspan="3" scope="row" style="background-color: #8e6235; color:#fff;">
                     <?php echo langline("event_Total") . " : " . $row->total . " " . langline("event_Subscribers"); ?>
                 </td>
             </tr>
                <?php
	results_list($row->rac_ID,$row->rac_FID);
            endforeach; 
        endif;
			?>  
</table>
 
 
<?php /*?>            <div class="btn btn-default pull-right"><a href="<?php echo site_url("requests/event/" . $event->event_ID) ?>"><?php echo langline("Request_now"); ?></a>
            </div><?php */?>
        </div>              
        <!-- Testimonials -->
    </div>
</div>