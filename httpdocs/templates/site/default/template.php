<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <meta name="description" content="<?php echo $site->meta_description ?>">
    <meta name="keywords" content="<?php echo $site->meta_description ?>">
    <meta name="author" content="Durar Smart Solution">
    <meta name="description" content="SVG Drawing Animation: Line drawing animation with illustration fade-in"/>
    <meta name="keywords" content="svg, line drawing, animation, illustration, fade in, web design, responsive"/>
    <meta name="author" content="Codrops"/>
    <link rel="shortcut icon" href="<?php echo $themepath ?>/assets/lib/img/favicon.ico">
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Bootstrap Core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo $themepath ?>/assets/lib/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo $themepath ?>/assets/lib/css/bootstrap-theme.min.css">


    <link href="<?php echo $themepath ?>/assets/lib/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $themepath ?>/assets/lib/css/font-awesome.css">

    <link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/lib/css/style.css">
    <!--carousel-->
    <link href="<?php echo $themepath ?>/assets/lib/css/carousel.css" rel="stylesheet" media="screen"/>
    <!--animtion-->
    <!-- SLIDER-->
    <?php if (!segment(1)) { ?>
        <link rel="stylesheet" href="<?php echo $themepath ?>/assets/lib/css/skin 4/style.css" /><?php } ?>
    <script type="text/javascript"
            src="//platform-api.sharethis.com/js/sharethis.js#property=58be8a0064d1cb0012a3cdbe&product=inline-share-buttons"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-93242720-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body onLoad="gettime()">
<!--
  <div class="ad-right"><a href="#"><img src="<?php echo $themepath ?>/assets/lib/img/side-ad.jpg"></a></div>
		<div class="ad-left"><a href="#"><img src="<?php echo $themepath ?>/assets/lib/img/side-ad.jpg"></a></div>-->


<header class="<?php echo segment(1) ? 'header-inner' : 'header-home'; ?>">
    <div class="container">
        <!--top nav-->
        <div class="col-md-12 no-padding">
            <div class="top-tips">
                <div class="col-lg-3 col-md-4 col-xs-12 col-sm-12 date"><span id="digitalclock" class="styling"
                                                                              dir="ltr"></span>
                    <script language="JavaScript">
                        var fixd;
                        function isGregLeapYear(year) {
                            return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
                        }


                        function gregToFixed(year, month, day) {
                            var a = Math.floor((year - 1) / 4);
                            var b = Math.floor((year - 1) / 100);
                            var c = Math.floor((year - 1) / 400);
                            var d = Math.floor((367 * month - 362) / 12);

                            if (month <= 2)
                                e = 0;
                            else if (month > 2 && isGregLeapYear(year))
                                e = -1;
                            else
                                e = -2;

                            return 1 - 1 + 365 * (year - 1) + a - b + c + d + e + day;
                        }

                        function Hijri(year, month, day) {
                            this.year = year;
                            this.month = month;
                            this.day = day;
                            this.toFixed = hijriToFixed;
                            this.toString = hijriToString;
                        }

                        function hijriToFixed() {
                            return this.day + Math.ceil(29.5 * (this.month - 1)) + (this.year - 1) * 354 +
                            Math.floor((3 + 11 * this.year) / 30) + 227015 - 1;
                        }

                        function hijriToString() {
                            var months = new Array("محرم", "صفر", "ربيع أول", "ربيع ثانى", "جمادى أول", "جمادى ثانى", "رجب", "شعبان", "رمضان", "شوال", "ذو القعدة", "ذو الحجة");
                            return this.day + " " + months[this.month - 1] + " " + this.year;
                        }

                        function fixedToHijri(f) {
                            var i = new Hijri(1100, 1, 1);
                            i.year = Math.floor((30 * (f - 227015) + 10646) / 10631);
                            var i2 = new Hijri(i.year, 1, 1);
                            var m = Math.ceil((f - 29 - i2.toFixed()) / 29.5) + 1;
                            i.month = Math.min(m, 12);
                            i2.year = i.year;
                            i2.month = i.month;
                            i2.day = 1;
                            i.day = f - i2.toFixed() + 1;
                            return i;
                        }

                        var tod = new Date();
                        var weekday = new Array("الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت");
                        var monthname = new Array("يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر");

                        var y = tod.getFullYear();
                        var m = tod.getMonth();
                        var d = tod.getDate();
                        var dow = tod.getDay();
                        document.write(weekday[dow] + " " + d + " " + monthname[m] + " " + y);
                        m++;
                        fixd = gregToFixed(y, m, d);
                        var h = new Hijri(1421, 11, 28);
                        h = fixedToHijri(fixd);
                        document.write(" م - " + h.toString() + " هـ &nbsp;&nbsp;");
                    </script>
                </div>

                <?php menu_up(); ?>
            </div>
        </div>
        <!-- logo-->
        <div class="col-md-12 logo bg-top">
            <h1 class="text-center">
                <ul class="right-top-icons">
                    <li><a href="<?php echo site_url() ?>"><img
                                src="<?php echo $themepath ?>/assets/lib/img/homepage.png"><span class="sr-only">(current)</span></a>
                    </li>
                    <li><a href="<?php echo site_url("pages/contact") ?>"><img
                                src="<?php echo $themepath ?>/assets/lib/img/contact.png" width="19" height="19"
                                alt=""/></a></li>
                    <li><a href="<?php echo site_url("pages/location") ?>"><img
                                src="<?php echo $themepath ?>/assets/lib/img/location-icon.png" width="19" height="19"
                                alt=""/></a></li>
                    <li></li>
                </ul>
               <!-- <div class="counter">
                        <span>
                            الوقت المتبقي لختام مهرجان البشائر
                        </span>
                    <br>

                    <p id="demo">
                    </p>
                </div>
                <script>
                    // Set the date we're counting down to
                    var countDownDate = new Date("Feb 15, 2018 15:30:00").getTime();

                    // Update the count down every 1 second
                    var x = setInterval(function () {

                        // Get todays date and time
                        var now = new Date().getTime();

                        // Find the distance between now an the count down date
                        var distance = countDownDate - now;

                        // Time calculations for days, hours, minutes and seconds
                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                        // Output the result in an element with id="demo"
                        document.getElementById("demo").innerHTML = days + "  يوم  " + hours + "  ساعة  "
                        + minutes + "  دقيقة  " + seconds + "  ثانية  ";

                        // If the count down is over, write some text
                        if (distance < 0) {
                            clearInterval(x);
                            document.getElementById("demo").innerHTML = "تم إنطلاق ميدان البشائر";
                        }
                    }, 1000);
                </script>
            -->
                <img src="<?php echo $themepath ?>/assets/lib/img/2018/img/logo.png" alt=""/>
                <?php social_icons($site); ?>
            </h1>
        </div>


        <!-- Navigation -->
        <div class="col-md-12 logo">
            <nav class="navbar navbar-default main-menu">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php menu_bar(); ?>
                        <?php //search_box(); ?>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>

    <div class="container">
        <?php segment(1) ? '' : side_bar($upload_path); ?>
        <?php marq_news($user_lang); ?>
        <!-- االنشرات الإخبارية -->
        <div class="marquee-loop2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <h1 class="col-md-3"> النشرات الإخبارية</h1>

        <div class="news_area2 col-md-9">
            <div data-speed="13000" data-gap="130" data-direction="right" class='marquee2'>
               
                        <span>
                            <a href="<?php echo site_url("assets/downloads/news/1.pdf") ?>" target="_blank" style="margin:0px 20px;"> 
                                النشرة الإخبارية لليوم الأول
                                <span class="news-date">11 فبراير 2018 </span>
                            </a>
                        </span>
                        <span>
                            <a href="<?php echo site_url("assets/downloads/news/2.pdf") ?>" target="_blank" style="margin:0px 20px;"> 
                                النشرة الإخبارية لليوم الثاني
                                <span class="news-date">12 فبراير 2018 </span>
                            </a>
                        </span>
                        <span>
                            <a href="<?php echo site_url("assets/downloads/news/3.pdf") ?>" target="_blank" style="margin:0px 20px;"> 
                                النشرة الإخبارية لليوم الثالث
                                <span class="news-date">13 فبراير 2018 </span>
                            </a>
                        </span>
                          <span>
                            <a href="<?php echo site_url("assets/downloads/news/5.pdf") ?>" target="_blank" style="margin:0px 20px;"> 
                                 النشرة الاخبارية اليوم الختامي 
                                <span class="news-date">15 فبراير 2018 </span>
                            </a>
                        </span>
                          <span>
                            <a href="<?php echo site_url("assets/downloads/news/Booklet.pdf") ?>" target="_blank" style="margin:0px 20px;"> 
                                كتاب مهرجان ميدان البشائر للهجن العربية نسخة 2018
                                <span class="news-date">15 فبراير 2018 </span>
                            </a>
                        </span>
            </div>
        </div>
    </div>
    <!--end  االنشرات الإخبارية-->
    </div>
</header>
<div class="container no-padding">
    <?php view($template . "/" . $content); ?>
</div>
</div>
<!-- Footer -->
<?php footer(); ?>


<!-- /.container -->
<!-- jQuery -->
<script src="<?php echo $themepath ?>/assets/lib/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $themepath ?>/assets/lib/js/bootstrap.min.js"></script>

<!--lightbox-->
<script src="<?php echo $themepath ?>/assets/lib/js/ekko-lightbox.min.js"></script>
<script type="text/javascript">
    $(document).ready(function ($) {
        // delegate calls to data-toggle="lightbox"
        $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {
                    if (window.console) {
                        return console.log('Checking our the events huh?');
                    }
                },
                onNavigate: function (direction, itemIndex) {
                    if (window.console) {
                        return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                    }
                }
            });
        });

        //Programatically call
        $('#open-image').click(function (e) {
            e.preventDefault();
            $(this).ekkoLightbox();
        });
        $('#open-youtube').click(function (e) {
            e.preventDefault();
            $(this).ekkoLightbox();
        });

        $(document).delegate('*[data-gallery="navigateTo"]', 'click', function (event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function () {
                    var lb = this;
                    $(lb.modal_content).on('click', '.modal-footer a', function (e) {
                        e.preventDefault();
                        lb.navigateTo(2);
                    });
                }
            });
        });

    });
</script>
<script src="<?php echo $themepath ?>/assets/lib/js/jquery.touchSwipe.min.js">
</script>
<script src="<?php echo $themepath ?>/assets/lib/js/jquery.transit.min.js">
</script>
<!--date-picker-->
<script src="<?php echo $themepath ?>/assets/lib/js/moment.min.js "></script>
<script src="<?php echo $themepath ?>/assets/lib/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        //Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
        $("#weeklyDatePicker").datetimepicker({
            format: 'DD-MM-YYYY'
        });

        //Get the value of Start and End of Week
        $('#weeklyDatePicker').on('dp.change', function (e) {
            var value = $("#weeklyDatePicker").val();
            var firstDate = moment(value, "DD-MM-YYYY").day(0).format("MM-DD-YYYY");
            $("#weeklyDatePicker").val(firstDate + " - " + lastDate);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
        $("#weeklyDatePicker2").datetimepicker({
            format: 'DD-MM-YYYY'
        });

        //Get the value of Start and End of Week
        $('#weeklyDatePicker2').on('dp.change', function (e) {
            var value = $("#weeklyDatePicker2").val();
            var firstDate = moment(value, "DD-MM-YYYY").day(0).format("MM-DD-YYYY");
            $("#weeklyDatePicker2").val(firstDate + " - " + lastDate);
        });
    });
</script>
<!--input-->
<script src="<?php echo $themepath ?>/assets/lib/js/jquery.inputfile.js"></script>
<script>
    $('input[type="file"]').inputfile({
        uploadText: '<span class="glyphicon glyphicon-upload"></span> إختر ملف',
        removeText: '<span class="glyphicon glyphicon-trash"></span>',
        restoreText: '<span class="glyphicon glyphicon-remove"></span>',
        uploadButtonClass: 'btn btn-primary',
        removeButtonClass: 'btn btn-default'
    });
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? '' : '') + '';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
<?php if (!segment(1)) { ?>
    <!-- SLIDER-->
    <script src="<?php echo $themepath ?>/assets/lib/js/jquery-1.7.1.min.js"></script>


<!--calender event-->
<link rel='stylesheet' href='<?php echo $themepath ?>/assets/lib/css/fullcalendar.min.css' />
<script src='<?php echo $themepath ?>/assets/lib/js/fullcalendar.min.js'></script>
<script src='<?php echo $themepath ?>/assets/lib/js/home.js'></script>
<style type="text/css">
.fc-basic-view .fc-body .fc-row {
    min-height: 20px;
    max-height: 100px;
}

.fc-scroller.fc-day-grid-container {
    height: 440px !important;
        overflow-y: hidden !important;
    }
@media (max-width: 2000px){
.fc-scroller.fc-day-grid-container {
    height: 440px !important;
}
}
@media (max-width: 992px){
.fc-scroller.fc-day-grid-container {
    height: 320px !important;
}
}
</style>
<!--end calender event-->

    <script type="text/javascript" src="<?php echo $themepath ?>/assets/lib/js/raphael-min.js"></script>
    <script type="text/javascript" src="<?php echo $themepath ?>/assets/lib/js/jquery.easing.js"></script>
    <script src="<?php echo $themepath ?>/assets/lib/js/iview.js"></script>
    <script>
        $(document).ready(function () {
            $('#iview').iView({
                pauseTime: 7000,
                pauseOnHover: true,
                directionNavHoverOpacity: 0,
                timer: "Bar",
                timerDiameter: "50%",
                timerPadding: 0,
                timerStroke: 7,
                timerBarStroke: 0,
                timerColor: "#FFF",
                timerPosition: "bottom-right"
            });
        });
    </script>
<?php } ?>
<!--news-->
<link href="<?php echo $themepath ?>/assets/lib/css/mquery.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo $themepath ?>/assets/lib/js/jquery.pause.js"></script>
<script src="<?php echo $themepath ?>/assets/lib/js/jquery.marquee.js"></script>
<script>
    $(function () {
        var $mwo = $('.marquee-with-options');
        $('.marquee').marquee();
        $('.marquee-with-options').marquee({
            //speed in milliseconds of the marquee
            speed: 10000,
            //gap in pixels between the tickers
            gap: 100,
            //gap in pixels between the tickers
            delayBeforeStart: 1000,
            //'left' or 'right'
            direction: 'right',
            //true or false - should the marquee be duplicated to show an effect of continues flow
            duplicated: true,
            //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
            pauseOnHover: true
        });

//pause and resume links
        $('.pause').click(function (e) {
            e.preventDefault();
            $mwo.trigger('pause');
        });
        $('.resume').click(function (e) {
            e.preventDefault();
            $mwo.trigger('resume');
        });
//toggle
        $('.toggle').hover(function (e) {
            $mwo.trigger('pause');
        }, function () {
            $mwo.trigger('resume');
        })
            .click(function (e) {
                e.preventDefault();
            })
    });
</script>

<script>
    $(function () {
        var $mwo = $('.marquee-with-options2');
        $('.marquee2').marquee();
        $('.marquee-with-options2').marquee({
            //speed in milliseconds of the marquee
            speed: 10000,
            //gap in pixels between the tickers
            gap: 100,
            //gap in pixels between the tickers
            delayBeforeStart: 1000,
            //'left' or 'right'
            direction: 'right',
            //true or false - should the marquee be duplicated to show an effect of continues flow
            duplicated: true,
            //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
            pauseOnHover: true
        });

//pause and resume links
        $('.pause').click(function (e) {
            e.preventDefault();
            $mwo.trigger('pause');
        });
        $('.resume').click(function (e) {
            e.preventDefault();
            $mwo.trigger('resume');
        });
//toggle
        $('.toggle').hover(function (e) {
            $mwo.trigger('pause');
        }, function () {
            $mwo.trigger('resume');
        })
            .click(function (e) {
                e.preventDefault();
            })
    });
</script>

<script>
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 60) {
            $(".social-media").addClass("social-media-animi");
        } else {
            $(".social-media").removeClass("social-media-animi");
        }
    });
</script>


<script>
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 60) {
            $(".social-media").addClass("social-media-animi");
        } else {
            $(".social-media").removeClass("social-media-animi");
        }
    });
</script>
<script>
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 60) {
            $(".right-top-icons").addClass("right-top-icons-animi");
        } else {
            $(".right-top-icons").removeClass("right-top-icons-animi");
        }
    });
</script>
</body>
</html>  
