
<div class="col-md-12">
    <div class="internal-page">
        <div class="col-sm-7 contact-form wow fadeInLeft">
            <p>
                ميدان البشائر للجن العربية - يوضع هنا بعض المعلومات التي تمكن زائر الموقع من التواصل المباشر
            </p>
            <?php
            $_action = "";
            $_attributes = array(
                "role" => "form",
                "method" => "post");
            //echo form_open($_action, $_attributes);
            ?>  
            <form role="form" action="#" method="post">
                <input type="hidden" name="____token_midanalbashir" value="bc56d30e41c837855442f62f7c1cec85" style="display:none;">
                <div class="form-group">
                    <label for="contact-name">الإسم بالكامل</label>
                    <input type="text" name="name" placeholder="إدخل إسمك بالكامل..." class="contact-name" id="contact-name">
                </div>
                <div class="form-group">
                    <label for="contact-email">البريد الإلكتروني</label>
                    <input type="text" name="email" placeholder="إدخل بريدك الإلكتروني..." class="contact-email" id="contact-email">
                </div>
                <div class="form-group">
                    <label for="contact-subject">الموضوع</label>
                    <input type="text" name="subject" placeholder="موضوع الرسالة..." class="contact-subject" id="contact-subject">
                </div>
                <div class="form-group">
                    <label for="contact-message">الرسالة</label>
                    <textarea name="message" placeholder="الرسالة..." class="contact-message" id="contact-message"></textarea>
                </div>
                <button type="submit" class="btn">إرسال</button>
            </form>
            <?php //echo form_close() ?>
        </div>
        <div class="col-sm-5 contact-address wow fadeInUp">
            <h3>موقعنا</h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d9446.324347205182!2d57.55653267398545!3d22.71570955268835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e0!4m0!4m3!3m2!1d22.7183894!2d57.555389899999994!5e0!3m2!1sar!2som!4v1516263167750" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            <h3>تواصل معنا</h3>
            <p>
            <div class="col-md-4">
                <i class="fa fa-location-arrow"></i>
                العنوان</div>
            <div class="col-md-8">ميدان البشائر٬ ولاية أدم ٬محافظة الداخلية ٬ سلطنة عمان</div></p>
            <p><div class="col-md-4">
                <i class="fa fa-phone"></i>
                هاتف</div>
            <div class="col-md-8 ltr"> 00968  25417011 - 00968 25417018 - 00968 91260634 </div></p>

            </div>
        </div>
    </div>


</div>