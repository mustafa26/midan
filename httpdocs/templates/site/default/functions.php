<?php

function menu_up() {
    ?>
    <ul class="col-md-7 col-xs-12 col-sm-12 nav nav-pills top-links">
        <li role="presentation">
            <?php if (isset($_SESSION['mlogged_in'])): ?>
                <a href="<?php echo site_url("users/profile") ?>">
                    مرحبا <?php echo $_SESSION['mfname']; ?>
                </a>
            <?php else: ?>
                <a href="<?php echo site_url("users/regist") ?>">
                    تسجيل عضوية
                </a>
            <?php endif; ?>
        </li>
        <li role="presentation">
            <?php if (isset($_SESSION['mlogged_in'])): ?>
                <a href="<?php echo site_url("users/logout") ?>">
                    تسجيل خروج
                </a>
            <?php else: ?>
                <a href="<?php echo site_url("users/login") ?>">
                    دخول الأعضاء
                </a>
            <?php endif; ?>
        </li>
        <!--        <li role="presentation">-->
        <!--            <a href="--><?php //echo site_url("events")       ?><!--">-->
        <!--                تسجيل الهجن-->
        <!--            </a>-->
        <!--        </li>-->
        <!--
        <li role="presentation">
            <a href="<?php echo site_url("requests/ezab") ?>">
                تسجيل العزب
            </a>
        </li> -->
        <li role="presentation">
            <a href="<?php echo site_url("faq") ?>">
                أسئلة الجمهور
            </a>
        </li><?php /* ?>
          <li role="presentation">
          <a href="<?php echo site_url("galleries") ?>">
          معرض الصور
          </a>
          </li>
          <li role="presentation">
          <a href="<?php echo site_url("videos") ?>">
          معرض الفيديوهات
          </a>
          </li><?php */
            ?>
        <li role="presentation">
            <a href="<?php echo site_url("events/statistics"); ?>">
                إحصائيات عامة
            </a>
        </li>
        <li role="presentation">
            <a href="<?php echo site_url() ?>#home-chart">
                الدول المشاركة
            </a>
        </li>
    </ul>
    <?php
}

function marq_news($user_lang = "") {
    $ins = &get_instance();
    $ins->load->model("news/News_model");
    $newss = $ins->News_model->get_all_marq();
    ?>
    <div class="marquee-loop">
        <h1 class="col-md-1"> الأخبار</h1>

        <div class="news_area col-md-11">
            <div data-speed="13000" data-gap="130" data-direction="right" class='marquee'>
                <?php
                if ($newss):
                    foreach ($newss["data"] as $value):
                        ?>
                        <span>
                            <a href="<?php echo site_url("news/details/" . $value->nws_ID) ?>" style="margin:0px 20px;">
                                <?= _t($value->nws_Title, $user_lang) ?>
                                <span class="news-date"><?php echo $value->nws_From ?></span>
                            </a>
                        </span>
                        <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </div>
    <?php
}

function social_icons($site) {
    global $themepath;
    ?>
    <ul class="social-media">

        <!-- <li><a href="#">EN</a></li> -->
        <li><a href="<?php echo $site->social_instagram ?>" target="_bank"><img
                    src="<?php echo $themepath ?>/assets/lib/img/instagram.png" width="19" height="19" alt=""/></a></li>
        <li><a href="<?php echo $site->social_youtube ?>" target="_bank"><img
                    src="<?php echo $themepath ?>/assets/lib/img/youtube.png" width="19" height="19" alt=""/></a></li>
        <li><a href="<?php echo $site->social_twitter ?>" target="_bank"><img
                    src="<?php echo $themepath ?>/assets/lib/img/twitter.png" width="19" height="19" alt=""/></a></li>
        <li><a href="<?php echo $site->social_facebook ?>" target="_bank"><img
                    src="<?php echo $themepath ?>/assets/lib/img/facebook.png" width="19" height="19" alt=""/></a></li>
    </ul>
    <?php
}

function menu_bar() {
    $user_lang = get_user_lang();
    global $themepath;
    $ins = &get_instance();
    $ins->load->model("home/Home_model");
    $all_photos = $ins->Home_model->get_all_photos();
    $all_videos = $ins->Home_model->get_all_videos();
    $all_news = $ins->Home_model->get_all_news();
    $pages_pid3 = $ins->Home_model->get_all_pages(3);
    ?>
    <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url("pages" . "/1/" . str_replace(' ', '_', langline("Camel_Sport"))) ?>"><?php echo langline("Camel_Sport") ?></a>
        </li>
        <li><a href="<?php echo site_url("pages" . "/6/" . str_replace(' ', '_', langline("Bashayer_Department"))) ?>"><?php echo langline("Bashayer_Department") ?></a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false"><?php echo langline("Informations") ?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <?php
                $i = 1;
                if ($pages_pid3): foreach ($pages_pid3['data'] as $row):
                        echo $i == 1 ? '' : '<li role="separator" class="divider"></li>';
                        ?>
                        <li>
                            <a href="<?php echo site_url("pages" . "/" . $row->page_ID . '/' . str_replace(' ', '_', _t($row->page_Title, $user_lang))) ?>">
                                <?php echo _t($row->page_Title, $user_lang); ?>
                            </a>
                        </li>
                        <?php
                        $i++;
                    endforeach;
                endif;
                ?>
            </ul>
        </li>
        <li><a href="<?php echo site_url("news") ?>"><?php echo langline("News") ?><span class="badge"> <?php echo $all_news ?></span></a></li>
        <li><a href="<?php echo site_url("events") ?>"><?php echo langline("Race_schedule") ?></a></li>
        <li><a href="<?php echo site_url("pages" . "/17/المخالفات") ?>"><?php echo langline("Violations") ?></a></li>
        <li><a href="<?php echo site_url("events") ?>"><?php echo langline("Camel_registration") ?></a></li>
        <?php /* ?><li><a href="<?php echo site_url("members") ?>">المشاركون</a></li><?php */ ?>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false"><?php echo langline("MediaCenter") ?> <span class="caret"></span></a>
            <ul class="dropdown-menu">  
                <li><a href="<?php echo site_url("videos/index/ProgramStage") ?>"><?php echo langline("ProgramStage") ?></a></li> 
                <li><a href="<?php echo site_url("videos/index/Racing") ?>"><?php echo langline("Racing") ?></a></li> 
                <li><a href="<?php echo site_url("videos/index/SpecialInterviews") ?>"><?php echo langline("SpecialInterviews") ?></a></li>
                <li><a href="<?php echo site_url("galleries") ?>"> <?php echo langline("Pictures_library") ?></li> 
                <li><a href="<?php echo site_url("galleries/index/hv") ?>"> <?php echo langline("Heritage_Village") ?></a></li> 
                <li><a href="<?php echo site_url("pdf") ?>"><?php echo langline("Versions") ?></a></li>  
            </ul>
        </li>
    </ul> 
    <!-- <span class="badge"> <?php //echo $all_photos  ?></span></a>-->
    <?php
}

function footer() {
    $user_lang = get_user_lang();
    global $themepath;
    $ins = &get_instance();
    $ins->load->model("home/Home_model");
    $pages_pid3 = $ins->Home_model->get_all_pages(3);
    ?>
    <footer>
        <div class="container">
            <div class="col-md-2 col-xs-6 col-sm-6">
                <a href="<?php echo site_url("pages" . "/1/" . str_replace(' ', '_', 'عن رياضة الهجن')) ?>">- عن رياضة
                    الهجن</a>
                <a href="<?php echo site_url("pages" . "/6/" . str_replace(' ', '_', 'عن دائرة الهجن')) ?>">- عن دائرة
                    الهجن</a>
                <a href="#">- معلومات</a>
                <?php
                $i = 1;
                if ($pages_pid3): foreach ($pages_pid3['data'] as $row):
                        ?>
                        <a href="<?php echo site_url("pages" . "/" . $row->page_ID . '/' . str_replace(' ', '_', _t($row->page_Title, $user_lang))) ?>">
                            <i class="glyphicon glyphicon-chevron-left"></i> <?php echo _t($row->page_Title, $user_lang); ?>
                        </a>
                        <?php
                        $i++;
                    endforeach;
                endif;
                ?>
            </div>
            <div class="col-md-2 col-xs-6 col-sm-6">
                <a href="<?php echo site_url("events") ?>">- جدول السباقات</a>
                <a href="<?php echo site_url("pages" . "/17/المخالفات") ?>">- المخالفات</a>
                <?php /* ?><a href="<?php echo site_url("members") ?>">- المشاركون</a><?php */ ?>
                <a href="<?php echo site_url("faq") ?>">- أسئلة الجمهور </a>
                <a href="<?php echo site_url("galleries") ?>">- ألبوم الصور</a>
                <a href="<?php echo site_url("videos") ?>">- ألبوم الفيديوهات</a>

            </div>
            <div class="col-md-2 col-xs-6 col-sm-6">
                <a href="<?php echo site_url("news") ?>">
                    - الأخبار
                </a>
                <?php if (isset($_SESSION['mlogged_in'])): ?>
                    <a href="<?php echo site_url("users/logout") ?>">
                        - تسجيل خروج
                    </a>
                <?php else: ?>
                    <a href="<?php echo site_url("users/login") ?>">- تسجيل دخول الأعضاء</a>
                <?php endif; ?>
                <a href="<?php echo site_url("events") ?>">- تسجيل الهجن</a>
                <a href="<?php echo site_url("pages/location"); ?>">
                    - موقع ميدان البشائر
                </a>
                <a href="<?php echo site_url("pages/contact") ?>">- إتصل بنـا </a>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-12">
                <a href="#">
                    <strong> مواقع صديقة</strong></a>
                <a href="http://ceo.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i> البرنامج الوطني للرؤساء التنفيذيين</a>
                <a href="http://educouncil.gov.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    مجلس التعليم </a>
                <a href="http://omum.gov.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    موقع حفظ البيئة
                </a>
                <a href="http://icd.gov.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    معهد تطوير الكفاءات </a>
                <a href="http://mdp.gov.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    البرنامج الوطني للرؤساء التنفيذيين
                </a>
                <a href="http://www.maf.gov.om/" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    وزارة الزراعة والثروة السمكية </a>

            </div>
            <div class="col-md-3 col-xs-12 col-sm-12">

                <a href="#" target="_blank">
                    &nbsp;                  </a>
                <a href="http://www.cra.ae/ar/index.php" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    الاتحاد العماني لسباقات الهجن </a>
                <a href="http://msd.gov.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    المديرية العامة للخدمات الطبية </a>
                <a href="http://sqhccs.gov.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    مركز السلطان قابوس العالي للثقافة والعلوم
                </a>
                <a href="http://pso.gov.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    جمعية التصوير الضوئي </a>
                <a href="http://quran.gov.om" target="_blank">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    مسابقة السلطان قابوس للقرآن الكريم
                </a>
                <a href="http://harmonyweek.gov.om" target="_blank">

            </div>

        </div>
        <div class="<?php echo segment(1) ? 'row copyrights copyrights-inner' : 'row copyrights'; ?>">
            جميع الحقوق محفوظة &copy; 2018 - دائرة حوكمة الأنظمة والتطبيقات الذكية <br> المديرية العامة للإتصالات ونظم
            المعلومات - ديوان البلاط السلطاني
        </div>
    </footer>
    <?php
}

function search_box() {
    $user_lang = get_user_lang();
    global $themepath;
    ?>
    <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="كلمة البحث">
        </div>
        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
    </form>
    <?php
}

function side_bar($upload_path) {
    $user_lang = get_user_lang();
    global $themepath;
    $ins = &get_instance();
    $ins->load->model("sliders/Sliders_model");
    $_sliders = $ins->Sliders_model->get_all_active();
    if ($_sliders):
        ?>
        <div id="cont">
            <div id="iview">
                <?php
                foreach ($_sliders['data'] as $key => $value) :
                    ?>
                    <div data-iview:thumbnail="<?php echo $upload_path . "/sliders/" . $value->slid_Img ?>"
                         data-iview:image="<?php echo $upload_path . "/sliders/" . $value->slid_Img ?>" class="cover-bg">
                        <div class="iview-caption caption1" data-x="100" data-y="300"
                             data-transition="expandDown"><?php echo _t($value->slid_Name, $user_lang); ?></div>
                    </div>
                    <?php
                endforeach;
                ?>
            </div>
            <?php
        endif;
    }

    function las_list($RaceNum = 0, $event_ID = 0) {
        $user_lang = get_user_lang();
        global $themepath;
        $ins = &get_instance();
        $ins->load->model("events/Events_Model");
        $_las_list = $ins->Events_Model->get_las_list($RaceNum, $event_ID);
        if ($_las_list['data']):
            ?>
            <tr>
                <th scope="row" style="background-color: #ca9e66; color:#fff;"> م</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">اسم الناقة</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;"> اسم المالك</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">رقم الشريحة</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">رقم المشارك</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">الدولة</th>
            </tr>
            <?php
            $n = 1;
            foreach ($_las_list['data'] as $key => $value) :
                ?>
                <tr>
                    <th scope="row" style="background-color: #fff;"><?php echo $n ?></th>
                    <th scope="row" style="background-color: #ebcaa0;"><?php echo $value->Cname ?></th>
                    <th scope="row" style="background-color: #fff;"><?php echo $value->Oname ?></th>
                    <th scope="row" style="background-color: #ebcaa0;"><?php echo $value->CID ?></th>
                    <th scope="row" style="background-color: #fff;"><?php echo $value->MID ?></th>
                    <th scope="row" style="background-color: #fff;"><?php echo langline($value->Nationlaty) ?></th>
                </tr>
                <?php
                $n++;
            endforeach;
            ?>
            <?php
        endif;
    }

    function box_links_subscribers($upload_path = "") {
        $user_lang = get_user_lang();
        $ins = &get_instance();
        $ins->load->model("events/Events_Model");
        $_events = $ins->Events_Model->get_all_home();
        if ($_events['data']):
            $n = 1;
            foreach ($_events['data'] as $key => $row) :
                ?>
                <div class="item <?php echo $n == 1 ? "active" : "" ?>">
                    <div class="row-fluid">
                        <center> <img src="<?php echo $upload_path . '/events/' . $row->event_HeaderPhoto ?>" width="260" height="130" style="border-radius: 8px;"></center>
                        <div class="col-sm-12 no-margin no-padding">
                            <center style="font-family:'bein'; color:#96291f; margin-top: 3px;
                                    margin-bottom: 6px;"><?php echo _t($row->event_Title, $user_lang); ?></center>
                        </div>
                    </div><!--/row-fluid-->
                    <div class="more">
                        <a href="<?php echo site_url("events/subscribers/" . $row->event_ID); ?>" >استعراض </a>
                    </div>
                </div><!--/item-->
                <?php
                $n++;
            endforeach;
            ?>
            <?php
        endif;
    }

    function las_list_subscribers($RaceNum = 0) {
        $ins = &get_instance();
        $ins->load->model("requests/Requests_Model");
        $_las_list = $ins->Requests_Model->get_all_subscribers($RaceNum);
        if ($_las_list['data']):
            ?>
            <tr>
                <th scope="row" style="background-color: #ca9e66; color:#fff;"> م</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">اسم الناقة</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;"> اسم المالك</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">رقم الشريحة</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">رقم المشارك</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">الدولة</th>
            </tr>
            <?php
            $n = 1;
            foreach ($_las_list['data'] as $key => $value) :
                ?>
                <tr>
                    <th scope="row" style="background-color: #fff;"><?php echo $n ?></th>
                    <th scope="row" style="background-color: #ebcaa0;"><?php echo $value->rqst_Name ?></th>
                    <th scope="row" style="background-color: #fff;"><?php echo $value->UR_Name ?></th>
                    <th scope="row" style="background-color: #ebcaa0;"><?php echo $value->rqst_NumCard ?></th>
                    <th scope="row" style="background-color: #fff;"><?php echo $value->UR_MemberID ?></th>
                    <th scope="row" style="background-color: #fff;"><?php echo langline($value->UR_Nationlaty) ?></th>
                </tr>
                <?php
                $n++;
            endforeach;
            ?>
            <?php
        endif;
    }

    function results_list($RaceNum = 0, $event_ID = 0) {
        $user_lang = get_user_lang();
        global $themepath;
        $ins = &get_instance();
        $ins->load->model("events/Events_Model");
        $_sliders = $ins->Events_Model->get_results($RaceNum, $event_ID);
        if ($_sliders['data']):
            ?>
            <tr>
                <th scope="row" style="background-color: #ca9e66; color:#fff;"> المركز</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">اسم الناقة</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;"> اسم المالك</th>
                <th scope="row" style="background-color: #ca9e66; color:#fff;">الزمن</th>
                <?php /* ?><th scope="row" style="background-color: #ca9e66; color:#fff;">رقم الشريحة</th>
                  <th scope="row" style="background-color: #ca9e66; color:#fff;">رقم المشارك</th><?php */
                ?>
            </tr>
            <?php
            $n = 1;
            foreach ($_sliders['data'] as $key => $value) :
                ?>
                <tr>
                    <th scope="row"
                        style="background-color: #ebcaa0;"><?php echo langline("NumRace_" . $value->winner_sort) ?></th>
                    <th scope="row" style="background-color: #fff;"><?php echo $value->Cname ?></th>
                    <th scope="row" style="background-color: #ebcaa0;"><?php echo $value->Oname ?></th>
                    <th scope="row" style="background-color: #fff;"><?php echo $value->RaceTime ?></th>
                    <?php /* ?><th scope="row" style="background-color: #ffd56c;"><?php echo $value->CID ?></th>
                      <th scope="row" style="background-color: #d1f6e2;"><?php echo $value->MID ?></th><?php */
                    ?>
                </tr>
                <?php
                $n++;
            endforeach;
            ?>
        <?php else:
            ?>
            <tr>
                <td colspan="5" scope="row" style="background-color: #fff; color:red"
                    align="center"><?php echo langline("no_results"); ?></td>
            </tr>
        <?php
        endif;
    }

    if (!function_exists("theme_messages")) {

        function theme_messages() {
            $messages = flash_messages();
            $notifications = null;
            if ($messages) {
                ?>

                <?php
                foreach ($messages as $msg) {
                    switch ($msg['type']) {
                        case "error":
                            $notifications .= "<hr><div class='alert alert-danger'>{$msg['msg']}</div>";
                            break;
                        case "success":
                            $notifications .= "<hr><div class='alert alert-success'>{$msg['msg']}</div>";
                            break;
                        case "warning":
                            $notifications .= "<hr><div class='alert alert-warning'>{$msg['msg']}</div>";
                            break;
                        case "info":
                            $notifications .= "<hr><div class='alert alert-info'>{$msg['msg']}</div>";
                            break;
                    }
                }
                ?>

                <?php
            }
            return $notifications;
        }

    }