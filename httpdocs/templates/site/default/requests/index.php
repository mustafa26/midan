<link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/intlTelInput.css">
<link rel="stylesheet" href="<?php echo site_url() ?>assets/buildfalg/css/demo.css">

<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3>
          <span>  <?php
            if ($total == 0)
                $total = "صفر";
            echo langline("Requests") . "($total)";
            ?></span>
            <a href="<?php echo site_url("users/profile"); ?>"
               class="btn btn-lg btn-default btn-primary btn-submit btn-new pull-right"
               style="margin-right: 10px;"><?php echo langline("Profile_Info") ?></a>&nbsp; &nbsp;
            <a href="<?php echo site_url("users/edit"); ?>"
               class="btn btn-lg btn-default btn-primary btn-submit btn-new pull-right"><?php echo langline("link_edit") ?></a>
        </h3>

        <h3 class="col-md-8 green-border green-text">
            <?php echo theme_messages(); ?>
        </h3>
        <?php if ($_SESSION['mlogged_in']): ?>
            <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                <thead>
                <tr>
                    <th width="5%">م</th>
                    <th width="15%">اسم المطية</th>
                    <th width="15%">رقم الشريحة</th>
                    <!--                        <th width="10%">الفئة العمرية</th> -->
                    <!--                        <th width="10%">جنس المطية</th>  -->
                    <th width="25%">رقم الشوط</th>
                    <th width="20%">صورة البرنت</th>
                    <th width="10%">حالة الطلب</th>
                    <th width="10%"><a href="<?php echo site_url("requests/printreq") ?>" target="_blank"><img src="<?php echo site_url() ?>assets/images/print.png" border="0" height="35"></a></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                if ($data):
                    foreach ($data as $row):
                        ?>
                        <tr class="odd gradeX">
                            <td>
                                <?php echo $i; ?>
                            </td>
                            <td>
                                <?php echo $row->rqst_Name; ?>
                            </td>
                            <td>
                                <?php echo $row->rqst_NumCard; ?>
                            </td>
                            <!--                                <td>-->
                            <?php //echo langline("rqst_AgeGroup_" . $row->rqst_AgeGroup); ?><!--</td> -->
                            <!--                                <td>-->
                            <?php //echo langline("rqst_Gender_0" . $row->rqst_Gender); ?><!--</td>-->
                            <td>

                                <?php echo $row->rac_Sort; ?>
                                - الشوط : <?php echo $row->rac_Name; ?> | <?php echo $row->rac_Age; ?>
                                | <?php echo $row->rac_Gender; ?>
                            </td>
                            <td><img
                                    src="<?php echo $row->rqst_PermitImage ? $upload_path . '/requests/' . $row->rqst_PermitImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                                    width="200"/>
                            </td>
                            <td>
                                <?php echo $row->rqst_Status == 1 ? "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" : ($row->rqst_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>") ?>
                            </td>
                            <td>
                                <?php if ($row->rqst_Status != 2) { ?>
                                    <a href="<?php echo site_url("requests/edit/" . $row->rqst_ID) ?>"
                                       class="btn btn-lg green btn-submit"><?php echo langline("link_edit") ?></a>
                                    <a href="<?php echo site_url("requests/delete/" . $row->rqst_ID) ?>" onclick="return confirm('<?php echo langline("delete_confirm_message") ?>')"
                                       class="btn btn-lg btn-default btn-submit btn-new"><?php echo langline("link_delete") ?></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                else:
                    ?>
                    <tr>
                        <td colspan="7"><p class="note note-info">لم تقم بتسجيل مطية بعد.</p></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>

        <?php else: ?>
            <?php redirect(site_url("users/login")); ?>
        <?php endif; ?>

    </div>
</div>