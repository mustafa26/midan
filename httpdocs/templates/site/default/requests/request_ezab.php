<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3>تسجيل الهجن</h3>
        <?php echo theme_messages(); ?>
        <form class="form-signin col-md-8" action="" method="post">
            <?php
            /*
              <div class="form-group">
                <label for="inputEmail" class="col-md-4 col-sm-4 col-xs-12"> الاسم بالكامل </label>
                <input type="text" name="rqst_Name" id="rqst_Name" class="login-field col-md-8 col-sm-8 col-xs-12" placeholder="الاسم بالكامل" required autofocus>
              </div>
              <div class="form-group">
                <label for="inputPassword" class="col-md-4 col-sm-4 col-xs-12">البريد الإلكتروني</label>
                <input type="email" name="rqst_Email" id="rqst_Email" class="login-field col-md-8 col-sm-8 col-xs-12" placeholder="البريد الإلكتروني" required>
              </div>
              <div class="form-group">
                <label for="inputPassword" class="col-md-4 col-sm-4 col-xs-12">رقم الهاتف</label>
                <input type="text" name="rqst_Phone" id="rqst_Phone" class="login-field col-md-8 col-sm-8 col-xs-12" placeholder="رقم الهاتف" required>
              </div>
             *              */
            ?>
            <div class="form-group">
                <label for="inputPassword" class="col-md-4 col-sm-4 col-xs-12">اسم العزبة</label>
                <input type="text" name="rqst_EzbaName" id="rqst_EzbaName" class="login-field col-md-8 col-sm-8 col-xs-12" placeholder="اسم الهجن" required />
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-md-4 col-sm-4 col-xs-12">الموقع</label>
                <input type="text" name="rqst_Location" id="rqst_Location" class="login-field col-md-8 col-sm-8 col-xs-12" placeholder="الموقع" required />
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-md-4 col-sm-4 col-xs-12">ملاحظات</label>
                <textarea name="rqst_Notes" id="rqst_Notes" class="login-field col-md-8 col-sm-8 col-xs-12" placeholder="ملاحظات" required></textarea>
            </div>
            <div class="form-group">          
                <button class="btn btn-lg btn-primary col-md-offset-4 col-sm-offset-4 btn-submit" type="submit">تسجيل العزب</button>
            </div>
        </form>
    </div> 
</div>