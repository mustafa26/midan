<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3>
            <a href="<?php echo site_url("requests"); ?>"><?php echo langline("Requests") ?></a> > <?php echo langline("link_edit") ?></h3>
        <?php if ($site->site_closed == 1) { ?>
            <?php echo $site->closed_message;
        } else {
            ?>
            <?php echo theme_messages(); ?>

            <form class="form-signin" action="" method="post" enctype="multipart/form-data" id="formdata">
                <div class="form-group">
                    <label for="rqst_Name" class="col-md-4 col-sm-4 col-xs-5"><span
                            style="color:red">* </span><?php echo langline("rqst_Name") ?></label>
                    <input type="text" id="rqst_Name" name="rqst_Name" class="login-field col-md-8 col-sm-8 col-xs-7"
                           placeholder="<?php echo langline("rqst_Name") ?>"
                           value="<?php echo $event->rqst_Name ?>" autofocus autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="rqst_NumCard" class="col-md-4 col-sm-4 col-xs-5"><span
                            style="color:red">* </span><?php echo langline("rqst_NumCard") ?></label>
                    <input type="text" name="rqst_NumCard" id="rqst_NumCard" class="login-field col-md-8 col-sm-8 col-xs-7"
                           placeholder="<?php echo langline("rqst_NumCard_") ?>"
                           value="<?php echo $event->rqst_NumCard ?>"
                           onChange="$('#rescard').load('<?php echo site_url("requests/getcard") ?>/' + this.value)"
                           required>
                </div>
                <div class="form-group">
                    <label for="rqst_NumRace" class="col-md-4 col-sm-4 col-xs-5"><span
                            style="color:red">* </span><?php echo langline("rqst_NumRace") ?></label>

                    <select name="rqst_NumRace" name="rqst_NumRace" class="login-field col-md-8 col-sm-8 col-xs-7">
                        <?php if ($races): foreach ($races["data"] as $row):?>
                            <option <?php echo $event->rqst_NumRace == $row->rac_ID ? 'selected' : '' ?> value="<?php echo $row->rac_ID; ?>"><?php echo $row->rac_Sort; ?>
                                - الشوط : <?php echo $row->rac_Name; ?> | <?php echo langline("rqst_AgeGroup") ?> : <?php echo $row->rac_Age; ?>
                                | <?php echo langline("rqst_Gender") ?> : <?php echo $row->rac_Gender; ?></option>
                        <?php
                        endforeach; endif;
                        ?>
                    </select>
                </div>
                <div class="form-group col-sm-12 pull-left">
                    <label for="rqst_PermitImage" class="col-md-4 col-sm-4 col-xs-4 col-xs-5">
                        <span style="color:red">* </span><?php echo langline("rqst_PermitImage") ?>
                        <br>
                        <div class="alert alert-warning">
                            <?php echo langline("error_Maxfile_Allow") ?>
                        </div>
                    </label>
                    <input type="file" name="rqst_PermitImage" id="rqst_PermitImage"
                           data-text="<?php echo langline("rqst_Select_File") ?>"
                           class="login-field col-md-8 col-sm-8 col-xs-8  col-xs-7"/>
                    <br>
                    <img
                        src="<?php echo $event->rqst_PermitImage ? $upload_path . '/requests/' . $event->rqst_PermitImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                        width="200"/>
                </div>
                <div class="form-group col-sm-12 pull-left">
                    <br>
                    <button class="btn btn-lg btn-primary col-md-offset-4 col-sm-offset-4 btn-submit" type="submit"
                            name="RequestNow" id="RequestNow"><?php echo langline("btn_save") ?></button>
                </div>
            </form>
        <?php } ?>
    </div>
</div> 