<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><?php echo langline("Request") ?> > <?php echo _t($event->event_SubTitle, $user_lang); ?></h3>
        <?php if ($site->site_closed == 1) { ?>
            <?php echo $site->closed_message;
        } else {
            ?>
            <?php echo theme_messages(); ?>

            <form class="form-signin col-md-8" action="" method="post" enctype="multipart/form-data" id="formdata">
                <div class="form-group">
                    <label for="rqst_Name" class="col-md-4 col-sm-4 col-xs-5"><span
                            style="color:red">* </span><?php echo langline("rqst_Name") ?></label>
                    <input type="text" id="rqst_Name" name="rqst_Name" class="login-field col-md-8 col-sm-8 col-xs-7"
                           placeholder="<?php echo langline("rqst_Name") ?>"
                           value="<?php echo input_value("rqst_Name") ?>" autofocus autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="rqst_NumCard" class="col-md-4 col-sm-4 col-xs-5"><span
                            style="color:red">* </span><?php echo langline("rqst_NumCard") ?></label>
                    <input type="text" name="rqst_NumCard" id="rqst_NumCard" class="login-field col-md-8 col-sm-8 col-xs-7"
                           placeholder="<?php echo langline("rqst_NumCard_") ?>"
                           value="<?php echo input_value("rqst_NumCard") ?>"
                           onChange="$('#rescard').load('<?php echo site_url("requests/getcard") ?>/' + this.value)"
                           required>
                </div>
                <div class="form-group">
                    <label for="rqst_NumRace" class="col-md-4 col-sm-4 col-xs-5"><span
                            style="color:red">* </span><?php echo langline("rqst_NumRace") ?></label>
<div class="col-md-8 col-sm-8 col-xs-7 no-padding">
                    <select name="rqst_NumRace" name="rqst_NumRace" class="form-control">
                        <?php if ($races): foreach ($races["data"] as $row):?>
                            <option value="<?php echo $row->rac_ID; ?>" <?php echo get("t")==$row->rac_ID?"selected":""?>><?php echo $row->rac_Sort; ?>
                                - الشوط : <?php echo $row->rac_Name; ?> | <?php echo langline("rqst_AgeGroup") ?> : <?php echo $row->rac_Age; ?>
                                | <?php echo langline("rqst_Gender") ?> : <?php echo $row->rac_Gender; ?></option>
                        <?php
                        endforeach; endif;
                        ?>
                    </select>
                </div>
                </div>
                <div class="col-sm-12 col-md-12">
                    <label class="col-md-4 col-sm-4 col-xs-5 no-padding"></label>

                    <div class="form-group" id="rescard"></div>
                </div>
                <?php /*if(segment(3) != 14){?>
<!--                <div class="form-group">-->
<!--                    <label for="rqst_AgeGroup" class="col-md-4 col-sm-4"><span-->
<!--                            style="color:red">* </span>--><?php //echo langline("rqst_AgeGroup") ?><!--</label>-->
<!--                    <select name="Age_Group" id="Age_Group" class="login-field col-md-5 col-sm-8">-->
<!--                        <option-->
<!--                            value="2" --><?php //echo $event->event_AgeGroup == 2 ? 'selected' : '' ?><!-->--><?php //echo langline("rqst_AgeGroup_2"); ?><!--</option>-->
<!--                        <option-->
<!--                            value="3" --><?php //echo $event->event_AgeGroup == 3 ? 'selected' : '' ?><!-->--><?php //echo langline("rqst_AgeGroup_3"); ?><!--</option>-->
<!--                        <option-->
<!--                            value="4" --><?php //echo $event->event_AgeGroup == 4 ? 'selected' : '' ?><!-->--><?php //echo langline("rqst_AgeGroup_4"); ?><!--</option>-->
<!--                        <option-->
<!--                            value="5" --><?php //echo $event->event_AgeGroup == 5 ? 'selected' : '' ?><!-->--><?php //echo langline("rqst_AgeGroup_5"); ?><!--</option>-->
<!--                        <option-->
<!--                            value="6" --><?php //echo $event->event_AgeGroup == 6 ? 'selected' : '' ?><!-->--><?php //echo langline("rqst_AgeGroup_6"); ?><!--</option>-->
<!--                    </select>-->
<!--                </div>-->
                <?php }*/?>
                <?php /*
            <div class="form-group">
                <label for="rqst_AgeGroup" class="col-md-4 col-sm-4"><?php echo langline("rqst_Gender") ?></label>
                <select name="rqst_Gender" id="rqst_Gender" class="login-field col-md-5 col-sm-8">
                    <option value="2"><?php echo langline("rqst_Gender_02"); ?></option>
                    <option value="1"><?php echo langline("rqst_Gender_01"); ?></option>
                </select>
            </div>
            */
                ?>
                <div class="form-group col-sm-12 pull-left">
                    <label for="rqst_PermitImage" class="col-md-4 col-sm-4 col-xs-12 no-padding">
                        <span style="color:red">* </span><?php echo langline("rqst_PermitImage") ?>
                        <br>

                        <div class="alert alert-warning">
                            <?php echo langline("error_Maxfile_Allow") ?>
                        </div>
                    </label>
                    <input type="file" name="rqst_PermitImage" id="rqst_PermitImage"
                           data-text="<?php echo langline("rqst_Select_File") ?>"
                           class="login-field col-md-8 col-sm-8 col-xs-12"/>
                </div>
                <div class="form-group col-sm-12 pull-left">
                    <button class="btn btn-lg btn-primary col-md-offset-4 col-sm-offset-4 btn-submit" type="submit"
                            name="RequestNow" id="RequestNow"><?php echo langline("Request") ?></button>
                </div>
                <input name="rqst_AgeGroup" value="<?php echo $event->event_AgeGroup ?>" type="hidden">
                <input name="eventDate" value="<?php echo $event->event_From ?>" type="hidden">
            </form>
        <?php } ?>
    </div>
</div> 