<div class="col-md-12">
    <div class="col-sm-12 about-us-text wow fadeInLeft internal-page">
        <h3><?php echo langline("Request") ?> > <?php echo _t($event->event_SubTitle, $user_lang); ?></h3>
        <?php if ($site->site_closed == 1) { ?>
            <?php echo $site->closed_message;
        } else {
            ?>
            <?php //echo langline("rqst_NumRace") ?>

        <table border="1" cellpadding="3" cellspacing="2" style="width:100%">
            <?php
            if ($races['data']):
                ?>
                <tr>
                    <th scope="row" style="background-color: #ca9e66; color:#fff;"> م</th>
                    <th scope="row" style="background-color: #ca9e66; color:#fff;">الشرط</th>
                    <th scope="row"
                        style="background-color: #ca9e66; color:#fff;"><?php echo langline("rqst_AgeGroup") ?></th>
                    <th scope="row"
                        style="background-color: #ca9e66; color:#fff;"><?php echo langline("rqst_Gender") ?></th>
                    <th scope="row" style="background-color: #ca9e66; color:#fff;">عدد المسجلين</th>
                    <th scope="row" style="background-color: #ca9e66; color:#fff;"></th>
                </tr>
                <?php
                $n = 1; $total_count =0;
                foreach ($races['data'] as $key => $row) :
                    $total_count +=$row->total;
                    ?>
                    <tr>
                        <td scope="row" style="background-color: #8e6235; color:#fff;"><?php echo $row->rac_Sort; ?></td>
                        <td scope="row" style="background-color: #ebcaa0;"><?php echo $row->rac_Name; ?></td>
                        <td scope="row" style="background-color: #fff;"><?php echo $row->rac_Age; ?></td>
                        <td scope="row" style="background-color: #fff;"><?php echo $row->rac_Gender; ?></td>
                        <td scope="row" style="background-color: #fff;"><?php echo $row->total; ?></td>
                        <td scope="row" style="background-color: #fff;">
                            <div class="btn btn-default pull-right">
                                <a href="<?php echo site_url("requests/event/" . $event->event_ID).'?t='.$row->rac_ID ?>"><?php echo langline("Request_now"); ?></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $n++;
                endforeach;
            endif;
            ?>
            <tr>
                <td scope="row" align="left" style="background-color: #ebcaa0;" colspan="4">الأجمالي : </td>
                <td scope="row" style="background-color: #fff;"><?php echo $total_count; ?></td>
                <td scope="row" style="background-color: #fff;"></td>
            </tr>
            </table>
        <?php } ?>
    </div>
</div> 