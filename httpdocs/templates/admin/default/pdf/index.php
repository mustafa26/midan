<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/> 
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css">
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("pdf") ?>"><?php echo langline("pdf_link") ?></a> &nbsp;
                </div> 
                <div class="caption">
                    <i class="icon-angle-left"></i>  
                    <?php echo langline("list_all") ?> 
                </div>
            </div>
            <div class="portlet-body">   
                <?php echo theme_messages(); ?>  
                <?php if (check_user_permission(current_module(), "add")): ?> 
                    <div class="portlet box blue form">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?php echo langline("link_add_new") ?></div>
                            <div class="tools">
                                <a href="javascript:;" <?php echo!get("open") ? 'class="expand"' : 'class="collapse"' ?>></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            </div>
                        </div>
                        <div class="portlet-body" <?php echo!get("open") ? 'style="display: none;"' : '' ?>>
                            <?php
                            $_action = admin_url("pdf/add");
                            $_attributes = array(
                                "method" => "post",
                                "class" => "form-horizontal",
                                "role" => "form",
                                "enctype" => "multipart/form-data",
                                "id" => "dataForm");
                            echo form_open($_action, $_attributes);
                            ?> 
                            <div class="tabbable-custom tabs-below ">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_2_1">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo langline("pdf_Name") ?> (عربي)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="pdf_Name[ar]" class="form-control input-lg"
                                                       placeholder="<?php echo langline("pdf_Name") ?>"
                                                       value="<?php echo input_value("pdf_Name[ar]") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_2_2">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> <?php echo langline("pdf_Name") ?> (English)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="pdf_Name[en]" class="form-control input-lg" placeholder="<?php echo langline("pdf_Name") ?>"
                                                       value="<?php echo input_value("pdf_Name[en]") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_2_1" data-toggle="tab"><?php echo langline("language_tab_arabic") ?></a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_2_2" data-toggle="tab"><?php echo langline("language_tab_english") ?></a>
                                    </li>
                                </ul> 
                            </div>
                            <div class="col-md-8"> 
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>
                                    <div class="col-md-7">    
                                        <div class="i-checkbox">
                                            <input type="checkbox" name="pdf_status" id="pdf_status" class="iButton" value="1" checked />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group last">
                                    <label class="col-md-2 control-label"><?php echo langline("slider_HeaderPhoto") ?></label> 
                                    <div class="col-md-8">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo site_url("assets/images/pdf.png") ?>" alt=""/>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                                <img src="<?php echo site_url("assets/images/pdf.png") ?>" style="max-height: 150px;">
                                            </div> 
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> <?php echo langline("btn_select_File") ?></span>
                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                                    <input type="file" class="default" name="pdf_Img" />
                                                </span>
                                                <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                            </div>
                                        </div>
                                        <span class="label label-danger">NOTE!</span> 
                                        <span>
                                            Attached image thumbnail is supported in Latest Firefox, 
                                            Chrome, Opera, Safari and Internet Explorer 10 only
                                        </span> 
                                    </div>
                                </div>
                                <div class="form-group">      
                                    <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                                        <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                                    </button>
                                </div>
                            </div> 
                            <?php echo form_close(); ?>
                            <div class="clearfix"></div>
                        </div>
                    </div> 
                <?php endif; ?> 
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr>
                                <!--<th class="table-checkbox"> 
                                    <input type="checkbox" class="group-checkable" data-set=".dataTable .checkboxes"/></th>-->
                                <th><?php echo langline("pdf_Name") ?></th> 
                                <th><?php echo langline("slider_HeaderPhoto") ?></th> 
                                <th><?php echo langline("project_status") ?></th>
                                <th width="200">&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data): foreach ($data as $row): ?> 
                                    <tr class="odd gradeX"> 
                                        <td><?php echo _t($row->pdf_Name, $user_lang); ?></td> 
                                        <td>
                                            <a href="<?php echo $row->pdf_Img ? $upload_path . "/pdf/" . $row->pdf_Img:'#'?>" target="_blank">
                                            <img src="<?php echo site_url("assets/images/pdf.png") ?>" width="150" />
                                            </a>
                                        </td> 
                                        <td> 
                                            <?php echo $row->pdf_Status == 0 ? "<span class='label label-danger'>" . langline("status_inactive") . "</span>" : "<span class='label label-success'>" . langline("status_active") . "</span>" ?> 
                                        </td> 
                                        <td class="text-center">
                                            <?php
                                            if (check_user_permission(current_module(), "edit")):
                                                ?>
                                                <a href="<?php echo admin_url("pdf/edit/" . $row->pdf_ID) ?>"
                                                   class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                                   title="<?php echo langline("link_edit"); ?>">
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </a>
                                                <?php
                                            endif;
                                            if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                                                ?>
                                                <a href="<?php echo admin_url("pdf/delete/" . $row->pdf_ID) ?>"
                                                   class="btn btn-danger confirm_delete" data-toggle="tooltip"
                                                   data-placement="top" title="<?php echo langline("link_delete"); ?>">
                                                    <i class="glyphicon glyphicon-remove"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr> 
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <nav><?php echo $pagination ?></nav>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div> 
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>