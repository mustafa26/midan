<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/> 
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css">
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("pdf") ?>"><?php echo langline("pages_link") ?></a> &nbsp;
                </div> 
            </div>
            <div class="portlet-body">  
                <?php if (check_user_permission(current_module(), "edit")): ?> 
                    <div class="portlet box blue form">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?php echo langline("log_edit_catgory") ?></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <?php echo theme_messages(); ?> 
                            <?php
                            $_action = admin_url("pdf/edit/" . segment(4));
                            $_attributes = array(
                                "method" => "post",
                                "class" => "form-horizontal",
                                "role" => "form",
                                "enctype" => "multipart/form-data",
                                "id" => "dataForm");
                            echo form_open($_action, $_attributes);
                            ?>   
                            <div class="tabbable-custom tabs-below ">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_2_1">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo langline("pdf_Name") ?> (عربي)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="pdf_Name[ar]" class="form-control input-lg"
                                                       placeholder="<?php echo langline("pdf_Name") ?>"
                                                       value="<?php echo _t($data->pdf_Name, "ar") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_2_2">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> <?php echo langline("pdf_Name") ?> (English)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="pdf_Name[en]" class="form-control input-lg" placeholder="<?php echo langline("pdf_Name") ?>"
                                                       value="<?php echo _t($data->pdf_Name, "en") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_2_1" data-toggle="tab"><?php echo langline("language_tab_arabic") ?></a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_2_2" data-toggle="tab"><?php echo langline("language_tab_english") ?></a>
                                    </li>
                                </ul> 
                            </div>
                            <div class="col-md-8"> 
                                <div class="form-group last">
                                    <label class="col-md-2 control-label"><?php echo langline("slider_HeaderPhoto") ?></label>  
                                    <div class="col-md-8">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <a href="<?php echo $data->pdf_Img ? $upload_path . "/pdf/" . $data->pdf_Img : '#' ?>" target="_blank">
                                                    <img src="<?php echo site_url("assets/images/pdf.png") ?>" width="150" />
                                                </a> 
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                                <a href="<?php echo $data->pdf_Img ? $upload_path . "/pdf/" . $data->pdf_Img : '#' ?>" target="_blank">
                                                    <img src="<?php echo site_url("assets/images/pdf.png") ?>" width="150" />
                                                </a> 
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> <?php echo langline("btn_select_File") ?></span>
                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                                    <input type="file" class="default" name="pdf_Img" value="<?php echo input_value("pdf_Img") ?>" />
                                                </span>
                                                <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                            </div>
                                        </div>
                                        <span class="label label-danger">NOTE!</span> 
                                        <span>
                                            Attached image thumbnail is supported in Latest Firefox, 
                                            Chrome, Opera, Safari and Internet Explorer 10 only
                                        </span> 
                                        <input type="hidden" value="<?php echo $data->pdf_Img ?>" name="pdf_Img_old">
                                    </div>   
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>
                                    <div class="col-md-7">    
                                        <div class="i-checkbox">
                                            <input type="checkbox" name="pdf_status" class="iButton" value="1" <?php echo $data->pdf_Status ? "checked" : "" ?> />
                                        </div>
                                    </div>
                                </div> 
                                <div class="form-group">      
                                    <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                                        <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                                    </button>
                                </div>
                            </div> 
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div> 
                <?php endif; ?>  
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div> 
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>