<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css"/>
<!-- END PAGE LEVEL STYLES -->


<div class="row">
<div class="col-md-12">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box light-grey">
<div class="portlet-title">
    <div class="caption">
        <i class="icon-home"></i>
        <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
    </div>
    <div class="caption">
        <i class="icon-angle-left"></i>
        <a href="<?php echo admin_url("requests/index/" . segment(4)) ?>"><?php echo langline("requests_link") ?></a>
        <span class="badge badge-warning">&nbsp;<?php echo $total; ?>&nbsp;</span>
    </div>
    <div class="pull-right">
        <a href="?status=1" class="btn btn-sm yellow"><?php echo langline("statusNew") ?></a>
        <a href="?status=2" class="btn btn-sm green"><?php echo langline("statusAccept") ?></a>
        <a href="?status=3" class="btn btn-sm red"><?php echo langline("statusNonAccept") ?></a>
        <a href="?" class="btn btn-sm blue"><?php echo langline("All") ?></a>
    </div>
</div>

<div class="portlet-body">
    <?php echo theme_messages(); ?>

    <div class="portlet box blue form">
        <div class="portlet-title">
            <div class="caption"><i class="icon-reorder"></i><?php echo langline("link_add_new") ?></div>
            <div class="tools">
                <a href="javascript:;" <?php echo !get("open") ? 'class="expand"' : 'class="collapse"' ?>></a>
                <a href="#portlet-config" data-toggle="modal" class="config"></a>
            </div>
        </div>
        <div class="portlet-body" <?php echo !get("open") ? 'style="display: none;"' : '' ?>>
            <?php
            $_action = "";
            $_attributes = array(
                "method" => "post",
                "class" => "form-horizontal",
                "role" => "form",
                "enctype" => "multipart/form-data",
                "id" => "dataForm");
            echo form_open($_action, $_attributes);
            ?>
            <div class="col-md-10">
                <div class="form-group" id="rescard"></div>
                <div class="form-group">
                    <label class="col-md-2 control-label"> <?php echo langline("rqst_Name") ?> </label>

                    <div class="col-md-4">
                        <input type="text" name="rqst_Name" id="rqst_Name" class="form-control input-lg"
                               placeholder="<?php echo langline("rqst_Name") ?>"
                               value="<?php echo input_value("rqst_Name") ?>">
                    </div>
                    <label class="col-md-2 control-label"><?php echo langline("rqst_NumCard") ?> </label>

                    <div class="col-md-4">
                        <input type="text" name="rqst_NumCard" id="rqst_NumCard"
                               class="form-control input-lg"
                               placeholder="<?php echo langline("rqst_NumCard_") ?>"
                               value="<?php echo input_value("rqst_NumCard") ?>"
                               onChange="$('#rescard').load('<?php echo admin_url("requests/getcard") ?>/' + this.value)"
                               required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label"> <?php echo langline("Events") ?> </label>

                    <div class="col-md-4">
                        <select name="rqst_FID" name="rqst_FID" class="form-control"
                                onchange="$('#div_races').load('<?php echo admin_url("members/load_ajax_races") ?>/'+this.value);">
                            <option value=""><?php echo langline("Select"); ?></option>
                            <?php if ($events["data"]): foreach ($events["data"] as $v): ?>
                                <option value="<?php echo $v->event_ID; ?>">
                                    <?php echo _t($v->event_Title, $user_lang); ?></option>
                            <?php
                            endforeach; endif;
                            ?>
                        </select>
                    </div>
                    <label class="col-md-2 control-label"><?php echo langline("rqst_NumRace") ?> </label>

                    <div class="col-md-4" id="div_races"></div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"> <?php echo langline("rqst_PermitImage") ?> </label>

                    <div class="col-md-8">
                        <div class="col-md-8">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img
                                        src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                        alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" style="max-height: 150px;">
                                </div>
                                <div>
                                        <span class="btn default btn-file">
                                            <span class="fileupload-new"><i
                                                    class="icon-paper-clip"></i> <?php echo langline("btn_select_image") ?></span>
                                            <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                            <input type="file" class="default" name="PermitImage" id="PermitImage"/>
                                        </span>
                                    <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i
                                            class="icon-trash"></i> Remove</a>
                                </div>
                            </div>
                            <span class="label label-danger">NOTE!</span>
                                <span>
                                    Attached image thumbnail is supported in Latest Firefox,
                                    Chrome, Opera, Safari and Internet Explorer 10 only
                                </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                        <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                    </button>
                </div>
            </div>
            <input value="<?php echo segment(4) ?>" type="hidden" name="rqst_URID">
            </form>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
            <thead>
            <tr>
                <th width="2%">&nbsp;</th>
                <th width="30%"><?php echo langline("rqst_Owner") ?></th>
                <th width="30%"><?php echo langline("rqst_Camel") ?></th>
                <th width="20%"><?php echo langline("rqst_PermitImage") ?></th>
                <th width="8%"><?php echo langline("project_status") ?></th>
                <th width="10%">&nbsp;</th>

            </tr>
            </thead>
            <tbody>
            <?php
            $i = $offset;
            if ($data): foreach ($data as $row):
                if (is_file('uploads/requests/' . $row->rqst_PermitImage) && !is_file('uploads/requests/thumb/' . $row->rqst_PermitImage)) {
                    resizeProp('uploads/requests/' . $row->rqst_PermitImage, 'uploads/requests/thumb/' . $row->rqst_PermitImage, 150, 150);
                }
                ?>
                <tr class="odd gradeX">
                    <td><?php echo $i; ?></td>
                    <td>
                        <strong class="col-md-5 text-muted"><?php echo langline("rqst_Created") ?></strong>
                        : <span class="label label-info"><?php echo $row->rqst_Created; ?></span><br>
                        <strong
                            class="col-md-5 text-muted"><?php echo langline("rqst_OwnerName") ?></strong>
                        : <?php echo $row->UR_Name; ?><br>
                        <strong class="col-md-5 text-muted"><?php echo langline("National_ID") ?></strong>
                        : <?php echo $row->UR_NationalID; ?><br>
                        <strong class="col-md-5 text-muted"><?php echo langline("UR_MemberID") ?></strong>
                        : <?php echo $row->UR_MemberID; ?><br>
                        <strong class="col-md-5 text-muted"><?php echo langline("UR_Phone") ?></strong>
                        : <?php echo $row->UR_Phone; ?><br>
                        <strong class="col-md-5 text-muted"><?php echo langline("UR_Nationlaty") ?></strong>
                        : <?php echo langline($row->UR_Nationlaty); ?>
                    </td>
                    <td>
                        <strong class="col-md-5 text-muted"><?php echo langline("rqst_Name") ?></strong>
                        : <?php echo $row->rqst_Name; ?><br>
                        <strong class="col-md-5 text-muted"><?php echo langline("rqst_NumCard") ?></strong>
                        : <?php echo $row->rqst_NumCard; ?><br>

                        <strong class="col-md-5 text-muted"><?php echo langline("rqst_AgeGroup") ?></strong>
                        : <?php echo $row->rac_Age; ?><br>
                        <strong class="col-md-5 text-muted"><?php echo langline("rqst_Gender") ?></strong>
                        : <?php echo $row->rac_Gender; ?><br>
                        <strong class="col-md-5 text-muted"><?php echo langline("rqst_NumRace") ?></strong> :
                        <?php echo langline("Race"); ?> <?php echo $row->rac_Name; ?>
                    </td>
                    <td>
                        <img
                            src="<?php echo $row->rqst_PermitImage ? $upload_path . '/requests/thumb/' . $row->rqst_PermitImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                            width="200"/>
                    </td>
                    <td>
                        <?php echo $row->rqst_Status == 1 ? "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" : ($row->rqst_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>") ?>
                    </td>
                    <td class="text-center">
                        <?php
                        if (check_user_permission(current_module(), "show")):
                            ?>
                            <a href="<?php echo admin_url("requests/view/" . $row->rqst_ID) ?>"
                               class="btn btn-default" data-toggle="tooltip" data-placement="top"
                               title="<?php echo langline("link_edit"); ?>">
                                <i class="icon icon-eye-open"></i>
                            </a>
                        <?php
                        endif;
                        if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                            ?>
                            <a href="<?php echo admin_url("requests/delete/" . $row->rqst_FID . '/' . $row->rqst_ID) ?>"
                               class="btn btn-danger confirm_delete" data-toggle="tooltip"
                               data-placement="top" title="<?php echo langline("link_delete"); ?>">
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php
                $i++;
            endforeach;
            else:
                ?>
                <tr>
                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <nav><?php echo $pagination ?></nav>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>