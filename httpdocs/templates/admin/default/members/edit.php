<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-left"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("members") ?>"><?php echo langline("members_link") ?></a>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box green form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("Request") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>
        <form id="dataForm" action="" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Status") ?>:</label>

                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $row->UR_Status == 1 ? "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" : ($row->UR_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : ($row->UR_Status == 3 ? "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>" : "<span class='btn btn-sm purple'>" . langline("statusProcessingStill") . "</span>")) ?>
                                    <label><input type="radio" value="1"
                                                  name="Status" <?php echo $row->UR_Status == 1 ? 'checked' : ''; ?>><?php echo langline("statusNew"); ?>
                                    </label>
                                    <label><input type="radio" value="2"
                                                  name="Status" <?php echo $row->UR_Status == 2 ? 'checked' : ''; ?> ><?php echo langline("statusAccept"); ?>
                                    </label>
                                    <label><input type="radio" value="3"
                                                  name="Status" <?php echo $row->UR_Status == 3 ? 'checked' : ''; ?> ><?php echo langline("statusNonAccept"); ?>
                                    </label>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Created") ?>:</label>

                            <div class="col-md-9">
                                <p class="form-control-static"><?php echo $row->UR_Created ?></p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <h3 class="form-section"><?php echo langline("Personal_Info") ?></h3>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Name") ?>:</label>

                            <div class="col-md-9">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <select name="nikName" id="nikName" class="form-control">
                                    <option
                                        value="<?php echo langline("users_nikName_5"); ?>"  <?php echo $row->UR_nikName == langline("users_nikName_5") ? 'selected' : '' ?>><?php echo langline("users_nikName_5"); ?></option>
                                    <option
                                        value="<?php echo langline("users_nikName_1"); ?>" <?php echo $row->UR_nikName == langline("users_nikName_1") ? 'selected' : '' ?>><?php echo langline("users_nikName_1"); ?></option>
                                    <option
                                        value="<?php echo langline("users_nikName_2"); ?>" <?php echo $row->UR_nikName == langline("users_nikName_2") ? 'selected' : '' ?>><?php echo langline("users_nikName_2"); ?></option>
                                    <option
                                        value="<?php echo langline("users_nikName_3"); ?>" <?php echo $row->UR_nikName == langline("users_nikName_3") ? 'selected' : '' ?>><?php echo langline("users_nikName_3"); ?></option>
                                    <option
                                        value="<?php echo langline("users_nikName_4"); ?>" <?php echo $row->UR_nikName == langline("users_nikName_4") ? 'selected' : '' ?>><?php echo langline("users_nikName_4"); ?></option>
                                </select>
                            </div>
                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="FName" name="FName" class="form-control"
                                           placeholder="<?php echo langline("users_FName") ?>"
                                           value="<?php echo $row->UR_FName ?>" required>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="SName" name="SName" class="form-control"
                                           placeholder="<?php echo langline("users_SName") ?>"
                                           value="<?php echo $row->UR_SName ?>" required></div>
                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="TName" name="TName" class="form-control input-small"
                                           placeholder="<?php echo langline("users_TName") ?>"
                                           value="<?php echo $row->UR_TName ?>" required></div>
                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" id="QName" name="QName" class="form-control input-small"
                                           placeholder="<?php echo langline("users_QName") ?>"
                                           value="<?php echo $row->UR_QName ?>" required>
                                </div>


                                <!--                                <p class="form-control-static">-->
                                <?php //echo $row->UR_Name ?><!--</p>-->
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Phone") ?>:</label>
                            <div class="col-md-9">
                                <input name="Phone" id="Phone" type="text"
                                       class="form-control" value="<?php echo $row->UR_Phone ?>">
<!--                                <p class="form-control-static">--><?php //echo $row->UR_Phone ?><!--</p>-->
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("btn_change_password") ?>:</label>
                            <div class="col-md-9">
                                <input name="UserPassword" id="UserPassword" type="password"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_NationalID") ?>:</label>
                            <div class="col-md-9">
                                <input name="NationalID" id="NationalID" type="text"
                                       class="form-control" value="<?php echo $row->UR_NationalID ?>">
<!--                                <p class="form-control-static">-->
<!--                                    --><?php //echo $row->UR_NationalID ?>
<!--                                </p>-->
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_MemberID") ?>:</label>
                            <div class="col-md-9">
                                <input name="MemberID" id="MemberID" type="text"
                                       class="form-control" value="<?php echo $row->UR_MemberID ?>">
<!--                                <p class="form-control-static">--><?php //echo $row->UR_MemberID ?><!--</p>-->
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Nationlaty") ?>:</label>
                            <div class="col-md-9">
                                <select name="Nationlaty" id="Nationlaty" class="form-control">
                                    <option value="oman" <?php echo $row->UR_Nationlaty == "oman" ? "selected" : "" ?> >سلطنة عُمان</option>
                                    <option value="uea" <?php echo $row->UR_Nationlaty == "uea" ? "selected" : "" ?>>الإمارات العربية المتحدة</option>
                                    <option value="ksa" <?php echo $row->UR_Nationlaty == "ksa" ? "selected" : "" ?>>المملكة العربية السعودية</option>
                                    <option value="kba" <?php echo $row->UR_Nationlaty == "kba" ? "selected" : "" ?>>ممكلة البحرين</option>
                                    <option value="qatar" <?php echo $row->UR_Nationlaty == "qatar" ? "selected" : "" ?>>دولة قطر</option>
                                    <option value="kiwat" <?php echo $row->UR_Nationlaty == "kiwat" ? "selected" : "" ?>>دولة الكويت</option>
                                </select>
<!--                                <p class="form-control-static">--><?php //echo langline($row->UR_Nationlaty) ?><!--</p>-->
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_NationalIDImage") ?>:</label>
                            <div class="col-md-9">
                                <div class="col-md-8">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img
                                                src="<?php echo $row->UR_NationalIDImage ? $upload_path . '/requests/' . $row->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                                                alt=""/>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                            <img src="<?php echo $upload_path . '/requests/' . $row->UR_NationalIDImage ?>"
                                                 style="max-height: 150px;">
                                        </div>
                                        <div>
                                        <span class="btn default btn-file">
                                            <span class="fileupload-new"><i
                                                    class="icon-paper-clip"></i> <?php echo langline("btn_select_image") ?></span>
                                            <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                            <input type="file" class="default" name="NationalIDImage" id="NationalIDImage"/>
                                            <input type="hidden" class="default" name="old_NationalIDImage" id="old_NationalIDImage"
                                                   value="<?php echo $row->UR_NationalIDImage ?>"/>
                                        </span>
                                            <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i
                                                    class="icon-trash"></i> Remove</a>
                                        </div>
                                    </div>
                                    <span class="label label-danger">NOTE!</span>
                                <span>
                                    Attached image thumbnail is supported in Latest Firefox,
                                    Chrome, Opera, Safari and Internet Explorer 10 only
                                </span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
</div>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>


