<!-- BEGIN PAGE LEVEL STYLES --> 
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" /> 
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB--> 
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-left"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("members") ?>"><?php echo langline("members_link") ?></a> 
            </li>                    
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box green form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("Request") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>
        <form id="dataForm" action="" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-body"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Status") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                 <?php echo $row->UR_Status == 1 ? "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" : ($row->UR_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : ($row->UR_Status == 3?"<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>":"<span class='btn btn-sm purple'>" . langline("statusProcessingStill") . "</span>")) ?> 
                                    <label><input type="radio" value="1" name="UR_Status" <?php echo $row->UR_Status == 1?'checked':'';?>><?php echo langline("statusNew"); ?></label>
                                    <label><input type="radio" value="2" name="UR_Status" <?php echo $row->UR_Status == 2?'checked':'';?> ><?php echo langline("statusAccept"); ?></label>
                                    <label><input type="radio" value="3" name="UR_Status" <?php echo $row->UR_Status == 3?'checked':'';?> ><?php echo langline("statusNonAccept"); ?></label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>  
                <h3 class="form-section"><?php echo langline("Personal_Info") ?></h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Name") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><?php echo $row->UR_Name ?></p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Created") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><?php echo $row->UR_Created ?></p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <?php /*
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Email") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><?php echo $row->UR_Email ?></p>
                            </div>
                        </div>
                    </div>
                     */?>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Phone") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><?php echo $row->UR_Phone ?></p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("btn_change_password") ?>:</label>
                            <div class="col-md-9">
                                <input name="UserPassword" id="UserPassword" type="password" class="form-control input-lg" >
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->             
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_NationalID") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $row->UR_NationalID ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_MemberID") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><?php echo $row->UR_MemberID ?></p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->              
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_Nationlaty") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><?php echo langline($row->UR_Nationlaty) ?></p>
                            </div>
                        </div>
                    </div> 
                    <!--/span-->
                </div>
                <!--/row-->          
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo langline("UR_NationalIDImage") ?>:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <img src="<?php echo $row->UR_NationalIDImage ? $upload_path . '/requests/' . $row->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="300" />
                                </p>
                            </div>
                        </div>
                    </div> 
                    <!--/span-->
                </div> 
                
            </div> 
        </form>
        <div class="clearfix"></div>
    </div>
</div>  


