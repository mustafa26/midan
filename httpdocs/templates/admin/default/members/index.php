<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("members") ?>"><?php echo langline("members_link") ?></a>
                    <span class="badge badge-warning">&nbsp;<?php echo $total; ?>&nbsp;</span>
                </div> 
                <div class="pull-right">
                    <a href="?status=1" class="btn btn-sm yellow"><?php echo langline("statusNew") ?></a>
                    <a href="?status=2" class="btn btn-sm green"><?php echo langline("statusAccept") ?></a>
                    <a href="?status=3" class="btn btn-sm red"><?php echo langline("statusNonAccept") ?></a>
                    <a href="?" class="btn btn-sm blue"><?php echo langline("All") ?></a>
                </div>
            </div> 
            <div class="portlet-body">   
                <?php echo theme_messages(); ?>
                <div class="col-md-12 col-sm-6"> 
                    <div class="col-md-12">
                        <form action="" method="get" class="form-inline">
                            <div class="form-group">  
                                <input type="text" name="MemberID" class="form-control col-md-3" placeholder="<?php echo langline("UR_MemberID") ?>" value="<?php echo get("MemberID")?get("MemberID"):""; ?>"> 
                            </div>
                            <div class="form-group">
                                <input type="text" name="NationalID" class="form-control col-md-3" placeholder="<?php echo langline("UR_NationalID") ?>" value="<?php echo get("NationalID")?get("NationalID"):""; ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" name="Phone" class="form-control col-md-3" placeholder="96895000000" value="<?php echo get("Phone")?get("Phone"):""; ?>">
                            </div>
                            <button type="submit" class="btn btn-default"><?php echo langline("filter_btn") ?></button>
                            <?php if (check_user_permission(current_module(), "add")): ?>
                                <div class="btn-group pull-right">
                                    <a href="<?php echo admin_url("members/add") ?>" id="add_new_row"
                                       class="btn green">
                                        <?php echo langline("link_add_new") ?> <i class="icon-plus"></i></a>
                                </div>
                            <?php endif; ?>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr>  
                                <th width="2%">&nbsp;</th>
                                <th width="35%"><?php echo langline("Personal_Info") ?></th>
                                <th width="30%"><?php echo langline("Personal_Info") ?></th> 
                                <th width="20%" ><?php echo langline("UR_NationalIDImage") ?></th> 
                                <th width="5%"><?php echo langline("project_status") ?></th>
                                <th width="10%">&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $offset;
                            if ($data): foreach ($data as $row):
                                    if (is_file('uploads/requests/' . $row->UR_NationalIDImage) && !is_file('uploads/requests/thumb/' . $row->UR_NationalIDImage)) {
                                        resizeProp('uploads/requests/' . $row->UR_NationalIDImage, 'uploads/requests/thumb/' . $row->UR_NationalIDImage, 150, 150);
                                    }
                                    ?>
                                    <tr class="odd gradeX">  
                                        <td><?php echo $i; ?></td>
                                        <td> 
                                            <strong class="col-md-5 text-muted"><?php echo langline("UR_Created") ?></strong> : <span class="label label-info"><?php echo $row->UR_Created; ?></span><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("UR_Name") ?></strong> : <?php echo $row->UR_Name; ?><br> 
                                            <strong class="col-md-5 text-muted"><?php echo langline("UR_Phone") ?></strong> : <?php echo $row->UR_Phone; ?><br>
                                            <strong class="col-md-5 text-muted">Active N</strong> : <?php echo $row->UR_ActiveCode; ?>
                                        </td>    
                                        <td>
                                            <strong class="col-md-5 text-muted"><?php echo langline("National_ID") ?></strong> : <?php echo $row->UR_NationalID; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("UR_MemberID") ?></strong> : <?php echo $row->UR_MemberID; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("UR_Nationlaty") ?></strong> : <?php echo langline($row->UR_Nationlaty); ?><br>
                                            <?php /* ?><strong class="col-md-5 text-muted"><?php echo langline("UR_Job") ?></strong> : <?php echo $row->UR_Job; ?><br>
                                              <strong class="col-md-5 text-muted"><?php echo langline("UR_JobPlace") ?></strong> : <?php echo $row->UR_JobPlace; ?><br><?php */ ?>

                                        </td> 
                                        <td>
                                            <img src="<?php echo $row->UR_NationalIDImage ? $upload_path . '/requests/thumb/' . $row->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="150" />
                                        </td>    
                                        <td>
                                            <?php
                                            echo
                                            $row->UR_Status == 1 ?
                                                    "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" :
                                                    ($row->UR_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" :
                                                            ($row->UR_Status == 3 ? "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>" :
                                                                    "<span class='btn btn-sm purple'>" . langline("statusProcessingStill") . "</span>"))
                                            ?>                                            
                                        </td> 
                                        <td class="text-center">
                                            <?php
                                            if (check_user_permission(current_module(), "show")):
                                                ?>
                                                <a href="<?php echo admin_url("members/requests/" . $row->UR_ID) ?>" class="btn btn-sm blue"><?php echo langline("members_link") ?></a>
                                                <a class=" btn default" href="<?php echo admin_url("members/view/" . $row->UR_ID) ?>" data-target="#ajax_<?php echo $row->UR_ID?>" data-toggle="modal">
                                                    <i class="icon icon-eye-open"></i>
                                                </a>
                                                <div class="modal fade" id="ajax_<?php echo $row->UR_ID?>" tabindex="-1" role="basic" aria-hidden="true">
                                                    <img src="<?php echo $themepath ?>/assets/img/ajax-modal-loading.gif" alt="" class="loading">
                                                </div>​
                                            <?php
                                            endif;
                                            if (check_user_permission(current_module(), "show")):
                                                ?>
                                                <a href="<?php echo admin_url("members/edit/" . $row->UR_ID) ?>"
                                                   class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                                   title="<?php echo langline("link_edit"); ?>">
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </a>
                                            <?php
                                            endif;
                                            if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                                                ?>
                                                <a href="<?php echo admin_url("members/delete/" . $row->UR_ID) ?>"
                                                   class="btn btn-danger confirm_delete" data-toggle="tooltip" data-placement="top" title="<?php echo langline("link_delete"); ?>" >
                                                    <i class="glyphicon glyphicon-remove"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr> 
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <nav><?php echo $pagination ?></nav>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>