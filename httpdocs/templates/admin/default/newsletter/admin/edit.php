

<div class="content-box">
    <!-- Start Content Box -->

    <div class="content-box-header">

        <h3>{lang:title_edit_newsletter_member}</h3>

        <ul class="content-box-tabs">

            <li>
                <a href="#details_tab" class="current">{lang:tab_details}</a
            </li>
            <!-- href must be unique and match the id of target div -->

        </ul>
        <div class="form_options">
            <input class="button "id="save_form_btn" type="button" value="{lang:save}" onclick="(function ($) {
                                    $('#formdata').submit()
                                })(jQuery)" />
            <a href="{base}newsletter/admin/index/<?= segment(4) ?>">
                <input class="button" type="button" value="{lang:cancel}" />
            </a>
        </div>
        <div class="clear"></div>

    </div>
    <!-- End .content-box-header -->

    <div class="content-box-content">
        <div id="respmsg">
            {message}
        </div>
        <?php $i = 1 ?>
        <?php
        $_action = "";
        $_attributes = array(
            "method" => "post",
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data",
            "id" => "formdata");
        echo form_open($_action, $_attributes);
        ?>    

        <div class="tab-content default-tab" id="details_tab">
            <!-- This is the target div. id must match the href of this div's tab -->
            <?$i++?>

            <fieldset>

                <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                <p>
                    <label>
                        {lang:label_name}*
                    </label>

                    <input class="text-input small-input" type="text" id="small-input" name="name" value="<?= $data->mn_name ?>" autocomplete="off"/>

                    <br/>
                </p>
                <p>
                    <label>
                        {lang:label_email}*
                    </label>
                    <input class="text-input small-input" type="text" id="small-input" name="email" value="<?= $data->mn_email ?>" />
                    <input type="hidden" name="hidden_email" value="<?= $data->mn_email ?>" />
                    <br/>
                </p>
                <p>
                    <label><input type="checkbox" name="active" <?= $data->mn_active == 1 ? "checked" : "" ?> value="1"/>{lang:label_active}</label>

                </p>



            </fieldset>

            <div class="clear"></div><!-- End .clear -->

        </div>
        <!-- End #tab1 -->

        <!-- End #tab2 -->
        <?php echo form_close() ?>
    </div>
    <!-- End .content-box-content -->

</div>
<script type="text/javascript">
    saveForm({
        form: "#formdata",
        text_saving: "{lang:saving}",
        respmsg: "#respmsg",
        save_button: "#save_form_btn"
    });


</script>
<!-- End .content-box -->