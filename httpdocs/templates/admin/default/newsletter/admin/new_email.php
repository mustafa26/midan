<div class="content-box">
    <!-- Start Content Box -->
    <div class="content-box-header">
        <h3>{lang:title_new_email}</h3>
        <ul class="content-box-tabs">
            <li>
                <a href="#details_tab" class="current">{lang:tab_details}</a
            </li>
            <!-- href must be unique and match the id of target div -->
        </ul>

        <div class="form_options">
            <input class="button" id="save_form_btn" type="button" value="{lang:save}" onclick="(function ($) {
                        $('#formdata').submit()
                    })(jQuery)" />
            <a href="<?php echo base() ?>newsletter/admin/index/<?= segment(4) ?>/?mtype=<?= get("mtype") == "users" ? "users" : "newsletter" ?>">
                <input class="button" type="button" value="{lang:cancel}" />
            </a>

        </div>
        <div class="clear"></div>

    </div>

    <!-- End .content-box-header -->
    <div class="content-box-content">
        <div id="respmsg">
            {message}
        </div>
        <?php $i = 1 ?>
        <?php
        $_action = "";
        $_attributes = array(
            "method" => "post",
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data",
            "id" => "formdata");
        echo form_open($_action, $_attributes);
        ?>   

        <div class="tab-content default-tab" id="details_tab">
            <!-- This is the target div. id must match the href of this div's tab -->
            <?$i++?>

            <fieldset>

                <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                <p>
                    <label>
                        {lang:label_subject}*
                    </label>

                    <input class="text-input small-input" type="text" id="small-input" name="subject" value="" autocomplete="off"/>

                    <br/>
                </p>

                <p style="direction:ltr;text-align: right">
                    <label>
                        {lang:label_message}*
                    </label>
                    <strong>you can user the following tags to customaiz your email without (@):</strong>
                <p style="direction:ltr;text-align: right">
                    {@name},{@email}
                </p>
                <strong>and you can user the Following language tags without (@):</strong>
                <p style="direction:ltr;text-align: right">
                    {@lang:label_name},{@lang:label_email}
                </p>
                <textarea class="ckeditor" name="message"><?= $email_theme ?></textarea>
                <br/>
                </p>

                <p>
                    <label><input type="checkbox" name="active" checked value="1"/>{lang:label_active}</label>

                </p>

            </fieldset>

            <div class="clear"></div><!-- End .clear -->

        </div>
        <!-- End #tab1 -->

        <!-- End #tab2 -->
        <?php echo form_close() ?>
    </div>
    <!-- End .content-box-content -->

</div>
<script type="text/javascript">



</script>
<!-- End .content-box -->