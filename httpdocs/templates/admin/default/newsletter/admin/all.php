<style>
	#module_sublinks {
		padding: 5px;
		margin: 10px;
		border-bottom: 1px solid #ccc;
	}
	#module_sublinks a {
		display: inline-block;
		margin: 5px;
		padding: 5px;
		background: #555555;
		color: #fff;
		border-radius: 5px;
	}
	#module_sublinks a:hover, #module_sublinks a.active_link {
		background: #459300;
		color: #000000;
		border-radius: 5px;
	}

</style>
<div class="content-box"><!-- Start Content Box --> 
        <div class="content-box-header"> 
                <h3>{lang:title_newsletter_members}</h3>
                <div class="form_options">
                        <a href="{base}newsletter/admin/add/<?=segment(4) ?>"><input type="submit" class="button"  value="{lang:add}"/></a>
                        <a><?php view("admin/$template/$search_form")?></a>
                </div>
                <div class="clear"></div> 
        </div> <!-- End .content-box-header -->
<div class="content-box-content">
	<div id="module_sublinks">
		<a href="?mtype=users" class="<?=get("mtype") == "users" ? "active_link" : "" ?>">{lang:title_members_list}</a>
		<a href="?mtype=newsletter" class="<?=get("mtype") == "newsletter" ? "active_link" : "" ?>">{lang:title_newsletter_members}</a>
		<a href="{base}newsletter/admin/new_email/<?=segment(4) ?>/?mtype=<?=get("mtype") == "users" ? "users" : "newsletter" ?>" >{lang:title_send_letter}</a></div>
	<div id="messages">{message}</div>
	<div class="tab-content default-tab" id="tab1">
		<!-- This is the target div. id must match the href of this div's tab -->
		<table width="5%">
        <form action="" method="post" id="formdata" onsubmit="if($('#table_actions').val()=='delete') return confirm('{lang:confirm_delete_one_item}');">
            <thead> 
                    <tr>
                        <th>
                        <input class="check-all" type="checkbox" />
                        </th>
                        <th>{lang:label_name}</th>
                        <th>{lang:label_email}</th>
                        <th>{lang:label_join_date}</th>
                        <th>{lang:label_status}</th>
                        <th>{lang:label_options}</th>

                    </tr> 
            </thead> 
                <tbody>
                        <?php if($data): foreach($data as $val):?>
                        <tr>
                            <td>
                            <input type="checkbox" name="item_id[]" value="<?=$val -> id ?>"/>
                            </td>
                            <td><?=$val -> name ?></td>
                            <td><?=$val -> email ?></td>
                            <td><?=date("Y-m-d H:i:s A", $val -> created) ?></td>
                            <td><?=$val -> active == 1 ? "<font color='green'>{lang:active}</font>" : "<font color='red'>{lang:inactive}</font>" ?></td>
                            <td><!-- Icons -->
                            <a href="{base}newsletter/admin/edit/<?=segment(4) ?>/<?=$val -> id ?>/" title="{lang:edit}">
                            <img src="{themepath}/resources/images/icons/pencil.png" alt="{lang:edit}" />
                            </a>
                            <a href="{base}newsletter/admin/delete/<?=segment(4) ?>/<?=$val -> id ?>/" title="{lang:delete}" class="delete_item">
                            <img src="{themepath}/resources/images/icons/cross.png" alt="Delete" />
                            </a> 
                            </td> 
                        </tr> 
                <?php endforeach;endif; ?>
                </tbody> 
                <tfoot>
                        <tr>
                                <td colspan="6">
                                <div class="bulk-actions align-left">
                                        <select name="action" id="table_actions">
                                                <option value="">{lang:choose_an_action_label}...</option>
                                                <option value="enable">{lang:enable_label}</option>
                                                <option value="disable">{lang:disable_label}</option>
                                                <option value="delete">{lang:delete_label}</option>
                                        </select>
                                        <input type="submit" class="button" name="save_form_act" id="save_form_btn" value="{lang:Apply_to_selected}" />
                                </div>
                                <div class="pagination">
                                        {pagination}
                                </div><!-- End .pagination --><div class="clear"></div>
                                </td>
                        </tr> 
                </tfoot>
                </form>
		</table> 
	</div>
	<!-- End #tab1 -->
</div>
</div>
<script type="text/javascript">
	delete_item({
		selector : ".delete_item",
		alert_text : "{lang:confirm_delete_one_item}"
	});

</script>