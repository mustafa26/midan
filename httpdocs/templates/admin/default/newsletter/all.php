<!-- BEGIN PAGE HEADER-->   
<div class="row">
    <div class="col-md-12"> 
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <span class="icon-angle-left"></span>
            </li>
            <li>
                <a href="#"><?php echo langline("title_newsletter") ?></a> 
            </li> 
        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12"> 

        <!-- BEGIN PORTLET-->   
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i><?php echo langline("title_newsletter_members") ?></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a> 
                </div>
            </div>
            <div class="portlet-body">
                <?php echo theme_messages(); ?>  
                <table class="table table-hover table-striped table-bordered table-hover">
                    <thead>
                        <tr> 
                            <th>Username</th>
                            <th >Email</th>
                            <th >Points</th> 
                            <th >&nbsp;</th>
                        </tr>
                    </thead>
                    <?php if ($data): foreach ($data as $val): ?>
                            <tr> 
                                <td><?= $val->mn_email ?></td>
                                <td><?= date("Y-m-d H:i:s A", $val->mn_created) ?></td>
                                <td>
                                    <?php echo $val->mn_active == 0 ? "<span class='label label-danger'>" . langline("status_inactive") . "</span>" : "<span class='label label-success'>" . langline("status_active") . "</span>" ?>
                                </td>
                                <td>
                                    <?php if ($val->mn_active == 0) { ?>                                
                                        <a href="<?php echo admin_url("newsletter/active/" . $val->mn_id) ?>" class="btn blue">
                                            <?php echo langline("btn_active") ?> 
                                        </a> 
                                    <?php } else { ?>                                      
                                        <a href="<?php echo admin_url("newsletter/inactive/" . $val->mn_id) ?>" class="btn red">
                                            <?php echo langline("btn_inactive") ?>
                                        </a>                                            
                                    <?php } ?> 
                                    <a href="<?php echo admin_url("newsletter/delete/" . $val->mn_id) ?>" class="btn red">
                                        <?php echo langline("link_delete") ?> 
                                    </a>  
                                </td>
                            </tr> 
                            <?php
                        endforeach;
                    endif;
                    ?>
                </table> 
                <!-- /.modal --> 
            </div>
            <nav><?php echo $pagination ?></nav>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT--> 
