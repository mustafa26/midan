<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <!-- Facebook sharing information tags -->
        <meta property="og:title" content="*|MC:SUBJECT|*" />

        <title><?= $this->site_name ?></title>
        <style type="text/css">
            /* Client-specific Styles */
            #outlook a {
                padding: 0;
            }/* Force Outlook to provide a "view in browser" button. */
            body {
                width: 100% !important;
            }
            .ReadMsgBody {
                width: 100%;
            }
            .ExternalClass {
                width: 100%;
            }/* Force Hotmail to display emails at full width */
            body {
                -webkit-text-size-adjust: none;
            }/* Prevent Webkit platforms from changing default text sizes. */

            /* Reset Styles */
            body {
                margin: 0;
                padding: 0;
                direction: rtl;
            }
            img {
                border: 0;
                height: auto;
                line-height: 100%;
                outline: none;
                text-decoration: none;
            }
            table td {
                border-collapse: collapse;
            }
            #backgroundTable {
                height: 100% !important;
                margin: 0;
                padding: 0;
                width: 100% !important;
            }
            body, #backgroundTable {
                background-color: #FAFAFA;
            }
            .preheaderContent div {
                color: #505050;
                font-family: Arial;
                font-size: 10px;
                line-height: 100%;
                text-align: right;
            }
            .preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts/* Yahoo! Mail Override */ {
                color: #336699;
                font-weight: normal;
                text-decoration: underline;
            }
            #templateHeader {
                background-color: #FFFFFF;
                border-bottom: 0;
            }
            .headerContent {
                color: #202020;
                font-family: Arial;
                font-size: 34px;
                font-weight: bold;
                line-height: 100%;
                padding: 0;
                text-align: right;
                vertical-align: middle;
            }
            .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts/* Yahoo! Mail Override */ {
                color: #336699;
                font-weight: normal;
                text-decoration: underline;
            }
            #headerImage {
                height: auto;
                max-width: 600px;
            }
            .bodyContent div {
                color: #505050;
                font-family: Arial;
                font-size: 14px;
                line-height: 150%;
                text-align: right;
            }
            .bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts/* Yahoo! Mail Override */ {
                color: #336699;
                font-weight: normal;
                text-decoration: underline;
            }
            .bodyContent img {
                display: inline;
                height: auto;
            }
            #templateFooter {
                background-color: #fff;
                border-top: 0;
            }
            .footerContent div {
                color: #707070;
                font-family: Arial;
                font-size: 12px;
                line-height: 125%;
                text-align: right;
            }
            .footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts/* Yahoo! Mail Override */ {
                color: #336699;
                font-weight: normal;
                text-decoration: underline;
            }
            .footerContent img {
                display: inline;
            }
            #social {
                background-color: #F5F5F5;
                border: 0 solid #999;
            }
            #social div {
                text-align: center;
            }
            #utility {
                background-color: #F5F5F5;
                border: 0;
            }
            #utility div {
                text-align: center;
            }
            .txt_green {
                font-family: Arial, Gadget, sans-serif;
                font-size: 16px;
                color: #390;
                font-weight: bold;
            }
            p {
                margin: 0px;
            }
            #templateContainer {
                /*@editable*/
                border: 1px solid #DDDDDD;
            }

        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" id="templateContainer">
            <tr>
                <td>
                    <center>

                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td class="headerContent"><!-- // Begin Module: Standard Header Image \\ -->
                                                <img src="header.jpg" style="max-width:600px;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
                                                <!-- // End Module: Standard Header Image \\ --></td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                        <tr>
                                            <td colspan="4" valign="top" class="bodyContent"><!-- // Begin Module: Standard Content \\ -->
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <div  mc:edit="std_content00">
                                                                <p class="txt_green">
                                                                    <?= langline("thank_you_for_join_us"); ?>
                                                                </p>
                                                            </div></td>
                                                    </tr>
                                                </table><!-- // End Module: Standard Content \\ --></td>
                                        </tr>

                                    </table>
                                    <table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">
                                        <tr>
                                            <td valign="top" class="footerContent"><!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="social">

                                                            <div mc:edit="std_social">
                                                                &nbsp;<a href="#"><?= base() ?></a> | <a href="#"><?= $this->site_email ?></a> | <a href="#">forward to a friend</a>&nbsp;
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2" valign="middle" id="utility">
                                                            <div mc:edit="std_utility">
                                                                &nbsp;<a href="{base}news_letter_unsubscribe/{id}/{email}">unsubscribe from this list</a> &nbsp;
                                                            </div></td>
                                                    </tr>
                                                </table><!-- // End Module: Standard Footer \\ --></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                    </center></td>
            </tr>
        </table>

    </body>
</html>