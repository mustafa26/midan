<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/> 
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css">
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("galleries") ?>"><?php echo langline("galleries_link") ?></a> &nbsp;
                </div> 
                <div class="caption">
                    <i class="icon-angle-left"></i>  
                    <?php echo langline("list_all") ?> 
                </div>
            </div>
            <div class="portlet-body">   
                <?php echo theme_messages(); ?>  
                <?php if (check_user_permission(current_module(), "add")): ?> 
                    <div class="portlet box blue form">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?php echo langline("link_add_new") ?></div>
                            <div class="tools">
                                <a href="javascript:;" <?php echo!get("open") ? 'class="expand"' : 'class="collapse"' ?>></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            </div>
                        </div>
                        <div class="portlet-body" <?php echo!get("open") ? 'style="display: none;"' : '' ?>>

                            <?php
                            $_action = admin_url("galleries/add");
                            $_attributes = array(
                                "id" => "dataForm",
                                "method" => "post",
                                "class" => "form-horizontal",
                                "role" => "form",
                                "enctype" => "multipart/form-data");
                            echo form_open($_action, $_attributes);
                            ?>  
                            <div class="tabbable-custom tabs-below ">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_2_1">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo langline("cats_name") ?> (عربي)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="cat_name[ar]" class="form-control input-lg"
                                                       placeholder="<?php echo langline("cats_name") ?>"
                                                       value="<?php echo input_value("cat_name[ar]") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_2_2">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> <?php echo langline("cats_name") ?> (English)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="cat_name[en]" class="form-control input-lg" placeholder="<?php echo langline("cats_name") ?>"
                                                       value="<?php echo input_value("cat_name[en]") ?>"> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_2_1" data-toggle="tab"><?php echo langline("language_tab_arabic") ?></a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_2_2" data-toggle="tab"><?php echo langline("language_tab_english") ?></a>
                                    </li>
                                </ul> 
                            </div>
                            <div class="col-md-12"> 
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>
                                    <div class="col-md-7">    
                                        <div class="i-checkbox">
                                            <input type="checkbox" name="cat_status" id="cat_status" class="iButton" value="1" checked />
                                        </div>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("cats_name") ?></label>
                                    <div class="col-md-2">  
                                        <select name="cat_Type" class="form-control select2-default">
                                            <option value="g"><?php echo langline("galleries_link") ?></option>
                                            <option value="hv"><?php echo langline("Heritage_Village") ?></option> 
                                        </select> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("year") ?></label>
                                    <div class="col-md-2">  
                                        <select name="cat_Year" class="form-control select2-default">
                                            <?php for ($i = date("Y"); $i >= 2016; $i--): ?>
                                                <option value="<?php echo $i ?>"><?php echo $i ?></option> 
                                            <?php endfor; ?>
                                        </select> 
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-md-2"><?php echo langline("project_attaches") ?></label> 
                                    <div class="col-md-8">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" style="max-height: 150px;">
                                            </div> 
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> <?php echo langline("btn_select_image") ?></span>
                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                                    <input type="file" class="default" name="cat_image" />
                                                </span>
                                                <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                            </div>
                                        </div>
                                        <span class="label label-danger">NOTE!</span> 
                                        <span>
                                            Attached image thumbnail is supported in Latest Firefox, 
                                            Chrome, Opera, Safari and Internet Explorer 10 only
                                        </span> 
                                    </div>
                                </div> 
                                <div class="form-group last">      
                                    <button type="submit" class="btn blue" data-close-others="true" form="dataForm">
                                        <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                                    </button>
                                </div> 
                            </div>
                        </div> 
                        <?php echo form_close() ?>
                        <div class="clearfix"></div>
                    </div>
                </div> 
            <?php endif; ?> 
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                    <thead>
                        <tr>
                            <!--<th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set=".dataTable .checkboxes"/></th>-->
                            <th><?php echo langline("cats_name") ?></th> 
                            <th><?php echo langline("gallery_HeaderPhoto") ?></th>
                            <th><?php echo langline("year") ?></th>
                            <th><?php echo langline("project_status") ?></th>
                            <th width="200">&nbsp;</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($data): foreach ($data as $row): ?> 
                                <tr class="odd gradeX"> 
                                    <td><?php echo _t($row->cat_name, 'ar'); ?></td> 
                                    <td><img src="<?php echo $row->cat_image ? $upload_path . '/galleries/' . $row->cat_image : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="150" /></td>
                                    <td><span class='label label-warning'><?php echo $row->cat_Year; ?></span></td>
                                    <td>
                                        <div class="i-checkbox">
                                            <input type="checkbox" class="iButton" value="1" <?php echo $row->cat_status ? 'checked' : '' ?> />
                                        </div>
                                    </td> 
                                    <td class="text-center">
                                        <?php
                                        if (check_user_permission(current_module(), "show")):
                                            ?>
                                            <a href="<?php echo admin_url("galleries/cats/" . $row->cat_id) ?>"
                                               class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                               title="<?php echo langline("link_edit"); ?>">
                                                <i class="icon icon-eye-open"></i>
                                            </a>
                                            <?php
                                        endif;
                                        if (check_user_permission(current_module(), "edit")):
                                            ?>
                                            <a href="<?php echo admin_url("galleries/edit/" . $row->cat_id) ?>"
                                               class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                               title="<?php echo langline("link_edit"); ?>">
                                                <i class="glyphicon glyphicon-edit"></i>
                                            </a>
                                            <?php
                                        endif;
                                        if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                                            ?>
                                            <a href="<?php echo admin_url("galleries/delete/" . $row->cat_id) ?>"
                                               class="btn btn-danger confirm_delete" data-toggle="tooltip"
                                               data-placement="top" title="<?php echo langline("link_delete"); ?>">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr> 
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <nav><?php echo $pagination ?></nav>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div> 
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>