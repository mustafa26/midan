<!--[if IE 8]><html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]><html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-Frame-Options" content="deny">
        <meta name="author" content="DRC">
        <title><?php echo $title ?></title>  
        <meta name="description" content="<?php echo $site->meta_description ?>"> 
        <meta name="keywords" content="<?php echo $site->meta_description ?>">    
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/> 
        <meta name="MobileOptimized" content="320"> 
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo $themepath ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES --> 
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="<?php echo $themepath ?>/assets/plugins/gritter/css/jquery.gritter-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css"rel="stylesheet" />
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo $themepath ?>/assets/css/style-metronic-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/style-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/style-responsive-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/plugins-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/pages/tasks-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/themes/blue-rtl.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo $themepath ?>/assets/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/jquery.ibutton/css/jquery.ibutton.min.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?php echo $themepath ?>/favicon.ico"/>
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?php echo $themepath ?>/assets/plugins/respond.min.js"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/excanvas.min.js"></script>
        <![endif]-->
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-migrate-1.4.1.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.ibutton/lib/jquery.ibutton.min.js" type="text/javascript"></script> 
        <?php echo $scripts ?>
        <script src="<?php echo $themepath ?>/assets/scripts/app.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed" style="direction: rtl">
        <!-- BEGIN HEADER -->
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <!-- BEGIN LOGO -->
                <a class="navbar-brand" href="<?php echo admin_url() ?>">
                    <img src="<?php echo $themepath ?>/assets/img/logo.png" alt="logo" class="img-responsive" />
                </a>
                <div class="col-md-offset-2 col-md-4">
                    &nbsp;
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <img src="<?php echo $themepath ?>/assets/img/menu-toggler.png" alt=""/>
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <li class="dropdown"  id="btn-user-notifications">
                        <a href="#" id="btn-user-notifications" class="dropdown-toggle" data-toggle="dropdown" data-ng-click="dropdown"
                           data-close-others="true">
                            <i class="icon-warning-sign"></i>
                            <span class="badge notification-counter" ></span>
                        </a>
                        <ul class="dropdown-menu extended notification">

                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" id="notifications-list">

                                </ul>
                            </li>
                            <li class="external">
                                <a href="<?php echo admin_url("notifications") ?>"><?php echo langline("see_all") ?><i class="m-icon-swapright"></i></a>
                            </li>
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-ng-click="dropdown" data-close-others="true">
                            <?php if ($user->user_avatar): ?>
                                <img alt="" src="<?php echo $upload_path . "/" . $user->user_avatar ?>" style="width:29px;height:29px;"/>
                            <?php else: ?>
                                <img alt="" src="<?php echo $themepath ?>/assets/img/avatar.png" style="width:29px;height:29px;"/>
                            <?php endif; ?>
                            <span class="username"><?php echo $user->user_name ?></span>
                            <i class="icon-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo admin_url("users/account") ?>"><i class="icon-user"></i> <?php echo langline("my_account_link") ?></a>
                            </li> 
                            <!--
                            <li>
                                <a href="inbox.html"><i class="icon-envelope"></i> <?php /* echo langline("my_inbox_link") */ ?><span class="badge badge-danger">3</span></a>
                            </li>
                            <li>
                                <a href="<?php //echo admin_url("projects");         ?>"><i class="icon-tasks"></i> <?php //echo langline("my_tasks_link")         ?></a>
                            </li>-->
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:;" id="trigger_fullscreen"><i class="icon-move"></i> <?php echo langline("full_screen_link") ?></a>
                            </li>
                            <li>
                                <a href="<?php echo admin_url("logout") ?>" onclick="if (!confirm(jslang.logout_confirm_message))
                                            return false;"><i class="icon-key"></i> <?php echo langline("logout_link") ?></a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php echo side_bar(); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content"> 
                <?php view($template . "/" . $content); //echo $this->template->content($theme_dir."/".$theme_name."/". $content);  ?>
            </div>
            <!-- END PAGE -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="footer">
            <div class="footer-inner">
                <a href="http://www.durar-it.com" class="label label-danger">2015 &copy; المديرية العامة للإتصالات ونظم المعلومات</a> 
            </div>
            <div class="footer-tools"><span class="go-top"><i class="icon-angle-up"></i></span></div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN PAGE LEVEL PLUGINS --> 
        <script src="<?php echo $themepath ?>/assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js"
        type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"
        type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>

        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        <script src="<?php echo $themepath ?>/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"
        type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"
        type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script type="text/javascript">
                                    $(document).ready(function () {
                                        $(".confirm_delete").click(function () {
                                            if (!confirm(jslang.delete_confirm_message)) {
                                                return false;
                                            }
                                        });
                                    });

                                    jQuery(document).ready(function () {
                                        App.init(); // initlayout and core plugins 
                                        Portfolio.init();

                                        /*
                                         $(".confirm_delete").click(function (e) {
                                         if (!confirm(jslang.delete_confirm_message)) {
                                         return false;
                                         } else {
                                         e.preventDefault();
                                         return true;
                                         }
                                         });
                                         */

                                        var checkNotificaitons = function () {
                                            // get notifications by json
                                            $.getJSON(admin_base + "notifications/ajax", function (resp) {
                                                if (resp.success) {
                                                    var new_notifys = 0;
                                                    var limit = 5;
                                                    var html = "";
                                                    $(resp.html).each(function (k, v) {
                                                        var message = v.notify_message;
                                                        var avatar = upload_path + "/" + v.user_avatar;
                                                        if (!v.user_avatar) {
                                                            avatar = themepath + "/assets/img/avatar.png";
                                                        }

                                                        if (!parseInt(v.un_opened))
                                                            new_notifys++;

                                                        if (k < limit) {
                                                            html += '<li>' +
                                                                    '<a href="' + v.notify_link + '">' +
                                                                    '<img src="' + avatar + '" alt="" class="thumbnail pull-left" style="width:50px;height:50px;margin:5px;">' + message +
                                                                    '<span class="time "> ' + jslang.time_from + ' ' + v.notify_created + '</span> ' +
                                                                    '</a> ' +
                                                                    '</li>';
                                                        }


                                                        if (!parseInt(v.un_appear)) {
                                                            var unique_id = $.gritter.add({
                                                                text: message,
                                                                image: avatar,
                                                                sticky: false,
                                                                time: '',
                                                                class_name: 'my-sticky-class'
                                                            });
                                                            $.getJSON(admin_base + "notifications/ajax?appeared=" + v.un_id);
                                                        }

                                                    });

                                                    if (!new_notifys)
                                                        new_notifys = "";
                                                    $(".notification-counter").text(new_notifys);
                                                    $("#notifications-list").html(html);

                                                }
                                            });
                                        }


                                        checkNotificaitons();
                                        setInterval(checkNotificaitons, 200 * 60);
                                        //        update showed notifications
                                        $("#btn-user-notifications").click(function ()
                                        {
                                            $.getJSON(admin_base + "notifications/ajax?opened=all", function () {
                                                $(".notification-counter").text("");
                                            });

                                        })

                                        $(".iButton").iButton({
                                            duration: 200, // the speed of the animation
                                            easing: "swing",
                                            labelOn: "Yes", // the text to show when toggled on
                                            labelOff: "No"

                                        });

                                        $('[data-toggle="tooltip"]').tooltip();
                                    });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>