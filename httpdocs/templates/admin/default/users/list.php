<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title"); ?>
            <small><?php echo module_info("module_description"); ?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb"> 
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-right"></i>
            </li> 
            <li>
                <a href="<?php echo admin_url("users") ?>"><?php echo langline("link_users") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li><a href="#"><?php echo langline("list_all") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">

                <div class="caption"><i class="icon-globe"></i><?php echo langline("users_page_header") ?></div>

                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>

            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="btn-group">

                        <?php if (check_user_permission(current_module(), "add")): ?>
                            <a href="<?php echo admin_url("users/add") ?>" id="add_new_row" class="btn green">
                                <?php echo langline("link_add_new") ?> <i class="icon-plus"></i>
                            </a>
                        <?php endif; ?>

                    </div>
                </div>
                <?php echo theme_messages() ?>
                <div class="col-md-12 col-sm-6">
                    <div class="col-md-1">
                        <?php echo langline("filter_label") ?>
                    </div>
                    <div class="col-md-11">
                        
        <?php
        $_action = "";
        $_attributes = array(
            "id" => "dataForm",
            "method" => "get", 
            "class" => "form-inline");
        echo form_open($_action, $_attributes);
        ?>   
                            <div class="form-group">
                                <label class="sr-only"
                                       for="filter_name"><?php echo langline("users_user_name_col") ?></label>
                                <input type="search" name="filter[name]" class="form-control" id="filter_name"
                                       placeholder="<?php echo langline("users_user_name_col") ?>"
                                       value="<?php echo get("filter")['name'] ?>"/>
                            </div>
                            <div class="form-group">
                                <label class="sr-only"
                                       for="filter_email"><?php echo langline("users_user_email_col") ?></label>
                                <input type="search" name="filter[email]" class="form-control" id="filter_email"
                                       placeholder="<?php echo langline("users_user_email_col") ?>"
                                       value="<?php echo get("filter")['email'] ?>"/>
                            </div>
                            <div class="form-group">
                                <label class="sr-only"
                                       for="filter_login"><?php echo langline("users_user_login_col") ?></label>
                                <input type="search" name="filter[login]" class="form-control" id="filter_login"
                                       placeholder="<?php echo langline("users_user_login_col") ?>"
                                       value="<?php echo get("filter")['login'] ?>"/>
                            </div>

                            <button type="submit" class="btn btn-default"><?php echo langline("filter_btn") ?></button>
                            <?php echo form_close() ?>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                </div>
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                    <thead>
                    <tr>
                        <th class="table-checkbox">
                            <input type="checkbox" class="group-checkable" data-set=".dataTable .checkboxes"/></th>
                        <th><?php echo langline("users_user_avatar_col") ?></th>
                        <th><?php echo langline("users_user_name_col") ?></th>
                        <th><?php echo langline("users_user_email_col") ?></th>
                        <th><?php echo langline("users_user_login_col") ?></th>

                        <th><?php echo langline("users_user_created_col") ?></th>
                        <th width="150">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if ($data): foreach ($data as $row): ?>

                        <tr class="odd gradeX">
                            <td>
                                <input type="checkbox" class="checkboxes" value="<?php echo $row->user_id ?>"
                                       name="item_id[]"/>
                            </td>
                            <td><img class="thumbnail"
                                     src="<?php echo $row->user_avatar ? $upload_path . "/" . $row->user_avatar : "http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=no+image" ?>"
                                     style="width:100px;height:100px"/></td>
                            <td><?php echo $row->user_name ?></td>
                            <td><?php echo $row->user_email ?></td>
                            <td><?php echo $row->user_username ?></td>
                            <td class="center"><?php echo date("Y-m-d", strtotime($row->user_created)) ?></td>
                            <td class="text-center"><?php if (check_user_permission("log", "show")): ?>
                                <a href="<?php echo admin_url("log/user/" . $row->user_id) ?>"
                                       class="btn btn-default"  data-toggle="tooltip" data-placement="top" title="<?php echo langline("users_user_history") ?>"><i
                                            class="icon-file-text"></i></a>

                            <?php endif;
                            if (check_user_permission(current_module(), "edit")): ?>
                                <a href="<?php echo admin_url("users/edit/" . $row->user_id) ?>"
                                       class="btn btn-default"  data-toggle="tooltip" data-placement="top" title="<?php echo langline("link_edit") ?>"><i
                                            class="glyphicon glyphicon-edit"></i></a>

                            <?php endif;
                            if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")): ?>

                                    <a href="<?php echo admin_url("users/delete/" . $row->user_id) ?>"
                                       class="btn btn-danger confirm_delete"  data-toggle="tooltip" data-placement="top" title="<?php echo langline("link_delete") ?>"><i
                                            class="glyphicon glyphicon-remove"></i>
                                    </a>

                            <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach;
                    else: ?>

                    <?php  endif; ?>
                    </tbody>
                </table>

            </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<!-- END PAGE CONTENT-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/app.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/table-managed.js"></script>

