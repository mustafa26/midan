<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/select2/select2_metro_rtl.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/css/multi-select-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro-rtl.css"
      rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/jquery-tags-input/jquery.tagsinput-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title");?>
            <small><?php echo module_info("module_description");?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">

            <li>
                <i class="icon-home"></i>
                <a href="index.html"><?php echo langline("link_dashboard")?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("users") ?>"><?php echo langline("users_link") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li><a href="#"><?php echo $data->user_name?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box red form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("users_page_header") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>
        
        <?php
        $_action = "";
        $_attributes = array(
            "id" => "dataForm",
            "method" => "post", 
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data");
        echo form_open($_action, $_attributes);
        ?>  
         
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-md-2 control-label"><?php echo langline("users_user_name") ?></label>

                    <div class="col-md-7">
                        <p><?php echo input_value("user_name",$data->user_name) ?></p>
                    </div>
                </div>

                <div class="form-group">

                    <label class="col-md-2 control-label"><?php echo langline("users_user_email") ?></label>

                    <div class="col-md-7">
                        <p><?php echo input_value("user_email",$data->user_email) ?></p>

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label"><?php echo langline("users_user_phone") ?></label>

                    <div class="col-md-7">
                        <p><?php echo input_value("user_phone",$data->user_phone) ?></p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?php echo langline("users_user_groups") ?></label>

                    <div class="col-md-7">

                        <?php if($groups_id=explode(",",$data->groups_id)): $groups_name=explode(",",$data->groups_name); for($i=0;$i<count($groups_id);$i++) :?>
                            <b><?php echo $groups_name[$i]?></b><br/>
                        <?php endfor; endif;?>
                    </div>
                </div>

                <?php if($data->user_about):?>
                    <div class="form-group">

                        <label class="col-md-2 control-label"><?php echo langline("users_user_about") ?></label>

                        <div class="col-md-7">
                            <div class="well"><?php echo $data->user_about;?></div>
                        </div>
                    </div>
                <?php endif;?>


            </div>
            <div class="col-md-4">
                <div class="form-group last">
                    <label class="control-label col-md-2"><?php echo langline("users_user_avatar") ?></label>

                    <div class="col-md-8">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <?php if ($data->user_avatar): ?>
                                    <img src="<?php echo $upload_path . "/" . $data->user_avatar ?>" alt="" style="width: 200px; height: 150px;"/>
                                <?php else:?>
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

                <?php echo form_close(); ?>
        <div class="clearfix"></div>
    </div>
</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js"
        type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"
        type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"
        type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"
        type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/form-components.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/uniform/jquery.uniform.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {


        FormComponents.init();

        if (typeof CKEDITOR !== "undefined") {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    });
</script>