<!DOCTYPE html> 
<!--[if IE 8]>
<html lang="<?php echo $user_lang ?>" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="<?php echo $user_lang ?>" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $user_lang ?>" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <meta charset="utf-8"/>
        <title><?php echo $title ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="Ahmed Fathi" name="author"/>
        <meta name="MobileOptimized" content="320">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo $themepath ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
              type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet"
              type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet"
              type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css"
              href="<?php echo $themepath ?>/assets/plugins/select2/select2_metro_rtl.css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo $themepath ?>/assets/css/style-metronic-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/style-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/style-responsive-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/plugins-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/themes/default-rtl.css" rel="stylesheet" type="text/css"
              id="style_color"/>
        <link href="<?php echo $themepath ?>/assets/css/pages/login-soft-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
        <?php echo $scripts ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo"> 
            <img src="<?php echo $themepath ?>/assets/img/logo-big.png" alt=""/>
            <br/>
            <h3 style="text-shadow: 1px 1px 2px #999;"><?php echo $site->site_name ?></h3>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->

            <?php
            $_action = "";
            $_attributes = array(
                "id" => "dataForm",
                "method" => "post",
                "class" => "login-form" );
            echo form_open($_action, $_attributes);
            ?>   
                <h3 class="form-title text-center"><?php echo langline("login_page_header") ?></h3>

                <?php echo theme_messages() ?>
                <div class="form-group">
                    <?php if (config_item("admin.login_type") == "username"): ?>
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                        <label class="control-label visible-ie8 visible-ie9"><?php echo langline("users_login_name") ?></label>
                        <div class="input-icon">
                            <i class="icon-user"></i>
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off"
                                   placeholder="<?php echo langline("users_login_name") ?>" name="username" value="<?php echo input_value("username") ?>"/>
                        </div>
                    <?php else: ?>
                        <label class="control-label visible-ie8 visible-ie9"><?php echo langline("users_email") ?></label>
                        <div class="input-icon">
                            <i class="icon-user"></i>
                            <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="<?php echo langline("users_email") ?>"
                                   name="email"  value="<?php echo input_value("email") ?>"/>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9"><?php echo langline("users_user_password") ?></label>
                    <div class="input-icon">
                        <i class="icon-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo langline("users_user_password") ?>"
                               name="password"/>
                    </div>
                </div>
                <div class="form-actions">
                    <label class="checkbox">
                        <input type="checkbox" name="remember" value="1"/> <?php echo langline("label_remember_me") ?>
                    </label>
                    <button type="submit" class="btn blue pull-right">
                        <?php echo langline("btn_login") ?> <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                </div> 
                <?php echo form_close(); ?>
            <!-- END LOGIN FORM --> 
        </div> 
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <!--<div class="copyright">
            2013 &copy; Metronic - Admin Dashboard Template.
        </div>-->
        <!-- END COPYRIGHT -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?php echo $themepath ?>/assets/plugins/respond.min.js"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/excanvas.min.js"></script>
        <![endif]-->
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-validation/dist/jquery.validate.min.js"
        type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/backstretch/jquery.backstretch.min.js"
        type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo $themepath ?>/assets/scripts/app.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/scripts/login-soft.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                App.init();
                Login.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>