<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title");?>
            <small><?php echo module_info("module_description");?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb"> 
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url()?>">Home</a>
                <i class="icon-angle-right"></i>
            </li>

            <li>
                <a href="<?php echo admin_url("permissions") ?>"><?php echo langline("link_permissions") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li><a href="#"><?php echo langline("list_all")?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">

                <div class="caption"><i class="icon-globe"></i><?php echo langline("perm_page_header_title")?></div>

                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>

            </div>
            <div class="portlet-body">
        <?php
        $_action = "";
        $_attributes = array(
            "id" => "permissions_form",
            "method" => "post", 
            "class" => "form-horizontal",
            "role" => "form");
        echo form_open($_action, $_attributes);
        ?>  
                <div class="table-toolbar">
                    <div class="btn-group">
                        <button  type="submit" id="add_new_row" class="btn green">
                            <?php echo langline("btn_save")?> <i class="icon-save"></i></button>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="form-group">
                            <?php if($groups):?>
                            <label class="col-md-5 control-label"><?php echo langline("select_group")?></label>
                            <div class="col-md-7">
                                <select name="group" form="permissions_form" class="form-control input-lg" onchange="window.location.href='<?php echo admin_url("permissions")?>/'+$(this).val()">
                                    <option value="">-- <?php echo langline("select_group")?> --</option>
                                    <?php foreach($groups as $g):?>
                                        <option value="<?php echo $g->group_id?>" <?php echo segment(3)==$g->group_id?"selected":""?>><?php echo $g->group_name?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>


                        <?php endif;?>
                    </div>
                </div>
                <?php echo theme_messages() ?>

                    <div class="table-responsive">
                    <table class="table table-bordered table-hover " id="sample_1">
                        <tbody>
                        <?php if ($data): foreach ($data as $key => $row):
                            $sub_modules = permission_sub_modules($group, $row->module_id) ?>

                            <tr class="odd gradeX parent-row ">
                                <td><?php echo _t($row->module_title, $user_lang) ?></td>
	                            <td width="100">
		                            <label>
			                            <input type="checkbox" class=" select_childes"
			                                   id="perm_supervision_action_<?php echo $key ?>" value="1"
			                                   name="perm_supervision_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_supervision == 1 ? "checked" : "" ?>/>
			                            <?php echo langline("perm_supervision_action") ?>
		                            </label>
	                            </td>
                                <td width="100">
                                    <label>
                                        <input type="checkbox" class=" select_childes"
                                               id="perm_show_action_<?php echo $key ?>" value="1"
                                               name="perm_show_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_show==1?"checked":""?>/>
                                        <?php echo langline("perm_show_action") ?>
                                    </label>
                                </td>
                                <td width="100">
                                    <label>
                                        <input type="checkbox" class=" select_childes"
                                               id="perm_add_action_<?php echo $key ?>" value="1"
                                               name="perm_add_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_add==1?"checked":""?>/>
                                        <?php echo langline("perm_add_action") ?>
                                    </label>
                                </td>
                                <td width="100">
                                    <label>
                                        <input type="checkbox" class=" select_childes"
                                               id="perm_edit_action_<?php echo $key ?>" value="1"
                                               name="perm_edit_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_edit==1?"checked":""?>/>
                                        <?php echo langline("perm_edit_action") ?>
                                    </label>
                                </td>
                                <td width="100">
                                    <label>
                                        <input type="checkbox" class=" select_childes"
                                               id="perm_delete_action_<?php echo $key ?>" value="1"
                                               name="perm_delete_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_delete==1?"checked":""?>/>
                                        <?php echo langline("perm_delete_action") ?>
                                    </label>
                                </td>
                            </tr>
                            <?php if ($sub_modules): foreach ($sub_modules as $k => $item): ?>
                                <tr class="odd gradeX child-row">
                                    <td><?php echo _t($item->module_title, $user_lang) ?></td>
	                                <td width="100">
		                                <label>
			                                <input type="checkbox"
			                                       class=" perm_supervision_action_<?php echo $key ?>" value="1"
			                                       name="perm_supervision_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_supervision == 1 ? "checked" : "" ?>/>
			                                <?php echo langline("perm_supervision_action") ?>
		                                </label>
	                                </td>
                                    <td width="100">
                                        <label>
                                            <input type="checkbox" class=" perm_show_action_<?php echo $key ?>" value="1"
                                                   name="perm_show_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_show==1?"checked":""?>/>
                                            <?php echo langline("perm_show_action") ?>
                                        </label>
                                    </td>
                                    <td width="100">
                                        <label>
                                            <input type="checkbox" class=" perm_add_action_<?php echo $key ?>" value="1"
                                                   name="perm_add_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_add==1?"checked":""?>/>
                                            <?php echo langline("perm_add_action") ?>
                                        </label>
                                    </td>
                                    <td width="100">
                                        <label>
                                            <input type="checkbox" class=" perm_edit_action_<?php echo $key ?>" value="1"
                                                   name="perm_edit_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_edit==1?"checked":""?>/>
                                            <?php echo langline("perm_edit_action") ?>
                                        </label>
                                    </td>
                                    <td width="100">
                                        <label>
                                            <input type="checkbox" class=" perm_delete_action_<?php echo $key ?>" value="1"
                                                   name="perm_delete_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_delete==1?"checked":""?>/>
                                            <?php echo langline("perm_delete_action") ?>
                                        </label>
                                    </td>
                                </tr>
                            <?php endforeach;endif; ?>
                        <?php endforeach;
                        else: ?>

                        <?php  endif; ?>
                        </tbody>
                    </table>
                        </div>
                    <?php echo form_close() ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<!-- END PAGE CONTENT-->
<style>
    tr.parent-row {

    }

    tr.parent-row td:first-child {
        font-weight: bold;
    }

    tr.child-row {

    }

    tr.child-row td:first-child {
        padding-right: 40px;
    }

    tr.child-row td:first-child:before {
        content: " - ";
    }
</style>
<script>

    $(document).ready(function () {

        $(".select_childes").click(function (e) {
            var id = $(this).attr("id");
            select_all(this, id);
        });
        var select_all = function (element, el_id) {
            if (element.checked) { // check select status
                $('.' + el_id).each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
                $.uniform.update();
            } else {
                $('.' + el_id).each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
                $.uniform.update();
            }
        }
    })
</script>