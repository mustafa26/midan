<link href="<?php echo $themepath?>/assets/css/pages/error-rtl.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title");?>
            <small><?php echo module_info("module_description");?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">


            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url()?>"><?php echo langline("link_dashboard")?></a>
                <i class="icon-angle-right"></i>
            </li>

            <li><a href="#"><?php echo langline("perm_denied_link")?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">

    <div class="col-md-12 page-500">
        <div class=" number">
            403
        </div>
        <div class=" details">
            <?php echo langline("perm_denied_message")?>
        </div>
    </div>

</div>

