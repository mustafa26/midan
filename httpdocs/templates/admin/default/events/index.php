<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption">
                    <i class="icon-angle-left"></i>
                    <a href="<?php echo admin_url("events") ?>"><?php echo langline("events_link") ?></a> &nbsp;
                </div>
                <?php if (check_user_permission(current_module(), "add")): ?>
                    <div class="btn-group pull-right">
                        <a href="<?php echo admin_url("events/add/" . segment(4)) ?>" id="add_new_row"
                           class="btn green">
                            <?php echo langline("link_add_new") ?> <i class="icon-plus"></i></a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="portlet-body">
                <?php echo theme_messages(); ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                        <tr>
                            <!--<th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set=".dataTable .checkboxes"/></th>-->
                            <th><?php echo langline("cats_name") ?></th>
                            <th><?php echo langline("event_HeaderPhoto") ?></th>
                            <th><?php echo langline("project_status") ?></th>
                            <th><?php echo langline("Label_Date") ?></th>
                            <th><?php echo langline("event_Year") ?></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th width="150">&nbsp;</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($data): foreach ($data as $row): ?>
                            <tr class="odd gradeX">
                                <td><?php echo _t($row->event_Title, 'ar'); ?></td>
                                <td>
                                    <img
                                        src="<?php echo $row->event_HeaderPhoto ? $upload_path . '/events/' . $row->event_HeaderPhoto : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                                        width="150"/>
                                </td>
                                <td>
                                    <?php echo $row->event_Active == 0 ? "<span class='label label-danger'>" . langline("status_inactive") . "</span>" : "<span class='label label-success'>" . langline("status_active") . "</span>" ?>
                                </td>
                                <td><?php echo $row->event_From; ?></td>
                                <td><span class='label label-warning'><?php echo $row->event_Year; ?></span></td>
                                <td>
                                    <?php if ($row->event_Year == date("Y")) { ?>
                                        <a class="btn default btn-xs green-stripe"
                                           href="<?php echo admin_url("requests/index/" . $row->event_ID) ?>">
                                            <?php echo langline("View_Orders") ?>
                                        </a>
                                        <br>
                                        <a class="btn default btn-xs green-stripe" href="<?php echo admin_url("events/toexcel/" . $row->event_ID) ?>" target="_blank">
                                            To Excel File
                                        </a>
<!--                                        <br>-->
<!--                                        <a class="btn default btn-xs green-stripe" href="--><?php //echo admin_url("events/toword/" . $row->event_ID) ?><!--" target="_blank">To Word File</a>-->

                                        <a class="btn default btn-xs green-stripe" href="<?php echo admin_url("events/toprint/" . $row->event_ID) ?>" target="_blank">
                                            Print
                                        </a>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if ($row->event_Year == date("Y")) { ?>
                                        <a class="btn default btn-xs green-stripe"
                                           href="<?php echo admin_url("races/index/" . $row->event_ID) ?>">
                                            <?php echo langline("event_Races") ?> (<?php echo $row->total_races;?>)
                                        </a>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <?php
                                    if (check_user_permission(current_module(), "edit")):
                                        ?>
                                        <a href="<?php echo admin_url("events/edit/" . $row->event_ID) ?>"
                                           class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                           title="<?php echo langline("link_edit"); ?>">
                                            <i class="glyphicon glyphicon-edit"></i>
                                        </a>
                                    <?php
                                    endif;
                                    if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                                        ?>
                                        <a href="<?php echo admin_url("events/delete/" . $row->event_ID) ?>"
                                           class="btn btn-danger confirm_delete" data-toggle="tooltip"
                                           data-placement="top" title="<?php echo langline("link_delete"); ?>">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <nav><?php echo $pagination ?></nav>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>