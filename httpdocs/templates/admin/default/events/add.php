<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="event-title">
            <?php echo module_info("module_title"); ?>
            <small><?php echo module_info("module_description"); ?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-left"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("events") ?>"><?php echo langline("events_link") ?></a>
                <i class="icon-angle-left"></i>
            </li>   
            <li><a href="#"><?php echo langline("link_add_new") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box red form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("projects_event_header") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>
        <form id="dataForm" action="" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="tabbable-custom tabs-below ">
                <div class="tab-content">
                    <?php
                    $i = 1;
                    foreach ($languages as $key => $value):
                        ?>  
                        <div class="tab-pane <?php echo $i == 1 ? 'active' : '' ?>" id="tab_<?php echo $key; ?>">
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo langline("event_Title") ?></label> 
                                <div class="col-md-6"> 
                                    <input type="text" name="event_Title[<?php echo $key; ?>]" class="form-control input-lg"
                                           placeholder="<?php echo langline("event_Title") ?>"
                                           value="<?php echo input_value("event_Title[$key]"); ?>"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo langline("event_SubTitle") ?></label> 
                                <div class="col-md-6"> 
                                    <input type="text" name="event_SubTitle[<?php echo $key; ?>]" class="form-control input-lg"
                                           placeholder="<?php echo langline("event_SubTitle") ?>"
                                           value="<?php echo input_value("event_SubTitle[$key]"); ?>"> 
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo langline("event_Content") ?></label>
                                <div class="col-md-7">
                                    <textarea name="event_Content[<?php echo $key; ?>]" class="ckeditor form-control" rows="10">
                                        <?php echo input_value("event_Content[$key]"); ?>
                                    </textarea>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo langline("event_Gifts") ?></label>
                                <div class="col-md-7">
                                    <textarea name="event_Gifts[<?php echo $key; ?>]" class="ckeditor form-control" rows="10">
                                        <?php echo input_value("event_Gifts[$key]"); ?>
                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo langline("event_MetaDescr") ?></label> 
                                <div class="col-md-6">
                                    <textarea class="form-control" name="event_MetaDescr[<?php echo $key; ?>]" placeholder="<?php echo langline("event_MetaDescr"); ?>" rows="5"><?php echo input_value("event_MetaDescr[$key]"); ?></textarea> 
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo langline("event_MetaKeywords") ?></label> 
                                <div class="col-md-6">
                                    <textarea class="form-control" name="event_MetaKeywords[<?php echo $key; ?>]" placeholder="<?php echo langline("event_MetaKeywords"); ?>" rows="5"><?php echo input_value("event_MetaKeywords[$key]"); ?></textarea> 
                                </div>
                            </div>
                        </div>  
                        <?php
                        $i++;
                    endforeach;
                    ?> 
                    <div class="tab-pane" id="tab_settings">  
                        <div class="form-group last">
                            <label class="control-label col-md-2"><?php echo langline("project_attaches") ?></label>
                            <div class="col-md-8">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail"
                                         style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" style="max-height: 150px;">
                                    </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileupload-new"><i
                                                    class="icon-paper-clip"></i> <?php echo langline("btn_select_image") ?></span>
                                            <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                            <input type="file" class="default" name="event_HeaderPhoto"
                                                   value="<?php echo input_value("event_HeaderPhoto") ?>"/>
                                        </span>
                                        <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i
                                                class="icon-trash"></i> Remove</a>
                                    </div>
                                </div>
                                <span class="label label-danger">NOTE!</span>
                                <span>
                                    Attached image thumbnail is supported in Latest Firefox,
                                    Chrome, Opera, Safari and Internet Explorer 10 only
                                </span>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("task_duration") ?></label>
                            <div class="col-md-7">
                                <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012"
                                     data-date-start-view="10/11/2015" data-date-format="yyyy-mm-dd">
                                    <input class="form-control" name="event_From" type="text"
                                           value="<?php echo input_value("event_From")?input_value("event_From"):date("Y-m-d") ?>">
                                    <span class="input-group-addon"><?php echo langline("Lable_To") ?></span>
                                    <input class="form-control" name="event_To" type="text"
                                           value="<?php echo input_value("event_To")?input_value("event_To"):date("Y-m-d") ?>">
                                </div>
                                <!-- /input-group -->
                                <span class="help-block"><?php echo langline("select_date_range") ?></span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>
                            <div class="col-md-7">
                                <div class="i-checkbox">
                                    <input type="checkbox" name="event_Active" class="iButton" value="1" checked />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("event_RaceNum") ?></label>
                            <div class="col-md-7">
                                <input type="number" class="form-control input-small" placeholder="0" name="event_RaceNum" id="event_RaceNum">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("rqst_AgeGroup") ?></label>
                            <div class="col-md-7">
                                <select name="event_AgeGroup" id="event_AgeGroup" class="form-control input-small">
                                    <option value="2"><?php echo langline("rqst_AgeGroup_2"); ?></option>
                                    <option value="3"><?php echo langline("rqst_AgeGroup_3"); ?></option>
                                    <option value="4"><?php echo langline("rqst_AgeGroup_4"); ?></option>
                                    <option value="5"><?php echo langline("rqst_AgeGroup_5"); ?></option>
                                    <option value="6"><?php echo langline("rqst_AgeGroup_6"); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("event_Year") ?></label>
                            <div class="col-md-7">
                                <select name="event_Year" id="event_Year" class="form-control input-small">
                                    <?php for($i = date("Y");$i>=2017;$i--){?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                    </div>  
                </div>
                <ul class="nav nav-tabs">
                    <?php
                    $i = 1;
                    foreach ($languages as $key => $value):
                        ?>                      
                        <li <?php echo $i == 1 ? 'class="active"' : '' ?>>
                            <a href="#tab_<?php echo $key; ?>" data-toggle="tab"><?php echo langline("language_tab_$value"); ?></a>
                        </li>  
                        <?php
                        $i++;
                    endforeach;
                    ?>                    
                    <li>
                        <a href="#tab_settings" data-toggle="tab"><?php echo langline("Tab_Settings"); ?></a>
                    </li>  
                </ul> 
            </div>   
        </form>
        <div class="clearfix"></div>
    </div>
</div>   
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js"
        type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"
        type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"
        type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"
        type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/form-components.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/uniform/jquery.uniform.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        FormComponents.init();
        if (typeof CKEDITOR !== "undefined") {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    });
</script>

