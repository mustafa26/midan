<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/select2/select2_metro_rtl.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/css/multi-select-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro-rtl.css"
      rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/jquery-tags-input/jquery.tagsinput-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title"); ?>
            <small><?php echo module_info("module_description"); ?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>

            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("groups") ?>"><?php echo langline("groups_link") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li><a href="#">List</a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box red form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("groups_page_header") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>
        
        <?php
        $_action = "";
        $_attributes = array(
            "id" => "dataForm",
            "method" => "post",
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data");
        echo form_open($_action, $_attributes);
        ?>  
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("groups_group_name") ?></label>

                <div class="col-md-7">
                    <input type="text" name="group_name" class="form-control input-lg"
                           placeholder="<?php echo langline("groups_group_name") ?>"
                           value="<?php echo input_value("groups_group_name") ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("groups_group_description") ?></label>

                <div class="col-md-7">
                    <textarea name="group_description" class="form-control"
                              rows="5"><?php echo input_value("group_description") ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("groups_group_in_admin") ?></label>

                <div class="col-md-7">
                    <div class="i-checkbox">
                        <input type="checkbox" name="in_admin" class="iButton"
                               value="1" <?php echo input_value("in_admin") == 1 ? "checked" : "" ?>/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2"><?php echo langline("groups_permissions") ?></label>

                <div class="col-md-10">
                    <label><input type="checkbox" id="full_permissions"
                                  class="select_childes"/> <?php echo langline("full_permissions") ?></label>
                    <br/>
                    <table class="table table-bordered table-hover " id="sample_1">
                        <tbody>
                            <?php
                            if ($permissions): foreach ($permissions as $key => $row):
                                    $sub_modules = permission_sub_modules(0, $row->module_id)
                                    ?>

                                    <tr class="odd gradeX parent-row ">
                                        <td><?php echo _t($row->module_title, $user_lang) ?></td>
                                        <td width="100">
                                            <label>
                                                <input type="checkbox" class=" select_childes"
                                                       id="perm_supervision_action_<?php echo $key ?>" value="1"
                                                       name="perm_supervision_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_supervision == 1 ? "checked" : "" ?>/>
        <?php echo langline("perm_supervision_action") ?>
                                            </label>
                                        </td>
                                        <td width="100">
                                            <label>
                                                <input type="checkbox" class="full_permissions select_childes"
                                                       id="perm_show_action_<?php echo $key ?>" value="1"
                                                       name="perm_show_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_show == 1 ? "checked" : "" ?>/>
        <?php echo langline("perm_show_action") ?>
                                            </label>
                                        </td>
                                        <td width="100">
                                            <label>
                                                <input type="checkbox" class="full_permissions select_childes"
                                                       id="perm_add_action_<?php echo $key ?>" value="1"
                                                       name="perm_add_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_add == 1 ? "checked" : "" ?>/>
        <?php echo langline("perm_add_action") ?>
                                            </label>
                                        </td>
                                        <td width="100">
                                            <label>
                                                <input type="checkbox" class="full_permissions select_childes"
                                                       id="perm_edit_action_<?php echo $key ?>" value="1"
                                                       name="perm_edit_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_edit == 1 ? "checked" : "" ?>/>
        <?php echo langline("perm_edit_action") ?>
                                            </label>
                                        </td>
                                        <td width="100">
                                            <label>
                                                <input type="checkbox" class="full_permissions select_childes"
                                                       id="perm_delete_action_<?php echo $key ?>" value="1"
                                                       name="perm_delete_action[<?php echo $row->module_id ?>]" <?php echo $row->permission_delete == 1 ? "checked" : "" ?>/>
        <?php echo langline("perm_delete_action") ?>
                                            </label>
                                        </td>
                                    </tr>
        <?php if ($sub_modules): foreach ($sub_modules as $k => $item): ?>
                                            <tr class="odd gradeX child-row">
                                                <td><?php echo _t($item->module_title, $user_lang) ?></td>

                                                <td width="100">
                                                    <label>
                                                        <input type="checkbox"
                                                               class=" perm_supervision_action_<?php echo $key ?>" value="1"
                                                               name="perm_supervision_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_supervision == 1 ? "checked" : "" ?>/>
                <?php echo langline("perm_supervision_action") ?>
                                                    </label>
                                                </td>
                                                <td width="100">
                                                    <label>
                                                        <input type="checkbox"
                                                               class="full_permissions perm_show_action_<?php echo $key ?>" value="1"
                                                               name="perm_show_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_show == 1 ? "checked" : "" ?>/>
                <?php echo langline("perm_show_action") ?>
                                                    </label>
                                                </td>
                                                <td width="100">
                                                    <label>
                                                        <input type="checkbox"
                                                               class="full_permissions perm_add_action_<?php echo $key ?>" value="1"
                                                               name="perm_add_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_add == 1 ? "checked" : "" ?>/>
                <?php echo langline("perm_add_action") ?>
                                                    </label>
                                                </td>
                                                <td width="100">
                                                    <label>
                                                        <input type="checkbox"
                                                               class="full_permissions perm_edit_action_<?php echo $key ?>" value="1"
                                                               name="perm_edit_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_edit == 1 ? "checked" : "" ?>/>
                <?php echo langline("perm_edit_action") ?>
                                                    </label>
                                                </td>
                                                <td width="100">
                                                    <label>
                                                        <input type="checkbox"
                                                               class="full_permissions perm_delete_action_<?php echo $key ?>" value="1"
                                                               name="perm_delete_action[<?php echo $item->module_id ?>]" <?php echo $item->permission_delete == 1 ? "checked" : "" ?>/>
                <?php echo langline("perm_delete_action") ?>
                                                    </label>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                    endif;
                                    ?>
                                <?php
                                endforeach;
                            else:
                                ?>

<?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

        <?php echo form_close() ?>
    </div>
</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js"
type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"
type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"
type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"
type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"
type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/form-components.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/uniform/jquery.uniform.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {


        FormComponents.init();
        if (typeof CKEDITOR !== "undefined") {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    });
</script>

<!-- END PAGE CONTENT-->
<style>
    tr.parent-row {

    }

    tr.parent-row td:first-child {
        font-weight: bold;
    }

    tr.child-row {

    }

    tr.child-row td:first-child {
        padding-right: 40px;
    }

    tr.child-row td:first-child:before {
        content: " - ";
    }
</style>
<script>

    $(document).ready(function () {

        $(".select_childes").click(function (e) {
            var id = $(this).attr("id");
            select_all(this, id);
        });
        var select_all = function (element, el_id) {
            if (element.checked) { // check select status
                $('.' + el_id).each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
                $.uniform.update();
            } else {
                $('.' + el_id).each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
                $.uniform.update();
            }
        }
    })
</script>