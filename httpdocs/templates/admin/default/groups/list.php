<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title"); ?>
            <small><?php echo module_info("module_description"); ?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb"> 
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-right"></i>
            </li>

            <li>
                <a href="<?php echo admin_url("groups") ?>"><?php echo langline("link_groups") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li><a href="#"><?php echo langline("list_all") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title"> 
                <div class="caption"><i class="icon-globe"></i><?php echo langline("groups_page_title") ?></div>

                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                </div> 
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <?php if (check_user_permission(current_module(), "add")): ?>
                        <div class="btn-group">
                            <a href="<?php echo admin_url("groups/add") ?>" id="add_new_row" class="btn green">
                                <?php echo langline("link_add_new") ?><i class="icon-plus"></i></a>
                        </div>
                    <?php endif; ?> 
                </div>
                <?php echo theme_messages() ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr>
                                <th class="table-checkbox">
                                    <input type="checkbox" class="group-checkable" data-set=".dataTable .checkboxes"/></th>
                                <th><?php echo langline("groups_group_name") ?></th>
                                <th><?php echo langline("groups_group_in_admin") ?></th>
                                <th><?php echo langline("groups_super_group") ?></th>
                                <th><?php echo langline("groups_group_members") ?></th>
                                <th><?php echo langline("groups_created_at") ?></th>
                                <th><?php echo langline("groups_last_update") ?></th>
                                <th colspan="2">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data): foreach ($data as $row): ?>

                                    <tr class="odd gradeX">
                                        <td>
                                            <input type="checkbox" class="checkboxes" value="<?php echo $row->group_id ?>" name="item_id[]"/>
                                        </td>
                                        <td><?php echo $row->group_name ?></td>
                                        <td><?php echo $row->group_inAdmin ? "<span class='label label-danger'>" . langline("text_yeas") . "</span>" : "<span class='label label-success'>" . langline("text_no") . "</span>" ?></td>
                                        <?php if (check_user_permission(current_module(), "edit")): ?>
                                            <td>
                                                <input type="radio" name="is_super"  value="<?php echo $row->group_id ?>" class="super_group" <?php echo $row->group_isSuper == 1 ? "checked" : "" ?> />
                                            </td>
                                        <?php else: ?>
                                            <td><?php echo $row->group_isSuper ? "<span class='label label-danger'>" . langline("text_yeas") . "</span>" : "<span class='label label-success'>" . langline("text_no") . "</span>" ?></td>
                                        <?php endif; ?>
                                        <td>
                                            <?php if ($row->members_num): ?>
                                                <a href="<?php echo admin_url("users") . "?group=" . $row->group_id ?>"><?php echo sprintf(langline("groups_members_count"), $row->members_num) ?></a>
                                            <?php else: ?>
                                                <a href="<?php echo admin_url("users") . "?group=" . $row->group_id ?>"> <span><?php echo langline("groups_no_members") ?></span></a>

                                            <?php endif; ?>
                                        </td>
                                        <td class="center"><?php echo date("Y-m-d", strtotime($row->group_created)) ?></td>
                                        <td class="center"><?php echo date("Y-m-d h:i:s A", strtotime($row->group_updated)) ?></td>
                                        <?php if (check_user_permission(current_module(), "edit")): ?>
                                            <td>
                                                <a href="<?php echo $admin_base ?>groups/edit/<?php echo $row->group_id ?>" class="btn btn-default"><i
                                                        class="glyphicon glyphicon-edit"></i><?php echo langline("link_edit") ?></a>
                                            </td>
                                        <?php endif; ?>
                                        <?php if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")): ?>
                                            <td>
                                                <a href="<?php echo $admin_base ?>groups/delete/<?php echo $row->group_id ?>" class="btn btn-danger confirm_delete"><i
                                                        class="glyphicon glyphicon-remove"></i><?php echo langline("link_delete") ?></a>
                                            </td>
                                        <?php endif; ?>
                                    </tr>

                                <?php endforeach;
                            endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<!-- END PAGE CONTENT-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/app.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/table-managed.js"></script>

<script>

    $(document).ready(function () {


        $(".super_group").iButton({
            duration: 200, // the speed of the animation
            easing: "swing",
            labelOn: "Yes", // the text to show when toggled on
            labelOff: "No",
            change: function (el) {

                $.getJSON("<?php echo $admin_base ?>groups/setSuperGroup/" + $(el).val());
            }

        }
        );

    });

</script>
