<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css">
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box light-grey">
<div class="portlet-title">
    <div class="caption">
        <i class="icon-home"></i>
        <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
    </div>
    <div class="caption">
        <i class="icon-angle-left"></i>
        <a href="<?php echo admin_url("events") ?>"><?php echo langline("event_link") ?></a> &nbsp;
    </div>
    <div class="caption">
        <i class="icon-angle-left"></i>
        <a href="<?php echo admin_url("races/index/" . segment(4)) ?>"><?php echo langline("races_link") ?></a> &nbsp;
    </div>
    <div class="caption">
        <i class="icon-angle-left"></i>
        <?php echo langline("list_all") ?> (<?php echo $total?>)
    </div>
</div>
<div class="portlet-body">
<?php echo theme_messages(); ?>
<?php //if (check_user_permission(current_module(), "add")): ?>
<div class="portlet box blue form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("link_add_new") ?></div>
        <div class="tools">
            <a href="javascript:;" <?php echo !get("open") ? 'class="expand"' : 'class="collapse"' ?>></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body" <?php echo !get("open") ? 'style="display: none;"' : '' ?>>
        <?php
        $_action = admin_url("races/add/" . segment(4));
        $_attributes = array(
            "method" => "post",
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data",
            "id" => "dataForm");
        echo form_open($_action, $_attributes);
        ?>
        <div class="col-md-10">
            <div class="form-group">
                <label class="col-md-2 control-label"> <?php echo langline("Sort") ?> </label>

                <div class="col-md-4">
                    <input type="text" name="rac_Sort" id="rac_Sort" class="form-control input-lg"
                           placeholder="<?php echo langline("Sort") ?>"
                           value="<?php echo input_value("rac_Sort") ?>">
                </div>
                <label class="col-md-2 control-label"><?php echo langline("races_Name") ?> </label>

                <div class="col-md-4">
                    <input type="text" name="rac_Name" id="rac_Name" class="form-control input-lg"
                           placeholder="<?php echo langline("races_Name") ?>"
                           value="<?php echo input_value("rac_Name") ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"> <?php echo langline("Age") ?> </label>

                <div class="col-md-4">
                    <input type="text" name="rac_Age" id="rac_Age" class="form-control input-lg"
                           placeholder="<?php echo langline("Age") ?>"
                           value="<?php echo input_value("rac_Age") ?>">
                </div>
                <label class="col-md-2 control-label"><?php echo langline("Gender") ?> </label>

                <div class="col-md-4">
                    <input type="text" name="rac_Gender" id="rac_Gender" class="form-control input-lg"
                           placeholder="<?php echo langline("Gender") ?>"
                           value="<?php echo input_value("rac_Gender") ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("Orders") ?> </label>

                <div class="col-md-8">
                    <div class="col-md-4">
                        <input type="text" name="rac_ord1" id="rac_ord1" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord1") ?>"
                               value="<?php echo input_value("rac_ord1") ?>">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="rac_ord2" id="rac_ord2" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord2") ?>"
                               value="<?php echo input_value("rac_ord2") ?>">
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="rac_ord3" id="rac_ord3" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord3") ?>"
                               value="<?php echo input_value("rac_ord3") ?>">
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="rac_ord4" id="rac_ord4" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord4") ?>"
                               value="<?php echo input_value("rac_ord4") ?>">
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="rac_ord5" id="rac_ord5" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord5") ?>"
                               value="<?php echo input_value("rac_ord5") ?>">
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="rac_ord6" id="rac_ord6" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord6") ?>"
                               value="<?php echo input_value("rac_ord6") ?>">
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="rac_ord7" id="rac_ord7" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord7") ?>"
                               value="<?php echo input_value("rac_ord7") ?>">
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="rac_ord8" id="rac_ord8" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord8") ?>"
                               value="<?php echo input_value("rac_ord8") ?>">
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="rac_ord9" id="rac_ord9" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord9") ?>"
                               value="<?php echo input_value("rac_ord9") ?>">
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="rac_ord10" id="rac_ord10" class="form-control input-lg"
                               placeholder="<?php echo langline("rac_ord10") ?>"
                               value="<?php echo input_value("rac_ord10") ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>

                <div class="col-md-7">
                    <div class="i-checkbox">
                        <input type="checkbox" name="rac_status" id="rac_status" class="iButton"
                               value="1" checked/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </div>
        </div>
        </form>
        <div class="clearfix"></div>
    </div>
</div>
<?php //endif; ?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo langline("races_Name") ?></th>
            <th><?php echo langline("Age") ?></th>
            <th><?php echo langline("Gender") ?></th>
            <th><?php echo langline("Orders") ?></th>
            <th><?php echo langline("project_status") ?></th>
            <th>اجمالي الطلبات</th>
            <th>&nbsp;</th>

        </tr>
        </thead>
        <tbody>
        <?php if ($data): foreach ($data as $row): ?>
            <tr class="odd gradeX">
                <td><?php echo $row->rac_Sort; ?></td>
                <td><?php echo $row->rac_Name; ?></td>
                <td><?php echo $row->rac_Age; ?></td>
                <td><?php echo $row->rac_Gender; ?></td>
                <td>
                    <table class="table table-striped table-bordered table-hover dataTable">
                        <tr>
                            <td><span class='label label-warning'><?php echo langline("rac_ord1") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord2") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord3") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord4") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord5") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord6") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord7") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord8") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord9") ?></span></td>
                            <td><span class='label label-warning'><?php echo langline("rac_ord10") ?></span></td>
                        </tr>
                        <tr>
                            <td><?php echo $row->rac_ord1; ?></td>
                            <td><?php echo $row->rac_ord2; ?></td>
                            <td><?php echo $row->rac_ord3; ?></td>
                            <td><?php echo $row->rac_ord4; ?></td>
                            <td><?php echo $row->rac_ord5; ?></td>
                            <td><?php echo $row->rac_ord6; ?></td>
                            <td><?php echo $row->rac_ord7; ?></td>
                            <td><?php echo $row->rac_ord8; ?></td>
                            <td><?php echo $row->rac_ord9; ?></td>
                            <td><?php echo $row->rac_ord10; ?></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <?php echo $row->rac_Status == 0 ? "<span class='label label-danger'>" . langline("status_inactive") . "</span>" : "<span class='label label-success'>" . langline("status_active") . "</span>" ?>
                </td>
                <td>
                    <a href="<?php echo admin_url("requests/lastlist?eventid=$row->rac_FID&rqst_NumRace=$row->rac_ID") ?>">
                    <?php echo $row->total; ?>
                </a></td>
                <td class="text-center">
                    <?php
                    // if (check_user_permission(current_module(), "edit")):
                    ?>
                    <a href="<?php echo admin_url("races/edit/" . $row->rac_ID) ?>"
                       class="btn btn-default" data-toggle="tooltip" data-placement="top"
                       title="<?php echo langline("link_edit"); ?>">
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    <?php
                    //endif;
                    //if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                    ?>
                    <a href="<?php echo admin_url("races/delete/$row->rac_FID/$row->rac_ID") ?>"
                       class="btn btn-danger confirm_delete" data-toggle="tooltip"
                       data-placement="top" title="<?php echo langline("link_delete"); ?>">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                    <?php //endif; ?>
                </td>
            </tr>
        <?php
        endforeach;
        else:
            ?>
            <tr>
                <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<nav><?php echo $pagination ?></nav>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>