<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css">
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption">
                    <i class="icon-angle-left"></i>
                    <a href="<?php echo admin_url("events") ?>"><?php echo langline("event_link") ?></a> &nbsp;
                </div>
                <div class="caption">
                    <i class="icon-angle-left"></i>
                    <a href="<?php echo admin_url("races/index/" . $data->rac_FID) ?>"><?php echo langline("races_link") ?></a> &nbsp;
                </div>
                <div class="caption">
                    <i class="icon-angle-left"></i>
                    <?php echo langline("link_edit") ?>
                </div>
            </div>
            <div class="portlet-body">
                    <div class="portlet box blue form">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?php echo langline("log_edit_catgory") ?>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <?php echo theme_messages(); ?>
                            <?php
                            $_action = admin_url("races/edit/" . segment(4));
                            $_attributes = array(
                                "method" => "post",
                                "class" => "form-horizontal",
                                "role" => "form",
                                "enctype" => "multipart/form-data",
                                "id" => "dataForm");
                            echo form_open($_action, $_attributes);
                            ?>
                            <div class="col-md-10">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"> <?php echo langline("Sort") ?> </label>

                                    <div class="col-md-4">
                                        <input type="text" name="rac_Sort" id="rac_Sort" class="form-control input-lg"
                                               placeholder="<?php echo langline("Sort") ?>"
                                               value="<?php echo $data->rac_Sort ?>">
                                    </div>
                                    <label class="col-md-2 control-label"><?php echo langline("races_Name") ?> </label>

                                    <div class="col-md-4">
                                        <input type="text" name="rac_Name" id="rac_Name" class="form-control input-lg"
                                               placeholder="<?php echo langline("races_Name") ?>"
                                               value="<?php echo $data->rac_Name ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"> <?php echo langline("Age") ?> </label>

                                    <div class="col-md-4">
                                        <input type="text" name="rac_Age" id="rac_Age" class="form-control input-lg"
                                               placeholder="<?php echo langline("Age") ?>"
                                               value="<?php echo $data->rac_Age ?>">
                                    </div>
                                    <label class="col-md-2 control-label"><?php echo langline("Gender") ?> </label>

                                    <div class="col-md-4">
                                        <input type="text" name="rac_Gender" id="rac_Gender"
                                               class="form-control input-lg"
                                               placeholder="<?php echo langline("Gender") ?>"
                                               value="<?php echo $data->rac_Gender ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("Orders") ?> </label>

                                    <div class="col-md-8">
                                        <div class="col-md-4">
                                            <input type="text" name="rac_ord1" id="rac_ord1"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord1") ?>"
                                                   value="<?php echo $data->rac_ord1 ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" name="rac_ord2" id="rac_ord2"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord2") ?>"
                                                   value="<?php echo $data->rac_ord2 ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="rac_ord3" id="rac_ord3"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord3") ?>"
                                                   value="<?php echo $data->rac_ord3 ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="rac_ord4" id="rac_ord4"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord4") ?>"
                                                   value="<?php echo $data->rac_ord4 ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="rac_ord5" id="rac_ord5"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord5") ?>"
                                                   value="<?php echo $data->rac_ord5 ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="rac_ord6" id="rac_ord6"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord6") ?>"
                                                   value="<?php echo $data->rac_ord6 ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="rac_ord7" id="rac_ord7"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord7") ?>"
                                                   value="<?php echo $data->rac_ord7 ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="rac_ord8" id="rac_ord8"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord8") ?>"
                                                   value="<?php echo $data->rac_ord8 ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="rac_ord9" id="rac_ord9"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord9") ?>"
                                                   value="<?php echo $data->rac_ord9 ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="rac_ord10" id="rac_ord10"
                                                   class="form-control input-lg"
                                                   placeholder="<?php echo langline("rac_ord10") ?>"
                                                   value="<?php echo $data->rac_ord10 ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label
                                        class="col-md-2 control-label"><?php echo langline("project_status") ?></label>

                                    <div class="col-md-7">
                                        <div class="i-checkbox">
                                            <input type="checkbox" name="rac_status" class="iButton"
                                                   value="1" <?php echo $data->rac_Status ? "checked" : "" ?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                                        <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="rac_FID" value="<?php echo $data->rac_FID ?>" >
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div> 
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>