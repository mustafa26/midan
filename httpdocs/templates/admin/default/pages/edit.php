<!-- BEGIN PAGE LEVEL STYLES --> 
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/select2/select2_metro_rtl.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/css/multi-select-rtl.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title"); ?>
            <small><?php echo module_info("module_description"); ?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("pages") ?>"><?php echo langline("pages_link") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li class="caption"> 
                <a href="<?php echo admin_url("pages/cats/" . segment(4)) ?>"><?php echo _t($cats->cat_name, $user_lang) ?> </a>
                <i class="icon-angle-right"></i> 
            </li> 
            <li><a href="#"><?php echo langline("link_edit_page") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box red form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("projects_page_header") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>

        <?php
        $_action = "";
        $_attributes = array(
            "id" => "dataForm",
            "method" => "post",
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data");
        echo form_open($_action, $_attributes);
        ?>  
        <div class="tabbable-custom tabs-below ">
            <div class="tab-content">
                <?php
                $i = 1;
                foreach ($languages as $key => $value):
                    ?>  
                    <div class="tab-pane <?php echo $i == 1 ? 'active' : '' ?>" id="tab_<?php echo $key; ?>">
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("page_Title") ?></label> 
                            <div class="col-md-6"> 
                                <input type="text" name="page_Title[<?php echo $key; ?>]" class="form-control input-lg"
                                       placeholder="<?php echo langline("page_Title") ?>"
                                       value="<?php echo _t($page->page_Title, $key); ?>"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("page_SubTitle") ?></label> 
                            <div class="col-md-6"> 
                                <input type="text" name="page_SubTitle[<?php echo $key; ?>]" class="form-control input-lg"
                                       placeholder="<?php echo langline("page_SubTitle") ?>"
                                       value="<?php echo _t($page->page_SubTitle, $key); ?>"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("page_Content") ?></label>
                            <div class="col-md-7">
                                <textarea name="page_Content[<?php echo $key; ?>]" class="ckeditor form-control" rows="10">
                                    <?php echo _t($page->page_Content, $key); ?>
                                </textarea>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("page_MetaDescr") ?></label> 
                            <div class="col-md-6">
                                <textarea class="form-control" name="page_MetaDescr[<?php echo $key; ?>]" placeholder="<?php echo langline("page_MetaDescr"); ?>" rows="5"><?php echo _t($page->page_MetaDescr, $key); ?></textarea> 
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("page_MetaKeywords") ?></label> 
                            <div class="col-md-6">
                                <textarea class="form-control" name="page_MetaKeywords[<?php echo $key; ?>]" placeholder="<?php echo langline("page_MetaKeywords"); ?>" rows="5"><?php echo _t($page->page_MetaKeywords, $key); ?></textarea> 
                            </div>
                        </div>
                    </div>  
                    <?php
                    $i++;
                endforeach;
                ?> 
                <div class="tab-pane" id="tab_settings">
                    <div class="form-group last">
                        <label class="control-label col-md-2"><?php echo langline("project_attaches") ?></label> 
                        <div class="col-md-8">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="<?php echo $page->page_HeaderPhoto ? $upload_path . '/pages/' . $page->page_HeaderPhoto : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                    <img src="<?php echo $upload_path . '/pages/' . $page->page_HeaderPhoto ?>" style="max-height: 150px;">
                                </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileupload-new"><i class="icon-paper-clip"></i> <?php echo langline("btn_select_image") ?></span>
                                        <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                        <input type="file" class="default" name="page_HeaderPhoto" value="<?php echo input_value("page_HeaderPhoto") ?>"/>
                                    </span>
                                    <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                </div>
                            </div>
                            <span class="label label-danger">NOTE!</span> 
                            <span>
                                Attached image thumbnail is supported in Latest Firefox, 
                                Chrome, Opera, Safari and Internet Explorer 10 only
                            </span> 
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>
                        <div class="col-md-7">
                            <div class="i-checkbox">
                                <input type="checkbox" name="page_Active" class="iButton" value="1" <?php echo $page->page_Active == 1 ? "checked" : "" ?>/>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <ul class="nav nav-tabs">
                <?php
                $i = 1;
                foreach ($languages as $key => $value):
                    ?>                      
                    <li <?php echo $i == 1 ? 'class="active"' : '' ?>>
                        <a href="#tab_<?php echo $key; ?>" data-toggle="tab"><?php echo langline("language_tab_$value"); ?></a>
                    </li> 
                    <?php
                    $i++;
                endforeach;
                ?>                    
                <li>
                    <a href="#tab_settings" data-toggle="tab"><?php echo langline("Tab_Settings"); ?></a>
                </li>  
            </ul> 
        </div>   
        <?php echo form_close(); ?>
        <div class="clearfix"></div>
    </div>
</div>  

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/form-components.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/uniform/jquery.uniform.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        FormComponents.init();
        if (typeof CKEDITOR !== "undefined") {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    });
</script>

