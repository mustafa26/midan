<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("pages") ?>"><?php echo langline("pages_link") ?></a> &nbsp;
                </div> 
            </div>
            <div class="portlet-body">  
                <?php if (check_user_permission(current_module(), "edit")): ?> 
                    <div class="portlet box blue form">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?php echo langline("log_edit_catgory") ?></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <?php echo theme_messages(); ?>  
                            <?php
                            $_action = admin_url("pages/edit/" . segment(4));
                            $_attributes = array(
                                "id" => "dataForm",
                                "method" => "post",
                                "class" => "form-horizontal",
                                "role" => "form",
                                "enctype" => "multipart/form-data");
                            echo form_open($_action, $_attributes);
                            ?>
                            <div class="tabbable-custom tabs-below ">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_2_1">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo langline("cats_name") ?> (عربي)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="cat_name[ar]" class="form-control input-lg"
                                                       placeholder="<?php echo langline("cats_name") ?>"
                                                       value="<?php echo _t($data->cat_name, "ar") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_2_2">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> <?php echo langline("cats_name") ?> (English)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="cat_name[en]" class="form-control input-lg" placeholder="<?php echo langline("cats_name") ?>"
                                                       value="<?php echo _t($data->cat_name, "en") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_2_1" data-toggle="tab"><?php echo langline("language_tab_arabic") ?></a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_2_2" data-toggle="tab"><?php echo langline("language_tab_english") ?></a>
                                    </li>
                                </ul> 
                            </div>
                            <div class="col-md-8"> 
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>
                                    <div class="col-md-7">    
                                        <div class="i-checkbox">
                                            <input type="checkbox" name="cat_status" class="iButton" value="1" <?php echo $data->cat_status ? "checked" : "" ?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">      
                                    <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                                        <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="catsID" id="catsID" value="<?php echo $data->cat_id ?>">
                            <?php echo form_close() ?>
                            <div class="clearfix"></div>
                        </div>
                    </div> 
                <?php endif; ?>  
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>   