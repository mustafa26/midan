<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title"); ?>
            <small><?php echo module_info("module_description"); ?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">

            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-right"></i>
            </li>

            <li>
                <a href="<?php admin_url("settings") ?>"><?php echo module_info("module_title"); ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li><a href="#"><?php echo langline("list_all") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">

                <div class="caption"><i class="icon-globe"></i><?php echo langline("settings_page_title") ?></div>

            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="btn-group">
                        <a href="<?php echo admin_url("settings/add") ?>" id="add_new_row" class="btn green">
                            <?php echo langline("link_add_new") ?> <i class="icon-plus"></i></a>
                    </div>
                </div>
                <?php echo theme_messages() ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr>
                                <th class="table-checkbox">
                                    <input type="checkbox" class="group-checkable" data-set=".dataTable .checkboxes"/></th>
                                <th><?php echo langline("settings_name") ?></th>
                                <th><?php echo langline("settings_site_name") ?></th>
                                <th><?php echo langline("settings_is_default") ?></th>
                                <th><?php echo langline("settings_created_at") ?></th>
                                <th><?php echo langline("settings_last_update") ?></th>
                                <th colspan="2">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data): foreach ($data as $row): ?>

                                    <tr class="odd gradeX">
                                        <td>
                                            <input type="checkbox" class="checkboxes" value="<?php echo $row->id ?>" name="item_id[]"/>
                                        </td>
                                        <td><?php echo $row->setting_name ?></td>
                                        <td><a href="<?php echo prep_url($row->site_url) ?>"><?php echo _t($row->site_name, "en") ?></a></td>
                                        <td>
                                            <input type="radio" name="is_default"  value="<?php echo $row->id ?>" class="default_settings" <?php echo $row->is_default == 1 ? "checked" : "" ?> />
                                        </td>
                                        <td class="center"><?php echo date("Y-m-d", strtotime($row->created)) ?></td>
                                        <td class="center"><?php echo date("Y-m-d h:i:s A", strtotime($row->updated)) ?></td>

                                        <?php if (check_user_permission(current_module(), "edit")): ?>
                                            <td>
                                                <a href="<?php echo $admin_base ?>settings/edit/<?php echo $row->id ?>" class="btn btn-default"><i
                                                        class="glyphicon glyphicon-edit"></i><?php echo langline("link_edit") ?></a>
                                            </td>
                                        <?php endif;
                                        if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")): ?>
                                            <td>
                                                <a href="<?php echo $admin_base ?>settings/delete/<?php echo $row->id ?>" class="btn btn-danger confirm_delete"><i
                                                        class="glyphicon glyphicon-remove"></i><?php echo langline("link_delete") ?></a>
                                            </td>
        <?php endif; ?>
                                    </tr>

    <?php endforeach;
endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<!-- END PAGE CONTENT-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/app.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/table-managed.js"></script>


<script>

    $(document).ready(function () {


        $(".default_settings").iButton({
            duration: 200, // the speed of the animation
            easing: "swing",
            labelOn: "Yes", // the text to show when toggled on
            labelOff: "No",
            change: function (el) {

                $.getJSON("<?php echo $admin_base ?>settings/setDefaultSettings/" + $(el).val());
            }

        }
        );

    });

</script>
