<?php
/*********************************************
 *  Project     : Projects Management
 *  File        : dashboard.php
 *  Created at  : 5/12/15 - 9:58 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : php.ahmedfathi@gmail.com
 *  site        : teqtuts.com
 *********************************************/
?>
<?php if (check_user_permission("log", "show") || check_user_permission("log", "supervision")): ?>
    <div class="row ">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-bell"></i><?php echo langline("recent_activities") ?></div>
                    <div class="actions">
                        <div class="btn-group">
                            <!--<a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown"
                               data-close-others="true">
                                Filter By
                                <i class="icon-angle-down"></i>
                            </a>-->

                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                <label><input type="checkbox"/> Finance</label>
                                <label><input type="checkbox" checked=""/> Membership</label>
                                <label><input type="checkbox"/> Customer Support</label>
                                <label><input type="checkbox" checked=""/> HR</label>
                                <label><input type="checkbox"/> System</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <ul class="feeds">
                            <?php if (isset($users_log) && $users_log):foreach ($users_log as $row): ?>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    <?php if ($row->module_icon_class): ?>
                                                        <i class="<?php echo $row->module_icon_class; ?>"></i>
                                                    <?php else: ?>
                                                        <i class="icon-check"></i>
                                                    <?php endif; ?>

                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">
                                                    <?php echo langline($row->ul_message) ?>

                                                    <span class="label label-sm label-warning">
                                                        <?php echo $row->user_name ?>
                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date">
                                            <?php echo time_ago($row->ul_created) ?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach;endif; ?>
                        </ul>
                    </div>
                    <div class="scroller-footer">
                        <div class="pull-right">
                            <a href="<?php echo admin_url("log") ?>"><?php echo langline("see_all") ?> <i
                                    class="m-icon-swapright m-icon-gray"></i></a> &nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
<?php endif; ?>
<?php if (check_user_permission("projects", "show") || check_user_permission("projects", "supervision")): ?>
    <div class="row ">
        <div class="col-md-12 col-sm-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-cogs"></i><?php echo langline("projects_status") ?></div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive scroller" style="height: 300px;" data-always-visible="1"
                         data-rail-visible="0">
                        <table class="table">
                            <thead>
                            <tr>
                                <th><?php echo langline("project_name") ?></th>
                                <th><?php echo langline("project_manager") ?></th>
                                <th><?php echo langline("project_duration") ?></th>
                                <th><?php echo langline("project_sub_tasks") ?></th>
                                <th><?php echo langline("project_status") ?></th>
                                <th width="150">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($projects_status) && $projects_status): foreach ($projects_status as $row): ?>

                                <tr class="odd gradeX">
                                    <!--<td>
                                <input type="checkbox" class="checkboxes" value="<?php /*echo $row->project_id */ ?>"
                                       name="item_id[]"/>
                            </td>-->
                                    <td>
                                        <a href="<?php echo admin_url("tasks/index/" . $row->project_id) ?>"><?php echo $row->project_name ?></a>
                                    </td>
                                    <td>
                                        <a href="<?php echo admin_url("users/show/" . $row->manager_id) ?>"><?php echo $row->manager_name ?></a>
                                    </td>
                                    <td>
                                        <div class="text-center">
                                            <?php echo date("Y-m-d", strtotime($row->project_from)) . " - " . date("Y-m-d", strtotime($row->project_to)) ?>
                                        </div>
                                        <div class="text-center">
                                            <span
                                                class="label label-info"><?php echo dateDifference(date("Y-m-d", strtotime($row->project_from)), date("d-m-Y 23:59:59", strtotime($row->project_to))) | 1 ?>  <?php echo langline("text_days") ?></span>
                                        </div>
                                    </td>
                                    <td><?php if ($tasks_id = explode(",", $row->tasks_id)): ?>
                                            <a href="<?php echo admin_url("tasks/index/{$row->project_id}") ?>"><?php echo sprintf(langline("sub_tasks_num"), count(array_unique($tasks_id))) ?></a>
                                        <?php else: ?>
                                            <p class="text-center">-</p>
                                        <?php endif; ?></td>

                                    <td>
                                        <span class="label"
                                              style="background-color:<?php echo $status[$row->project_status]['color'] ?>"><?php echo $status[$row->project_status]['name'] ?></span>

                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar"
                                                 aria-valuenow="<?php echo $row->tasks_process | 0 ?>" aria-valuemin="0"
                                                 aria-valuemax="100"
                                                 style="min-width: 2em;width: <?php echo $row->tasks_process | 1 ?>%;">
                                                <?php echo $row->tasks_process | 0 ?>%
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <?php if (check_user_permission(current_module(), "show") || check_user_permission(current_module(), "supervision")): ?>
                                            <a href="<?php echo admin_url("projects/show/" . $row->project_id) ?>"
                                               class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                               title="<?php echo langline("link_show"); ?>"><i
                                                    class="glyphicon glyphicon-eye-open"></i></a>
                                        <?php endif;
                                        if (check_user_permission(current_module(), "edit")): ?>
                                            <a href="<?php echo admin_url("projects/edit/" . $row->project_id) ?>"
                                               class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                               title="<?php echo langline("link_edit"); ?>"><i
                                                    class="glyphicon glyphicon-edit"></i></a>
                                        <?php endif;
                                        if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")): ?>
                                            <a href="<?php echo admin_url("projects/delete/" . $row->project_id) ?>"
                                               class="btn btn-danger confirm_delete" data-toggle="tooltip"
                                               data-placement="top" title="<?php echo langline("link_delete"); ?>"><i
                                                    class="glyphicon glyphicon-remove"></i></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>

                            <?php endforeach;
                            else: ?>
                                <tr>
                                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="scroller-footer">
                        <div class="pull-right">
                            <a href="<?php echo admin_url("projects") ?>"><?php echo langline("see_all") ?> <i
                                    class="m-icon-swapright m-icon-gray"></i></a> &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->

        </div>

    </div>
    <div class="clearfix"></div>
<?php endif; ?>