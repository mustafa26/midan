<?php
/* * *******************************************
 *  Project     : Projects Management
 *  File        : functions.php
 *  Created at  : 5/12/15 - 10:07 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : php.ahmedfathi@gmail.com
 *  site        : http://durar-it.com
 * ******************************************* */

function side_bar()
{
    $user_lang = get_user_lang();
    ?>
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu">
            <li>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone"></div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <!--
            <li>
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -- >
            <form class="sidebar-search" action="extra_search.html" method="POST">
                <div class="form-container">
                    <div class="input-box">
                        <a href="javascript:;" class="remove"></a>
                        <input type="text" placeholder="<?php echo langline("input_search") ?>"/>
                        <input type="button" class="submit" value=" "/>
                    </div>
                </div>
            </form>
            <!-- END RESPONSIVE QUICK SEARCH FORM -- >
            </li>
            -->

            <?php
            if ($modules = list_modules()): foreach ($modules as $key => $module):
                if (check_user_permission($module->module_name, "show")):
                    $childs = child_menu($modules, $module->module_id);
                    if ($module->module_parent == 0 and $module->module_show):
                        ?>
                        <li class="<?php echo current_module() == $module->module_name ? "active" : "" ?> ">
                            <a href="<?php echo $childs ? "javascript:;" : admin_url($module->module_name) ?>">
                                <i class="<?php echo $module->module_icon_class ?>"></i>
                                <span class="title"><?php echo _t($module->module_title, $user_lang) ?></span>
                                <?php if ($childs): ?>
                                    <span
                                        class="<?php echo current_module() == $module->module_name || $childs ? "active" : "" ?>"></span>
                                    <span
                                        class="arrow <?php echo current_module() == $module->module_name || $childs ? "open" : "" ?>"></span>
                                <?php else: ?>
                                    <span class="selected"></span>
                                <?php endif; ?>
                            </a>
                            <?php echo $childs['html'] ?>
                        </li>
                    <?php
                    endif;
                endif;
            endforeach;
            endif;
            ?>

        </ul>
        <!-- END SIDEBAR MENU -->
        <script>
            $(document).ready(function () {
                $(".page-sidebar-menu ul.sub-menu li.active").parent().parent("li").addClass("active");
            });
        </script>
    </div>
<?php
}

function child_menu($menu = array(), $parent = 0)
{

    $user_lang = get_user_lang();
    $html = "";
    $arr = array();
    if ($menu) {
        foreach ($menu as $key => $child) {
            if ($child->module_parent == $parent and $child->module_show) {
                $active = current_module() == $child->module_name ? "active" : "";
                $html .= "<li class='$active'>
                            <a href='" . admin_url($child->module_name) . "'>
                            <i class='" . $child->module_icon_class . "'></i> " . _t($child->module_title, $user_lang) . "</a> 
                        </li>";
                $arr[] = $child->module_id;
            }
        }
    }

    if ($html != "") {
        $html = "<ul class='sub-menu'>$html</ul>";
    }
    $data['list'] = $arr;
    $data['html'] = $html;

    return $arr ? $data : false;
}

if (!function_exists("theme_messages")) {

    function theme_messages()
    {
        $messages = flash_messages();
        $notifications = null;
        if ($messages) {
            ?>

            <?php
            foreach ($messages as $msg) {
                switch ($msg['type']) {
                    case "error":
                        $notifications .= "<div class='note note-danger'>{$msg['msg']}</div>";
                        break;
                    case "success":
                        $notifications .= "<div class='note note-success'>{$msg['msg']}</div>";
                        break;
                    case "warning":
                        $notifications .= "<div class='note note-warning'>{$msg['msg']}</div>";
                        break;
                    case "info":
                        $notifications .= "<div class='note note-info'>{$msg['msg']}</div>";
                        break;
                }
            }
            ?>

        <?php
        }
        return $notifications;
    }

}
