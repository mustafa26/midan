<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("requests/allrequests/") ?>"><?php echo langline("requests_link") ?></a> 
                    <span class="badge badge-warning">&nbsp;<?php echo $total; ?>&nbsp;</span>
                </div> 
                <div class="pull-right">
                    <a href="?status=1" class="btn btn-sm yellow"><?php echo langline("statusNew") ?></a>
                    <a href="?status=2" class="btn btn-sm green"><?php echo langline("statusAccept") ?></a>
                    <a href="?status=3" class="btn btn-sm red"><?php echo langline("statusNonAccept") ?></a>
                    <a href="?" class="btn btn-sm blue"><?php echo langline("All") ?></a>
                </div>
            </div> 
            <div class="portlet-body">   
                <?php echo theme_messages(); ?>  
                <div class="col-md-12 col-sm-6"> 
                    <div class="col-md-11">
                        <?php
                        $_action = "";
                        $_attributes = array(
                            "method" => "get",
                            "class" => "form-inline");
                        echo form_open($_action, $_attributes);
                        ?>   
                            <div class="form-group">  
                                <input type="text" name="MemberID" class="form-control col-md-3" placeholder="<?php echo langline("rqst_memberNumber") ?>" value="<?php echo get("MemberID") ? get("MemberID") : ""; ?>"> 
                            </div>
                            <div class="form-group"> 
                                <input type="text" name="NumCard" class="form-control col-md-3" placeholder="<?php echo langline("rqst_NumCard") ?>" value="<?php echo get("NumCard") ? get("NumCard") : ""; ?>"> 
                            </div>  
                            <div class="form-group"> 
                                <input type="text" name="NationalID" class="form-control col-md-3" placeholder="<?php echo langline("rqst_OwnerID") ?>" value="<?php echo get("NationalID") ? get("NationalID") : ""; ?>"> 
                            </div>  
                            <button type="submit" class="btn btn-default"><?php echo langline("filter_btn") ?></button>
                            <?php echo form_close(); ?>
                    </div>
                    <div class="clearfix"></div>  
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr> 
                                <th width="2%">&nbsp;</th>
                                <th width="30%"><?php echo langline("rqst_Owner") ?></th>
                                <th width="30%"><?php echo langline("rqst_Camel") ?></th>
                                <th width="20%"><?php echo langline("rqst_PermitImage") ?></th>  
                                <th width="8%"><?php echo langline("project_status") ?></th> 
                                <th width="10%">&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $offset;
                            if ($data): foreach ($data as $row):
                                    if (is_file('uploads/requests/' . $row->rqst_PermitImage) && !is_file('uploads/requests/thumb/' . $row->rqst_PermitImage)) {
                                        resizeProp('uploads/requests/' . $row->rqst_PermitImage, 'uploads/requests/thumb/' . $row->rqst_PermitImage, 150, 150);
                                    }
                                    ?>
                                    <tr class="odd gradeX"> 
                                        <td><?php echo $i; ?></td>
                                        <td> 
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_Created") ?></strong> : <span class="label label-info"><?php echo $row->rqst_Created; ?></span><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_OwnerName") ?></strong> : <?php echo $row->UR_Name; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("National ID") ?></strong> : <?php echo $row->UR_NationalID; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_memberNumber") ?></strong> : <?php echo $row->UR_MemberID; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_OwnerPhone") ?></strong> : <?php echo $row->UR_Phone; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_OwnerNationlaty") ?></strong> : <?php echo langline($row->UR_Nationlaty); ?>
                                        </td>  
                                        <td>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_Name") ?></strong> : <?php echo $row->rqst_Name; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_NumCard") ?></strong> : <?php echo $row->rqst_NumCard; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_AgeGroup") ?></strong> : <?php echo $row->rac_Age; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_Gender") ?></strong> : <?php echo $row->rac_Gender; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_NumRace") ?></strong> :
                                            <?php echo langline("Race"); ?> <?php echo $row->rac_Name; ?>
                                            <?php //echo $row->rqst_NumRace; ?>
                                        </td>    
                                        <td>
                                            <img src="<?php echo $row->rqst_PermitImage ? $upload_path . '/requests/thumb/' . $row->rqst_PermitImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="200" />
                                        </td>    
                                        <td>
                                            <?php echo $row->rqst_Status == 1 ? "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" : ($row->rqst_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>") ?>                                            
                                        </td> 
                                        <td class="text-center">  
                                            <?php
                                            if (check_user_permission(current_module(), "show")):
                                                ?>
                                                <a href="<?php echo admin_url("requests/view/" . $row->rqst_ID) ?>"
                                                   class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                                   title="<?php echo langline("link_edit"); ?>">
                                                    <i class="icon icon-eye-open"></i>
                                                </a>
                                                <?php
                                            endif;
                                            if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                                                ?>
                                                <a href="<?php echo admin_url("requests/delete/" . $row->rqst_FID . '/' . $row->rqst_ID) ?>"
                                                   class="btn btn-danger confirm_delete" data-toggle="tooltip"
                                                   data-placement="top" title="<?php echo langline("link_delete"); ?>">
                                                    <i class="glyphicon glyphicon-remove"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr> 
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <nav><?php echo $pagination ?></nav>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div> 