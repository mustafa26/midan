<!-- BEGIN PAGE LEVEL STYLES --> 
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/select2/select2_metro_rtl.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/css/multi-select-rtl.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB--> 
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-left"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("events") ?>"><?php echo langline("events_event_title") ?></a>
                <i class="icon-angle-left"></i>
            </li>                   
            <li><a href="<?php echo admin_url("/requests/ezab/") ?>"><?php echo langline("requests_request_title") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box green form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("Request") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>

        <?php
        $_action = "";
        $_attributes = array(
            "id" => "dataForm",
            "method" => "post",
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data");
        echo form_open($_action, $_attributes);
        ?> 

        <div class="form-body"> 
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_Status") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static">
                                <?php echo $row->rqst_Status == 1 ? "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" : ($row->rqst_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>") ?> 
                                <label><input type="radio" value="1" name="rqst_Status" <?php echo $row->rqst_Status == 1 ? 'checked' : ''; ?>><?php echo langline("statusNew"); ?></label>
                                <label><input type="radio" value="2" name="rqst_Status" <?php echo $row->rqst_Status == 2 ? 'checked' : ''; ?> ><?php echo langline("statusAccept"); ?></label>
                                <label><input type="radio" value="3" name="rqst_Status" <?php echo $row->rqst_Status == 3 ? 'checked' : ''; ?> ><?php echo langline("statusNonAccept"); ?></label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>  
            <h3 class="form-section"><?php echo langline("rqst_Info") ?> <?php echo langline("rqst_Owner") ?></h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_OwnerName") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_Name ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_OwnerBD") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_BD ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_OwnerNationlaty") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_Nationlaty ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_OwnerPhone") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_Phone ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row 
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php //echo langline("rqst_OwnerEmail")   ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php //echo $row->rqst_OwnerEmail   ?></p>
                        </div>
                    </div>
                </div>
            </div> 
            --> 
            <!--/row-->              
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_OwnerID") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_NationalID ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_OwnerIDExp") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->UR_NationalIDExp ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->        
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_OwnerIDImage") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static">
                                <a data-toggle="modal" href="#basic_624">
                                    <img src="<?php echo $row->UR_NationalIDImage ? $upload_path . '/requests/' . $row->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="200" />
                                </a>
                            </p>
                        </div>
                        <div class="modal fade" id="basic_624" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> 
                                        <img src="<?php echo $row->UR_NationalIDImage ? $upload_path . '/requests/' . $row->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="500" />
                                    </div>         
                                </div> 
                            </div> 
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_OwnerImage") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static">
                                <a data-toggle="modal" href="#basic_625">
                                    <img src="<?php echo $row->UR_Image ? $upload_path . '/requests/' . $row->UR_Image : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="200" />
                                </a>
                            </p>
                        </div>
                        <div class="modal fade" id="basic_625" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> 
                                        <img src="<?php echo $row->UR_Image ? $upload_path . '/requests/' . $row->UR_Image : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="500" />
                                    </div>         
                                </div> 
                            </div> 
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->          
            <h3 class="form-section"> <?php echo langline("rqst_EzabaInfo") ?></h3>  
            <!--/row-->  
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_AgeGroup") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->rqst_EzbaName; ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_AgeGroup") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->rqst_Location; ?></p>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>     
            <!--/row-->   
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo langline("rqst_NumRace") ?>:</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?php echo $row->rqst_Notes ?></p>
                        </div>
                    </div>
                </div> 
                <!--/span-->
            </div>       
        </div> 
        <?php echo form_close(); ?>
        <div class="clearfix"></div>
    </div>
</div>  


