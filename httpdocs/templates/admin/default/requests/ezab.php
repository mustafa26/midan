<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("requests/index/" . segment(4)) ?>"><?php echo langline("requests_link") ?></a> (<?php echo $total; ?>)&nbsp;
                </div> 
                <div class="pull-right">
                    <a href="?status=1" class="btn btn-sm yellow"><?php echo langline("statusNew") ?></a>
                    <a href="?status=2" class="btn btn-sm green"><?php echo langline("statusAccept") ?></a>
                    <a href="?status=3" class="btn btn-sm red"><?php echo langline("statusNonAccept") ?></a>
                    <a href="?" class="btn btn-sm blue"><?php echo langline("All") ?></a>
                </div>
            </div> 
            <div class="portlet-body">   
                <?php echo theme_messages(); ?>   
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr>  
                                <th width="30%"><?php echo langline("rqst_Owner") ?></th>
                                <th width="30%"><?php echo langline("rqst_Camel") ?></th> 
                                <th width="5%"><?php echo langline("project_status") ?></th> 
                                <th width="5%">&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data): foreach ($data as $row): ?>
                                    <tr class="odd gradeX"> 
                                        <td> 
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_OwnerName") ?></strong> : <?php echo $row->UR_Name; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_OwnerID") ?></strong> : <?php echo $row->UR_NationalID; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_OwnerPhone") ?></strong> : <?php echo $row->UR_Phone; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_OwnerNationlaty") ?></strong> : <?php echo $row->UR_Nationlaty; ?>
                                        </td>  
                                        <td>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_Name") ?></strong> : <?php echo $row->rqst_EzbaName; ?><br>
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_NumCard") ?></strong> : <?php echo $row->rqst_Location    ; ?><br> 
                                            <strong class="col-md-5 text-muted"><?php echo langline("rqst_NumRace") ?></strong> : <?php echo $row->rqst_Notes; ?>
                                        </td>   
                                        <td>
                                            <?php echo $row->rqst_Status == 1 ? "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" : ($row->rqst_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>") ?>                                            
                                        </td> 
                                        <td class="text-center">  
                                            <?php
                                            if (check_user_permission(current_module(), "show")):
                                                ?>
                                                <a href="<?php echo admin_url("requests/ezabview/" . $row->rqst_ID) ?>"
                                                   class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                                   title="<?php echo langline("link_edit"); ?>">
                                                    <i class="icon icon-eye-open"></i>
                                                </a>
                                                <?php
                                            endif;
                                            if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                                                ?>
                                                <a href="<?php echo admin_url("requests/ezabdelete/" . $row->rqst_ID) ?>"
                                                   class="btn btn-danger confirm_delete" data-toggle="tooltip"
                                                   data-placement="top" title="<?php echo langline("link_delete"); ?>">
                                                    <i class="glyphicon glyphicon-remove"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr> 
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <nav><?php echo $pagination ?></nav>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div> 