NationalID<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-left"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("events") ?>"><?php echo langline("events_event_title") ?></a>
                <i class="icon-angle-left"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("/requests/index/" . $row->rqst_FID) ?>"><?php echo langline("requests_request_title") ?></a>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box green form">
<div class="portlet-title">
    <div class="caption"><i class="icon-reorder"></i><?php echo langline("Request") ?></div>
    <div class="tools">
        <a href="javascript:;" class="collapse"></a>
        <a href="#portlet-config" data-toggle="modal" class="config"></a>
    </div>
</div>
<div class="portlet-body">
<?php echo theme_messages(); ?>
<?php
$_action = "";
$_attributes = array(
    "id" => "dataForm",
    "method" => "post",
    "class" => "form-horizontal",
    "role" => "form",
    "enctype" => "multipart/form-data");
echo form_open($_action, $_attributes);
?>
<div class="form-body">
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_Status") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static">
                    <?php echo $row->rqst_Status == 1 ? "<span class='btn btn-sm yellow'>" . langline("statusNew") . "</span>" : ($row->rqst_Status == 2 ? "<span class='btn btn-sm green'>" . langline("statusAccept") . "</span>" : "<span class='btn btn-sm red'>" . langline("statusNonAccept") . "</span>") ?>
                    <label><input type="radio" value="1"
                                  name="rqst_Status" <?php echo $row->rqst_Status == 1 ? 'checked' : ''; ?>><?php echo langline("statusNew"); ?>
                    </label>
                    <label><input type="radio" value="2"
                                  name="rqst_Status" <?php echo $row->rqst_Status == 2 ? 'checked' : ''; ?> ><?php echo langline("statusAccept"); ?>
                    </label>
                    <label><input type="radio" value="3"
                                  name="rqst_Status" <?php echo $row->rqst_Status == 3 ? 'checked' : ''; ?> ><?php echo langline("statusNonAccept"); ?>
                    </label>
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_Send_SMS") ?>:</label>

            <div class="col-md-9">
                <input type="checkbox" name="send_sms" value="1">
            </div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="col-md-9">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3">
                <?php echo langline("Send_SMS") ?>
                <br>
                <?php echo langline("Send_SMS_Limit") ?>:
                <br>
                <span id="charNum" style="color: red"></span>
            </label>
            <div class="col-md-9">
                <select name="messagesms_tmplt" id="messagesms_tmplt" class="form-control input-lg">
                    <option value=""><?php echo langline("Select"); ?></option>
                    <option value="<?php echo langline("SMS_T1"); ?>"><?php echo langline("SMS_T1"); ?></option>
                    <option value="<?php echo langline("SMS_T2"); ?>"><?php echo langline("SMS_T2"); ?></option>
                    <option value="<?php echo langline("SMS_T3"); ?>"><?php echo langline("SMS_T3"); ?></option>
                    <option value="<?php echo langline("SMS_T4"); ?>"><?php echo langline("SMS_T4"); ?></option>
                </select>
                <textarea maxlength="160" name="messagesms" id="messagesms" class="form-control"></textarea>
            </div>
        </div>
    </div>
</div>
<h3 class="form-section"><?php echo langline("rqst_Info") ?> <?php echo langline("rqst_Owner") ?></h3>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_Created") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><span class="label label-info"><?php echo $row->UR_Created ?></span></p>
            </div>
        </div>
    </div>
    <!--/span-->
</div>
<!--/row-->
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_OwnerName") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><?php echo $row->UR_Name ?></p>
            </div>
        </div>
    </div>
    <!--/span-->
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_OwnerPhone") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><?php echo $row->UR_Phone ?></p>
                <input name="UR_Phone" type="hidden" value="<?php echo $row->UR_Phone ?>">
                <input name="rqst_SentSMS" type="hidden" value="<?php echo $row->rqst_SentSMS ?>">
            </div>
        </div>
    </div>
    <!--/span-->
</div>
<!--/row-->
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_OwnerNationlaty") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><?php echo langline($row->UR_Nationlaty) ?></p>
            </div>
        </div>
    </div>
    <!--/span-->
</div>
<!--/row-->
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_OwnerID") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><?php echo $row->UR_NationalID ?></p>
            </div>
        </div>
    </div>
    <!--/span-->
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_memberNumber") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><?php echo $row->UR_MemberID ?></p>
            </div>
        </div>
    </div>
    <!--/span-->
</div>
<!--/row-->
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("UR_MemberIDImag") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static">
                    <a data-toggle="modal" href="#basic_624">
                        <img
                            src="<?php echo $row->UR_NationalIDImage ? $upload_path . '/requests/' . $row->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                            width="200"/>
                    </a>
                </p>
            </div>
            <div class="modal fade" id="basic_624" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <img
                                src="<?php echo $row->UR_NationalIDImage ? $upload_path . '/requests/' . $row->UR_NationalIDImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                                width="500"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/span-->
</div>
<!--/row-->
<h3 class="form-section"><?php echo langline("rqst_Info") ?> <?php echo langline("rqst_Camel") ?></h3>
<!--/row-->
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_Created") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><span class="label label-info"><?php echo $row->rqst_Created ?></span>
                </p>
            </div>
        </div>
    </div>
    <!--/span-->
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("Date") ?>:</label>

            <div class="col-md-9">
                <select name="rqst_FID" id="rqst_FID" class="form-control input-lg">
                    <?php if ($res_all): foreach ($res_all as $v): ?>
                        <option
                            value="<?php echo $v->event_ID; ?>" <?php echo $row->rqst_FID == $v->event_ID ? 'selected' : '' ?>><?php echo _t($v->event_Title, 'ar'); ?>
                            | <?php echo $v->event_From; ?></option>
                    <?php
                    endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </div>
    </div>
    <!--/span-->
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_Name") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><?php //echo $row->rqst_Name ?></p>
                <input name="rqst_Name" type="text" class="form-control input-lg" value="<?php echo $row->rqst_Name ?>">
            </div>
        </div>
    </div>
    <!--/span-->
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_NumCard") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static"><?php //echo $row->rqst_NumCard ?></p>
                <input name="rqst_NumCard" type="text" class="form-control input-lg"
                       value="<?php echo $row->rqst_NumCard ?>">
            </div>
        </div>
    </div>
    <!--/span-->
</div>
<!--/row-->
<div class="row">
    <!--                    <div class="col-md-6">-->
    <!--                        <div class="form-group">-->
    <!--                            <label class="control-label col-md-3">-->
    <?php //echo langline("rqst_Gender") ?><!--:</label>-->
    <!--                            <div class="col-md-9">-->
    <!--								<select name="rqst_Gender" id="rqst_Gender" class="form-control input-lg">-->
    <!--									<option value="2" -->
    <?php //echo $row->rqst_Gender == 2?'selected':''?><!--><?php //echo langline("rqst_Gender_02"); ?><!--</option>-->
    <!--									<option value="1" -->
    <?php //echo $row->rqst_Gender == 1?'selected':''?><!--><?php //echo langline("rqst_Gender_01"); ?><!--</option>-->
    <!--								</select> -->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--/span-->
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_AgeGroup") ?>:</label>

            <div class="col-md-9">
                <select name="rqst_AgeGroup" id="rqst_AgeGroup" class="form-control input-lg">
                    <option
                        value="2" <?php echo $row->rqst_AgeGroup == 2 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_2"); ?></option>
                    <option
                        value="3" <?php echo $row->rqst_AgeGroup == 3 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_3"); ?></option>
                    <option
                        value="4" <?php echo $row->rqst_AgeGroup == 4 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_4"); ?></option>
                    <option
                        value="5" <?php echo $row->rqst_AgeGroup == 5 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_5"); ?></option>
                    <option
                        value="6" <?php echo $row->rqst_AgeGroup == 6 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_6"); ?></option>
                </select>
            </div>
        </div>
    </div>
    <!--/span-->
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_NumRace") ?>:</label>
            <div class="col-md-9">
                <select name="rqst_NumRace" name="rqst_NumRace" class="form-control">
                    <?php if ($races): foreach ($races["data"] as $v):?>
                        <option value="<?php echo $v->rac_ID; ?>" <?php echo $v->rac_ID==$row->rac_ID?"selected":""?>><?php echo $v->rac_Sort; ?>
                            - <?php langline("Race")?> :
                            <?php echo $v->rac_Name; ?> | <?php echo langline("rqst_AgeGroup") ?> : <?php echo $v->rac_Age; ?>
                            | <?php echo langline("rqst_Gender") ?> : <?php echo $v->rac_Gender; ?></option>
                    <?php
                    endforeach; endif;
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>
<!--/row-->
<!--/row-->
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo langline("rqst_PermitImage") ?>:</label>

            <div class="col-md-9">
                <p class="form-control-static">
                    <a data-toggle="modal" href="#basic_626">
                        <img
                            src="<?php echo $row->rqst_PermitImage ? $upload_path . '/requests/' . $row->rqst_PermitImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                            width="200"/>
                    </a>
                </p>
            </div>
            <div class="modal fade" id="basic_626" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <img
                                src="<?php echo $row->rqst_PermitImage ? $upload_path . '/requests/' . $row->rqst_PermitImage : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>"
                                width="500"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/span-->
</div>
</div>
<?php echo form_close(); ?>
<div class="clearfix"></div>
</div>
</div>

<script>
    $('#messagesms_tmplt').change(function () {
        //alert(this.value);
        $('#messagesms').val(this.value);
        //$('#charNum').text(length);
    });
    var maxLength = 160;
    $('#messagesms').keyup(function () {
        var length = $(this).val().length;
        var length = maxLength - length;
        $('#charNum').text("متبقي : " + length + "حرف");
        //$('#charNum').text(length);
    });
</script>


