<html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $title ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320"> 
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo $themepath ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES --> 
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="<?php echo $themepath ?>/assets/plugins/gritter/css/jquery.gritter-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css"rel="stylesheet" />
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo $themepath ?>/assets/css/style-metronic-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/style-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/style-responsive-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/css/plugins-rtl.css" rel="stylesheet" type="text/css"/>  
        <link href="<?php echo $themepath ?>/assets/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $themepath ?>/assets/plugins/jquery.ibutton/css/jquery.ibutton.min.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?php echo $themepath ?>/favicon.ico"/>
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?php echo $themepath ?>/assets/plugins/respond.min.js"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/excanvas.min.js"></script>
        <![endif]-->
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo $themepath ?>/assets/plugins/jquery.ibutton/lib/jquery.ibutton.min.js" type="text/javascript"></script>  
        <!-- END CORE PLUGINS -->
    </head>
    <body class="page-header-fixed" style="direction: rtl" onload="print()"> 
        <div style="width:1000px;">
            <div class="row">
                <div class="col-md-12">  
                    <div class="portlet-body"> 
                        <div class="table-responsive"> 
                            <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                                <thead>
                                    <tr> 
                                        <td colspan="5" align="center" style="font-weight: bold;">
                                            المهرجان السنوي الثالث لسباقات الهجن بميدان البشائر للهجن العربية
                                        </td>
                                    </tr>
                                    <tr> 
                                        <td colspan="5" align="center" style="font-weight: bold;">
                                            الولاية : أدم 
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            الشوط :
                                            <?php echo get("rqst_NumRace") ? $race->rac_Name.' - '.$race->rac_Age.' - '.$race->rac_Gender.' - '.$race->rac_ord : '-'; ?>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            التاريخ : <?php echo $event ? $event->event_From . ' م' : '-' ?> 
                                        </td> 
                                    </tr>
                                    <tr> 
                                        <th width="5%">م</th>
                                        <th width="20%"><?php echo langline("rqst_NameCamel") ?></th> 
                                        <th width="20%"><?php echo langline("rqst_OwnerName") ?></th>
                                        <th width="25%"><?php echo langline("rqst_NumCard") ?></th> 
                                        <th width="10%"><?php echo langline("rqst_memberNumber") ?></th>  

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = $offset;
                                    if ($data): foreach ($data as $row):
                                            ?>
                                            <tr class="odd gradeX"> 
                                                <td>
                                                    <?php echo $i; ?>
                                                </td>     
                                                <td>
                                                    <?php echo $row->Cname; ?>
                                                </td>   
                                                <td>
                                                    <?php echo $row->Oname; ?>
                                                </td>  
                                                <td> 
                                                    <?php echo $row->CID; ?>
                                                </td>  
                                                <td>
                                                    <?php echo $row->MID; ?>
                                                </td>  
                                            </tr> 
                                            <?php
                                            $i++;
                                        endforeach;
                                    else:
                                        ?>
                                        <tr>
                                            <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </body>
</html>