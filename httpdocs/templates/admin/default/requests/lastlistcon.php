<div class="row">
<div class="col-md-12">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box light-grey">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-home"></i>
            <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
        </div>
        <div class="caption">
            <i class="icon-angle-left"></i>
            <a href="<?php echo admin_url("requests/index/" . segment(4)) ?>"><?php echo langline("requests_link") ?></a>
            <span class="badge badge-warning">&nbsp;<?php echo $total; ?>&nbsp;</span>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>
        <div class="col-md-12 col-sm-6">
            <div class="col-md-11">
                <?php
                $_action = "";
                $_attributes = array(
                    "method" => "get",
                    "class" => "form-inline");
                echo form_open($_action, $_attributes);
                ?>
                <div class="form-group">
                    <input type="text" name="Cname" class="form-control input-small"
                           placeholder="<?php echo langline("rqst_Name") ?>"
                           value="<?php echo get("Cname") ? get("Cname") : ""; ?>">
                </div>
                <div class="form-group">
                    <input type="text" name="MemberID" class="form-control input-small"
                           placeholder="<?php echo langline("rqst_memberNumber") ?>"
                           value="<?php echo get("MemberID") ? get("MemberID") : ""; ?>">
                </div>
                <div class="form-group">
                    <input type="text" name="NumCard" class="form-control input-medium"
                           placeholder="<?php echo langline("rqst_NumCard") ?>"
                           value="<?php echo get("NumCard") ? get("NumCard") : ""; ?>">
                </div>
                <div class="form-group input-small">
                    <select name="eventid" id="eventid" class="form-control"
                            onchange="$('#div_races').load('<?php echo admin_url("members/load_ajax_races") ?>/'+this.value);">
                        <option value="0"><?php echo langline("Date_Race") ?></option>
                        <?php if ($res_all): foreach ($res_all as $v): ?>
                            <option
                                value="<?php echo $v->event_ID; ?>" <?php echo get("eventid") == $v->event_ID ? 'selected' : '' ?>><?php echo _t($v->event_Title, 'ar'); ?>
                                | <?php echo $v->event_From; ?></option>
                        <?php
                        endforeach;
                        endif;
                        ?>
                    </select>
                </div>
                <div class="form-group" id="div_races"></div>
                <button type="submit" name="sch" value="1"
                        class="btn btn-default"><?php echo langline("filter_btn") ?></button>
                <?php echo form_close(); ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="table-responsive">
            <div class="col-md-12 col-sm-6 btn btn-lg green">
                <div class="col-lg-3">الولاية : أدم</div>
                <div class="col-lg-3"> الشوط
                    : <?php echo get("rqst_NumRace") ? $race->rac_Name . ' - ' . $race->rac_Age . ' - ' . $race->rac_Gender : '؟'; ?> </div>
                <div class="col-lg-3"> الفئة : <?php echo get("rqst_NumRace") ? $race->rac_ord : '؟'; ?></div>
                <div class="col-lg-3"> التاريخ : <?php echo $event ? $event->event_From . ' م' : '؟' ?></div>
            </div>


            <?php
            $_action = "";
            $_attributes = array(
                "method" => "post",
                "name" => "form_add");
            echo form_open($_action, $_attributes);
            ?>
            <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                <tbody>
                <tr class="odd gradeX">
                    <td>
                        <input name="Cname" type="text" class="form-control input-lg" placeholder="<?php echo langline("rqst_NameCamel") ?>" required style="padding-right: 3px;">
                    </td>
                    <td>
                        <input name="Oname" type="text" class="form-control input-lg" placeholder="<?php echo langline("rqst_OwnerName") ?>" required style="padding-right: 3px;">
                    </td>
                    <td>
                        <input name="CID" type="text" class="form-control input-lg" placeholder="<?php echo langline("rqst_NumCard") ?>" required style="padding-right: 3px;">
                    </td>
                    <td>
                        <input name="MID" type="text" class="form-control input-lg" placeholder="<?php echo langline("rqst_memberNumber") ?>" required style="padding-right: 3px;">
                    </td>
                    <td>
                        <select name="event_ID" id="event_ID" class="form-control"
                                onchange="$('#td_races').load('<?php echo admin_url("members/load_ajax_races") ?>/'+this.value);">
                            <option value="0"><?php echo langline("Date_Race") ?></option>
                            <?php if ($res_all): foreach ($res_all as $v): ?>
                                <option
                                    value="<?php echo $v->event_ID; ?>" <?php echo get("eventid") == $v->event_ID ? 'selected' : '' ?>><?php echo _t($v->event_Title, 'ar'); ?>
                                    | <?php echo $v->event_From; ?></option>
                            <?php
                            endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                    <td id="td_races"> </td>
                    <td>
                        <input type="submit" class="btn green" name="add" value="<?php echo langline("btn_save") ?>"></td>
                </tr>
                </tbody>
            </table>
            <?php echo form_close(); ?>


            <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                <thead>
                <tr>
                    <th width="5%">المركز</th>
                    <th width="8%"><?php echo langline("rqst_NameCamel") ?></th>
                    <th width="15%"><?php echo langline("rqst_OwnerName") ?></th>
                    <th width="15%"><?php echo langline("rqst_NumCard") ?></th>
                    <th width="8%"><?php echo langline("rqst_memberNumber") ?></th>
                    <th width="8%">الزمن</th>
                    <th width="8%">السيارة</th>
                    <th width="10%"><?php echo langline("event_title") ?></th>
                    <th width="15%"><?php echo langline("rqst_NumRace") ?></th>
                    <th width="10%"><?php echo langline("AgeGroup") ?></th>
                    <th width="5%">&nbsp;</th>
                </tr>
                </thead>
                <?php
                $i = $offset;
                if ($data): foreach ($data as $row):
                    ?>
                    <tr class="odd gradeX">
                        <td colspan="11">
                            <div class="col-md-12 col-sm-6" style="padding:0 !important;">
                                <div class="col-md-12" style="padding:0 !important;">
                                    <?php
                                    $_action = "";
                                    $_attributes = array(
                                        "method" => "post",
                                        "name" => "form_update",
                                        "class" => "form-inline");
                                    echo form_open($_action, $_attributes);
                                    ?>
                                    <?php //echo $i; ?>
                                    <div class="form-group">
                                        <input name="wsrt" type="text" class="form-control input-xsmall"
                                               value="<?php echo $row->winner_sort; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input name="Cname" type="text" class="form-control input-xsmall"
                                               value="<?php echo $row->Cname; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input name="Oname" type="text" class="form-control input-small"
                                               value="<?php echo $row->Oname; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input name="CID" type="text" class="form-control input-small"
                                               value="<?php echo $row->CID; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input name="MID" type="text" class="form-control input-xsmall"
                                               value="<?php echo $row->MID; ?>">
                                    </div>
                                    <div class="form-group">
                                        <input name="RaceTime" type="text" class="form-control input-xsmall"
                                               value="<?php echo $row->RaceTime; ?>"
                                               placeholder="الزمن">
                                    </div>
                                    <div class="form-group">
                                        <select name="Car" class="form-control">
                                            <option value="1" <?php echo $row->Car == 1 ? 'selected' : ''; ?>>نعم
                                            </option>
                                            <option value="0" <?php echo $row->Car == 0 ? 'selected' : ''; ?>>لا
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="event_ID" id="event_ID" class="form-control input-small"
                                                onchange="$('#div_races_<?php echo $row->id ?>').load('<?php echo admin_url("members/load_ajax_races") ?>/'+this.value);">
                                            <option value="0"><?php echo langline("Date_Race") ?></option>
                                            <?php if ($res_all): foreach ($res_all as $v): ?>
                                                <option
                                                    value="<?php echo $v->event_ID; ?>" <?php echo $row->event_ID == $v->event_ID ? 'selected' : '' ?>><?php echo _t($v->event_Title, 'ar'); ?>
                                                    | <?php echo $v->event_From; ?></option>
                                            <?php
                                            endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group" id="div_races_<?php echo $row->id ?>">
                                        <?php echo $row->rac_Name; ?> - <?php echo $row->rac_Age; ?>
                                        - <?php echo $row->rac_Gender; ?>
                                        <input type="hidden" name="rqst_NumRace"
                                               value="<?php echo $row->RaceNum ?>">
                                    </div>
                                    <div class="form-group">
                                        <input name="CGroup" type="text" class="form-control input-xsmall"
                                               value="<?php echo $row->CGroup ? $row->CGroup : $row->rac_ord; ?>">
                                    </div>
                                    <div class="form-group">

                                        <?php
                                        if (check_user_permission(current_module(), "edit")):
                                            ?>
                                            <input type="submit" class="btn blue" name="edit"
                                                   value="<?php echo langline("btn_save") ?>">
                                        <?php
                                        endif;
                                        if (check_user_permission(current_module(), "delete")):
                                            ?>
                                            <a href="<?php echo admin_url("requests/deletectrl/" . $row->id) ?>"
                                               class="btn red confirm_delete">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </a>
                                        <?php endif; ?>
                                        <input type="hidden" name="id" value="<?php echo $row->id ?>">
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                else:
                    ?>
                    <tr>
                        <td colspan="11"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                    </tr>
                <?php
                endif;
                ?>
                </tbody>
            </table>
        </div>
        <nav><?php echo $pagination ?></nav>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>


