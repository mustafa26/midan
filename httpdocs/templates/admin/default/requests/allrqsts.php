<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption">
                    <i class="icon-angle-left"></i>
                    <a href="<?php echo admin_url("requests/index/" . segment(4)) ?>"><?php echo langline("requests_link") ?></a>
                    <span class="badge badge-warning">&nbsp;<?php echo $total; ?>&nbsp;</span>
                </div>
            </div>
            <div class="portlet-body">
                <?php echo theme_messages(); ?>

                <div class="col-md-12 col-sm-6">
                    <div class="col-md-11">

                        <?php
                        $_action = "";
                        $_attributes = array(
                            "method" => "get",
                            "class" => "form-inline");
                        echo form_open($_action, $_attributes);
                        ?>
                        <div class="form-group">
                            <select name="Gender" id="Gender" class="form-control">
                                <option value="0"><?php echo langline("rqst_Gender") ?></option>
                                <option value="1" <?php echo get("Gender") == 1 ? 'selected' : '' ?>><?php echo langline("rqst_Gender_01"); ?></option>
                                <option value="2" <?php echo get("Gender") == 2 ? 'selected' : '' ?>><?php echo langline("rqst_Gender_02"); ?></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="AgeGroup" id="AgeGroup" class="form-control">
                                <option value="0"><?php echo langline("rqst_AgeGroup") ?></option>
                                <option value="2" <?php echo get("AgeGroup") == 2 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_2"); ?></option>
                                <option value="3" <?php echo get("AgeGroup") == 3 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_3"); ?></option>
                                <option value="4" <?php echo get("AgeGroup") == 4 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_4"); ?></option>
                                <option value="5" <?php echo get("AgeGroup") == 5 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_5"); ?></option>
                                <option value="6" <?php echo get("AgeGroup") == 6 ? 'selected' : '' ?>><?php echo langline("rqst_AgeGroup_6"); ?></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="Race" id="Race" class="form-control">
                                <option value="0"><?php echo langline("Race") ?></option>
                                <?php for ($i = 1; $i <= 20; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php echo get("Race") == $i ? 'selected' : '' ?>><?php echo langline("NumRace_" . $i); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="eventid" id="eventid" class="form-control">
                                <option value="0"><?php echo langline("Date") ?></option>
                                <?php if ($res_all): foreach ($res_all as $v): ?>
                                        <option value="<?php echo $v->event_ID; ?>" <?php echo get("eventid") == $v->event_ID ? 'selected' : '' ?>><?php echo _t($v->event_Title, 'ar'); ?> | <?php echo $v->event_From; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default"><?php echo langline("filter_btn") ?></button>
                        <?php echo form_close(); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="table-responsive">
                    <div class="col-md-12 col-sm-6 btn btn-lg green">
                        <div class="col-lg-3">الولاية : أدم </div>
                        <div class="col-lg-3"> الشوط : <?php echo get("Race") ? langline("NumRace_" . get("Race")) : '؟'; ?> </div>
                        <div class="col-lg-3"> الفئة :<?php echo get("AgeGroup") ? langline("rqst_AgeGroup_" . get("AgeGroup")) . ' سيارة' : '؟'; ?></div>
                        <div class="col-lg-3">  التاريخ : <?php echo $event ? $event->event_From . ' م' : '؟' ?></div>
                    </div>
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr>
                                <th width="5%"><?php echo langline("M") ?></th>
                                <th width="20%"><?php echo langline("rqst_NameCamel") ?></th>
                                <th width="20%"><?php echo langline("rqst_NumRace") ?> - <?php echo langline("rqst_Gender") ?></th>
                                <th width="20%"><?php echo langline("rqst_OwnerName") ?></th>
                                <th width="25%"><?php echo langline("rqst_NumCard") ?></th>
                                <th width="10%"><?php echo langline("rqst_memberNumber") ?></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $offset;
                            if ($data): foreach ($data as $row):
                                    ?>
                                    <tr class="odd gradeX">
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo $row->rqst_Name; ?>
                                        </td>
                                        <td>
                                            <?php echo $row->rac_Sort; ?>
                                            - الشوط : <?php echo $row->rac_Name; ?> | <?php echo $row->rac_Age; ?>
                                            | <?php echo $row->rac_Gender; ?>
                                            <?php //echo langline("rqst_Gender_0" . $row->rqst_Gender); ?>
                                        </td>
                                        <td>
                                            <?php echo $row->UR_Name; ?>
                                        </td>
                                        <td>
                                            <?php echo $row->rqst_NumCard; ?>
                                        </td>
                                        <td>
                                            <?php echo $row->UR_MemberID; ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <nav><?php echo $pagination ?></nav>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div> 