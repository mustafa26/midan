<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("requests/index/" . segment(4)) ?>"><?php echo langline("requests_link") ?></a>
                    <span class="badge badge-warning">&nbsp;<?php echo $total; ?>&nbsp;</span>
                </div>
                <div class="btn-group pull-right">
                    <a href="<?php echo admin_url("requests/toexcel?rqst_NumRace=" . get("rqst_NumRace") . "&eventid=" . get("eventid"). "&Cname=" . get("Cname") . "") ?>" class="btn green" target="_blank">
                        <?php echo langline("toexcel") ?> <i class="icon-plus"></i></a>
                </div>
                <div class="btn-group pull-right">
                    <a href="<?php echo admin_url("requests/printlist?rqst_NumRace=" . get("rqst_NumRace") . "&eventid=" . get("eventid"). "&Cname=" . get("Cname") . "") ?>" class="btn green" target="_blank">
                        <?php echo langline("printlist") ?> <i class="icon-plus"></i></a>
                </div>


            </div> 
            <div class="portlet-body">   
                <?php echo theme_messages(); ?>

                <div class="col-md-12 col-sm-6">
                    <div class="col-md-11">
                        <?php
                        $_action = "";
                        $_attributes = array(
                            "method" => "get",
                            "class" => "form-inline");
                        echo form_open($_action, $_attributes);
                        ?>
                        <div class="form-group">
                            <input type="text" name="Cname" class="form-control input-small" placeholder="<?php echo langline("rqst_Name") ?>" value="<?php echo get("Cname") ? get("Cname") : ""; ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" name="MemberID" class="form-control input-small" placeholder="<?php echo langline("rqst_memberNumber") ?>" value="<?php echo get("MemberID") ? get("MemberID") : ""; ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" name="NumCard" class="form-control input-medium" placeholder="<?php echo langline("rqst_NumCard") ?>" value="<?php echo get("NumCard") ? get("NumCard") : ""; ?>">
                        </div>
                        <div class="form-group input-small">
                            <select name="eventid" id="eventid" class="form-control"
                                    onchange="$('#div_races').load('<?php echo admin_url("members/load_ajax_races") ?>/'+this.value);">
                                <option value="0"><?php echo langline("Date_Race") ?></option>
                                <?php if ($res_all): foreach ($res_all as $v): ?>
                                    <option value="<?php echo $v->event_ID; ?>" <?php echo get("eventid") == $v->event_ID ? 'selected' : '' ?>><?php echo _t($v->event_Title, 'ar'); ?> | <?php echo $v->event_From; ?></option>
                                <?php
                                endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                        <div class="form-group" id="div_races"> </div>
                        <button type="submit" name="sch" value="1" class="btn btn-default"><?php echo langline("filter_btn") ?></button>
                        <?php echo form_close(); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="table-responsive"> 
                    <div class="col-md-12 col-sm-6 btn btn-lg green">
                        <div class="col-lg-3">الولاية : أدم </div>
                        <div class="col-lg-3"> الشوط : <?php echo get("rqst_NumRace") ? $race->rac_Name.' - '.$race->rac_Age.' - '.$race->rac_Gender : '؟'; ?> </div>
                        <div class="col-lg-3"> الفئة : <?php echo get("rqst_NumRace") ? $race->rac_ord: '؟'; ?></div>
                        <div class="col-lg-3">  التاريخ : <?php echo $event ? $event->event_From . ' م' : '؟' ?></div>
                    </div>
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr> 
                                <th width="5%"><?php echo langline("M") ?></th>
                                <th width="20%"><?php echo langline("rqst_NameCamel") ?></th> 
                                <th width="20%"><?php echo langline("rqst_OwnerName") ?></th>
                                <th width="25%"><?php echo langline("rqst_NumCard") ?></th> 
                                <th width="10%"><?php echo langline("rqst_memberNumber") ?></th>  

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = $offset;
                            if ($data): foreach ($data as $row):
                                    ?>
                                    <tr class="odd gradeX"> 
                                        <td>
                                            <?php echo $i; ?>
                                        </td>     
                                        <td>
                                            <?php echo $row->Cname; ?>
                                        </td>   
                                        <td>
                                            <?php echo $row->Oname; ?>
                                        </td>  
                                        <td> 
                                            <?php echo $row->CID; ?>
                                        </td>  
                                        <td>
                                            <?php echo $row->MID; ?>
                                        </td>  
                                    </tr> 
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <nav><?php echo $pagination ?></nav>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div> 