<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("news") ?>"><?php echo langline("news_link") ?></a> &nbsp;
                </div>  
                <?php if (check_user_permission(current_module(), "add")): ?>
                    <div class="btn-group pull-right">
                        <a href="<?php echo admin_url("news/add/" . segment(4)) ?>" id="add_new_row" class="btn green">
                            <?php echo langline("link_add_new") ?> <i class="icon-plus"></i></a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="portlet-body">   
                <?php echo theme_messages(); ?>   
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr>
                                <!--<th class="table-checkbox">
                                    <input type="checkbox" class="group-checkable" data-set=".dataTable .checkboxes"/></th>-->
                                <th width="40%"><?php echo langline("nws_Title") ?></th>
                                <th><?php echo langline("nws_HeaderPhoto") ?></th>
                                <th><?php echo langline("Lable_From") ?></th>
                                <th><?php echo langline("project_status") ?></th>
                                <th width="200">&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data): foreach ($data as $row): ?> 
                                    <tr class="odd gradeX"> 
                                        <td><?php echo _t($row->nws_Title, 'ar'); ?></td>  
                                        <td>
                                            <img src="<?php echo $row->nws_HeaderPhoto ? $upload_path . '/news/' . $row->nws_HeaderPhoto : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" width="150" />
                                        </td> 
                                        <td><?php echo $row->nws_From ?></td>  
                                        <td>
                                            <div class="i-checkbox">
                                                <input type="checkbox" class="iButton" value="1" <?php echo $row->nws_Active ? 'checked' : '' ?> />
                                            </div>
                                        </td> 
                                        <td class="text-center">
                                            <?php
                                            if (check_user_permission(current_module(), "edit")):
                                                ?>
                                                <a href="<?php echo admin_url("news/edit/" . $row->nws_ID) ?>"
                                                   class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                                   title="<?php echo langline("link_edit"); ?>">
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </a>
                                                <?php
                                            endif;
                                            if (check_user_permission(current_module(), "edit") && check_user_permission(current_module(), "delete")):
                                                ?>
                                                <a href="<?php echo admin_url("news/delete/" . $row->nws_ID) ?>"
                                                   class="btn btn-danger confirm_delete" data-toggle="tooltip"
                                                   data-placement="top" title="<?php echo langline("link_delete"); ?>">
                                                    <i class="glyphicon glyphicon-remove"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr> 
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="7"><p class="note note-info"><?php echo langline("no_results") ?></p></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <nav><?php echo $pagination ?></nav>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>