
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/> 
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/css/ajaxupload.css">
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"> 
                    <i class="icon-home"></i>
                    <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a> &nbsp;
                </div>
                <div class="caption"> 
                    <i class="icon-angle-left"></i> 
                    <a href="<?php echo admin_url("videos") ?>"><?php echo langline("videos_link") ?></a> &nbsp;
                </div> 
            </div>
            <div class="portlet-body">  
                <?php if (check_user_permission(current_module(), "edit")): ?> 
                    <div class="portlet box blue form">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i><?php echo langline("log_edit_catgory") ?></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <?php echo theme_messages(); ?>  

                            <?php
                            $_action = admin_url("videos/editcat/" . segment(4));
                            $_attributes = array(
                                "id" => "dataForm",
                                "method" => "post",
                                "class" => "form-horizontal",
                                "role" => "form",
                                "enctype" => "multipart/form-data");
                            echo form_open($_action, $_attributes);
                            ?>  
                            <div class="tabbable-custom tabs-below ">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_2_1">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo langline("cats_name") ?> (عربي)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="cat_name[ar]" class="form-control input-lg"
                                                       placeholder="<?php echo langline("cats_name") ?>"
                                                       value="<?php echo _t($data->cat_name, "ar") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_2_2">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> <?php echo langline("cats_name") ?> (English)</label> 
                                            <div class="col-md-6"> 
                                                <input type="text" name="cat_name[en]" class="form-control input-lg" placeholder="<?php echo langline("cats_name") ?>"
                                                       value="<?php echo _t($data->cat_name, "en") ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_2_1" data-toggle="tab"><?php echo langline("language_tab_arabic") ?></a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_2_2" data-toggle="tab"><?php echo langline("language_tab_english") ?></a>
                                    </li>
                                </ul> 
                            </div>
                            <div class="col-md-8"> 
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>
                                    <div class="col-md-7">    
                                        <div class="i-checkbox">
                                            <input type="checkbox" name="cat_status" class="iButton" value="1" <?php echo $data->cat_status ? "checked" : "" ?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("cats_name") ?></label>
                                    <div class="col-md-2">  
                                        <select name="cat_Type" class="form-control select2-default"> 
                                            <option value="ProgramStage" <?php echo $data->cat_Type == "ProgramStage" ? "selected" : "" ?>><?php echo langline("ProgramStage") ?></option> 
                                            <option value="Racing" <?php echo $data->cat_Type == "Racing" ? "selected" : "" ?>><?php echo langline("Racing") ?></option> 
                                            <option value="SpecialInterviews" <?php echo $data->cat_Type == "SpecialInterviews" ? "selected" : "" ?>><?php echo langline("SpecialInterviews") ?></option> 
                                        </select> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo langline("year") ?></label>
                                    <div class="col-md-2">  
                                        <select name="cat_Year" class="form-control select2-default">
                                            <?php for ($i = date("Y"); $i >= 2016; $i--): ?>
                                                <option value="<?php echo $i ?>" <?php echo $data->cat_Year == $i ? "selected" : "" ?>><?php echo $i ?></option> 
                                            <?php endfor; ?>
                                        </select> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-md-2"><?php echo langline("project_attaches") ?></label> 
                                    <div class="col-md-8">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo $data->cat_image ? $upload_path . '/videos/' . $data->cat_image : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                                <img src="<?php echo $upload_path . '/videos/' . $data->cat_image ?>" style="max-height: 150px;">
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> <?php echo langline("btn_select_image") ?></span>
                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                                    <input type="file" class="default" name="cat_image" value="<?php echo input_value("cat_image") ?>" />
                                                </span>
                                                <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                            </div>
                                        </div>
                                        <span class="label label-danger">NOTE!</span> 
                                        <span>
                                            Attached image thumbnail is supported in Latest Firefox, 
                                            Chrome, Opera, Safari and Internet Explorer 10 only
                                        </span> 
                                    </div>
                                </div>  
                                <div class="form-group last">      
                                    <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                                        <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="catsID" id="catsID" value="<?php echo $data->cat_id ?>">
                            <?php echo form_close() ?>
                            <div class="clearfix"></div>
                        </div>
                    </div> 
                <?php endif; ?>  
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div class="modal fade" id="projectAttachmentsList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo langline("project_attachments") ?></h4>
            </div>
            <form action="#" method="post"
                  class="form-horizontal" role="form" id="showProjectAttachmentsForm" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="resp_message"></div>
                    <div id="attachmentsList"></div>

                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn default"
                            data-dismiss="modal"><?php echo langline("btn_close") ?></button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>  
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>