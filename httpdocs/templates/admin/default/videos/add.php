<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/> 
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title"); ?>
            <small><?php echo module_info("module_description"); ?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">
            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-left"></i>
            </li>
            <li>
                <a href="<?php echo admin_url("videos") ?>"><?php echo langline("videos_title") ?></a>
                <i class="icon-angle-left"></i>
            </li>  
            <li><a href="#"><?php echo langline("link_add_new") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box red form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("videos_title") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php echo theme_messages(); ?>
        <?php
        $_action = "";
        $_attributes = array(
            "id" => "dataForm",
            "method" => "post", 
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data");
        echo form_open($_action, $_attributes);
        ?>  
            <div class="tabbable-custom tabs-below ">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_settings">
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("You-Tube") ?></label>
                            <div class="col-md-7">
                                <input type="text" name="vd_YouTube" id="vd_YouTube" class="form-control input-lg"
                                       placeholder="<?php echo langline("V value YouTube") ?>"
                                       value="<?php echo input_value("vd_YouTube"); ?>">
                            </div>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label class="col-md-2 control-label">--><?php //echo langline("File") ?><!--</label>-->
<!--                            <div class="col-md-7">-->
<!--                                <input type="text" name="vd_File" id="vd_File" class="form-control input-lg"-->
<!--                                       placeholder="--><?php //echo langline("File Name") ?><!--"-->
<!--                                       value="--><?php //echo input_value("vd_File"); ?><!--">-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("project_status") ?></label>
                            <div class="col-md-7">
                                <div class="i-checkbox">
                                    <input type="checkbox" name="vd_Active" class="iButton"
                                           value="1" checked />
                                </div>
                            </div>
                        </div>
                        <div class="form-group last">
                            <label class="control-label col-md-2"><?php echo langline("Vd_Photo") ?></label> 
                            <div class="col-md-8">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                    </div> 
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 2px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" style="max-height: 150px;">
                                    </div> 
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileupload-new"><i class="icon-paper-clip"></i> 
                                                <?php echo langline("btn_select") ?></span>
                                            <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                            <input type="file" class="default" name="vd_Photo" value="<?php echo input_value("vd_Photo") ?>"/>
                                        </span>
                                        <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                    </div>
                                </div>
                                <span class="label label-danger">نوع الملف!</span> 
                                <span>
                                    الصورة 
                                    jpg,png,jpeg
                                </span> 

                            </div>
                        </div> 
                        <div class="form-group last">
                            <label class="control-label col-md-2"><?php echo langline("Vd_File") ?></label> 
                            <div class="col-md-8">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                    </div> 
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileupload-new"><i class="icon-paper-clip"></i> 
                                                <?php echo langline("btn_select") ?></span>
                                            <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                            <input type="file" class="default" name="vd_File" value="<?php echo input_value("vd_File") ?>"/>
                                        </span>
                                        <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                    </div>
                                </div>
                                <span class="label label-danger">نوع الملف!</span> 
                                <span>
                                    فيديو MP4
                                </span> 

                            </div>
                        </div>
                    </div>  
                    <?php
                    foreach ($languages as $key => $value):
                        ?>  
                        <div class="tab-pane" id="tab_<?php echo $key; ?>">
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo langline("vd_Title") ?></label>
                                <div class="col-md-6">
                                    <input type="text" name="vd_Title[<?php echo $key; ?>]" class="form-control input-lg"
                                           placeholder="<?php echo langline("vd_Title") ?>"
                                           value="<?php echo input_value("vd_Title[$key]"); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo langline("Date") ?></label>
                                <div class="col-md-6">
                                    <input type="text" name="vd_Date[<?php echo $key; ?>]" class="form-control input-lg"
                                           placeholder="<?php echo langline("Date") ?>"
                                           value="<?php echo input_value("vd_Date[$key]"); ?>">
                                </div>
                            </div>
                        </div>  
                        <?php
                    endforeach;
                    ?> 
                </div>
                <ul class="nav nav-tabs">                  
                    <li class="active">
                        <a href="#tab_settings" data-toggle="tab"><?php echo langline("Tab_Settings"); ?></a>
                    </li> 
                    <?php
                    foreach ($languages as $key => $value):
                        ?>                      
                        <li>
                            <a href="#tab_<?php echo $key; ?>" data-toggle="tab"><?php echo langline("language_tab_$value"); ?></a>
                        </li>  
                        <?php
                    endforeach;
                    ?>   
                </ul> 
            </div>   
        <?php echo form_close(); ?>
        <div class="clearfix"></div>
    </div>
</div>   
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script> 