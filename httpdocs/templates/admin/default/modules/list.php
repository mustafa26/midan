<?php 
$qs = get() ? "?" . http_build_query(get()) : ""; // current query strings.
?>

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo langline("com_page_title") ?>
            <small><?php echo langline("com_page_header") ?></small>
        </h3>
        <ul class="page-breadcrumb breadcrumb"> 
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo site_url("components") ?>"><?php echo langline("com_link") ?></a>
                <i class="icon-angle-right"></i>
            </li>

            <li><a href="#"><?php echo langline("com_list_all") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

 <div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title"> 
                <div class="caption"><i class="icon-globe"></i><?php echo langline("com_page_title") ?></div> 
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a> 
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="btn-group">
                        <a href="<?php echo site_url("components/add?" . http_build_query(get())) ?>" id="add_new_row" class="btn green">
                            Add New <i class="icon-plus"></i></a>
                    </div>

                </div>
                <?php echo theme_messages() ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                        <thead>
                            <tr>
                                <th class="table-checkbox">
                                    <input type="checkbox" class="group-checkable" data-set=".dataTable .checkboxes"/></th>
                                <th><?php echo langline("module_sort") ?></th>
                                <th><?php echo langline("com_title") ?></th>
                                <th><?php echo langline("com_name") ?></th>
                                <th><?php echo langline("com_status") ?></th>
                                <th colspan="2">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data): foreach ($data as $row): ?>

                                    <tr class="odd gradeX">
                                        <td>
                                            <input type="checkbox" class="checkboxes" value="<?php echo $row->module_id ?>"
                                                   name="item_id[]"/>
                                        </td>
                                        <td><?php echo $row->module_sort ?></td>
                                        <td>
                                            <a href="<?php echo admin_url($row->module_name) ?>"><?php echo _t($row->module_title, $user_lang) ?></a>
                                        </td>
                                        <td><?php echo $row->module_name ?></td>
                                        <td>
                                            <?php echo $row->module_status == 0 ? "<span class='label label-danger'>" . langline("status_inactive") . "</span>" : "<span class='label label-success'>" . langline("status_active") . "</span>" ?>
                                        </td>
                                        <td>
                                            <?php if (!get("parent")): ?>
                                                <a href="<?php echo site_url("components?parent=" . $row->module_id) ?>"
                                                   class="btn btn-success"><i
                                                        class="glyphicon glyphicon-list"> </i><?php echo langline("com_sub_modules"); ?>
                                                </a>
                                            <?php endif; ?>
                                            <a href="<?php echo site_url("components/edit/" . $row->module_id . $qs) ?>"
                                               class="btn btn-warning"><i
                                                    class="glyphicon glyphicon-edit"> </i>Edit</a>
                                                    
                                            <a href="<?php echo site_url("components/delete/" . $row->module_id . $qs) ?>"
                                               class="btn btn-danger confirm_delete"><i
                                                    class="glyphicon glyphicon-remove"> </i>Delete</a>
                                        </td>
                                    </tr>
                                <?php
                                endforeach;
                            else: ?> <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<!-- END PAGE CONTENT-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/app.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/table-managed.js"></script>

