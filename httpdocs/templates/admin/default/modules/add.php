<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/select2/select2_metro_rtl.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $themepath ?>/assets/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/css/multi-select-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro-rtl.css"
      rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/jquery-tags-input/jquery.tagsinput-rtl.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title">
            <?php echo langline("link_components") ?>
            <small>Add New Group</small>
        </h3>

        <ul class="page-breadcrumb breadcrumb">

            <li class="btn-group">
                <button type="submit" class="btn blue " data-close-others="true" form="dataForm">
                    <span><?php echo langline("btn_save") ?></span> <i class="icon-save"></i>
                </button>
            </li>

            <li>
                <i class="icon-home"></i>
                <a href="<?php echo admin_url(); ?>"><?php echo langline("link_dashboard") ?></a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo site_url("components"); ?>"><?php echo langline("com_link") ?></a>
                <i class="icon-angle-right"></i>
            </li>

            <li><a href="#"><?php echo langline("link_add_new") ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box red form">
    <div class="portlet-title">
        <div class="caption"><i class="icon-reorder"></i><?php echo langline("com_page_header") ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="#portlet-config" data-toggle="modal" class="config"></a>
        </div>
    </div>

    <div class="portlet-body">
        <?php echo theme_messages(); ?>
        
        <?php
        $_action = "";
        $_attributes = array(
            "id" => "dataForm",
            "method" => "post", 
            "class" => "form-horizontal",
            "role" => "form",
            "enctype" => "multipart/form-data");
        echo form_open($_action, $_attributes);
        ?> 
            <div class="tabbable-custom tabs-below ">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_2_1">
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("com_title") ?> (عربي)</label>

                            <div class="col-md-8">
                                <input type="text" name="title[ar]"
                                       class="form-control input-lg maxlength_threshold"
                                       value="<?php echo input_value("title[ar]") ?>"
                                       maxlength="150" placeholder="<?php echo langline("com_title") ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("com_description") ?> (عربي)</label>

                            <div class="col-md-7">
                                <textarea name="description[ar]" class="form-control" rows="5"><?php echo input_value("description[ar]"); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_2_2">
                        <div class="form-group">
                            <label class="col-md-2 control-label"> <?php echo langline("com_title") ?> (English)</label>

                            <div class="col-md-8">
                                <input type="text" name="title[en]" class="form-control input-lg"
                                       value="<?php echo input_value("title[en]") ?>"
                                       placeholder="<?php echo langline("com_title") ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo langline("com_description") ?> (English)</label>

                            <div class="col-md-7">
                                <textarea name="description[en]" class="form-control" rows="5"><?php echo input_value("description[en]"); ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2_1" data-toggle="tab"><?php echo langline("language_tab_arabic") ?></a>
                    </li>
                    <li class="">
                        <a href="#tab_2_2" data-toggle="tab"><?php echo langline("language_tab_english") ?></a>
                    </li>
                </ul>

            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("com_name") ?></label>

                <div class="col-md-7">
                    <input type="text" name="name" class="form-control input-lg"
                           placeholder="<?php echo langline("com_name") ?>"
                           value="<?php echo input_value("com_name") ?>">
                    <span class="help-block"><?php echo langline("com_name_hint") ?></span>
                </div>
            </div> 
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("com_parent") ?></label>

                <div class="col-md-7">
                    <select name="parent" class="form-control selectme2">
                        <option value="0"><?php echo langline("com_no_parent") ?></option>
                        <?php if ($modules): ?>

                            <?php foreach ($modules as $module): ?>
                                <option value="<?php echo $module->module_id ?>" <?php echo input_value("parent", get("parent")) ? "selected" : ""; ?>><?php echo _t($module->module_title, $user_lang) ?></option>
                            <?php endforeach; ?>

                        <?php endif; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("com_icon") ?></label>
                <div class="col-md-7">
                    <div class="input-group">
                        <input type="text" name="icon_class" class="form-control input-medium"
                               placeholder="css Class"
                               value="<?php echo input_value("icon_class") ?>">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <span class="btn default btn-file">
                                <span class="fileupload-new"><i class="icon-paper-clip"></i> Select file</span>
                                <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                <input type="file" class="default" name="module_icon" />
                            </span>
                            <span class="fileupload-preview" style="margin-left:5px;"></span>
                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("com_css") ?></label>
                <div class="col-md-7">
                    <input type="text" name="module_css" value="<?php echo input_value("module_css") ?>" class="form-control input-lg">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("com_show") ?></label>
                <div class="col-md-7">
                    <input type="checkbox" name="show" value="1" <?php echo input_value("show", 1) == "1" ? "checked" : "" ?> class="iButton" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("com_status") ?></label>
                <div class="col-md-7">
                    <input type="checkbox" name="status" value="1" <?php echo input_value("status", 1) == "1" ? "checked" : "" ?> class="iButton" />
                </div>
            </div> 
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo langline("com_Sort") ?></label>
                <div class="col-md-1">
                    <input type="text" name="module_sort" value="<?php echo input_value("module_sort") ?>" class="form-control input-lg">
                </div>
            </div>
            <?php echo form_close(); ?>
    </div>

</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript"
src="<?php echo $themepath ?>/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js"
type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"
type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"
type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"
type="text/javascript"></script>
<script type="text/javascript"
        src="<?php echo $themepath ?>/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"
type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/form-components.js"></script>
<script type="text/javascript" src="<?php echo $themepath ?>/assets/plugins/uniform/jquery.uniform.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {


        FormComponents.init();

        if (typeof CKEDITOR !== "undefined") {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    });
</script>