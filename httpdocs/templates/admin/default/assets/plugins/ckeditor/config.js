
/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */



CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 config.language = 'ar';
	 //config.uiColor = '#AADC6E';
	//config.extraPlugins = 'allmedias,fastimage,imagebrowser';
	/*config.toolbar =
			[
		{ name: 'document',    items : [ 'Source','-','NewPage','DocProps','Preview','Print','-','Templates' ] },
		{ name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
		{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize','TextColor','BackgroundColor' ] },
		];
		config.toolbarGroups = [
	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
	{ name: 'forms' },
	'/',
	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
	{ name: 'links' },
	{ name: 'insert' },
	'/',
	{ name: 'styles' },
	{ name: 'colors' },
	{ name: 'tools' },
	{ name: 'others' },

];*//*
//  ckfinder configration
	config.filebrowserBrowseUrl = base_url+'modules/MainLoad/views/js/ckeditor/plugins/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = base_url+'modules/MainLoad/views/js/ckeditor/plugins/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = base_url+'modules/MainLoad/views/js/ckeditor/plugins/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = base_url+'modules/MainLoad/views/js/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = base_url+'modules/MainLoad/views/js/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = base_url+'modules/MainLoad/views/js/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	*/

	// kcfinder configration
	// config.filebrowserBrowseUrl = base_url+'modules/MainLoad/views/js/ckeditor/plugins/kcfinder/browse.php?type=files';
	//themepath+
   config.filebrowserImageBrowseUrl = 'http://midanalbashir.gov.om/templates/admin/default/assets/plugins/ckeditor/plugins/kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = 'http://midanalbashir.gov.om/templates/admin/default/assets/plugins/ckeditor/plugins/kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl      = 'http://midanalbashir.gov.om/templates/admin/default/assets/plugins/ckeditor/plugins/kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = 'http://midanalbashir.gov.om/templates/admin/default/assets/plugins/ckeditor/plugins/kcfinder/upload.php?type=images';
   config.ToolbarStartExpanded = false ;
   config.filebrowserFlashUploadUrl = 'http://midanalbashir.gov.om/templates/admin/default/assets/plugins/ckeditor/plugins/kcfinder/upload.php?type=flash';


	config.toolbar              = 'Basic';
	config.toolbarCanCollapse   = true;
    config.toolbarCollapse      =true;
};
