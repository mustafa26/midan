
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo module_info("module_title"); ?>
<small><?php echo module_info("module_description"); ?></small>
</h3>
<ul class="page-breadcrumb breadcrumb"> 
    <li>
        <i class="icon-home"></i>
        <a href="<?php echo admin_url() ?>"><?php echo langline("link_dashboard") ?></a>
        <i class="icon-angle-right"></i>
    </li> 
    <li>
        <a href="<?php echo admin_url("users") ?>"><?php echo langline("link_users") ?></a>
        <i class="icon-angle-right"></i>
    </li>
    <li><a href="#"><?php echo langline("list_all") ?></a></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box light-grey">
            <div class="portlet-title">

                <div class="caption"><i class="icon-globe"></i><?php echo langline("users_page_header") ?></div>

                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>

            </div>
            <div class="portlet-body">
                <?php echo theme_messages() ?>
                <div class="col-md-12 col-sm-6">
                    <div class="col-md-1">
                        <?php echo langline("filter_label") ?>
                    </div>
                    <div class="col-md-11">
                        <form action="" method="get" class="form-inline">

                            <div class="form-group">
                                <label class="sr-only"
                                       for="filter_name"><?php echo langline("label_log_duration") ?></label>
                                <div class="col-md-7">
                                    <div class="input-group input-large date-picker input-daterange" data-date="10/11/2014" data-date-start-view="10/11/2015" data-date-format="yyyy-mm-dd">
                                        <input class="form-control" name="filter[from]" type="text" value="">
                                        <span class="input-group-addon"><?php echo langline("text_to")?></span>
                                        <input class="form-control" name="filter[to]" type="text"  value="">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="sr-only"
                                       for="filter_name"><?php echo langline("label_user_name") ?></label>
                                <input type="search" name="filter[name]" class="form-control" id="filter_name"
                                       placeholder="<?php echo langline("label_user_name") ?>"
                                       value="<?php echo get("filter")['name'] ?>"/>
                            </div>
                            <div class="form-group">
                                <label class="sr-only"
                                       for="filter_action"><?php echo langline("label_action_type") ?></label>
                                <select name="action"  class="form-control">
                                    <option value="show"><?php echo langline("log_action_show")?></option>
                                    <option value="add"><?php echo langline("log_action_add")?></option>
                                    <option value="edit"><?php echo langline("log_action_edit")?></option>
                                    <option value="delete"><?php echo langline("log_action_delete")?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="filter_group"><?php echo langline("label_module") ?></label>
                                <select name="module" class="form-control">
                                    <?php if($modules_list):foreach($modules_list as $m):?>
                                        <option value="<?php echo $m->module_name?>"><?php echo _t($m->module_title,$user_lang);?></option>
                                    <?php endforeach;endif;?>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default"><?php echo langline("filter_btn") ?></button>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                </div>

                <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                    <thead>
                    <tr>
                        <th><?php echo langline("log_event_date") ?></th>
                        <th><?php echo langline("log_user_name") ?></th>
                        <th><?php echo langline("log_event_type") ?></th>
                        <th><?php echo langline("log_event_description") ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if ($data): foreach ($data as $row): ?>
                        <tr class="odd gradeX">
                            <td class="text-center"><?php echo date("Y-m-d",strtotime($row->ul_created))?> <br/> <?php echo date("h:i:s A",strtotime($row->ul_created))?></td>
                            <td><?php echo $row->user_name?></td>
                            <td class="text-center" ><span class="label label-<?php echo $events_color[$row->ul_action]?>"><?php echo langline("log_action_".$row->ul_action);?></span></td>
                            <td class="text-center" ><?php echo langline($row->ul_message)?></td>
                        </tr>
                    <?php endforeach;
                    else: ?>
                    <tr><td colspan="4"><p  class="text-center note note-info "><?php echo langline("log_no_events")?></p></td></tr>
                    <?php  endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo $themepath ?>/assets/scripts/form-components.js"></script>


<script>

    $(document).ready(function () {
        FormComponents.init();

    });

</script>
