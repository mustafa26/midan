<?php

/* * *******************************************
 *  Project     : Projects Management
 *  File        : MY_Controller.php
 *  Created at  : 5/6/15 - 9:23 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : php.ahmedfathi@gmail.com
 *  site        : http://durar-it.com
 * ******************************************* */

class MY_Controller extends MX_Controller {

    protected $data = array();
    protected $jsonData = array("success" => false, "html" => null, "redir" => null);
    protected $errors = null;
    protected $users;
    public $template;
    public $user = null;
    public $site = null;
    public $languages;
    public $user_lang;
    public $module_info;
    private $messages = array();

    function __construct() {
        parent::__construct();
        // check php version firstly it should be 5.3.0 or higher
        if (!is_php('5.3.0')) {
            echo 'Your current PHP version is : ' . PHP_VERSION . " It should be 5.3.0 or higher \n";
            die;
        }

        $this->load
		->library(array('template','session',"email","parser","pagination","encrypt", 'user_agent'))
		->helper(array("main","url"));
		$this->load->database();
        $this->benchmark->mark('my_controller_start');

        $this->load->model("users/Users_model");
        $this->users = new Users_Model();
        $this->template = new template();
        $this->languages = config_item("lang.languages");
        $this->template->vars['user'] = $this->user;
        $this->session->set_userdata("user_lang", $this->config->item("lang.default"));
        $this->user_lang = $this->session->userdata("user_lang");
        $this->template->vars['languages'] = $this->languages;
        $this->template->vars['user_lang'] = $this->user_lang;


        $this->lang->load("main", $this->languages[$this->user_lang]);
        $this->lang->load("form_validation", $this->languages[$this->user_lang]);
        $this->template->vars['upload_dir'] = rtrim($this->config->item("upload_dir"), "/");
        $this->template->vars['upload_path'] = base_url() . rtrim($this->config->item("upload_dir"), "/");
        $this->template->vars['jslang'] = langline("js");
        $this->module_info = $this->get_module_info();
        $this->template->vars['module_info'] = $this->module_info;
        $this->site_settings();
        $this->set_prev_page();
    }

    /**
     *
     */
    function site_settings() {
        $this->load->model('settings/Settings_model');
        $settings = new Settings_model();
        $this->site = $settings->get_default_settings();
        if ($this->site) {
//            var_dump($this->site);die;
            $this->site->site_name = _t($this->site->site_name, $this->user_lang);
        }
        $this->template->vars['site'] = $this->site;
    }

    private function get_module_info() {
        $module_name = current_module();
        $this->load->model("components/Components_model");
        $data = $this->Components_model->get_by_name($module_name);
        if ($data) {
            $data->module_title = _t($data->module_title, $this->user_lang);
            $data->module_description = _t($data->module_description, $this->user_lang);
            return $data;
        }
        return false;
    }

    function set_prev_page() {

        //get login page url
        $skip = array("login", "logout");

        $current_url = current_url();

        $controller_name = $this->router->fetch_directory() . $this->router->fetch_class();
        $action_name = $this->router->fetch_method();
        $module = $this->router->fetch_class();

        //save current url if isn't in skip pages array
        if (!in_array($action_name, $skip)) {
            //save in session
            $this->session->set_flashdata("prev_link", $current_url);
        } else {
            // don't save skip pages in session
            $prev = $this->session->flashdata("prev_link");
            if ($prev == null) {
                $prev = site_url();
            }
            $this->session->set_flashdata("prev_link", $prev);
        }
        return;
    }

    function prev_page() {
        return $this->session->flashdata("prev_link");
    }

    function is_logged_in($redirect = false) {

        if ($user = $this->Users_model->is_logged_in()) {
            $this->user = $user;
            $this->template->vars['user'] = $this->user;
            return $user;
        } else {
            $this->user = false;
            $current_page = $this->router->fetch_method();
            $skip = array("login", "logout");

            if ($redirect) {
                if (!in_array($current_page, $skip)) {
                    redirect($redirect);
                }
            }
        }
        return false;
    }

    function is_ajax() {
        return $this->input->is_ajax_request();
    }

    function keep_data() {
        $this->session->set_flashdata("form_data", post());
        return;
    }

    function set_message($message = "", $type = "error") {
        array_push($this->messages, array("msg" => $message, "type" => $type));

        $this->session->set_flashdata("message", $this->messages);
        return;
    }

    /**
     * Send_mail
     * @param :$to (str) email address to receive the email
     * @param :$subject (str) email subject
     * @param :$message (str) message body
     * @param :$from(array){optional} if not passed .automatically will use site email and site name
     *            index[0] email
     *            index[1] site name
     *
     * @param :attache (mix[array/str]) {optional} pass an array if you have more than file
     * @param :$config (array) if you need to pass more email configurations or ove
     * @return (boolean) return true if send success.
     * @author Ahmed Fathi
     * @link  php.ahmedfathi@gmail.com
     *
     */
    
    function send_email($to, $subject, $message, $from = null, $attache = null, $config = array()) {
        $emailErr = "";
        //set email settings
        $this->load->library('email');
        // get email settings
        $this->load->model("settings/Settings_model");
        $settings = new Settings_model();

        $es = $settings->email();
        $config['protocol'] = $es->email_function;
        $config['smtp_host'] = $es->smtp_server;
        $config['smtp_port'] = $es->smtp_port;
        $config['smtp_user'] = $es->smtp_user;
        $config['smtp_pass'] = $es->smtp_password;
        //$config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] || $config['charset'] = 'utf-8';
        $config['mailtype'] || $config['mailtype'] = "html";
        $config['wordwrap'] || $config['wordwrap'] = TRUE;
        $this->email->initialize($config);

        if ($from && is_array($from)) {
            $this->email->from($from[0], $from[1]);
        } else {
            $this->email->from($this->site->site_email, $this->site->site_name);
        }

        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
        }
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        //if array passed
        if ($attache) {
            if (is_array($attache)) {
                for ($i = 0; $i < count($attache); $i++) {
                    $this->email->attach($attache[$i]);
                }
            } else {
                $this->email->attach($attache);
            }
        }
        if ($emailErr)
            return$emailErr;
        return $this->email->send();
    }

    /*

    function do_upload($file_name = "userfile", $file_module = "", $resize = false, $file_type = "", $allowed_types = "") {
        $this->load->library("upload");
        $this->load->library('image_lib');
        if (!empty($_FILES[$file_name]['name'])) {
            $config['upload_path'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/" . $file_type;
            $config['allowed_types'] = $allowed_types != "" ? $allowed_types : 'gif|jpg|png';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $config['max_size'] = '3000000';
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_name)) {
                $this->errors .= $this->upload->display_errors();
            } else {
                $_img_file = $this->upload->data();
                if ($resize) {
                    $config_thumb['image_library'] = 'gd2';
                    $config_thumb['source_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/$file_type" . $_img_file["file_name"];
                    $config_thumb['create_thumb'] = TRUE;
                    $config_thumb['maintain_ratio'] = TRUE;
                    $config_thumb['width'] = 250;
                    $config_thumb['height'] = 250;
                    $config_thumb['new_image'] = rtrim(APPPATH . $this->config->item("upload_dir"), "/") . "/$file_module/thumb";
                    $this->image_lib->initialize($config_thumb);
                    $this->image_lib->resize();
                }
                return $_img_file;
            }
        } else {
            return false;
        }
    }
     *      */
    
}
