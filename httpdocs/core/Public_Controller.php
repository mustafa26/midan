<?php

/* * ******************************************* 
 *  File        : MY_Public.php
 *  Created at  : 5/6/15 - 9:23 AM 
 * ******************************************* */

$themepath;

class Public_Controller extends MY_Controller {

    function __construct() {

        parent::__construct(); 
        $this->output->set_header('Cache-Control: no-cache, private'); 
        $this->output->set_header('Connection: close'); 
        $this->output->set_header('Content-Type: text/html; charset=UTF-8'); 
        $this->output->set_header('X-Frame-Options: SAMEORIGIN'); 
        //$this->output->set_header('X-Frame-Options: DENY');
        $this->output->set_header('X-Powered-By: none'); 
        $this->output->set_header('Set-Cookie: httpOnly'); 
        $this->output->set_header('X-XSS-Protection: 1; mode=block');  

        $this->template->theme_path = $this->config->item("theme.frontend_dir");
        $this->template->theme_name = $this->config->item("theme.frontend_theme");
        $this->template->full_path = $this->template->theme_path . "/" . $this->template->theme_name;
        $c = segment(1) ? segment(1) : 'home';
        $this->lang->load($c, $this->languages[$this->user_lang]);
        global $themepath;
        $themepath = $this->template->get_themepath();
    }

}
