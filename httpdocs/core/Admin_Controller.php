<?php

/* * *******************************************
 *  Project     : Projects Management
 *  File        : MY_AdminController.php
 *  Created at  : 5/6/15 - 9:23 AM
 *  Author      : Ahmed Fathi (MR.Developer)
 *  Email       : php.ahmedfathi@gmail.com
 *  site        : http://durar-it.com
 * ******************************************* */

class Admin_Controller extends MY_Controller {

    function __construct() {

        parent::__construct();

        $this->template->theme_path = $this->config->item("theme.backend_dir");
        $this->template->theme_name = $this->config->item("theme.backend_theme");
        $this->template->full_path = $this->template->theme_path . "/" . $this->template->theme_name;

        $this->template->vars["admin_base"] = site_url($this->config->item("url.admin")) . "/";
        $this->lang->load("admin/admin", $this->languages[$this->user_lang]);
        $this->admin_logged_in(true);
    }

    function admin_logged_in($redirect = false) {
        $url = false;
        if ($redirect) {
            $url = site_url();
        }

        if ($user = $this->is_logged_in($url)) {
            return true;
        }

        $this->template->vars["user"] = $this->user;
        return false;
    }

}
