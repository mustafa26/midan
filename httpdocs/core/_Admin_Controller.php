<?php 
class Admin_Controller extends MY_Controller{


    function __construct(){
        parent::__construct();
        $this->template->theme_path = $this->config->item("theme.backend_dir");
        $this->template->theme_name = $this->config->item("theme.backend_theme");
        $this->template->full_path  = $this->template->theme_path."/".$this->template->theme_name;
        $this->template->vars["admin_base"]=site_url($this->config->item("url.admin"))."/";
        $this->lang->load("admin/admin",$this->languages[$this->user_lang]);
        //$this->load->helper(array("permissions/permissions")); 
        $this->admin_logged_in(true);
    }

    function admin_logged_in($redirect=false){
//        die(print_r($this->input->cookie(config_item("remember_me_token"))));
        $url=false;
        if($redirect){
            $url=admin_url("login");
        }
        if($user=$this->is_logged_in($url)){
            $user_groups=explode(",",$user->groups_id);
            if($groups=$this->Users_model->list_admin_groups()){
                    // check if current logged in user is admin or not
                foreach($groups as $group){
                    if(in_array($group->group_id,$user_groups)){
                        return true;
                    }
                }
                $this->user=false;
            }
        }
        $this->template->vars["user"]=$this->user;
        return false;
    }
}